/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBPATBLD', {
		BILSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABCMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADCMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCPATRSP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCCMPRSP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDCMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHRMPATRSP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHRMCMPRSP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISCPATRSP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISCCMPRSP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MBPATBLD',
		timestamps: false
	});
};
