/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APRVLLOG', {
		CURNCYCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RVLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQENCE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURTBLTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATEOVRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRVMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJFRMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEPOSTED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APVERSION: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APRVLLOG',
		timestamps: false
	});
};
