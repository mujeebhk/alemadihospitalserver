/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCAPREAR', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EARNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PMAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PMPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PMFORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PMALLOCATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PMTOTPRCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NMAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NMPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NMFORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NMALLOCATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NMTOTPRCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYFORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PYALLOCATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PYTOTPRCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NYAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NYPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NYFORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NYALLOCATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NYTOTPRCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCAPREAR',
		timestamps: false
	});
};
