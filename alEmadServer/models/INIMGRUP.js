/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIMGRUP', {
		IMGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TODAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENERICYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMCAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INIMGRUP',
		timestamps: false
	});
};
