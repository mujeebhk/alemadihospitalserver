/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRTA', {
		CODETERM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MULTIPAYM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VATCODEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYSTRT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYSTRT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYSTRT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYSTRT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYEND1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYEND2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYEND3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYEND4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DMNTHADD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DMNTHADD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DMNTHADD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DMNTHADD4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYUSE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYUSE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYUSE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DDAYUSE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTDUEDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUENBRDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYST1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYST2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYST3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYST4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYEND1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYEND2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYEND3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYEND4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUMNTHAD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUMNTHAD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUMNTHAD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUMNTHAD4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYUSE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYUSE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYUSE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUDAYUSE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTEDUESYNC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DTEDSCSYNC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTENTERED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTDUETOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARRTA',
		timestamps: false
	});
};
