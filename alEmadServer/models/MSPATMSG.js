/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPATMSG', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MESSAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBILENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTYESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPONSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MESSAGE1: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPATMSG',
		timestamps: false
	});
};
