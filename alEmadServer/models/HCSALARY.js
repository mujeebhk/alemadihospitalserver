/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCSALARY', {
		SALUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECIPTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODEPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WAGES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALBASIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYSAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABSENTDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAIDHOLI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNPAIDHOLI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WORKING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HALFDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPENSAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEEKOFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WORKINGHR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERTIME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOLEXTOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTRAOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALEARNS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALDEDTS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUELOAN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSSSAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETSALARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTEMPRCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVANCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAIDDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPOFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCLEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLPROCESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALPROCESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PYDAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYPOSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYPOSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYPOSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PYBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVCOMPNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALARYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISCONVLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONVELEAVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONLEAVSAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RNDOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRTOTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANAPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEGMNTHSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEGYEARSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEGSALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEGSALNUMB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEGSALBALN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSETTLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETTLENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PENDUNPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCSALARY',
		timestamps: false
	});
};
