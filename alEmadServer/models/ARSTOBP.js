/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSTOBP', {
		STMTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPAYMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDMEMOXREF: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARSTOBP',
		timestamps: false
	});
};
