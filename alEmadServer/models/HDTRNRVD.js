/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTRNRVD', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFPRMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRYID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRFANSWER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STRANSWER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFGRPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTMARKS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRFANSWER1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRFANSWER2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRFANSWER3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRFANSWER4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRFANSWER5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDTRNRVD',
		timestamps: false
	});
};
