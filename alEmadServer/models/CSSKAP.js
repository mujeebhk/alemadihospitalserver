/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSSKAP', {
		SCHEDKEY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SCHEDLINK: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLICATIO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AOPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AOPERATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADESCRIPTI: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CSSKAP',
		timestamps: false
	});
};
