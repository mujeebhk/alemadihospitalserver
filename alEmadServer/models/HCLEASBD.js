/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEASBD', {
		LSALSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALARYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTLVPERD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVAPPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLEAVESESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLEASBD',
		timestamps: false
	});
};
