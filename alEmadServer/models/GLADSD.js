/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLADSD', {
		ANCESTOR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DESCENDANT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLADSD',
		timestamps: false
	});
};
