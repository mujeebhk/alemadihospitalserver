/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APP04', {
		RECID04: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SESSNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWGLPSTDFR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWGLAPDBTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWGLPSTCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEGLREF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEGLDESC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTLASPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLASINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLASRVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLASADJM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCTYPEIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPEDB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPECR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPEIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPEPY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPEED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPEGL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPEAD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPECO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPEPI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCTYPEPYR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APP04',
		timestamps: false
	});
};
