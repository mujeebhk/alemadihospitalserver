/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIMTYPE', {
		IMNTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFNODOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BOSTFREQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BOSTRUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROUTECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INIMTYPE',
		timestamps: false
	});
};
