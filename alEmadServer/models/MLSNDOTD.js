/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLSNDOTD', {
		SNDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDOUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECVDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECVTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLSNDOTD',
		timestamps: false
	});
};
