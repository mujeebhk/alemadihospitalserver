/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIBILCH', {
		BILLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCANNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCANDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCANTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPATPBLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDCOLLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EPISDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATEPISODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CAUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CAUTHTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CAUTHUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAUTHYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARRYFWDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRECPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECPLANYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCOMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IIBILCH',
		timestamps: false
	});
};
