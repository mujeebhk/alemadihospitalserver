/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPLOC', {
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TELEPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTTELE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MRNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PONPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRTNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DELNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRETPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRETNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECVLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECVPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECVNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGOVER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGNO1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGNO2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGNO3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGNO4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGNO5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGNO6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGNO7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGNO8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGNO9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MATREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PURORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DELINOTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECEIPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVOICE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PURRETURN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRDRNOTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSUMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSFER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSRET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORETPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORETNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDRETURN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILRETURN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHMORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNIDOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTPOSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RNDSALUOM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHIPMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPOSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANMGT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MPLOC',
		timestamps: false
	});
};
