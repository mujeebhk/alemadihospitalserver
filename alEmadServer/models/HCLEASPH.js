/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEASPH', {
		LSALPRSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALPRNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTLEAVESL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTAIRFARE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPENMONTHS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVEDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVESAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCLEASPH',
		timestamps: false
	});
};
