/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEMEDFRE', {
		MEDFQCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDFQSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREQTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INHRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ARABDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IVPREDSAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSULINSCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEMEDFRE',
		timestamps: false
	});
};
