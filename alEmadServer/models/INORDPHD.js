/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INORDPHD', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISPACKAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGTRNNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BALANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARMALOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORIGQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		READYBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDINSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDCOMPLTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERPRVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRECONSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREPHRMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESERVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRICEUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INORDPHD',
		timestamps: false
	});
};
