/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDRECREQ', {
		RECQSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECREQDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECRITPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRENTPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGENCYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGENCYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOTEDTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROUNDS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTROUNDS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPENTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPRECPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPENVACPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPLVACPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTVACREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTREPREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VACANTAPP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPLACAPP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPLID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDRECREQ',
		timestamps: false
	});
};
