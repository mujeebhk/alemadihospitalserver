/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INNUECFN', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDCOVER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMEENVIR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMEENDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOMEENVIRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOBILITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEAKNESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UPPERRTLFT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWERRTLFT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAILYLIVIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSISTNEED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASNEEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OCUPATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EDUCATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPEARDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINASTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSTAVAIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSTAVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSAVAIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELIGIOUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALCODRUG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYABUFEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYABUHABI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYABUCOND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYABUINSU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYABUELDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYABUOTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYABUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SIGNNEGL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFSWHMFAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFSWLIVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFSWFINND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFSWMECEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFSWSPLHM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFSWABUEV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFSWPOSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHYABUSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTDRASST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTDRMOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTDRLIV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTDRWEAK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYABSEXST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOCWRKEXST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOMONEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCUPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUNALSRNRV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYSCOLSCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INNUECFN',
		timestamps: false
	});
};
