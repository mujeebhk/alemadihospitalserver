/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSCASH', {
		SHIFTCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASHIERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPENDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPENTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CSHCLSTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIFFCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPCRED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTCRED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRDCLSTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIFFCRED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPCHQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTCHQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHQCLSTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIFFCHQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSCASH',
		timestamps: false
	});
};
