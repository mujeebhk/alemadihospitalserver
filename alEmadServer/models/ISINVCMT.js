/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISINVCMT', {
		INVRSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ISINVCMT',
		timestamps: false
	});
};
