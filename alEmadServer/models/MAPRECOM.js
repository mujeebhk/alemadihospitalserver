/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAPRECOM', {
		IPPRENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPCMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPCOMNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOSPCMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOSPCOMNT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MAPRECOM',
		timestamps: false
	});
};
