/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRSCHD', {
		SCHEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MRSCHD',
		timestamps: false
	});
};
