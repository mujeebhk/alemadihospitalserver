/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEDRNTAL', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELDNAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALERTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALERTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALERTDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVIEWEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIEWTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEDRNTAL',
		timestamps: false
	});
};
