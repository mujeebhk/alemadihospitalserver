/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOCLSTCM', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FASTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTVDCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANASRCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDLBLCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PTIDBCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRSHRCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MKRMCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NLRMCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JWLTPCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HEADCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSRMCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DNTRCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TETHCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURSTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLDGLCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WRCOTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRMEDCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATHSCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATEDCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRGKCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLGLCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCORCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATLBCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IVLINCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARTLNCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNVNCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRNTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URNCCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NASTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIOCCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HEMCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ECGCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		XRCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRMCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLAVCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WHLBUNTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RDCLUNTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRFPLSUNTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLATUNTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHUNTS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHUNTS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENCNCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCNCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANECNCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CBCCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PTTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELECTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLOODCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HBSCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCVCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HIVCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLANDCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATHECMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HEENACMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROSTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACEMCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMBSTCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASPIRCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREGNCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BREFDCMCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MOCLSTCM',
		timestamps: false
	});
};
