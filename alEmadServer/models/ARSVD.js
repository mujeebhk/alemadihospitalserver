/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSVD', {
		CODESVCCHR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTVSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTCHRG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TERMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SVCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PDUEDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RNDGUPSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRGCMPDSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARSVD',
		timestamps: false
	});
};
