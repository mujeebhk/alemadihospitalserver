/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHISRVW', {
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SOCHISYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOCHISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FAMHISYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FAMHISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLGYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLGYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGERYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGERYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPEPIYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPEPIDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEHISRVW',
		timestamps: false
	});
};
