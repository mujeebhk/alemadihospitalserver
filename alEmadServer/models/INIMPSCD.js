/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIMPSCD', {
		PATSCHUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMNTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RANGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRAGETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAGETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INIMPSCD',
		timestamps: false
	});
};
