/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APPJAIH', {
		TYPEBTCH: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GCNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GCNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTTRX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDTRX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEVNDGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMITTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVCAPPLTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDRNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCHRATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATERATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TERMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSCBASE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCAVL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSBWTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSBNTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTAXBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCALCTX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTINVCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTGROSTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRECTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTAXTOBE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ID1099CLAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMT1099: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDDISTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTDISTSET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VENDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RMITNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APPJAIH',
		timestamps: false
	});
};
