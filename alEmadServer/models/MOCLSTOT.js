/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOCLSTOT', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FASTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSTVDOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANASROTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDLBLOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTIDBOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRSHROTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MKRMOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NLRMOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		JWLTPOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSRMOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTROTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TETHOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURSTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLDGLOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WRCOTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRMEDOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHSOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATEDOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRGKOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLGLOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCOROTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATLBOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IVLINOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ARTLNOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNVNOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRNTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		URNCOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NASTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BIOCOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEMOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ECGOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		XROTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRMOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLAVOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENCNOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURCNOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANECNOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CBCOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELECTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLOODOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HBSOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCVOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIVOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLANDOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CATHEOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEENAOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROSTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACEMOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMBSTOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASPIROTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREGNOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREFDOTCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MOCLSTOT',
		timestamps: false
	});
};
