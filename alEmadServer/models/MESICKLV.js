/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MESICKLV', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NODAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDICOND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIANOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MESICKLV',
		timestamps: false
	});
};
