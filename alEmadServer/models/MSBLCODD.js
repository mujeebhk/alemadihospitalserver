/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSBLCODD', {
		BLCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGSPECFIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGANICODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGANIDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSBLCODD',
		timestamps: false
	});
};
