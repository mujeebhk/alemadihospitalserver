/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GPGLBB', {
		BSEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPANYID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUNCCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUNCCURDEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NENTRIES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEXTENTRY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'GPGLBB',
		timestamps: false
	});
};
