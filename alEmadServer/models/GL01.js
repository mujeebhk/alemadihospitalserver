/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GL01', {
		OPTIONID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONENBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPANYID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSESEG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSEG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKDFLT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKDELM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWBTCHEDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPROVPST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRTPBTCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODELOCK1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODELOCK2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODELOCK3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODELOCK4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODELOCK5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCKFILL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWQTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QTYDEC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YRSHIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YRACCTDEL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEXTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PSTSQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVPSTSQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PJRNLPRGTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BTCHPSTTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		JRNLPRGTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YRCLSLST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YRLSTACTL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDNOPSTPR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YRNOPSTPR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWMC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DFLRATETYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACCTGRP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRVYRPST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YRSTRANDTL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HSTCLRACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWUSESEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDEFACCSS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GL01',
		timestamps: false
	});
};
