/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDEMPORD', {
		EMPSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		REQDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		MEALID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IDEMPORD',
		timestamps: false
	});
};
