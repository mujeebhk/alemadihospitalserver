/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEARDED', {
		EARDEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STARTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENDS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REGULAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHIFT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERTIME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTFROM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTFROM2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTFROM3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTFROM4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTFROM5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTTOVAL1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTTOVAL2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTTOVAL3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTTOVAL4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTTOVAL5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPERCENT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPERCENT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPERCENT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPERCENT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPERCENT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXPROFTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRNTPAYSLP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPRCALMET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPRAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPRPRCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FORMULATXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUNDMETHD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROUNDBY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITWSCOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISOVERTIME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LIABACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CALCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFCOMPNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YEARMONTHS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLYLIMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LIMITAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LIMCOMPNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITBASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONVFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VARIABLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		METERREAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECINDEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISBONUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISHRA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGELIMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		XTRAPERCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERCNTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVTIMEAPPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVTIMEPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVTHOLPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ERLYJONDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHORTHRDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVEFFCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMMONTH2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMONTH2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCMONTH2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMMONTH3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMONTH3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCMONTH3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTFROM6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTTOVAL6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTFROM7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTTOVAL7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPERCENT6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPERCENT7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASECPI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FACTOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMMONTH4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMONTH4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCMONTH4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLWRNDOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RNDOFFTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RNDOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLOCPJC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVTWKLYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCLMTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LIMITDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTHREARN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQBANKACC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APINVOICE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VENDORID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCEARDED',
		timestamps: false
	});
};
