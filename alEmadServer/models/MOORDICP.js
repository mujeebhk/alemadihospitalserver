/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOORDICP', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SURGCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCATDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMARYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MOORDICP',
		timestamps: false
	});
};
