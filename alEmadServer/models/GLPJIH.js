/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLPJIH', {
		POSTINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		OBATCHNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		OENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LEVCOMP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BATCHNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEENTRY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		JRNLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCSYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCSPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWREVERSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLPJIH',
		timestamps: false
	});
};
