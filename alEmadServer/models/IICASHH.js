/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IICASHH', {
		TRNNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMSCRREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYERTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COUNTER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSHIFT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASHIERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMSCREEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATIENTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYDETAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYDETAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTUTILIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTUNBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MULPACKADV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULPACKCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGTRNNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETRF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOIPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TORECEPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASESELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMBILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CARRYFWDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFBILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULPACKDOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILADJDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHIFTCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STMTGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STMTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPOSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQTRNCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFADM: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IICASHH',
		timestamps: false
	});
};
