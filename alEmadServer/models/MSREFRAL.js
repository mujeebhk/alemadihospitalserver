/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSREFRAL', {
		REFCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCPICPHY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCPICINST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAILID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMISREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTRICT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRYDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUALFITION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLASS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSREFRAL',
		timestamps: false
	});
};
