/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOPATCKH', {
		OPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRAYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREOTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREOTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREOTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTOTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTOTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTOTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LISTSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREQTYTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTQTYTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITDIFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MOPATCKH',
		timestamps: false
	});
};
