/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APOFD', {
		LOCATION: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		OPTFIELD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DECIMALS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOWNULL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INITFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCONTROL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDISCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXRECOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXEXP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWREXCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWREXCHL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWUREXCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWUREXCHL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWROUND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDISTRIB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPREPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWMISCPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWBANK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWADJ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWLABOUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWOHEAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWREQUIRED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APOFD',
		timestamps: false
	});
};
