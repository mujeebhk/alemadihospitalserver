/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSNURICH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRISEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMNURSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMNURSEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMNURSETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TONURSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TONURSEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TONURSETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'DSNURICH',
		timestamps: false
	});
};
