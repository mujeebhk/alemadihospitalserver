/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APIBD', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWMANLDIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTTOTTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWMANLTX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDGLACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTTAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ID1099CLAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISTNET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINCLTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTGLDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTOBE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWIBT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDISCABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OCNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRTGDDTOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGAMTOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDISTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGINVDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOSTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTNETHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLRTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLRTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPRTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPRTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWFAS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APIBD',
		timestamps: false
	});
};
