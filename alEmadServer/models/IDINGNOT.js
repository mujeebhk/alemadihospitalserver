/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDINGNOT', {
		INGRDINTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDINGNOT',
		timestamps: false
	});
};
