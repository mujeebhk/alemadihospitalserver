/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TXAUTH', {
		AUTHORITY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCLUDABLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LIABTIMING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LIABILITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFERRECOV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCBASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUDITLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATERECOV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTRECOV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPSEPARTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTEXP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTMAINT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXRTGCTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'TXAUTH',
		timestamps: false
	});
};
