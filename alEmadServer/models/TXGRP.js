/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TXGRP', {
		GROUPID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORITY1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORITY2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORITY3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORITY4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORITY5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXABLE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXABLE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXABLE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXABLE4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXABLE5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTMAINT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURTAX1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURTAX2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURTAX3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURTAX4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURTAX5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'TXGRP',
		timestamps: false
	});
};
