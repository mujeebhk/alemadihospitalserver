/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDRECRQD', {
		RECQSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVALPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EVALPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROUNDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISMULTIPLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDRECRQD',
		timestamps: false
	});
};
