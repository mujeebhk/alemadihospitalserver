/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSCCD', {
		CURID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SYMBOL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DECIMALS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SYMBOLPOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		THOUSSEP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DECSEP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEGDISP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'CSCCD',
		timestamps: false
	});
};
