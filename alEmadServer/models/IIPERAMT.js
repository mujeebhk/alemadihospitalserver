/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIPERAMT', {
		INVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOPATIENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREMIUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVOICEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVOICETOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IIPERAMT',
		timestamps: false
	});
};
