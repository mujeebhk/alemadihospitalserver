/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSTOBL', {
		STMTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDORDERNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESCINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRXTYPETXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRXTYPEID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDCUSTSHPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETERM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARSTOBL',
		timestamps: false
	});
};
