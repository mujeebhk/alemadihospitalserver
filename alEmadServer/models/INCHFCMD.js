/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INCHFCMD', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHFCOMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ONSETDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ONSETVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTERVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEVERITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHRNIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INCHFCMD',
		timestamps: false
	});
};
