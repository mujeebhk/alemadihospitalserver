/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPARAMH', {
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTSAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTSAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKSHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKSHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STWKSHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STWKSHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HL7CODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VOLUME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESUNITS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOWLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLOWHIGH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFRECLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFRECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFPROLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFPRODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELCHKREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DELTACHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DELTAINTER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSPARAMH',
		timestamps: false
	});
};
