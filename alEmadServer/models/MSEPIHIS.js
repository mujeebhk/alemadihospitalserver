/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSEPIHIS', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SPECCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISODENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOFREEVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALFRVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSEPIHIS',
		timestamps: false
	});
};
