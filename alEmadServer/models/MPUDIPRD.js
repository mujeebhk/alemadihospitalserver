/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPUDIPRD', {
		UDISEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BINNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSAUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		READYBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISBILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PURQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSUNTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYPERDOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OUTSTDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDCLSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMADDTN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEPERHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARREMK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IVFLUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MPUDIPRD',
		timestamps: false
	});
};
