/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLPOST', {
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FISCALYR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FISCALPERD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SRCELEDGER: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTDETAIL: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JRNLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDITALLOWD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSOLIDAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPANYID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JNLDTLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JNLDTLREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCURNDEC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HCURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEMTCHCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILSRCTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLDWNLK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRILAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLPOST',
		timestamps: false
	});
};
