/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKJCTL', {
		PSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRINTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLDEFER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLCONSOL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLAPPEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLTRANS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BANKSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKJCTL',
		timestamps: false
	});
};
