/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPITMBCH', {
		DUMMY: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATETO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROUPFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROUPTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICELIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWITEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXISTITEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTROL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPITMBCH',
		timestamps: false
	});
};
