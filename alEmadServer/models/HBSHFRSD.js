/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBSHFRSD', {
		SHFRSSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SHIFTABBR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROSGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENDTIMTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCESSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBSHFRSD',
		timestamps: false
	});
};
