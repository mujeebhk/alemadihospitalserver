/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLOANAD', {
		LOANUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOOFINST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTRSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTAMNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PENDNGLOAN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STARTMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLOANAD',
		timestamps: false
	});
};
