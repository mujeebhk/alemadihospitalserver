/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEAVEH', {
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPPURP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCASHMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREDITTRNS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMBOTHLEV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCLSUNHOL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISCARRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARRYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCASHTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXENCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINCONLEAV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXCONLEAV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVEMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANDWEEK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVAILNOBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCOMBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANTCOMBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXCFDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECIALSB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEACALTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISANNLEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISMATLEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPORTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISEMERLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPROPLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISRELIGION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDVALIDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTCLRREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISDOCMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISHOLRMB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISYEARBOOK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLWRNDOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVDETAIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RNDOFFTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RNDOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELGDAYDOC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARKWKOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLEAVEH',
		timestamps: false
	});
};
