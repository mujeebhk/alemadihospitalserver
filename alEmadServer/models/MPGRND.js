/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPGRND', {
		GRNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FMTITMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VENITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYREJECT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYRECTODT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYOUTSTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DNLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LANDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STKUPDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRCLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICEUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDLANCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECOVERYN1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXCOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRPEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINVSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MARGINPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COSTPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYACHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETRPLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETRPPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETRPEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACUNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPGRND',
		timestamps: false
	});
};
