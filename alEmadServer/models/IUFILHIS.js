/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUFILHIS', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FDEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FDEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISSUETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FISSUETO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISSUEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FEMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FEMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TDEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TDEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TISSUETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TISSUETO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TISSUEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISCLOSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISACKNOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMREQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUFILHIS',
		timestamps: false
	});
};
