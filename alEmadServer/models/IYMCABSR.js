/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYMCABSR', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SPECIMTYP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORGGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORGANCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ABSTPANCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ANTBIOCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RLANTBIO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTIBIOTIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESULT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUSPTBLITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IYMCABSR',
		timestamps: false
	});
};
