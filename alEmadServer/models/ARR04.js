/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARR04', {
		IDR04: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGINPERD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGINPERD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGINPERD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGECR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEUAPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTZEROBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARR04',
		timestamps: false
	});
};
