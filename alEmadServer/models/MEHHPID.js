/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHHPID', {
		CATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HPI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEHPISEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HPIELEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEHHPID',
		timestamps: false
	});
};
