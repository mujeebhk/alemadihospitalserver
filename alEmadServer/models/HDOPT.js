/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HRCONTEMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HRPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HRMOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MIDDLEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTNMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENDERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOBYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDRYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TELPHONEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAILYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESUMEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESKEYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EDUCATONYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKILLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHAUTRMD: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDOPT',
		timestamps: false
	});
};
