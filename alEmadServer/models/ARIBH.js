/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARIBH', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDSHPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIPVIA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECINST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTTRX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDTRX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVCSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDRNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOBNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRTINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVCAPPLTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEASOF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWMANRTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCHRATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORIGRATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TERMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTERMOVRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCAVL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODESLSP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCTSASPLT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWTAXBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWMANTX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXSTTS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTXBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNOTTXBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPPD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPYMSCHD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDSTDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEPRCS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDPPD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBILL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOSTE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOSTE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOSTE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOSTE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOSTTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOPOST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOCTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOCTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPTOFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPROCPPD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUROPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLDWNLK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHPVIACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPVIADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRENTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTDSBWTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSBNTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSCBASE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAPPLYTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGTERMS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRTGDDTOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGAMTOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARVERSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXVERSION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXRTGRPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURNRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTXCTLRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPERC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATERC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTGROSHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTNETHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPPDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRTLBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDSHIPNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWOECOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARIBH',
		timestamps: false
	});
};
