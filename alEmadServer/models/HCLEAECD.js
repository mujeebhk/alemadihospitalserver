/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEAECD', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GROSSSAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYSALARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOOFLEAVES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTUALDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AIRFARE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLEAECD',
		timestamps: false
	});
};
