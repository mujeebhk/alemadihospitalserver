/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHALGYT', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ALRTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		ALRCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALRDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ONSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ONSETDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REACTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEVERITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIONAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRGTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATINACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEHALGYT',
		timestamps: false
	});
};
