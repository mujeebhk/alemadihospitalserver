/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCREMITT', {
		REMITNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERPROCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCREMITT',
		timestamps: false
	});
};
