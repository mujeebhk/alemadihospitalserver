/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYINFCTS', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMENTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMENTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT11: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT12: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT13: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT14: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCMT15: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYINFCTS',
		timestamps: false
	});
};
