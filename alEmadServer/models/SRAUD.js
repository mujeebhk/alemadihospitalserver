/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SRAUD', {
		OLDCUSTOME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		NEWCUSTOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SRTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SRUSERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWSHORTNA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDITTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDITUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDITORGAN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'SRAUD',
		timestamps: false
	});
};
