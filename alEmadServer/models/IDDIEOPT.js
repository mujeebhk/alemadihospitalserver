/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDDIEOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIME1REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME2REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME3REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME4REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME5REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME6REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME7REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME8REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME9REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME10REQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIME1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILDIETORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MENLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MENPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCREMENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GUESTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GUESTPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUESTBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUESTVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUESTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIAGMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALERTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADTNLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BULKSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLKORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLKORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLKORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLKORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDDIEOPT',
		timestamps: false
	});
};
