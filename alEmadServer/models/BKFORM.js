/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKFORM', {
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FORMID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STKTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FORMSPEC1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORMSPEC2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LANGUAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKFORM',
		timestamps: false
	});
};
