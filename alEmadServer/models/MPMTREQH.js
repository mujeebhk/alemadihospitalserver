/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPMTREQH', {
		MATREQSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MATREQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMLOCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQUIREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ONHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOLOCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOPOLINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORISED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREPAREBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHPRINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFTRF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTTRF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOOFPO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPOCMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH1DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH1TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH1BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTH2DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH2TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH2BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTH3DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH3TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH3BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PURENQIR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VENDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMLOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPMTREQH',
		timestamps: false
	});
};
