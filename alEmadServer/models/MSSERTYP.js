/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSSERTYP', {
		SRVTYPCOD: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPBILLORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSSERTYP',
		timestamps: false
	});
};
