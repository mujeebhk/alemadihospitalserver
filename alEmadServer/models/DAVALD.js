/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DAVALD', {
		VCLMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BILLNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ICDCODE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ICDCODE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LICENSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDPRVID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDPRVLIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLANTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECBILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NPOLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOAPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SYMPTOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SYMPTOM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODRPOSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHFCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'DAVALD',
		timestamps: false
	});
};
