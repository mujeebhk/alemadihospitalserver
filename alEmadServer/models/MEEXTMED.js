/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEEXTMED', {
		EPISODENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENRICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENRICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOOFDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTCONSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTCONSTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MEDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APROVBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APROVDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APROVTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSUNTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPENSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STOCKUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPMEDIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEEXTMED',
		timestamps: false
	});
};
