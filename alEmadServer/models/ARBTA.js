/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARBTA', {
		CODEPYMTYP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTENTER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTENTER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATCHSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLASTITM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEEXCHHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPSTNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJUSTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REAPLYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NBRERRORS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TYPECLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTREAPPLY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARBTA',
		timestamps: false
	});
};
