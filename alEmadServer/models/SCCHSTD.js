/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SCCHSTD', {
		CSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPREFUND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTOPADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPBILLREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPBILLREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPBCREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTOPBREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPPATREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPPATREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTOPCREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPREFUND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIPADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPSETREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPSETREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIPSREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPPATREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPPATREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIPCREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALPH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BDBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BDENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHPATREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHPATREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPHCREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'SCCHSTD',
		timestamps: false
	});
};
