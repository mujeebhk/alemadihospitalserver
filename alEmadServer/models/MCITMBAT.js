/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCITMBAT', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCITMBAT',
		timestamps: false
	});
};
