/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCTRVREQ', {
		REQSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLIENTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRVLBKNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BKDBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACMBDREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VHCONWJR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VHCRTNJR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTAMNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVREQAMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITPURP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVAMNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCTRVREQ',
		timestamps: false
	});
};
