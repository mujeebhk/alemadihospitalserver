/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DATMPDOC', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUTONO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELDOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'DATMPDOC',
		timestamps: false
	});
};
