/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEEHRHD', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHRON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHEFCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHEFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMPRESSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMPRDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTASSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ONSETDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UPDATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECURITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INITICDCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INITICDDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MECHONSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFVISIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHIFCOMNAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UPDATEVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VARVISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ONSETVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ONSETPRST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EPISTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FINALCHFCM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INICDDESED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FNICDDESED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREDITTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABORDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADORDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISORDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMUSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFPROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFPRODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HPICOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNDATFMOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPEPISODNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NORISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFRISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FALLRISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NUTRIRISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRADEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHRONPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEEHRHD',
		timestamps: false
	});
};
