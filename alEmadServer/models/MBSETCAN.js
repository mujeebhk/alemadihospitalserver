/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBSETCAN', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SETTLENO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMSETTLE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MBSETCAN',
		timestamps: false
	});
};
