/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDAPPSHD', {
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECRITPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRENTPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISCOMPLETE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPENTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDAPPSHD',
		timestamps: false
	});
};
