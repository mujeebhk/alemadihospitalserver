/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSREGCHG', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CHTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDTITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDHIJIRI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWDISNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWFNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWBDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEWAGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWAGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWAGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWSEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWMARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWTITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWHIJIRI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHBY1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHBY2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEINMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDPHOCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWPHOCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDPTIDTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDPTIDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWPTIDTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWPTIDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELIGION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NWRELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NWNATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NWNATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIRTHTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NWBIRTHTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWINACTIV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NWPATCAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NWPATTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWADDR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWADDR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWADDR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWADDR4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWFATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DECEASED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWSTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWCOUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATOCCUPTN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATOCCUPNW: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATFTOCCUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATFTOCNW: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLODGRP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLODGRPNW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DECDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DECDATENW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDHOMPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWHOMPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDCELL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWCELL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OSECPTIDTP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OSECPTIDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NSECPTIDTP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NSECPTIDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSREGCHG',
		timestamps: false
	});
};
