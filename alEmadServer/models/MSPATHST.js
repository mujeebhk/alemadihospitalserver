/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPATHST', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SEQNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITLECOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RACE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHONEOFF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONEEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOHOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DECEASED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DECDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SSN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPREMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILREMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHOTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUARTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REGFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PASSPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISAEXP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISATYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLOODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BABYBORN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHILDOF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIRTHTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONRELN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTSTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTCNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UHID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDPMINO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPATHST',
		timestamps: false
	});
};
