/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDAGEVLD', {
		EVALPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EVLPICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDAGEVLD',
		timestamps: false
	});
};
