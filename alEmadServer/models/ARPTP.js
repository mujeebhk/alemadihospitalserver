/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARPTP', {
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTVSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTELSTMTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARPTP',
		timestamps: false
	});
};
