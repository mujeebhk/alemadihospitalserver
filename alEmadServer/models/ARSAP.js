/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSAP', {
		CODESLSP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODEEMPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMEEMPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWCOMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTANLTARG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATECLRD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARSAP',
		timestamps: false
	});
};
