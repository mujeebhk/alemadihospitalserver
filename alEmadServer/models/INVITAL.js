/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INVITAL', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VITALCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VITALDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITALTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHTUN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHTUN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BMIFACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EYESIGHTR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EYESIGHTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEECH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		METHMESUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SYSSITTING: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSSTAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSSUPIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIASITTING: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIASTAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIASUPIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PPRESTRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PPHERTRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPRATURE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OXYSAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AXILIARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TYPHANIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAINTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINNOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINLASTWK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINNORMAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREVEFFECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHIFCOMCD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMNM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIRSTVIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROSCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPVITALTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VITALMODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VITALMODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITALMODTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLUCOFBS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLUCOPPBS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASSMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOREHEAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WAIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISVITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMERGASMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTASMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRIAGCATG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BSAMOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEADCER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CENVENPRE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VENTILATED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VENTILATPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABGYES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		O2SAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLUCOSERBS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLUCOFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHESTCIRM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLORCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIVITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEDATIONYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEDSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CVPFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VMODEFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VRATEFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VTIDALFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VVOLFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VAIRFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VF102FREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VPEEPFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VASBFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		V1EFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VO2FREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VETCFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABPHFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABPCOFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABPOFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABHCOFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABBEFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABSPOFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABNAFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABHBFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABACTFREE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FCEMSKTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABDMGRITH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PETELPLS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NORISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFRISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FALLRISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NUTRIRISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRADEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLURBTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEWPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INVITAL',
		timestamps: false
	});
};
