/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEDIETHD', {
		DIETSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISODCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURRENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACKWLGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACKBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACKDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACKTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIETDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEDIETHD',
		timestamps: false
	});
};
