/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSHL7VEN', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENNAME1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENIP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENPORT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEMOGRAPH1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITYN1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERYN1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPOINT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LISTENIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LISTENPORT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VENNAME2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENIP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENPORT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEMOGRAPH2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITYN2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERYN2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPOINT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSHL7VEN',
		timestamps: false
	});
};
