/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPMIMPJ', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROJSTYLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCPMIMPJ',
		timestamps: false
	});
};
