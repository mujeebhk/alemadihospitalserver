/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INFUTORD', {
		FUTORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUTORDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNBILORD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INFUTORD',
		timestamps: false
	});
};
