/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCTRFH', {
		STKTRFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STKTRFNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMLOCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMTRFNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MATREQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULTIMRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOLOCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GITLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GITLOCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRORATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTTRFQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTRECQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTMPRCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCPRCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCPACPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCTRFH',
		timestamps: false
	});
};
