/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDDOC2', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESRPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESRLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ESRNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPEPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPENEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPIDPRFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPIDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPIDNXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDDOC2',
		timestamps: false
	});
};
