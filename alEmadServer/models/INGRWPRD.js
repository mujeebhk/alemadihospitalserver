/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INGRWPRD', {
		GRONO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AGE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERVAL3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERVAL5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERVAL10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERVAL25: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERVAL50: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERVAL75: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERVAL90: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERVAL95: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERVAL97: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RANGEHIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RANGELOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INGRWPRD',
		timestamps: false
	});
};
