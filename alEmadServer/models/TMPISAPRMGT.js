/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPISAPRMGT', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMSCREEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQCREBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQCREDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQCRETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		READYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READYTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREAUTHTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCNEEDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCENQNEED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCENQCLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCCLSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCCLSBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'TMPISAPRMGT',
		timestamps: false
	});
};
