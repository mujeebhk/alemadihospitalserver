/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIMSCDT', {
		IMGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IMNTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FROMDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TODAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RANGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COLHEAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROWHEAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CELLDATA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRAGETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAGETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INIMSCDT',
		timestamps: false
	});
};
