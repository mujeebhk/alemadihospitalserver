/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APOBS', {
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPAID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTDUEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDCSRMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPYMRMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSCRMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPYMRMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDORDRNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDPONBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDPREPAID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXTTRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYSTOPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPYMLMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAPPLYTO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APOBS',
		timestamps: false
	});
};
