/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRSCHDD', {
		SCHEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EQUIPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMBREAKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBREAKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EQUIPETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MRSCHDD',
		timestamps: false
	});
};
