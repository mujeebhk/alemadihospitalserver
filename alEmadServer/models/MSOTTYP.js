/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSOTTYP', {
		OTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANESCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANSTCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTROMHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISBILCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSOTTYP',
		timestamps: false
	});
};
