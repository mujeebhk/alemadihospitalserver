/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESCPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESCPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESCPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPSODELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPSODEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPSODEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EPSODEVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNOTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILDETCOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLRPFC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRPROICD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRFINICD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRICP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRAUTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPYSOAPSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATREFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATREFPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATREFBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOSREFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOSREFPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOSREFBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MORNLBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOONLBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVENLBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NIGHTLBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGRYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGRYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGRYBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGRYVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLPROVIDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOULEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTPOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURADVLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURADVPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURADVBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURADVVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFVARSYM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESTRICT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WIPHRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNDREWHRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLOSEHRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPMNGYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PASTMEDRAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHEFCMPMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ONSETDTMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HPIADDTMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIAGNOSMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWCURSYMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDLEMELEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALERGYREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTYPEDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEMPLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPSTOCK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHISTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHOWBIOHAZ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACSURL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSEHRSIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENABLENURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFRSHMYDY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWIMGBUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLRPTSMHD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRPTSMSHD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRPTSMFSZ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRNAMESUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLERGMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIMEFORMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHYEXMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOCREVMND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDALRMND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FAMHISMND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PASTHISMND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHYEXMMND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SYSREVMND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNVLABRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSMDHA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTERBYREC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DELDATA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDNOSOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LDINPFOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFEMIRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWDATFMIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWPROGNTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPLOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESCIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALRGYALERT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISSUMMRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTCODESEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GOLIVEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HISTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISHISPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEOPT',
		timestamps: false
	});
};
