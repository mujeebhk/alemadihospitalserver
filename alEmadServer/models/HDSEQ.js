/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDSEQ', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPSEQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGNSEQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ESRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EPESEQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNHISSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSHISSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTMPLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECREQSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROUNDTYPNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SMSSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ATTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNSCHSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNEVLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGNEVLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HDSEQ',
		timestamps: false
	});
};
