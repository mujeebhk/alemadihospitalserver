/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLPACHD', {
		PARENT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLPACHD',
		timestamps: false
	});
};
