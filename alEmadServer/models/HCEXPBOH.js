/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEXPBOH', {
		EXPSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPTRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BOOKDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCEXPBOH',
		timestamps: false
	});
};
