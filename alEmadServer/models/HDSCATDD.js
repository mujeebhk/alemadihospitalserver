/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDSCATDD', {
		ATTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SCHCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TRNGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDSCATDD',
		timestamps: false
	});
};
