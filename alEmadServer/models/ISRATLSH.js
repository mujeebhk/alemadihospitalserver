/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISRATLSH', {
		RATLSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECTALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSLTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVENTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NURSEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISCLDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANESTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ISRATLSH',
		timestamps: false
	});
};
