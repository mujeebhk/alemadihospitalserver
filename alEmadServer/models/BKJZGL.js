/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKJZGL', {
		PSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		KEYSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DTLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCOUNTD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPONENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'BKJZGL',
		timestamps: false
	});
};
