/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSESOREQ', {
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQBYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOCATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSTDET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABDET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISCTSTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISCTSTDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMCOLLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TSTCONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLIENTPREP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISACKNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACKNLEDGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACKNLEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACKNLEDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSESOREQ',
		timestamps: false
	});
};
