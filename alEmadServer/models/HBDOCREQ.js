/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBDOCREQ', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQFRM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISSUEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBDOCREQ',
		timestamps: false
	});
};
