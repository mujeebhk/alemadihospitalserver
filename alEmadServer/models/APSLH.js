/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APSLH', {
		IDSELECT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASMNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWDOCSPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWSELLBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTEDISCFRM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDGRPFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGRPTHRU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDVENDFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDVENDTHRU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSETFR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSETTHR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWVENDEXCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDBANKASGN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURNTC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBANKPAYM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURNPY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTBNKLIMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTMINCHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTMAXCHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWBANKMTCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATECHECK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODERATETC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODERATEBC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXCHRATETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATEBC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEDATETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEDATEBC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CSVFILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DTEDISCTHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTEBATCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPTC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATEOPBC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATETC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATEBC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLYMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDSELECTED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PMCODEFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PMCODETHRU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTFIELD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DECIMALS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUEFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALUETHRU: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APSLH',
		timestamps: false
	});
};
