/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBMULBNK', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IBANNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCOUNTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WPSNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBMULBNK',
		timestamps: false
	});
};
