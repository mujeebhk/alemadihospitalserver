/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLANACOM', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		COMENTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICCOMNT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICCOMNT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICCOMNT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMDREASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTERPTCOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT3: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLANACOM',
		timestamps: false
	});
};
