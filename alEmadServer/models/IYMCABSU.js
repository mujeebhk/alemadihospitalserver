/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYMCABSU', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SPECIMTYP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORGGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORGANCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ABSTPANCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABSTPANDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MORPHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEMENGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUPPRESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTEXTERN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTRUN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IYMCABSU',
		timestamps: false
	});
};
