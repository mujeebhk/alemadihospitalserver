/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSTRNNUM', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXPERFUT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VSTORDREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLDEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDDUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EVENTID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALNONCONF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NONCONFDUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WINDOWPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRACEPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ESOREQLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ESOREQPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESOREQVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESOREQBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFCRNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFCRNPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFCRNVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFCRNBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFVIDNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFVIDNPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFVIDNVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFVIDNBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFOIDNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFOIDNPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFOIDNVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFOIDNBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTNUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPATTYPMN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFPATTYPC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFPATTYPD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSRVSTON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUOTENEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUOTELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUOTEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUOTEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUOTEVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXORDPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOEPISOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPISDDURTN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHIFTCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILCODEENB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLCYPRDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHFCOMPSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLCHGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRTNWEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSTRNNUM',
		timestamps: false
	});
};
