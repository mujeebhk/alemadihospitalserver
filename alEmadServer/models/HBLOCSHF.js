/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBLOCSHF', {
		SHFRSSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LOCATNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTABBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMTM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMTM2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTM2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMTM3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTM3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROSGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBLOCSHF',
		timestamps: false
	});
};
