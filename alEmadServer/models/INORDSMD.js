/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INORDSMD', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SPEMENTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SPEABBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TSTSITE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEABBRDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEMENDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SITEMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SITECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SITEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCEMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOURCECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECODTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSTSITEDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTUSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQVOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTCONTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEHANDET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECINSREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECINSPAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLCOLLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLCOLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLALLOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECLOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECLOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECALLOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TSTSITALOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STCONTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEMOPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTCONTSDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STCONTSDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECANRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ONHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STORTRACOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STORTRADES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATEXIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEACCEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABREVATION: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INORDSMD',
		timestamps: false
	});
};
