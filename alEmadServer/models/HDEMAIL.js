/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDEMAIL', {
		MSGTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MSGCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MSGFOR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		ROUNDSTS: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		TMPLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFLTMSG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MSGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LONG1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDEMAIL',
		timestamps: false
	});
};
