/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCTRVCLH', {
		CLAIMSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLAIMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLAIMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCTRVCLH',
		timestamps: false
	});
};
