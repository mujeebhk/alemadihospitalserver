/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARDUN', {
		CODESTMT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTELSTMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTSTMT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTMT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTMT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTMT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTMT5: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARDUN',
		timestamps: false
	});
};
