/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISEMPLYR', {
		EMPLRCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSUNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPACTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPINACTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYERID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISEMPLYR',
		timestamps: false
	});
};
