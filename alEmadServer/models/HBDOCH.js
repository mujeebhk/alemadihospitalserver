/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBDOCH', {
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FILEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCSURC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPNTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPNNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELDOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILESIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBDOCH',
		timestamps: false
	});
};
