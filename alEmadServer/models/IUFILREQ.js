/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUFILREQ', {
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISSUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECEIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISACKNOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHELFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUFILREQ',
		timestamps: false
	});
};
