/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBESILMT', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EARDEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPUTEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LIMITAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LIMITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALEARN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBESILMT',
		timestamps: false
	});
};
