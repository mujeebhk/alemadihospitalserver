/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKTRANR', {
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SEQUENCE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWREVDOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWREVINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVFISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVFISCPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTRYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DETAILTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDREMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DDLINK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEREMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCEAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYORID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYORNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'BKTRANR',
		timestamps: false
	});
};
