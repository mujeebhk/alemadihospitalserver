/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLAFS', {
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FSCSYR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FSCSDSG: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FSCSCURN: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CURNTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODERVL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNDEC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPENBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD11: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD12: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD13: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD14: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPERD15: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTIVITYSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLAFS',
		timestamps: false
	});
};
