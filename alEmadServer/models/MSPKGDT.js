/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPKGDT', {
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LISTPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHEXP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANESTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHYTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSPKGDT',
		timestamps: false
	});
};
