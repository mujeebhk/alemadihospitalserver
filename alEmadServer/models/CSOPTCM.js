/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSOPTCM', {
		PGMID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PGMVER: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CONVINIID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTITLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLENGTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTABLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOPTFIELD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NDEFVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CSOPTCM',
		timestamps: false
	});
};
