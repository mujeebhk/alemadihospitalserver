/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLRVRAT', {
		FSCSCURN: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FSCSYR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RVLRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE11: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE12: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVLRATE13: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'GLRVRAT',
		timestamps: false
	});
};
