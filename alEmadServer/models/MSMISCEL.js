/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSMISCEL', {
		MISCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISGRPTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSULTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MNORPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCODEREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREFOLWUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLWPRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLWPRDUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLWPRDDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFFOLWUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDTEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESTXTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSMISCEL',
		timestamps: false
	});
};
