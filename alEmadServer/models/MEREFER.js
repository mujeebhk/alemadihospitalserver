/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEREFER', {
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOSPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMPRVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMSPLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMSPLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERALTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFEREDNOT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEREFER',
		timestamps: false
	});
};
