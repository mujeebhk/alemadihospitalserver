/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARTCU', {
		CODEPAYM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODTRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDDISTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARTCU',
		timestamps: false
	});
};
