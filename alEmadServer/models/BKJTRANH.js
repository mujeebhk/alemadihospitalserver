/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKJTRANH', {
		PSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDSERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTRYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FSCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTBALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCLEARED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NXTLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINESPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINESREC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECERR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECERRPEND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEXGAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEXLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOUTSTND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUMMARY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECCLEARED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFUNCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTFUNCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOCLEAR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOWRITEOFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOREMAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VARIANCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINESREVIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LINESJOUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINESPUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOCLEARF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECRCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECWOSUMR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOCLEARR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECRWOSUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPREM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTWO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTERR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTGAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTFUNAM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURFUNC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURSTMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECWOSUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECDELTA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCMISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCWMISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCDMISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFWMISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFDMISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYORNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDORNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LINESPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTDTLREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDTLOUT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'BKJTRANH',
		timestamps: false
	});
};
