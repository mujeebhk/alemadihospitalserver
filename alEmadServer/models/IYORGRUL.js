/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYORGRUL', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ORGGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORGISMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RULEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYORGRUL',
		timestamps: false
	});
};
