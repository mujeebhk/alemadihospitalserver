/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCATTNDD', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABSENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTHOLIDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWORKDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWORKHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTOTHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WORKHOLIDY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPOFFS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPOFFHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TYPEATTEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESENTDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABSENTDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRACEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVEDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAIDHOLDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNPAIDHOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVEUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESNTDAY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABSENTDAY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVEDAY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVETYPE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVEUNIQ1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOLIDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOLIDAY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHORTTIME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WKOFFOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PENDUNPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCATTNDD',
		timestamps: false
	});
};
