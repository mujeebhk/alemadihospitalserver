/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DACLMH', {
		CLAIMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLAIMDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCMPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLCGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLCGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEMBERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		XMLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FACLICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLAIMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESUBMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESUBMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESUBMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESUBMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPLRCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTACTID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLAIMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NORESUBYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NORESUBAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCLAIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FILENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTADJAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTBADDEBT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWRITOFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMCLMDOC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'DACLMH',
		timestamps: false
	});
};
