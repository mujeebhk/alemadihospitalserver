/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INNUFARI', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HISOFFALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHISFALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIAGNOSIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATDIAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMBUAID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATAMBUAID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEPARISALI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHEPARI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GAITTRANSF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATGAITTRA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MENTALSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATMENTST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATTOTSCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSINJURY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSINJUCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSVITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSVITACMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INJNONE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJNONECMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INJMINOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJMINCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INJMAJOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJMAJCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INJDEATH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJDEATCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTRVTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FOLOWPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDEFFCT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMPOSTOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NARCADM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHNGAMBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHNGLOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNSFRCLN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEEKREASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHIFTREASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLLOWPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLLOWFALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWGTPTF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWBDSDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWTRNSPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWKEBD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWCLBEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWMOBLTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWHAZARD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWBATLIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWRSTOIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWUSESP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWCHFALLR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWNEBCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODIDRSK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODSUPAMB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODMNREV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODMOVNS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODCLLT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODHRSFCK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODREGDSA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODRSRAILS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODPATED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIGHGPRTR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIGHMTRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIGHHLTCAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIGHPHYT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIGINSOBV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAFFBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OCURASSMN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UFPI1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI7: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI9: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI10: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI11: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI12: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UFPI13: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LRPI1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LRPI2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LRPI3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LRPI4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI7: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI9: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI10: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI11: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRPI12: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIIC1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPSY1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPSY2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPSY3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPSY4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPSY5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPSY6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPH1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPH2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPH3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPH4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPH5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPH6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPH7: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIPH8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIAP1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIAP2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIAP3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRIAP4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRI6NOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CFR8NOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CFR4NOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INNUFARI',
		timestamps: false
	});
};
