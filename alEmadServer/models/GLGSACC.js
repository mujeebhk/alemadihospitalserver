/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLGSACC', {
		USER: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWCANSEE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEGINAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENDAC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLGSACC',
		timestamps: false
	});
};
