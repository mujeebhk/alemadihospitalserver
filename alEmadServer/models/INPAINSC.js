/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INPAINSC', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PAINELEM: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEVERITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INPAINSC',
		timestamps: false
	});
};
