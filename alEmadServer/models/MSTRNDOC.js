/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSTRNDOC', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATREGNEXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATREGLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATREGPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATREGBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATREGVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATVSTNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATVSTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATVSTPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATVSTBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATVSTVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATDEFVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SECONDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTNMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDVISIBLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATLABEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVLABEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ECGVISIB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCLBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOWVISIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINKVISIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATAUTOGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRANCHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRANCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRECONTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REGEXPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPIRYDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNINUMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AREALBL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AREALBL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AREALBL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AREALBL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRDLBREQIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MRDLBREQOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MRDFREQIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MRDFREQOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOPRELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOBDYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTOBODY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTOVALUE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCREMENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FATHERMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOBILEMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELIGNMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DFTBT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDTYPEMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFCUSTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERREGNEXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERREGLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERREGPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERREGBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERREGVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EPISEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANNUMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPBULKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MRDFREQAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CITYMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMSEQNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVCOMLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRVCOMPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRVCOMBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRVCOMVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMRGCYMAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMREGSMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAPPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAPPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREAPPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NONCONSULT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOAPYESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTATCHPDF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISAMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRLIVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUNTRYMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AREALBL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAILMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AREAMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTPRVMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECPATIDRQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUEAPPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MISSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MISPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSTRNDOC',
		timestamps: false
	});
};
