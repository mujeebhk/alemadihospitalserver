/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IATMPAPP', {
		TRANSNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CLIENTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLINICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLIENTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IATMPAPP',
		timestamps: false
	});
};
