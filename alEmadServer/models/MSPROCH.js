/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPROCH', {
		PROCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANESREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFANES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURNOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANESNOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RISKNOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTMODIF1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CPTMODIF2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CPTMODIF3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CPTMODIF4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HCPSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HL7CODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERCREF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANESDFUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLACEDFT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPMOD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPMOD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPMOD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPMOD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREFOLWUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLWPRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLWPRDUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLWPRDDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFFOLWUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANESSECPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORPRIPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORSECPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESTXTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPROCH',
		timestamps: false
	});
};
