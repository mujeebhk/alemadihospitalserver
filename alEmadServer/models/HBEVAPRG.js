/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEVAPRG', {
		EVAGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EMPGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PRFGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTMRK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCLUDEMRK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCLUDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBEVAPRG',
		timestamps: false
	});
};
