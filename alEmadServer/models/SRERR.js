/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SRERR', {
		OLDCUSTOME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		NEWCUSTOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATADICTIO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MESSAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRORTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDITTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDITUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDITORGAN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'SRERR',
		timestamps: false
	});
};
