/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSHIJRID', {
		HIJYEAR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		HIJMONTH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HIJMONTHNM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENGMNSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENGMNSTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENGMNSTYR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HIJMNNODAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSHIJRID',
		timestamps: false
	});
};
