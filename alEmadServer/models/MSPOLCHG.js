/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPOLCHG', {
		POLCHGNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHANGEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHANGEDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWPOLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHANGEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPOLCHG',
		timestamps: false
	});
};
