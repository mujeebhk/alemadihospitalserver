/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APTCR', {
		BTCHTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTENTR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMERMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTRMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRMITTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCHTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRATETC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTPAYMENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPPAYTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPEHC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEEXCHHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRATEHC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RMITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTLSTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERATETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPETC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTADJTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATERATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMREAPLTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJTOTDBHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRMITHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRNTRMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDRMITTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXTRMITREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJTOTCRHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDEPSSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEXTSTRE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPSTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKLANG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPERBANK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPERVEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJTOTDBTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJTOTCRTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEACTVPP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLYMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRENTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDINVCMTCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURNBC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASHACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILLAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLDWNLK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODE1099: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMT1099: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWTXAMTCTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXBSECTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXVERSION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXBSE1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECURNRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTXCTLRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPERC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATERC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPPAYHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMREAPLHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APVERSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTACC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTACCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTACCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APTCR',
		timestamps: false
	});
};
