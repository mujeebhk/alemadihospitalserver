/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLTATHIS', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COLLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COLLECTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLLECTED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCPTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCPBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCEPTED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFTATMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFTATSTR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RUNTATMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RUNTATSTR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISELAPSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELAPSEDMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELAPSEDSTR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISCONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLTATHIS',
		timestamps: false
	});
};
