/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUCODERT', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CDRSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISFRVISIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNDOSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIAGCHNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSIGNED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSIGUSR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSIGNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASSIGNTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REASSIGN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REASIGUSR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IUCODERT',
		timestamps: false
	});
};
