/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRTG', {
		RTGSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRXTYPETXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTDUE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTAMTBKWD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTAMTFWD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARRTG',
		timestamps: false
	});
};
