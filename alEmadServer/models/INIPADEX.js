/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIPADEX', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NTKNOWNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEGTIVEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSITVEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWABTKNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTDONE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFNOTIF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONNOTIF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMPNOTIF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDNOTIF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WALNOTIF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WHCNOTIF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNNOTIF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATNOTIF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RASNOTIF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOPROBEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEFMEDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETNUTR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USFMEDEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REHABEQI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POTINTBT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDANDFD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		KONKMTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINSYMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PADDRNM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PADDRDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PADDRTIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPHISTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADEDCTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FALRSKNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMDIAG: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INIPADEX',
		timestamps: false
	});
};
