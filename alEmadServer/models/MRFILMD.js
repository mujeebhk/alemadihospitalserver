/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRFILMD', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RADCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIMEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WASTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MRFILMD',
		timestamps: false
	});
};
