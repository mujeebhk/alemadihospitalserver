/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLRED', {
		RECID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TRANSNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPANYID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCURNDEC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HCURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEMTCHCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCELDGR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZEROSRCFG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLRED',
		timestamps: false
	});
};
