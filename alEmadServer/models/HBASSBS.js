/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBASSBS', {
		GRADECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		STEPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BASICSAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTAIRAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELGMNTHS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EDUALLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HBASSBS',
		timestamps: false
	});
};
