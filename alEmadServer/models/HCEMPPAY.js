/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEMPPAY', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYDET1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYDET2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRENCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANNLEAVSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANAPPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BPBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALARYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVESAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AIRFARE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETSALARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RNDOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRTOTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BNKSTATGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCEMPPAY',
		timestamps: false
	});
};
