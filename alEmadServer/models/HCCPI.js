/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCCPI', {
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPIVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		USED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCCPI',
		timestamps: false
	});
};
