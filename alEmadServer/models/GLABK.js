/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLABK', {
		ACCTBLKID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUSACTV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABLKVALUE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKUSAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVHUSAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARHUSAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEEFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQDSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLOSESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABLKDELM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NUMOFKEY: {
			type: "BINARY",
			allowNull: false
		}
	}, {
		tableName: 'GLABK',
		timestamps: false
	});
};
