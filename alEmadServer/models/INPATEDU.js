/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INPATEDU', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INPATEDU',
		timestamps: false
	});
};
