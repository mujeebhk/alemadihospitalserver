/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IACLISCH', {
		SCHEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFSLOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOSLOTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKBREAK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKBREAK2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKBREAK3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMBRKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMBRKHR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMBRKHR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OKMONDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKTUESDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKWEDNESDY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKTHURSDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKFRIDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKSATURDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKSUNDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLOBLCLINC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IACLISCH',
		timestamps: false
	});
};
