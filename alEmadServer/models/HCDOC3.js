/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCDOC3', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLCPJCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLCPJCPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLCPJCBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVOICELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVOICEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVOICEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCDOC3',
		timestamps: false
	});
};
