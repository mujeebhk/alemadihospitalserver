/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSDOCH', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PAGENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELDOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILESIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSDOCH',
		timestamps: false
	});
};
