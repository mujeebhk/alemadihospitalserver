/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPMRCMPOPT', {
		MROPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTSERV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVICED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHKINCOL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADSEQR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADRESPN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADRESEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADRESAU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADEPTES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISAPPOINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESPENDCON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMLAP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOLMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMPEG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOPEG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULEQP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRTICALCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFRPTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'TMPMRCMPOPT',
		timestamps: false
	});
};
