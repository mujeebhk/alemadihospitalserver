/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTRNSCH', {
		SCHCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNINGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDIACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNGDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNGFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNGTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNINGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CERTFYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNGHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CAPACITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMHOUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOHOUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BREAK1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREAK2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREAK3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRBRKHR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRBRKHR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRBRKHR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OKMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKTUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKTHU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKFRI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKSAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OKSUN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHRZEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHRZEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAILSNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDTRNSCH',
		timestamps: false
	});
};
