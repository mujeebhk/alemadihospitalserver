/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLAMDHIS', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMENDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REASONAMD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLAMDHIS',
		timestamps: false
	});
};
