/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPRFOBD', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBJECTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBJECTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSCORE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSCORE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSCORE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSCORE4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSCORE5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBJECTYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QTRLYRVW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GOALRSLT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCPRFOBD',
		timestamps: false
	});
};
