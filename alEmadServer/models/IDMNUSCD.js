/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDMNUSCD', {
		SCHECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MENUID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MENUTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUCATGRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IDMNUSCD',
		timestamps: false
	});
};
