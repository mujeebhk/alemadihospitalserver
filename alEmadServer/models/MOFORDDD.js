/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOFORDDD', {
		FORMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTRLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MOFORDDD',
		timestamps: false
	});
};
