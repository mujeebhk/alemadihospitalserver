/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIPHEXT', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		HCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NURSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHEXAMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHEXAMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSTEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXAMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTNOTIFD: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INIPHEXT',
		timestamps: false
	});
};
