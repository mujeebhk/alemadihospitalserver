/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCALLWH', {
		EXPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRATACC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRAVELTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALAMNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCALLWH',
		timestamps: false
	});
};
