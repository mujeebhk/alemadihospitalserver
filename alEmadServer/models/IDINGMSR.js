/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDINGMSR', {
		INGRDINTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		UOMID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UOMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRAMWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IDINGMSR',
		timestamps: false
	});
};
