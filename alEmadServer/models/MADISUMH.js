/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MADISUMH', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TABTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMITPRV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMPRVNM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPRV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPRVNM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRDIAGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRDIAGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PANTCDCSD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PANTCDCSDY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCSURNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLLOWDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FOLLOWTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FOLTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPNAPMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMERCNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCMTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCMTS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MADISUMH',
		timestamps: false
	});
};
