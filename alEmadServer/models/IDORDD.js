/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDORDD', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SERVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEALTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHOICEID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOMID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISSUED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETNOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALORIES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALFAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCARBOHD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROTEIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IDORDD',
		timestamps: false
	});
};
