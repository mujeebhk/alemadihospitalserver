/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MECDLIST', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDSNDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASTHMA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRONCHTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARDCFL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARDMPY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRPLDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRRENDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CORARTDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRONDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIABINSIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIABMELIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DYSRHYTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPILEPSY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLAUCOMA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HAEMOPH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HYPERLIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HYPERTEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HYPOTHYR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULTSCLR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PARKINDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RHEUMDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHIZOPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SYSTLUPUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ULCERTCOL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHERSTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MECDLIST',
		timestamps: false
	});
};
