/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INSTDTSD', {
		SORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXFUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INSTDTSD',
		timestamps: false
	});
};
