/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ASACTLOG', {
		SEQUENCE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VERSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MESSAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ASACTLOG',
		timestamps: false
	});
};
