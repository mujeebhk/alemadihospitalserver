/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLSAMLOC', {
		SAMPLECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MLBINCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BINDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RACKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHANGEBIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHABINDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLSAMLOC',
		timestamps: false
	});
};
