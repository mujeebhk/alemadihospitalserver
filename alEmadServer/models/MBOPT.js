/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBOPT', {
		IDR02: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNIQUENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVNCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVNCPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVNCNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETTLEPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLENEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFUNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFUNPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFUNNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCREMENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCDEPOT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCIPCTRL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUPPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUPNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCBTCNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SETTLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTITLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRDNOREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INORDCLRAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHMORDCLR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPPATRECAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPSHTDISAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPPKGDISAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCRDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCRDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCRDPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCRDNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCRDSTCAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPDSCWRTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVGLBSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVGLBPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVMSTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOWADV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOSSERBCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGAPPRREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFSRVLOAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXECPSRVLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFSRVEXTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BTCHSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPRALRTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALERTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALERTMSG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSUMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHGWRTOFAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPDONCTLAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHKPOSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERTAXBCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPRSOAPMN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPLDISREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPLDISPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MBOPT',
		timestamps: false
	});
};
