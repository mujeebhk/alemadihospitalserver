/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEAVEY', {
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EMPTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMMONTH1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMMONTH2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMMONTH3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMMONTH4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMMONTH5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCRUDAYS1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCRUDAYS2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCRUDAYS3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCRUDAYS4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCRUDAYS5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYDAYS1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYDAYS2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYDAYS3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYDAYS4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYDAYS5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCLEAVEY',
		timestamps: false
	});
};
