/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INSBARH', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SBARDT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SITUATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BACKGRND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HISTORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ATNDDOCTOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONORREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIAGNOSIS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INSBARH',
		timestamps: false
	});
};
