/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INNUASSD', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPENSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPENDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMINSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MEDSOURCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INNUASSD',
		timestamps: false
	});
};
