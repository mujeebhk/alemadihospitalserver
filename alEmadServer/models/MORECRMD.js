/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MORECRMD', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPSYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BPDIA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MORECRMD',
		timestamps: false
	});
};
