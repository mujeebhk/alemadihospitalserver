/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDAPPIND', {
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EVALPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVALPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APLCNTSCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APLCNTSTRN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APLCNTWKNS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDAPPIND',
		timestamps: false
	});
};
