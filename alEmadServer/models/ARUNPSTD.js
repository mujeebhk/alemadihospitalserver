/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARUNPSTD', {
		SELSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTREV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTINV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTCOGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARUNPSTD',
		timestamps: false
	});
};
