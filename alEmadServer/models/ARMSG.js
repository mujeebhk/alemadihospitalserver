/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARMSG', {
		MSGTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MSGID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTELSTMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BODY10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARMSG',
		timestamps: false
	});
};
