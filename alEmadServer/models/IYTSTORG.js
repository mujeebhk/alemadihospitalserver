/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYTSTORG', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLONYCNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUANTITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGISMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGISMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFNOTIFY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IYTSTORG',
		timestamps: false
	});
};
