/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSANALQL', {
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LISTDATA: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFRANGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTTOINF: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSANALQL',
		timestamps: false
	});
};
