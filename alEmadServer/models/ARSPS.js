/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSPS', {
		CODESLSP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTYR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTPERD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTWROF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWROF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARSPS',
		timestamps: false
	});
};
