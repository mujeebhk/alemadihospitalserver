/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAIPBED', {
		IPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DTFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIMEFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIMETO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDOCCP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATOCCP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STARTACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENDACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCHARGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDSTATU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDCANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMBEDCLSC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMBEDCLSD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MIDNGTSTA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MIDNGTEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MIDNGTSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MIDNGTEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISDISCPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATDISCPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILBEDFRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILBEDTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELGEXTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELGUNTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELGEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILCODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HL7MSGSTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRVBEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWBEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDMODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDMODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDMODTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MAIPBED',
		timestamps: false
	});
};
