/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOINTRAD', {
		INTRASEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIMESTART: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIMEEND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MOINTRAD',
		timestamps: false
	});
};
