/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKTNUM', {
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NXTDEPOSIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPSERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEXTSERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKTNUM',
		timestamps: false
	});
};
