/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISINVRH', {
		INVRSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVRNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATEMNTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHEQUENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVRTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTDUEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTAPPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTBALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIPTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNAPPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECTALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BDBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BDENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECTBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTRECIPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECEIPTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNAPDOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNAPPCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISDELETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DELETEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELETEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DELETEDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTADJAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADPOSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADPOSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADPOSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTOMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISINVRH',
		timestamps: false
	});
};
