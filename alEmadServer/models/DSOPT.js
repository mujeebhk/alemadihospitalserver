/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPWQNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTZDTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRMCATMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLICENOTF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMERDEPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLICDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WOUNDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WOUNDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WOUNDDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NURSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'DSOPT',
		timestamps: false
	});
};
