/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPATREF', {
		REFRALNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERLSRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFRLTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COVPERCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COVAMNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFISUDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFSRTDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFENDDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIGNOSIS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPATREF',
		timestamps: false
	});
};
