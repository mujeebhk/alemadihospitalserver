/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSCRD', {
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SOURCECUR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEMATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'CSCRD',
		timestamps: false
	});
};
