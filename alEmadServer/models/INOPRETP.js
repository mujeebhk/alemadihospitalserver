/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INOPRETP', {
		RETUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISPACKAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGTRNNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BALANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETURNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETURNTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARMALOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INOPRETP',
		timestamps: false
	});
};
