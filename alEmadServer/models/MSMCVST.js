/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSMCVST', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MCNUMBER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISPREGNENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISXRAYRQRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECIMENNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FITNESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FITNESSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FITNESSTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FITNESSBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPONSRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OCCUPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLNTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MCEPISODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTACTCDE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UPDATESTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSMCVST',
		timestamps: false
	});
};
