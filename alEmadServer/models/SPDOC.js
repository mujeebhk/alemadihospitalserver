/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SPDOC', {
		NEWCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		OLDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		OLDDOCUMEN: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		NEWDOCUMEN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'SPDOC',
		timestamps: false
	});
};
