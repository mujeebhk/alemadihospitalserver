/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCATTOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DBTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOGINNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PASSWORD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATABASE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILEPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TABLENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLAGFIELD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLAGIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLAGOUT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLAGDBTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SINGLEREC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FLAGBRKIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLAGBRKOUT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLATERUL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTTMRANG1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTTMRANG2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTTMRANG3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTTMRANG4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTTMRANG5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDTMTYPE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDTMTYPE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDTMTYPE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDTMTYPE4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDTMTYPE5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FLAGINOUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWIPEID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCRYPTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCATTOPT',
		timestamps: false
	});
};
