/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARURC', {
		CNTNUM: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTFR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHANGEBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCDEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCTCHANGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCHANGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDDISTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCTMAXCHG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTMAXCHG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCESSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NUMSIARECS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARURC',
		timestamps: false
	});
};
