/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPAYSLP', {
		SALARYNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAILID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PDFPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESENDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCPAYSLP',
		timestamps: false
	});
};
