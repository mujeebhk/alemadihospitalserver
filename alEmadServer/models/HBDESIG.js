/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBDESIG', {
		DESIGID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOBCLSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOBCLSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPONSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOOFDEPNTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBDESIG',
		timestamps: false
	});
};
