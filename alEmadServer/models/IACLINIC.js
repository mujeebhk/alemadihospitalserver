/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IACLINIC', {
		CLINICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LINKPROYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPRONAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LINKLOCYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SLOTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IACLINIC',
		timestamps: false
	});
};
