/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSCPTSET', {
		CPTCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEQNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHRTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STDBLCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STDRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDNEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LONGDESC1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONGDESC2: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSCPTSET',
		timestamps: false
	});
};
