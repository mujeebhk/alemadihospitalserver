/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DATMPID', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PAGENO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CARDIDTYP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'DATMPID',
		timestamps: false
	});
};
