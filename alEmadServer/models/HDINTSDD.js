/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDINTSDD', {
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INTVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		SUBPANELCD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTLISTD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTVEWDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HDINTSDD',
		timestamps: false
	});
};
