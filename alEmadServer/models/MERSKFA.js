/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MERSKFA', {
		PTIENTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROBLEMCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRBLEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RISKSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRONICITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ONSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ONSETDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTADATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MERSKFA',
		timestamps: false
	});
};
