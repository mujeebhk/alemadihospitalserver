/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUFILMOV', {
		FILENO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMITTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCHTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSUEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIVEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSUECMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIVECMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISACKNOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPRVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISREMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISRECEIVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IUFILMOV',
		timestamps: false
	});
};
