/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEPRESCH', {
		PREID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPICODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDITYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISALLERGY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISREACTMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INIICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INIICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPPOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPPRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISDISCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUMPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRNTSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACKWNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IVPRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VERBORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSLNPRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTPRESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WRDSTKISS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STOPPRESC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIABPRESC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOBEADMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSULINSCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STKWARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OWNMEDCN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEPRESCH',
		timestamps: false
	});
};
