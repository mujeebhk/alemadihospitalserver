/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPKGEDT', {
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLCODDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPKGEDT',
		timestamps: false
	});
};
