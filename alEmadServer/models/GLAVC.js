/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLAVC', {
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CURNID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVALSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVALID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLAVC',
		timestamps: false
	});
};
