/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IICASHD', {
		TRNNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARDEXPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRENCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULTICUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BDBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BDENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EQPTRNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IICASHD',
		timestamps: false
	});
};
