/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDAPPLH', {
		APPSEQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESUMHEAD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MIDDLENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRADDR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRADDR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRADDR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRSTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRCOUTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRZIPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRTELEPH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNADDR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNADDR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNADDR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNSTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNCOUTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNZIPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNTELEPH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRESHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEPOSTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WECOMPANY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WELOCATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WESTARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WERESLEAVE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WERESPONS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEREPORTTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERALTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGENCYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESUMEKEYW: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESUMEPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FATHERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOUSENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECRUITED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRSKILCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRSKILDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKILLYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKILLMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOBILENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTLISTD: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDAPPLH',
		timestamps: false
	});
};
