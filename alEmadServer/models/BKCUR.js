/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKCUR', {
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CURN: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTYPCHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTYPDEP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GAINACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOSSACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUNDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'BKCUR',
		timestamps: false
	});
};
