/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPMSDEPT', {
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDDEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISEMERGNCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISFREEVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOFREEVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPACKAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENERIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SREVPATYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEMANDVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVDEPSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHWORDPRV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFPROVMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NTLDORDPHY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDCOMDEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'TMPMSDEPT',
		timestamps: false
	});
};
