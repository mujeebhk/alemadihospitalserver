/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCHOLLVR', {
		HOLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPLICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RELIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCHOLLVR',
		timestamps: false
	});
};
