/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCSMSAPP', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSSENTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMLSENTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCESSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAILSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAILSTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSSTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCSMSAPP',
		timestamps: false
	});
};
