/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSGURAND', {
		SEQUNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRINCIPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BIRTHDY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSGURAND',
		timestamps: false
	});
};
