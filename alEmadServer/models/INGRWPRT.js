/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INGRWPRT', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		GRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AGETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEADCER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LENPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEGPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEADCERCUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARMCERCUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKINFOLD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RICEPS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SITINGWTS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STANDWTS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HANDSCRAW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WALKWASST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STANDALONE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WALKALONE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HIEGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSTOLICBP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIASTOLCBP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WAIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRWCHTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INGRWPRT',
		timestamps: false
	});
};
