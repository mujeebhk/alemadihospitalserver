/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MABEDBKH', {
		BOOKDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BALANCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MABEDBKH',
		timestamps: false
	});
};
