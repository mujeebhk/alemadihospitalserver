/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPITMGRP', {
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCSETCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IVFLUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MPITMGRP',
		timestamps: false
	});
};
