/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDPATDTL', {
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPTYP1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETTYPDES: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDPATDTL',
		timestamps: false
	});
};
