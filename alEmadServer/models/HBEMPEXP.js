/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPEXP', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPANY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPEXPYRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPEXPMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTEXPYRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTEXPMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPEXP',
		timestamps: false
	});
};
