/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBDNPKGN', {
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCLUDEPKG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILCODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKEXIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASETRF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISTPACKDIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IIDPACKDIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBPACKDIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRGWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MBDNPKGN',
		timestamps: false
	});
};
