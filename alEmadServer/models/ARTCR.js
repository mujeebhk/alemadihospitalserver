/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARTCR', {
		CODEPYMTYP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXTRMITREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTRMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRMITTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCHTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRATETC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTPAYMETR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODEPAYM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPEHC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEEXCHHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRATEHC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RMITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDINVCMTCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTLSTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPAYOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERATETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPETC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTADJENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATERATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMUNAPLTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMUNAPL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRMITHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTADJHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPERBANK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPERCUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDBADJHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRADJHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDBADJTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRADJTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLYMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRENTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURNBC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILLAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLDWNLK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXAMTCTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXVERSION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXBSE1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURNRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTXCTLRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPERC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATERC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTACC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTACCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTACCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMUNAPLHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARVERSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCPREVID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCPREVSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CCTRANID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCTRANSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCESSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARTCR',
		timestamps: false
	});
};
