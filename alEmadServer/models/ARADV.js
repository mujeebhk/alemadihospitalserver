/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARADV', {
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPLRUNN: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SORTCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PAYEECDE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		UNIQCNTR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCTAKN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETAMTPD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTSTRE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPSTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURDEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARADV',
		timestamps: false
	});
};
