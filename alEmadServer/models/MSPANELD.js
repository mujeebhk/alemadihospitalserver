/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPANELD', {
		PANELCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILCODDES: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPANELD',
		timestamps: false
	});
};
