/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLWAMF', {
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSLDSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QTYSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOCSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTOFSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSRTY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MCSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTRLACCTSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCELDGID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YRACCTCLOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTFMTTD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL01: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL02: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL03: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL04: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL05: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL06: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL07: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL08: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL09: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSEGVAL10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSEGVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTGRPSCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTOSEGID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTOSGCPY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFCURNCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOVALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLWAMF',
		timestamps: false
	});
};
