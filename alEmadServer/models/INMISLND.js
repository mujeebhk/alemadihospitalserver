/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INMISLND', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ORDLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		REPORTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INMISLND',
		timestamps: false
	});
};
