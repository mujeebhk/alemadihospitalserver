/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IISPBILL', {
		INVSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMBILLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBILLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPONSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPONSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IISPBILL',
		timestamps: false
	});
};
