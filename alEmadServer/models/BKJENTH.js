/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKJENTH', {
		PSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SEQUENCENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTSRCEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTFUNCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTSRCEGRO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTFUNCGRO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BIGCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RUNID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OFXTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DSETCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BKACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BKSTMTCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DSETCODED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTSTMTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECPENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPENTREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFENTNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSCMD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGERECLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETENTNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKJENTH',
		timestamps: false
	});
};
