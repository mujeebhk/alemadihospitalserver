/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBDYNPKG', {
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DETAILNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERSUBTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILCODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SLPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERPRVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANASTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTCHRGTY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTROOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKAGID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDSTRTACT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDENACT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPLINSTR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSSTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATDEDUCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISAPP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTFAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHTDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDNOUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDCOTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDCOPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDCOAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDPATDEDU: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDCOMPRES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDPATPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDCOMPPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKAPPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKDIFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INPACKAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETRF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISDISCPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATDISCPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETURNQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERPRVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELGEXTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELGUNTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELGEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDCLSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRGWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MBDYNPKG',
		timestamps: false
	});
};
