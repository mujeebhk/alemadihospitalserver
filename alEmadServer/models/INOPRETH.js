/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INOPRETH', {
		RETUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATIONID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHARMALOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETURNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETURNTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHARMADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BIRTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INOPRETH',
		timestamps: false
	});
};
