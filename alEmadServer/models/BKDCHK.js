/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKDCHK', {
		CHECKNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATECHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATACHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORPHANCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESTART: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOGTOFILE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTROL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKDCHK',
		timestamps: false
	});
};
