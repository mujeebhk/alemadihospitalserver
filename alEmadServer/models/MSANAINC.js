/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSANAINC', {
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ANALYTNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ANACOMNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSANAINC',
		timestamps: false
	});
};
