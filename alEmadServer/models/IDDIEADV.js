/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDDIEADV', {
		DIETCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLSTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIETCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETRMKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIAGCODE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MENUID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USERDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		USERTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMENDUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMENDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMENDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIETTYPDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDDIEADV',
		timestamps: false
	});
};
