/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPRVLEA', {
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESENTDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTLEAVELI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVAILTILDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVEMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVAILEDLEA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRENTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCPRVLEA',
		timestamps: false
	});
};
