/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDINTSCH', {
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INTVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PANELCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDINTSCH',
		timestamps: false
	});
};
