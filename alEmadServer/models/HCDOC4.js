/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCDOC4', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVPAYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVPAYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVPAYBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANPAYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANPAYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANPAYBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVPAYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVPAYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVPAYBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRATPAYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRATPAYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRATPAYBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETPAYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETPAYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETPAYBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALPAYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSALPAYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALPAYBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERPROLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERPROPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERPROBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESPROCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ESPROCPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESPROCBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMITNCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMITNCPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMITNCBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMUPTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMUPTPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMUPTBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCDOC4',
		timestamps: false
	});
};
