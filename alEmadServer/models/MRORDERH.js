/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRORDERH', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDCANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHECKED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHECKTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ESCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADSCHDLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADEXPCTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADEXPCTTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UPDATESTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSHOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MRORDERH',
		timestamps: false
	});
};
