/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDMENH', {
		MNUSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUSHT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCREMENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIETFOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEEKCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMWEEK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOWEEK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MENUADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDMENH',
		timestamps: false
	});
};
