/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLORDANA', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPEMENDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEMENABBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEABBRDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NUMRESULT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLOWLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLOWHIGH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRILOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRIHIGH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALHIGH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANASTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANAOLDSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REACANCEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATEED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RUN1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RUN2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RUN3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINALRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RLXTESTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RLXTESTEXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RXTESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RXTESTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTOVALREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TSTSITELOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSTSITEDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISRLXTXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATER1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIMER1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATER2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIMER2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATER3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIMER3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EQUIPMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRITICAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABSUBSECC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSUBSECD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDOUTTST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKLSTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKLSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRITICALVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRITICALBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRITICALDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRITICALTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEINSREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRICOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESCONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CULTURE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESCONFBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESCONFDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESCONFTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEMENTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTORETEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUPPRESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRITICTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISAMENDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISREFERIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEINDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATHWAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODIFYTEST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODTESTDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCUBATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINALLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXALLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESRNDOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESULTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTTOINF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKSHTGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKLSTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COLLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COLLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESENTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESENTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESENTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RELEASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTIFIY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTIFYREM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EQUCODE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUCODE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONVUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONVFACTOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONVRMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVRMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLORDANA',
		timestamps: false
	});
};
