/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MORECDTL', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MORECDTL',
		timestamps: false
	});
};
