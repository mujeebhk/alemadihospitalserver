/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPARLOC', {
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOWREC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOWPRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFRECLOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFPROLOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPARLOC',
		timestamps: false
	});
};
