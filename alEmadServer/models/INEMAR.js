/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INEMAR', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ADMINDT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMINBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMINTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFRMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFRMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAPRDOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVEMERG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPENBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPENDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPENTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRIMADDTN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEPERHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MEDADMST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WITREQADM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WITNSBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHEDMEDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WITSDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRUGREAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OWNMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HMPRSAPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DROPFCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLOWRT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IVNURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IVNURNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSPERUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STOPALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VOLOVHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IVCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IVFLUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VOLCLN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIABETIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUSPEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURFLWCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READDISPN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCONTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INEMAR',
		timestamps: false
	});
};
