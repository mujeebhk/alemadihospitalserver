/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEDUALW', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		FRMYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELGAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAIDAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBEDUALW',
		timestamps: false
	});
};
