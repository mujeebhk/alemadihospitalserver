/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDINTPAH', {
		PANELCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHFPANELST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHFPANNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDINTPAH',
		timestamps: false
	});
};
