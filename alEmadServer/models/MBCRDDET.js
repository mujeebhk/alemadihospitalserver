/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBCRDDET', {
		CREDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEPTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCLUDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREBALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MBCRDDET',
		timestamps: false
	});
};
