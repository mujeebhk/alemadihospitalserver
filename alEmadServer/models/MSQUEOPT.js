/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSQUEOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUETYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULTSESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SESS1TIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SESS2TIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SESS3TIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRCPERD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSQUEOPT',
		timestamps: false
	});
};
