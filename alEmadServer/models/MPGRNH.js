/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPGRNH', {
		GRNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TERMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURRENCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RERATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RERATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REEXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PORATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PORATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POEXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DETDISCSUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSSTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUMADDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUMMANPRO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINESCOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTGRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVOICENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOOFGRN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBTOTALHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROTOTALHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUMCOSTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTALHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVERYN1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INDAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INPOSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INPOSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INPOSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COSTPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUMQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUMUNTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYACHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VENDCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDLCURCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADLCURRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPGRNH',
		timestamps: false
	});
};
