/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APP02', {
		RECID02: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVCBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VCHRNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWALOWDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWUSE1099: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLASLABEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTNEXTINV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWCOAWARN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPOSTPRNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWALOWIVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWEDIT1099: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SW1099COPY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDATEBUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APP02',
		timestamps: false
	});
};
