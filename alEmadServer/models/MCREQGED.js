/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCREQGED', {
		PRQSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAFEQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REORDLEVEL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPRO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYMRQIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYMRQOUT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STOCKQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXREQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PURUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APREQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCONVFACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MCREQGED',
		timestamps: false
	});
};
