/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPATPER', {
		PERMITNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELIGION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBILENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAXNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEARS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTHS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPNAME1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPIDNO1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPNAME2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPIDNO2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGERY1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGERY2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGERY3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TREATDUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVSDATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVSDATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELRMKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REGISTERD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOBYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AFIRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMIDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALASTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLACKBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLACKDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLACKLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOBLNOCMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLKREMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAILID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPATPER',
		timestamps: false
	});
};
