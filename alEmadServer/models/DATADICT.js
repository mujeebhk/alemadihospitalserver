/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DATADICT', {
		TABLENAME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TABLEDATA: {
			type: "IMAGE",
			allowNull: false
		}
	}, {
		tableName: 'DATADICT',
		timestamps: false
	});
};
