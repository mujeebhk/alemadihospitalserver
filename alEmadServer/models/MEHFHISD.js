/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHFHISD', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		COMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FATHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOTHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BROTHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SISTER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNCLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRFATHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRMOTHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATUNCLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATAUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATGMOTHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATGFATHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FAMLYRELTN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEHFHISD',
		timestamps: false
	});
};
