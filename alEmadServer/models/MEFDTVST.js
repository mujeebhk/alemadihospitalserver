/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEFDTVST', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREGNANT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREASTFEED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DURCYLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LMP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MENSESQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASMENDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTPAP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GAVIDA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABORTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PARA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NORMDEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABNORDEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABNORDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTCEP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SBEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHILDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MENOPAUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFCHLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEFDTVST',
		timestamps: false
	});
};
