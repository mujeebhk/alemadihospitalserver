/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEGENASD', {
		GENAPPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DTLGENAPCD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SBDTLGNACD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEGENASD',
		timestamps: false
	});
};
