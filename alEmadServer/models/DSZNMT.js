/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSZNMT', {
		ZONECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZONEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'DSZNMT',
		timestamps: false
	});
};
