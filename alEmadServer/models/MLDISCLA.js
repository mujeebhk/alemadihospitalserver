/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLDISCLA', {
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCLAIM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCLAIM2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCLAIM3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCLAIM4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCLAIM5: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLDISCLA',
		timestamps: false
	});
};
