/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISPOLGRP', {
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATLSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICELIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCFROM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLGRPREQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATELST2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPONSOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTRACTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISTPACKDIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IIDPACKDIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBPACKDIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATELST3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATELST4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATELST5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETWORKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARDNOLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USEHLTAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OUTLIER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARGIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPSECDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ISPOLGRP',
		timestamps: false
	});
};
