/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCALLWD', {
		EXPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEKM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEPDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SETUPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LMTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LMTEXPTYPE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRAVELTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCALLWD',
		timestamps: false
	});
};
