/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDAPPEDD', {
		APPSEQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDUCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDUCATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNIVERSITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROGRAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PASSYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDAPPEDD',
		timestamps: false
	});
};
