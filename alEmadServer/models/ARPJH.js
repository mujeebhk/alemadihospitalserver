/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARPJH', {
		TYPEBTCH: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETERM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBILLCYC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTSHP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWNONRCVBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTADJNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVCAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEDEPST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPSTNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECURNTC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPETC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCHTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPTC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATETC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURNBC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPEBC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATEBC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCHBC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPBC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATEBC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTINVCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGTERMS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRTGRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAPPLYTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NAMERMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEXTREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARPJH',
		timestamps: false
	});
};
