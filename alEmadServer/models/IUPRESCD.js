/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUPRESCD', {
		PREID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FAVORID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPENSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NSTARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTRXDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOREFILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTDEA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHRONIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PURQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NDCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFIDEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPICODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQDPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHRIZ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQFCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UINPOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALTITMCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTITMNM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTSTREN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTDOSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTFREQU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRONICID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOSEMORN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSENOON: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSEEVEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSENGT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSEOTHR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUPRESCD',
		timestamps: false
	});
};
