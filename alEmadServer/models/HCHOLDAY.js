/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCHOLDAY', {
		HOLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEEKDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISWEEKOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NATHOLIDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELIGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPORTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCHOLDAY',
		timestamps: false
	});
};
