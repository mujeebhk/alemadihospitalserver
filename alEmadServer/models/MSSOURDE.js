/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSSOURDE', {
		SPECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SITENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SOURCENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SITECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSSOURDE',
		timestamps: false
	});
};
