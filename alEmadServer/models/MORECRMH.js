/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MORECRMH', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANESLOGIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CIRCUL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COLOUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSCIOUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIVITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRADTOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VOMTNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLEEDING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRESSTAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HEMDRAIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PENROSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NGTUBE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CENTRALLN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOLEY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHOL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINSCALE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAINSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MORECRMH',
		timestamps: false
	});
};
