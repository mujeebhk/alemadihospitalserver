/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MADSPHAR', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PREDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PRETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MADSPHAR',
		timestamps: false
	});
};
