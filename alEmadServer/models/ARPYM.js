/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARPYM', {
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LONGSERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMERMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKLANG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATECLRD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATERVRSD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTRETRN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRXTYPETXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CCTRANID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARPYM',
		timestamps: false
	});
};
