/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISAPRMCL', {
		REQID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CLRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CLRTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQUEST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCOMT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISAPRMCL',
		timestamps: false
	});
};
