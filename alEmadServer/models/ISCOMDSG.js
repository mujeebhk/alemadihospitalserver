/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISCOMDSG', {
		INSUCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		SERTYP: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		SERGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISCOMDSG',
		timestamps: false
	});
};
