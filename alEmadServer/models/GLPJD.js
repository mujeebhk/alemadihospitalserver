/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLPJD', {
		POSTINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BATCHNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TRANSNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JRNLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCALYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCALPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELEDGER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDITALLOWD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSOLIDAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPANYID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JNLDTLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JNLDTLREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCURNDEC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HCURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEMTCHCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEENTRY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORIGCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWREVERSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLPJD',
		timestamps: false
	});
};
