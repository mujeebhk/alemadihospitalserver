/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORSCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORRMSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORSCPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORSCNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORPRLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORPRPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORPRNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORMGLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORMGPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORMGNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALSRCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTCRSR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRMOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIAVER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANECSR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDFLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDETSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPSURSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURCHRSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGACC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTROOMCHR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTANECHR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDCLRAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREBODY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTEBODY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTEVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTRASEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SITESEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREQUETMPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHMORDCLR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTBOOKSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTOPICKOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURCONSTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANSCONSTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANACHGPICK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANASURPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHRSETMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSTPRVPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTSURCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSTSURPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		THRDSURCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		THRDSURPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORCHGPICK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFORCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITROPSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTRAOPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTRAOPPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTRAOPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SMSSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGONPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFAULTSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOADSURANE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RRROOMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANADURMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MOOPT',
		timestamps: false
	});
};
