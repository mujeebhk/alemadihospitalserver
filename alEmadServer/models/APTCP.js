/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APTCP', {
		BATCHTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTRMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PYMTRESL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTERNDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLASTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTADJ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTADJ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDPPD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDOCMTCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CDAPPLYTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEACTVPP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJTOTDBTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJTOTCRTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPAYMTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPLYMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGTOTDBTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGTOTCRTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGTERMS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRTGRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPAYMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APTCP',
		timestamps: false
	});
};
