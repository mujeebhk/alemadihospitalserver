/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INROST', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INREVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RVSCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RVSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INROST',
		timestamps: false
	});
};
