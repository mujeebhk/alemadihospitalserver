/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCLTPRVN', {
		POSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VENDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LTPRQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCLTPRVN',
		timestamps: false
	});
};
