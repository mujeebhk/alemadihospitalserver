/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDDOC1', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PSDPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PSDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSDNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECROUNDNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ATTPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ATTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDDOC1',
		timestamps: false
	});
};
