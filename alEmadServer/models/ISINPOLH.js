/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISINPOLH', {
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPELIGIBLT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELIGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISINCLCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPALLW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYFIXOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTIBLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPELIGIB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDCTLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENERALFEE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECIALFEE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COUNSLTFEE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFERPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTINC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FIXEDPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXCOPLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXDEDLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHRMELIGIB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHRMCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHRMFIXCOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHRMCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLGRPREQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHRMCOPOTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPALLOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATELIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCFROM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCHPRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHCOPDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFTSTDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFTETDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFPOLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONEXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPONSREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPONSREQOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPONSREQIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPONSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPLIMLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSULTLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVICELIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHARMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERCAPITA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERCAPRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVGENYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVGENMONT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDSERELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENTALELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATERNELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTICLELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINCRELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDSERPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDLIMITYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENTALPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENTLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENTLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATERNPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATRLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATRLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTICLPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTCLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTICLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDCOPYREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDCOPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCOPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTCOPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCOPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATCOPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCOPYREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTCOPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCOPYREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNCOPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHANGEBED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLSTARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLICYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFPACKCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ODPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MTPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLBEPISREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHRGT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSOLIDAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCOPLTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCOPAYLMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLCKPLCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOFDYSREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NETWRKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHRMPALMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHRMOPDLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHRMDENLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHRMMATLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHRMOPTLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHRMSKNLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPLETTVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPLETTVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITLIMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPRESMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLMTON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHMCOLMTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDPHMLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTPHMLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATPHMLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTPHMLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNPHMLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DYNPOLSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NETWORDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHGTOPCKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREDISLOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISINPOLH',
		timestamps: false
	});
};
