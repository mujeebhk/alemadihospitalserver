/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DARCLMH', {
		RCLMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RCLMDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLAIMDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSCMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCMPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLCGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLCGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEMBERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		XMLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FACLICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLAINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RCLMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESUBMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESUBMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESUBMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESUBMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IMPORTEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'DARCLMH',
		timestamps: false
	});
};
