/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRORDERD', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RADCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOURCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WARDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDBYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSTSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TSTPERFORM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERFORMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERFORMTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPATPRE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERFBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTFAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDTOTEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESENTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESENTTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISRADALLOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSHOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EQUIPTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADGENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTRAST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATARRIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ARRIVEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARRIVETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARRIVEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXAMSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXAMSTARTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXAMSTARTT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TECHUSERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXAMENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXAMENDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDCANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DICTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DICTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DICTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSCDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSCTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSCBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESCORTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ESCORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDULEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCHEDULETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDENDEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDENDEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDENDEDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISODENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTEDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPBYNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALBYNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPBYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALBYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRITRESON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRITYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRITBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADROOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADROMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MRORDERD',
		timestamps: false
	});
};
