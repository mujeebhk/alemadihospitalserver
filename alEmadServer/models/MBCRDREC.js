/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBCRDREC', {
		CREDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEPTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WRTOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEPTNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COUNTERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUMINCUL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTRECAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WRTOFFACC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WRITEOFFTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MBCRDREC',
		timestamps: false
	});
};
