/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GPGLBH', {
		BSEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ESEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PSEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABATCHNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AENTRYNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPERIO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILSRCTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLDWNLK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRILAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GPGLBH',
		timestamps: false
	});
};
