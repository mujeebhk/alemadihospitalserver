/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSVSTDEP', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREVSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREEVSTAVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALFRVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPISODENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSVSTDEP',
		timestamps: false
	});
};
