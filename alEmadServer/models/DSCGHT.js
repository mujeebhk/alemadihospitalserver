/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSCGHT', {
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLORCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACUITY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACUITYDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZONECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'DSCGHT',
		timestamps: false
	});
};
