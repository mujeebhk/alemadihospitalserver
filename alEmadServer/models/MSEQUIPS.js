/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSEQUIPS', {
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MINLOAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDEALLOAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXLOAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SETBAUDRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETPARITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETDATABIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETSTOPBIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPORT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BUFFER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMSETUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLOWCNTRL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIMEOUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SYSTEMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSEQUIPS',
		timestamps: false
	});
};
