/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDPOSREQ', {
		POSREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSREQDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSREQDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOBCLSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRENTNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VACANTNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NUMREQPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASSIGNPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQUESTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSIGNBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSIGNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSDEFBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSDEFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDPOSREQ',
		timestamps: false
	});
};
