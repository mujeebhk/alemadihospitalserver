/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPRFRVH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUPERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUPERVISOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		JOBSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRADECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIEWTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVIEWDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESULT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOMMEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPVIEW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPAGREE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTMARKS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPERMARK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL1APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL1APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL1APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL2APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL2APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL2APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL3APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL3APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL3APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVAGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVAGRPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBJSAVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTOBJSCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBTOBJSCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBJECTPERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL4APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL4APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL4APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL5APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL5APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL5APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OBJWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCPRFRVH',
		timestamps: false
	});
};
