/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIMORDD', {
		ORDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMTYPECOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATSCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSEUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMTYPSHRT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROUTECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GIVENBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GIVENDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GIVENTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANREAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SITE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SITELOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INIMORDD',
		timestamps: false
	});
};
