/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPINOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PKORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MVPKORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MVPKORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MVPKORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MVPKORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MVPKORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITALLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VITALPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VITALBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VITALVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VITALSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NURSNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MLTISELTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPNURLINK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPNURFILT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEMPUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEIGHTUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEIGHTUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NSIPPORDCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NSOPPORDCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NSIPIORDCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NSOPIORDCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INREVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPORETSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPORETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPORETPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPORETBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPORETVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPORETSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPORETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPORETPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPORETBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPORETVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DETADVLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DETADVPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DETADVBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DETADVVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PNLOCYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PNQUALTYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PNCONTXTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PNMODFACYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PNSYMSGNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHOWROS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSMVALREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSMVALDUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRIAGCALC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BPSYSTOMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPSYSTOMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPDIASTMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPDIASTMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OXYMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OXYMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPIRMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPIRMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETLAVIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALWTOPRNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENIPASMOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMARSCHMN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENBKRPNSC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEUNIDOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENABLEIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BPSCRITMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPSCRITMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPDCRITMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPDCRITMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMPCRIMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMPCRIMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OXYCRITMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OXYCRITMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPCRIMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPCRIMAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENBHENRADL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENABLEPNCP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VITALMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CIRCUMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDINTVREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWSURGERY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENBGENINFM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMTMSTAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMTMREG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRMPREDTTM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IVPREDTTM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASMVALIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENBNORISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENBIPNOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMUNPED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINASMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FUNLASMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FAMLYHTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOCILHTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NUTRNLSRNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYSCOSRNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HMEXTMEDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATFMLYED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GROWTHCHP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEIGHTPDP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAPROREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDAFTRAUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANSSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUTORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUTORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FUTORDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUTORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'TMPINOPT',
		timestamps: false
	});
};
