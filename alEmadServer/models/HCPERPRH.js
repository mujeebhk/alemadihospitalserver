/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPERPRH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERPROCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPNTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMITTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMITNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCPERPRH',
		timestamps: false
	});
};
