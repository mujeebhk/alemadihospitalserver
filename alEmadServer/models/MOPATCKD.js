/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOPATCKD', {
		OPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PSTOTTALLY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MOPATCKD',
		timestamps: false
	});
};
