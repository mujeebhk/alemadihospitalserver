/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HITRNHIS', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HISTONUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NUMOFBLOCK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECSOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLOCKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NUMOFSLIDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SLIDEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMDEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMDEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REASON1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REASON2: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HITRNHIS',
		timestamps: false
	});
};
