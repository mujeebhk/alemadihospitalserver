/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCSALPRO', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		SALARYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESENTDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MOBILE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARREARS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOANAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASICAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCSALPRO',
		timestamps: false
	});
};
