/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INSBARD', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SBARDT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSMNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		T: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPO2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IVSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DTOFINSERT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IVSITECH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRESSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRAINYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATHETER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URINE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VOMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FALLRISK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINSCORE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECOMEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRTLABORDX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PENDRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AWAITLABS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NURSCONCRN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENDOSIGN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECSIGN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INSBARD',
		timestamps: false
	});
};
