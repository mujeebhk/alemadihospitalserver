/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEMPATD', {
		ATTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCEMPATD',
		timestamps: false
	});
};
