/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPPOD', {
		POSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FMTITMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FOCQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VENITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYRECEIVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYOUTSTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTEXPEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTNONSTK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRCLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICEUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ATUHQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REJECTQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORIGQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXCOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRPEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STOCKUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UOMCHNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TENDERID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPPOD',
		timestamps: false
	});
};
