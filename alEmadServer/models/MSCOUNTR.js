/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSCOUNTR', {
		COUNTRCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTINVTOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVLOCCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVLOCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTIPHRTOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHRLOCCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHRLOCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISODDHOUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FILTERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGANSPEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGANICODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISCOMNCNTR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRDPAYMODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSCOUNTR',
		timestamps: false
	});
};
