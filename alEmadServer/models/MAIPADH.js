/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAIPADH', {
		IPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPPRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BABYBORN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BABYTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BABYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BABYMOTHER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFORELES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SIGNATURE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSIGNBEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMDTFRM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMDTTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMPCFRM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMPCTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMEDS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMEDD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMSSD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADCOMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADCOMPRELA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADCOMPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCHID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCHARG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATNOTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHIFTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREDISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREDISTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALSER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PFCCOMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCOMPICD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FINCOMPICD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICPCOMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISEMRGNCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREDISBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRECNCLBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRECNCLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMCANLBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMCANLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMCANLTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBSCRIBBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPLYRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOOFDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VSTCLOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPVISITCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPVISITDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPVISITTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSFERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSFERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPADVAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMBEDCLSC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMBEDCLSD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURBEDCLSC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURBEDCLSD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDISCYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHDISCBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDISCDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHDISCTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BABYADMCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALBEDSR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHANGEBED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELIGIBBED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLICYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFPACKCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMDIAGNS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRMDIADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INJCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIAGREMARK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMPRVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INPRVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVBILCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREAUTDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKGREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKGCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKGDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISINTIMATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HL7MSGSTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECPLANYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGICALYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRGFACTOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NURSEINCHG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETWCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKINDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHECKINTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPVISITNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLNDADMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESNREADM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESNREADMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MAIPADH',
		timestamps: false
	});
};
