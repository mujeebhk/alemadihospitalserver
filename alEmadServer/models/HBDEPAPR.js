/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBDEPAPR', {
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RATING: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATINGDESC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESCRPTION: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBDEPAPR',
		timestamps: false
	});
};
