/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INORDAND', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANALYGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEMENABBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEABBRDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANASUBCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANASUBDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFAULTRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORMULAYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TESTUSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INORDAND',
		timestamps: false
	});
};
