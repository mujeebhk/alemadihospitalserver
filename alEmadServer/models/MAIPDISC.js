/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAIPDISC', {
		DISCCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISREMARK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISREMARK1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISREMARK2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMERGADV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPROV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCSETL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCSETBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCSETDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISMLC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MLCDISCHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MLCDISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MLCDISTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MLCDISTAUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HL7MSGSTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISDAT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MAIPDISC',
		timestamps: false
	});
};
