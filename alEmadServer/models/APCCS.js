/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APCCS', {
		VENDORID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CLASFCID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTYEAR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTMONTH: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTPYMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMNTCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WITHHOLDNG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMNTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WITHHLDAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'APCCS',
		timestamps: false
	});
};
