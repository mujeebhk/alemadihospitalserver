/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSDICT', {
		ENGNAME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		NAMETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REGNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSDICT',
		timestamps: false
	});
};
