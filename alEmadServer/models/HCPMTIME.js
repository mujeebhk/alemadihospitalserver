/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPMTIME', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		EARNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTSEQNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIMECDPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALPRCSD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYEXPAC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCPMTIME',
		timestamps: false
	});
};
