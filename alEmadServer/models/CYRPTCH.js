/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CYRPTCH', {
		RPTYPECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSSCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MICROCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTERPRCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLINCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEADCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GROSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLIDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CYRPTCH',
		timestamps: false
	});
};
