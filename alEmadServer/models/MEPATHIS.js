/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEPATHIS', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TABLENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELDVALUE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHANGEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHANGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHANGTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECRDTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEPATHIS',
		timestamps: false
	});
};
