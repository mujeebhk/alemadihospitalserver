/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('AROBLJP', {
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQENCE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPEBTCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEEXCHHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATERMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYMCUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDMEMOXREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'AROBLJP',
		timestamps: false
	});
};
