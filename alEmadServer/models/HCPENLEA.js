/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPENLEA', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LEAVEUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJUSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCPENLEA',
		timestamps: false
	});
};
