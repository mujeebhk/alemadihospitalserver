/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCITMSTR', {
		STRUCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OFFSET1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OFFSET2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OFFSET3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OFFSET4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATE4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OFFSET5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATE5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MCITMSTR',
		timestamps: false
	});
};
