/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPGRNACD', {
		GRNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRORATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTEXP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPRORATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COSTPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PQEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPGRNACD',
		timestamps: false
	});
};
