/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSUICST', {
		PROFILEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		UIKEY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PGMID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PGMVER: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CTRLTOHIDE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CSUICST',
		timestamps: false
	});
};
