/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCTRVCLD', {
		CLAIMSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EXPENCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLAIMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOOFDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPENSEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRKFSTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LUNCHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DINNERREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISBILLAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCTRVCLD',
		timestamps: false
	});
};
