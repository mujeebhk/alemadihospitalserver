/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBTRNDSD', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCSTEP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPRESOL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIONDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCUSSWTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTEREDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HBTRNDSD',
		timestamps: false
	});
};
