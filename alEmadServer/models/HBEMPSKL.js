/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPSKL', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CERTIFYORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MONTHSPRAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTPRACT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKILLLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPSKL',
		timestamps: false
	});
};
