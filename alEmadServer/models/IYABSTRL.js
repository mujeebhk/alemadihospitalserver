/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYABSTRL', {
		ABSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ANTBIOCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RLANTBIO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTIBIOTIC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYABSTRL',
		timestamps: false
	});
};
