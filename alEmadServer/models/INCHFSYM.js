/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INCHFSYM', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMCD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMNM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMCD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMNM2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMCD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMNM3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMCD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMNM4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INCHFSYM',
		timestamps: false
	});
};
