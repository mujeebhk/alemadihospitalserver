/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRFD', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONGSERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLYMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTJOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARRFD',
		timestamps: false
	});
};
