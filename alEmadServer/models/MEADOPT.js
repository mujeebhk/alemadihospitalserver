/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEADOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HIDCUSTPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDSRCHTIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBJNOTCAP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HPICAP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHFCMPFTXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDNURSCHC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDSUMPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDRVWALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDPAINSCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RENPASTMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDEXTTRSH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDFAMHSCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDSVCLFMH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDPATDTLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDSTYOTHR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RENPATCHRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESDRGALC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDCUREMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWALGFTXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHOBJCAPTN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDPRNSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDEMBUTT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABORDCUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDEDTSRED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SICKLEAVCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERALCST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHEXSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HIDECUREMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURANESFTX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWIDNPATS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCHLABORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BYPSMANFOL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHFOLUPTRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALDPWTBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISBPMH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHPOPFPLC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAVVTLHIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATEDUFCP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNGCHRCAP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQALWEDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTRMKSVPS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUPRNOTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMNPRESINS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKCHROPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENAWRDSTK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREACTAB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSTDIAGMRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWPATDOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWFEESHT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHWTRFOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPOSMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIAGMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTBOOKFLW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLAIMFRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDEQEHR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDEQNXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIDPATHIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEADOPT',
		timestamps: false
	});
};
