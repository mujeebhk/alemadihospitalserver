/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDSTATB', {
		ORGCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSBUGNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEWVACNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPVACNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OCCPPOSNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HDSTATB',
		timestamps: false
	});
};
