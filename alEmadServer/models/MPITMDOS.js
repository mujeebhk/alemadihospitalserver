/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPITMDOS', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DOSEUOM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSUNTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFALTUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTNOUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFAULTDSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFLTDSEIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONVFACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPITMDOS',
		timestamps: false
	});
};
