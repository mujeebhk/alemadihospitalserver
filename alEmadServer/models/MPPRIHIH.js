/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPPRIHIH', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STOCKUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DSELLUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPPRIHIH',
		timestamps: false
	});
};
