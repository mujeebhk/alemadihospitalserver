/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPORDPRD', {
		ORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OUTSTDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPORDPRD',
		timestamps: false
	});
};
