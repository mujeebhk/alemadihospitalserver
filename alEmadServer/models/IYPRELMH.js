/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYPRELMH', {
		PRESEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECIMTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABSTPANCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABSTPANDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREABBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESSTATS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISNOGROWTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOGCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CMTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALDTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCONFMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCONDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCONTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PVALDTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PVALDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PVALTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IYPRELMH',
		timestamps: false
	});
};
