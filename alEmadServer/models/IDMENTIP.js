/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDMENTIP', {
		MENUID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIP10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDMENTIP',
		timestamps: false
	});
};
