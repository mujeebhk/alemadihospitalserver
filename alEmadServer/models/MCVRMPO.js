/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCVRMPO', {
		TENDERID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		VENDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		REQLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FOCQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCVRMPO',
		timestamps: false
	});
};
