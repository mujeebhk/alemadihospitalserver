/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSCOM', {
		ORGID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR01: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR02: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR03: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR04: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONEFMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTRYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERDFSC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QTR4PERD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULTICURSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARNDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EUROCURSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HNDLCKFSC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HNDINAACCT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HNDNEXACCT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GNLSSMTHD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEGALNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CSCOM',
		timestamps: false
	});
};
