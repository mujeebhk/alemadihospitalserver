/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IACLSCDD', {
		SCHEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CLINICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMBREAKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBREAKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMBRKHR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBREAKHR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMBRKHR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBREAKHR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IACLSCDD',
		timestamps: false
	});
};
