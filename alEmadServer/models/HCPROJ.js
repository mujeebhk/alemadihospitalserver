/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPROJ', {
		PROJCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVERTMAPPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKHR1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKHR2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKHR3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKHR4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKHR5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVTMPER1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVTMPER2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVTMPER3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVTMPER4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVTMPER5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCPROJ',
		timestamps: false
	});
};
