/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('AROBL', {
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDORDERNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDNATACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTSHPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRXTYPEID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRXTYPETXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESCINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEASOF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODETERM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRATEOVRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCHRATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTXBLHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNONTXHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTXBLTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNONTXTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPAID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELSTACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTSTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTDLQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODEDLQSTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTTOTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLSTPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLSTPYST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTREMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLASTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWTAXINPUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTBASE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASE5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODESLSP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCTSASPLT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDPREPAID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YPLASTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPSTNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGOUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGOAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGOAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGTERMS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRTGRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAPPLYTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARVERSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPEBTCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTOBLJ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURNRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPERC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATERC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXRTGRPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXVERSION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXCTLRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXBSERT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDSHIPNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEFRSTBK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTRVL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OSWRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWNONRCVBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETERR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'AROBL',
		timestamps: false
	});
};
