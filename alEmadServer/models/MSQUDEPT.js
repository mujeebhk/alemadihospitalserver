/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSQUDEPT', {
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DEPARTMNT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SESSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUEUENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOCHCOUT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOWAITNG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSQUDEPT',
		timestamps: false
	});
};
