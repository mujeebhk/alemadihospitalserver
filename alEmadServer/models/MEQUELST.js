/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEQUELST', {
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DOCID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ROOMNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRSAPPTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTAPPTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWLK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREQUENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NXTQUENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UPDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PENDIAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PENDIWLK: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEQUELST',
		timestamps: false
	});
};
