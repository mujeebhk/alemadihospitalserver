/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLCMPOPT', {
		MLOPT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWORDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCORDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REJORDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PENORDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORIZ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMCOLECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMREJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTAUTHOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSEQR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYSETUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKMANSM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MBC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECOLREPM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPELABMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPERECMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BTHRESMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMAGEPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODEOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NORCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOWCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HIGHCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOWCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIGHCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLCMPOPT',
		timestamps: false
	});
};
