/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARITD', {
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEWGT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBASETAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARITD',
		timestamps: false
	});
};
