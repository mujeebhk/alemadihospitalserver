/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPDAOPT1', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLAIMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLAIMLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLAIMPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLAIMBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECLMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECLAIMLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECLAIMPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECLAIMBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCONYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLAIMOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDVER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABRPTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADRPTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSUMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSCARDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUDTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLMFORMAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFEMIRID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MERGEXTCLM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECTICD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINKLOINC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTFRMRLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOADDAYCAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOADZEROPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VCLMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RCLMACTID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMITSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NORESUBAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLMEXPNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLMEXPPNNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNVNONRSUB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WRITOFFACC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BADDBTPACC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BADDEBTACC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WRTOFSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WRITOFFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WRITOFFPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WRITOFFBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'TMPDAOPT1',
		timestamps: false
	});
};
