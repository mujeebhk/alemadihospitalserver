/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBPRFEVA', {
		PRFPRMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EVAGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVAGRPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBPRFEVA',
		timestamps: false
	});
};
