/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARR05', {
		IDR05: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMNTDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLUPDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMNTTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWCMNTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARR05',
		timestamps: false
	});
};
