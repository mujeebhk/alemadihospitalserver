/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRDC', {
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTREV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTINV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTCOGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWDISCABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARRDC',
		timestamps: false
	});
};
