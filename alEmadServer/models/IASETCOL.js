/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IASETCOL', {
		SEQNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SLOTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FLDVAL11: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMSCHED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHEDCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOADDRAPPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FUTURESCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUTDAYMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FLDVAL12: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDVAL13: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANMOBILE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MANMIDNME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MANLASTNM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSTVISI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINFUTAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EARAPPCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLOSEVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMAPSD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALSBAFCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOWTLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EDITAPPMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOCHKIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMAPPTMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IASETCOL',
		timestamps: false
	});
};
