/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHENCOU', {
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMNAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFPROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFPRODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRENCOU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFENCOU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECURITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREVENCOU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISODENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INITENCOU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPISEQNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNCHECK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUFROM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINSCLVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIABP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSBP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHTFT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHTIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHTKG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHTGR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEARTRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPIRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPERATOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEADCIRCUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRTOENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFROMENUQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTOENUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTOENDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTOENTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTOENRESN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VITALDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SYSREVIEWD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SYSNOTREVW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REASONREVW: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCUSTEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCUSTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INDEPREVW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INDEPTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBTOLDRCRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBTOLDTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASEDISCUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASEDISTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HXOBTFROM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HXOBTTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDRVWSUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDRVWTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLNORMSIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIASTAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIASUPIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSSTAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSSUPIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AXILIARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TYPHANIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OXYSAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAINSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISVITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCEXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VTLCREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMASMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALOTSIGNOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOSPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISEMERG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCENCYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLABYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOREHEAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INITVISIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUEUENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEHENCOU',
		timestamps: false
	});
};
