/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APBTA', {
		PAYMTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTENTER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTENTER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATCHSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRTDEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTLSTRMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEEXCHHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDEPNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDEPSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FUNCAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NBRERRORS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECHKTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYMFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWBTCHEDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRECHKRG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTCHKPRNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTREAPPLY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APBTA',
		timestamps: false
	});
};
