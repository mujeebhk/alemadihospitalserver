/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSKITH', {
		KITCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KITDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANUFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPECTESTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUANTITY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALTMSGQTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSKITH',
		timestamps: false
	});
};
