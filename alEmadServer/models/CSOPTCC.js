/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSOPTCC', {
		OPTFIELD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTABLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOWNULL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'CSOPTCC',
		timestamps: false
	});
};
