/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPPOH', {
		POSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ONHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMMULREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TERMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURRENCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DETDISCSUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTDNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOOFDN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBTOTALHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTALHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELRM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH1DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH1TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH1BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTH2DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH2TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH2BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTH3DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH3TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTH3BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SLOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TENDERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOADFORM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDLCURCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPPOH',
		timestamps: false
	});
};
