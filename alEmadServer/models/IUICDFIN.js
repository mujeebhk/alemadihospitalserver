/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUICDFIN', {
		MRDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDFIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMARY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROFINDIG: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUICDFIN',
		timestamps: false
	});
};
