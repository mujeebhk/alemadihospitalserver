/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSINVH', {
		INVHCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHRTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVHGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSTGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISMICRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVLCLINC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTFACREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CULTURE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENSITIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERBYPROV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECINSREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECINSPAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECINSLAB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENTOUTTST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANBESTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERABLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TURNARTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TURNINTER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVLINEMR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ONEANALYTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHECKSUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUMVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SOUSITEMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFLEXREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFEXTFAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBLINFOREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBLCLIINFO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFFACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISCTEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEINSTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESORDREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESORDTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDTMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDTMTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDTMYMD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDTMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCIENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISPANEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDPRMTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSTANDORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXFUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPTCANCOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CERTIFIED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRINTNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANALYTNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		METHODOLOY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOPPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTFORINQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATINSTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATINSTRCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUNDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TUESDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEDNESDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		THURSDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRIDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SATURDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSINVH',
		timestamps: false
	});
};
