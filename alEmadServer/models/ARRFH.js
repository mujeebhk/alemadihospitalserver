/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRFH', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATECREATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DETAILCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLYMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRENTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDBANKCA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTCA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURNCA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPECA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATECA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCHCA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPCA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATECA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPCCA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTCCA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTHCCA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDBANKCK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHECKNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LONGSERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURNCK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPECK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATECK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCHCK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPCK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATECK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPCCK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTCCK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTHCCK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAMERMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPSTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKLANG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTTCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CCSPSCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CCORIGID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCPREVID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCPREVSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CCTRANID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCTRANSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCESSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARRFH',
		timestamps: false
	});
};
