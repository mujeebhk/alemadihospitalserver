/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPOPTRPD', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MRNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PONPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DELNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPOPTRPD',
		timestamps: false
	});
};
