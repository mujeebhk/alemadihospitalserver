/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEEHRSEQ', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEEHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MEHPISEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MEREVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMINDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PASMDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATHLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHYEXSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEEHRSEQ',
		timestamps: false
	});
};
