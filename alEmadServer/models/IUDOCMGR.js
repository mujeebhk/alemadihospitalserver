/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUDOCMGR', {
		DOCUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACLITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSCRIBE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISVIEWALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUDOCMGR',
		timestamps: false
	});
};
