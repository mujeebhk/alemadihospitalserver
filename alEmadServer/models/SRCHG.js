/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SRCHG', {
		OLDCUSTOME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		NEWCUSTOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWSHORTNA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDITTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDITUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDITORGAN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'SRCHG',
		timestamps: false
	});
};
