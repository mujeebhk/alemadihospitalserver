/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MENOTOPE', {
		NOTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISACUTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISCLEAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISXRAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLOODREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLODUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPLREQUE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLODTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORBYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISFOLLOWUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISFOLLOWBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEXTFLUPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEXTFLUPTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATDISCREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCAMTAUT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPERAUT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCAUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAUTHTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAUTHCM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSCOMPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQUESTSHD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHDREQBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHDREQDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHDREQTT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHDINSTR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREAPPRREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSCMPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCMPDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSPLYCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELREAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSPLYDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVALNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EFFECTVEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REJREASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUECOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANASTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLOODTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQOTBOOK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQFRMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQFRMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQTOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BOOKEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BOKFRMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BOKTOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROOM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOM2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BOOKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURFRMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURFRMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURTOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURDURATON: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BOOKINGSTA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOOTBKSEQN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DELIVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAESYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPOSTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERVIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERFVIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPESURGRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANESTHESIA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECMENDATN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOSPITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPERIOD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDREQ: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MENOTOPE',
		timestamps: false
	});
};
