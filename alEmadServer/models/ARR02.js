/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARR02', {
		IDR02: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVCBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTIVPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTIVPFLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTIVSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTCRPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTCRPFLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTCRSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTDRPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTDRPFLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDRSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTITPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTITPFLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTITSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTRCPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTRCPFLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRTINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWALOWDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWALOWIVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWALOWIVPS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWUSEITCMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDPLYITCS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATRINVCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWMANTAX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWUSESDOCS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEXTRIPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTRIPFLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRISEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTRXPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTRXPFLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRXSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTRDPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTRDPFLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGBASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGSCHDKEY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGSCHDLNK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGLASTRUN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGADVDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWARPEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWOEPEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWXXPEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXCTLRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXRTGRPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXDTLCLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDATEBUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARR02',
		timestamps: false
	});
};
