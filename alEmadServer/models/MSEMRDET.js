/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSEMRDET', {
		ERNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATIENTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRIAGECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ARVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARVTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRGTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODARV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFCENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICEINVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLICECUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLICEREFR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OFFICERNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPANNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTPLAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTEDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERFTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERUSERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSFERDOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSFERDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNSFERSUB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERREGIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODARVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOBYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMITEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHOCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSEMRDET',
		timestamps: false
	});
};
