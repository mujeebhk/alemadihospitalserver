/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APGLREF', {
		SOURCE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		GLDEST: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEPARATOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGMENT5: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APGLREF',
		timestamps: false
	});
};
