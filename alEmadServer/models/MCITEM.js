/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCITEM', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROUPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COSTMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRICELIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHEXP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NONINVITM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESITM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALRETURN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTDURG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTDURGLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NONPATIENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REORDERQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALTITM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANUFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANUITMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINFO1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINFO2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINFO3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINFO4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FMTITMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STOCKUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYGIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LTPURCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHAITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISLAUNDRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STKUOMSUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMERGDRUG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIVEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROPERTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DPURUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DSELLUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSIGITEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MCITEM',
		timestamps: false
	});
};
