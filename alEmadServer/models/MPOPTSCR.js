/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPOPTSCR', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OREPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORENEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORETPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORETNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPOPTSCR',
		timestamps: false
	});
};
