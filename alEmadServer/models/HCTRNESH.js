/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCTRNESH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHPRONO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPIYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPIMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPIVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCTRNESH',
		timestamps: false
	});
};
