/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPRCOMH', {
		COMMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMBILCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOBILCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPCOMMSN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCOMMSN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCRDTCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCRDTCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECTALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHOWSERVTP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MISCYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANSTHYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PKGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECTQURY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMISSION: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPSELTQURY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLTPYE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TARGETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TARGTAMOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTURNOV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERTURNOV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMTURNOV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETCOMMS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MIMPAYAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMIPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TDSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TDSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETCOPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSSLCTQRY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPRCOMH',
		timestamps: false
	});
};
