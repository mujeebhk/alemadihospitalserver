/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISCONETT', {
		NETWRKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SERTYP: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSOLIDAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ISCONETT',
		timestamps: false
	});
};
