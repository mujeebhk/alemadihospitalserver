/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCOPTION', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKHOUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPHOUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTPRD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ATTPRD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ATTPRD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDDAY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDDAY2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDDAY3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVELEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECALCOMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASICCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AIRTKTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEASALDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALPAYACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALPAYAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AIRFRPAYAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINSETTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVCASHACC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRATACC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCSALANN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRACEDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRACETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DELAYTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ATTENDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALSTART: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCLEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALPROCDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALCALCTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALFIXDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVENCPRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVENAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCAIRFARE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ESTABLSHID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKROUTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCAPPRSL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTHRU1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTOPT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTHRU2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTOPT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTHRU3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTOPT3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTHRU4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTOPT4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTEGAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISASSETYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLWRNDOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RNDOFFTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RNDOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TERMSALSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVIEWAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEASALFACT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRDAUTCMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MNDVLDLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONEMRLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONANNACRU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAILYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RPLCEMPYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDPDFYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVAUTHTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AIRACURFAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANNUALACUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYLOANSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVPROCDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANWRFACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCATTEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRMULTLOAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEPLENCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVLIMYRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LVPERCWACC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LVPERCNACC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISCNTCTMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISTRNDTEDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AIRFRELGBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCLUDWKOF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELGMNTHGRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPSALTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTCALTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVNCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALFACTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEPLEASAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAILALRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SMSALRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRCALTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOBREAKHOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMMPROCDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISCMMSAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALCALTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALWBCKLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRATPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRATPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVGDYSMNTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCASHFORM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCASHPAID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNPAIDFACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNPDFACREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANNLEAFACT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEMAILALRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LNSMSALRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERTMCAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTCALWITH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUTOFFDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCOPTION',
		timestamps: false
	});
};
