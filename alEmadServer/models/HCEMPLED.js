/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEMPLED', {
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOOFDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISCARRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINCONLEAV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXCONLEAV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTLEAVELI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVAILTILDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORIZED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH11: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONTH12: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVEMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVAILEDLEA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANDWEEK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVAILNOBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARRIEDLEA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITLEA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOOFINSTAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOBALELIGI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEEKOFFCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOLDAYSCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSTALMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARRYDONE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISANNLEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISMATLEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROJLEAVES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROJDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROJUTILI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROJBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROJCANCEL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROJACCBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCASHLEV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCASHAVL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CARFRWDLEV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCESSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTCLOSEBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANDUTILIZ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCEMPLED',
		timestamps: false
	});
};
