/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSGWQT', {
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUENO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'DSGWQT',
		timestamps: false
	});
};
