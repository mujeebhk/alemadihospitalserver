/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEPMEDD', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ADMINDT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPENSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSUEDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVEMERG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMINTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IVFLUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIABETIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREREMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHARREMK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMARREMARK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFRMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFRMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHEDMEDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEPMEDD',
		timestamps: false
	});
};
