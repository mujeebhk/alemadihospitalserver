/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSANPRNG', {
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRNTREF1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRNTREF2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRNTREF3: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSANPRNG',
		timestamps: false
	});
};
