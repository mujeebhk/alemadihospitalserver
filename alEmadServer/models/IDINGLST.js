/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDINGLST', {
		RECIPEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INGRDINTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVISIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALQUANT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IDINGLST',
		timestamps: false
	});
};
