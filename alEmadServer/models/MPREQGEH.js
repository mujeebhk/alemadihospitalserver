/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPREQGEH', {
		PRQSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RUNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREQFOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROLLIMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCNAME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VENDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPREQGEH',
		timestamps: false
	});
};
