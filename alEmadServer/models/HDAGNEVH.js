/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDAGNEVH', {
		AGNEVLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGENCYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGENCYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATINGDESC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBTSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGENINAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDAGNEVH',
		timestamps: false
	});
};
