/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAREFRLH', {
		DISCHSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTEREXTER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MAREFRLH',
		timestamps: false
	});
};
