/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCADJSTD', {
		ADJSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		WORKDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOURSWORK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCASHYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOLDAYOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WKOFFOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCADJSTD',
		timestamps: false
	});
};
