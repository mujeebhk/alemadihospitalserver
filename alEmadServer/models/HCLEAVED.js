/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEAVED', {
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EMPTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELIGIBLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STARTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCASHPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CARYFWDPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXFWDDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CARRYFWDTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WITHINDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFINSTAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOBALELIGI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEEKOFFCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOLDAYSCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELIGIBDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISMINSERV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YRSSER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ONCEINLFTM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPROBATON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTENDPROB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALENDDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESETBALYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESETCLBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MNALWUNPAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANDUTILIZ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLEAVED',
		timestamps: false
	});
};
