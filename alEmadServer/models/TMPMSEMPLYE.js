/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPMSEMPLYE', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANESTHS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGEON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOHOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAILID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESIGNA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LICENSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLIA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDICAID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UPIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHAMPUS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMC_ID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMC_SITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMC_DESG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMC_PRTY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECLTY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEC_LIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PICTURE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NURYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABTSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADTSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINPROCYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHEMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHELOYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANADMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINKCLINC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVIDERTP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRVTYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESCR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUEUEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUEUENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCTGIVEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GIVINCENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISGENERIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPELTYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCNTIVGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VEWSENSRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISINFYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISNURYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHYSIOTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRNTOFFS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOUSEKEEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSURANCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHARGEPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EQPCHRGPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGSRVPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRTPROF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUALFIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARDESIG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARQUALFIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHARMCYLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVETRYLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'TMPMSEMPLYE',
		timestamps: false
	});
};
