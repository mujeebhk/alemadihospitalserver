/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCTMPTYP', {
		EMPTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEMPID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MINVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MEDVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCTMPTYP',
		timestamps: false
	});
};
