/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKJTRAND', {
		PSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDREMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEREMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BTCHNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYORID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYORNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDORNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCEAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCTOVR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DDLINK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECSTATCHG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECCLEARED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECONCILED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECPENDING: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECERR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECERRPEND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEXGAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEXLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECSUGGEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOUTSTND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTARGET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWOSUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURFUNC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURSTMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUMAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECSPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPOUTSTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPWO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPERR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPGAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPFUNAM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPORIG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTBOOK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPBOOK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTREMAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECPREMAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECRWOSUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECRCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWOSUMR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCLRR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHKFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OFXTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECDELTAF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FSCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCEDOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANREVINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTRYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECDELTA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKJTRAND',
		timestamps: false
	});
};
