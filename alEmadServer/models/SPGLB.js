/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SPGLB', {
		OPTION: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDITLOGSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIVATION: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTINGS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUDITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDITTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDITUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDITORGAN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'SPGLB',
		timestamps: false
	});
};
