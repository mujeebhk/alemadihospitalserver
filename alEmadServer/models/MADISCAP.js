/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MADISCAP', {
		PROVIDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT7: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT7: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHKDEFAULT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CAPTION9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION11: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION12: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION13: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION14: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPTION15: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORT9: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDAT9: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT10: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDT10: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT11: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDT11: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT12: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDT12: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT13: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDT13: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT14: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDT14: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORT15: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTMANDT15: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MADISCAP',
		timestamps: false
	});
};
