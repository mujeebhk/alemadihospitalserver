/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APAGED', {
		AGESEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		RECORDNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ADJNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		RECTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRXTYPETXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRXTYPEID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPYSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDMEMOXREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTINVCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAIDTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAIDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE6TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE6HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE7TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE7HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE8TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE8HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE9TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE9HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTBKWDTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTBKWDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTFWDTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTFWDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALDUET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALDUEH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWNONRCVBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SORTVALUE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTVALUE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTVALUE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTVALUE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTTYPE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTTYPE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTTYPE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTTYPE4: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APAGED',
		timestamps: false
	});
};
