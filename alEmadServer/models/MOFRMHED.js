/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOFRMHED', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FORMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEIGHT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BPSYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BPDIA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HGB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POTASSIUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ECG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ECGDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PLAN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MOFRMHED',
		timestamps: false
	});
};
