/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LNKUSRCMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENTDEPTCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MATRDEPTCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTCDEPTCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKINDEPTCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DENTDEPTDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MATRDEPTDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTCDEPTDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKINDEPTDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATINJNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVNOLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVNOPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVNOBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVNOVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DFTDUROP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DFTDURIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVGENOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOADPENDBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CMBNOPPHRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVRNOLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVRNOPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVRNOBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVRNOVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAPITAREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INUAPSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INUNAPPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INUNAPPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INUNAPPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INUNAPPVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INAPDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INAPPDCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INAPPDCPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INAPPDCBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INAPPDCVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJRLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJRPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJRBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJRVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJACCREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGCODEREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVENUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLDATMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LBLNKEMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCBILLAMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJINCRAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENSTMTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTCAPTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MSGFRAPPRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ISOPT',
		timestamps: false
	});
};
