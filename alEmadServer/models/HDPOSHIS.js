/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDPOSHIS', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSBUDNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEWVACNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPVACNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OCCPPOSNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HDPOSHIS',
		timestamps: false
	});
};
