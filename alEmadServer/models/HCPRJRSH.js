/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPRJRSH', {
		SCHEDNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCPRJRSH',
		timestamps: false
	});
};
