/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSRATEHS', {
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ODDHOUR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		PATYPALL: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		BEDTYPALL: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UPTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARKUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKUPDIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARKUPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARKDOWN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKDWDIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARKDWAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STNDPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARKPICK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARKAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROUNDOF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSRATEHS',
		timestamps: false
	});
};
