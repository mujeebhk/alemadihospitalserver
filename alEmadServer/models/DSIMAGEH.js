/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSIMAGEH', {
		FALRIANO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IMAGEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMAGENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMAGESIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'DSIMAGEH',
		timestamps: false
	});
};
