/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPGENRIC', {
		GENRICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENRICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISAUTOMATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSESUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROUTESUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MPGENRIC',
		timestamps: false
	});
};
