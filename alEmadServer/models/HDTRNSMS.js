/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTRNSMS', {
		TRNSCHSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNINGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNSTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SNDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAIL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAILSNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDTRNSMS',
		timestamps: false
	});
};
