/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDINTSPD', {
		PANELCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SUBPANELCD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANELDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTVEWDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROUNDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDINTSPD',
		timestamps: false
	});
};
