/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISRATPRH', {
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDCLASS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLASSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISRATPRH',
		timestamps: false
	});
};
