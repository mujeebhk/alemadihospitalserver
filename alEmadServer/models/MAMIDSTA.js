/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAMIDSTA', {
		SDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRSINCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRSOUCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADBEDCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRELCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCHCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMCNCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPENBALANC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MAMIDSTA',
		timestamps: false
	});
};
