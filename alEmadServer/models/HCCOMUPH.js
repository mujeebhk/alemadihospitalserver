/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCCOMUPH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EFCTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCBYVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORIZED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMBLKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMBLKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVAMOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCCOMUPH',
		timestamps: false
	});
};
