/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCFINHED', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL1YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL1ID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL1DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL2YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL2ID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL2DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALEAR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALDED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTSALARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTLOAN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTEOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTLEAVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SETTLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESIGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOTICEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FORFEITPEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FORFEITEFS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCFINHED',
		timestamps: false
	});
};
