/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDPANTOT', {
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EMPID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INTVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PANELCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIRSTSCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECONDSCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		THIRDSCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOURTHSCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALSCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDPANTOT',
		timestamps: false
	});
};
