/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPKGAPP', {
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPKGAPP',
		timestamps: false
	});
};
