/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSANALYH', {
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEMENTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEMENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTSPECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTWORKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STWORKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HL7CODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VOLUME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPEMENUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESUNITS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOWLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLOWHIGH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFRECLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFPROLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELCHKREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DELTACHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DELTAUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESRNDOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFLSTDATA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CULTURE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANASHTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICROSCOPY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONVUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONVFACTOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLCONDMND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOOTERSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		METHODOLOY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOPPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMPRLOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMPRHIGH: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSANALYH',
		timestamps: false
	});
};
