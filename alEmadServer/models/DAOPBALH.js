/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DAOPBALH', {
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIVERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMPORTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATSHARE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FACLICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUNTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCEDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMIRATEID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'DAOPBALH',
		timestamps: false
	});
};
