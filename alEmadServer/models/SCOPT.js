/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SCOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CSTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CSTNOLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CSTNOPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CSTNOBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CSTNOVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'SCOPT',
		timestamps: false
	});
};
