/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARCSP', {
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDCUSTSHPT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTIV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAMELOCN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPSTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETERR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXSTTS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHIPVIA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPCLINST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCTSASPLT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRICLIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FOB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPVIACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPVIADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCATION: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARCSP',
		timestamps: false
	});
};
