/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLRVAL', {
		RVALID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETCHGSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTGAIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTLOSS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RACCTGAIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RACCTLOSS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLRVAL',
		timestamps: false
	});
};
