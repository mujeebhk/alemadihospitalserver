/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRTB', {
		CODETERM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PCTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NUMDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DUEDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARRTB',
		timestamps: false
	});
};
