/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLACGRP', {
		ACCTGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTGRPDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPCOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLACGRP',
		timestamps: false
	});
};
