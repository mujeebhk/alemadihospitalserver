/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKRSTRT', {
		COMPANY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PROCESS: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMETERS: {
			type: "BINARY",
			allowNull: false
		},
		MESSAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'BKRSTRT',
		timestamps: false
	});
};
