/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MECHFCMD', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHFCOMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ONSETDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ONSETVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTERVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEVERITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOOTHFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRNIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MECHFCMD',
		timestamps: false
	});
};
