/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSINVRXR', {
		INVHCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESULTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAGTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUALRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RXTESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSINVRXR',
		timestamps: false
	});
};
