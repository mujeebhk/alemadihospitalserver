/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MELABCRI', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIEWTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRITICTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MELABCRI',
		timestamps: false
	});
};
