/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKACCT', {
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULTICUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURNSTMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BKACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTERR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRSPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTMNTND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECFP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECLSTFY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECLSTFP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECLSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECLSTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECSTMTDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECSTMTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECINTRANS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOUTSTND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECBKENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDEPOSIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECCHECK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCDEP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCCHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCDEPIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCCHKOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECRECALC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECLSMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCBKENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECFCENTRE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTCCC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCCSPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXSPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWTERR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWTWO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWTGAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWTLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWTCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWTCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWTFUNAM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWPERR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWPWO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWPGAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWPLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWPCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWPCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWPFUNAM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDTERR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDTWO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDTGAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDTLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDTCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDTCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDTFUNAM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDPERR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDPWO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDPGAIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDPLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDPCCC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDPCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDPFUNAM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECWFCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDFCLR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNWTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNWPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNDTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNDPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODETXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXVCLSS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXVCLSS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXVCLSS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXVCLSS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXVCLSS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEPURGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTPOSTDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTSTMTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECWEXDIFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECDEXDIFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'BKACCT',
		timestamps: false
	});
};
