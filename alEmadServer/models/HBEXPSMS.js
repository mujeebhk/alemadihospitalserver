/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEXPSMS', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DEPTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDCARDNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSSENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SMSSENTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSSENTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAILSNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAILSNTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAILSNTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RELATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDDATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSSENT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SMSSENTDT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSSENTTM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAILSNT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAILSTDT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAILSTTM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HBEXPSMS',
		timestamps: false
	});
};
