/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLTRNKIT', {
		KITDOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISSUEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISSUEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		KITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KITDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUANTITY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISSUELOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISSULOCDSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RUNTESTNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPECTESTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLTRNKIT',
		timestamps: false
	});
};
