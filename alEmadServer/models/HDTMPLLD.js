/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTMPLLD', {
		POSTMPLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LANGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LANGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		READ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WRITE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEAK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDTMPLLD',
		timestamps: false
	});
};
