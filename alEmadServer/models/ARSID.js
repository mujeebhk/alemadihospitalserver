/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSID', {
		IDSTDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPRIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOGS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTXBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTOTTX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWMANLTAX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXSTTS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTREV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDJOBPROJ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTINVCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWDISCABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRTSTMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDACCTINV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTCOGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARSID',
		timestamps: false
	});
};
