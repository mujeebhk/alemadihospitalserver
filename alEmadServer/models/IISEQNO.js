/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IISEQNO', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLADJSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTMTCHGNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IISEQNO',
		timestamps: false
	});
};
