/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSSKTB', {
		SCHEDKEY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTERVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEEKDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTHDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEEK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDFSUN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDFMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDFTUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDFWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDFTHU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDFFRI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDFSAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTIVEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMINDLEAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USERMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CSSKTB',
		timestamps: false
	});
};
