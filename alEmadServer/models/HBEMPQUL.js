/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPQUL', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUALIFY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUALTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YEARPASS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSTITUTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMPERIOD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		T0PERIOD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERCENTAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDUCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPQUL',
		timestamps: false
	});
};
