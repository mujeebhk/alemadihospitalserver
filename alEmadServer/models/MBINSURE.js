/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBINSURE', {
		ENTITLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOCUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOCUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOCUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPOLGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPOLGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPOLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MBINSURE',
		timestamps: false
	});
};
