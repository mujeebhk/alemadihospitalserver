/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDSMSAPP', {
		SERIAL: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SMSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MIDDLENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTVEWDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTVEWTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCESSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAILSNTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPMOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPSMSSNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPMAILSNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMLPROSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDSMSAPP',
		timestamps: false
	});
};
