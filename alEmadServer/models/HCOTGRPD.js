/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCOTGRPD', {
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EARNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKINGPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEEKOFFPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOLIDAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCOTGRPD',
		timestamps: false
	});
};
