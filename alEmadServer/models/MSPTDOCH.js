/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPTDOCH', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUTONO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCTITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELDOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILESIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRONAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPTDOCH',
		timestamps: false
	});
};
