/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLPERR', {
		POSTINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BATCHNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TRANSNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ERRORCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MSGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRBATCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRENTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRTRANS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'GLPERR',
		timestamps: false
	});
};
