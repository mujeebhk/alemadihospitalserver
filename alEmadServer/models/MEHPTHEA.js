/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHPTHEA', {
		PATHLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLICARCOND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCARCONDSC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLICARPARM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCARPARDSC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLICARTASK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CCARTSKDSC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOMDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTRECODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMINDADV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMINDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEHPTHEA',
		timestamps: false
	});
};
