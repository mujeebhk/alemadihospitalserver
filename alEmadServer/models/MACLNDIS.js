/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MACLNDIS', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIABP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSBP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHTFT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHTIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHTKG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHTGR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEARTRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPIRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPERATOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEADCIRCUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DECDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DECTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MACLNDIS',
		timestamps: false
	});
};
