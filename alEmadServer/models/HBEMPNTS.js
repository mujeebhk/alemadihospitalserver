/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPNTS', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ENRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPNTS',
		timestamps: false
	});
};
