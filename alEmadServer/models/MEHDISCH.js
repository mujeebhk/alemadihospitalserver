/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHDISCH', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISREMARK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPROV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPRVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPROVDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHOBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHONAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHOBYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEHDISCH',
		timestamps: false
	});
};
