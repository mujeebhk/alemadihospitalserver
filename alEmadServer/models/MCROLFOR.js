/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCROLFOR', {
		ROLCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SSLFORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MINFORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXFORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROLFORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEADTIME: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MCROLFOR',
		timestamps: false
	});
};
