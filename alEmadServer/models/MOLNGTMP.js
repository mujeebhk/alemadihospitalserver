/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOLNGTMP', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		COMMENTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG4: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MOLNGTMP',
		timestamps: false
	});
};
