/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSINVSAM', {
		INVHCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SPEABBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEABBRDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEMENTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQVOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPESOURMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPESOUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEHANDET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECINSREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECINSPAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLCOLLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLALLOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECLOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECALLOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TSTSITELOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSTSITALOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPESITEMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOUSITEMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEMOPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECODTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECANRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ONHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STORTRACOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABREVATION: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSINVSAM',
		timestamps: false
	});
};
