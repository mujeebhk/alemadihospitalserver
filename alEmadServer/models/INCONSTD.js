/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INCONSTD', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSENTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSENTBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHERSNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHERSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INCONSTD',
		timestamps: false
	});
};
