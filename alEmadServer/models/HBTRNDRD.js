/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBTRNDRD', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINALRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTEREDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HBTRNDRD',
		timestamps: false
	});
};
