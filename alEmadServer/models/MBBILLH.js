/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBBILLH', {
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERSUBTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKORGTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKAPPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKDIFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKFROMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKTODT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILCODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERPRVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANASTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTCHRGTY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTROOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKAGID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLINSTR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRVNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROMNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANENETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHYNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTUALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMNETPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECTINS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDNOUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDCOTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDCOPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDCOAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDPATDEDU: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDCOMPRES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATDISCPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGPATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGCMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMNGVN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPLDISBAS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPLDISPAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVALNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MBBILLH',
		timestamps: false
	});
};
