/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DACLMBHD', {
		CLAIMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPHARMACY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCLUDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLCYDTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'DACLMBHD',
		timestamps: false
	});
};
