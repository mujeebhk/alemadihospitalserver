/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEASLH', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AIRTKTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEASALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVEDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALLEAVES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALLEASAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALAIRTKT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAACCDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCLEASLH',
		timestamps: false
	});
};
