/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIPERINV', {
		INVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TONO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOPATIENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCENTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCENTTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IIPERINV',
		timestamps: false
	});
};
