/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEIVFLCG', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VOLUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IMPLTD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHNGDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHNGDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHNGDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEIVFLCG',
		timestamps: false
	});
};
