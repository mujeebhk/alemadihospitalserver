/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APIBC', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BTCHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTINVCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTENTR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BTCHTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BTCHSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTLSTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NBRERRORS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTELSTEDIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWICT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APIBC',
		timestamps: false
	});
};
