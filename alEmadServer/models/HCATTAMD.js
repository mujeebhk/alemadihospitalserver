/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCATTAMD', {
		EMPID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ATTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ENTRYNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTINTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTOUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTOUTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODINTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODOUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODOUTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCATTAMD',
		timestamps: false
	});
};
