/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPTPLCY', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDCTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURPRINS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANNUALDET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURETYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSUREREL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBSCRIBBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPLYRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSUIDPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPPATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLOYEEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPMRNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDEXPRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPBASIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETWORKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NETWORK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECPLANYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SINSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SINSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SSTARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPENDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECCARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECDEPNDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECSUBBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECEMPLYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECEMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPTPLCY',
		timestamps: false
	});
};
