/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSTRUN', {
		STMTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWFINISH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATECUTOFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWINVCDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDEBIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCREDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWZEROBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWINCLPAID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDETAIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTYPERUN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDTLSRTBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDDUNNING: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDFROM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTO1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INDEX1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDFROM2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTO2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INDEX2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDFROM3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTO3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INDEX3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDFROM4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTO4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INDEX4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTINDEX1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTINDEX2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTINDEX3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTINDEX4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEPERIOD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEPERIOD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEPERIOD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEPERIOD4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWOVERDUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERDUEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STMTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARSTRUN',
		timestamps: false
	});
};
