/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSDIGNOS', {
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRDCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMDIGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTMOHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALDTXML: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRNICYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSDIGNOS',
		timestamps: false
	});
};
