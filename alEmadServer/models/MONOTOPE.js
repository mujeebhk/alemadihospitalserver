/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MONOTOPE', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISACUTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISCLEAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISXRAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLOODREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLODUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPLREQUE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLODTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORBYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MONOTOPE',
		timestamps: false
	});
};
