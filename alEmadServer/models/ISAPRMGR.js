/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISAPRMGR', {
		REQID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS2: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISAPRMGR',
		timestamps: false
	});
};
