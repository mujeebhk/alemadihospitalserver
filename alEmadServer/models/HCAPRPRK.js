/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCAPRPRK', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PERKID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PMONAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NMONAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYERAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NYERAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCAPRPRK',
		timestamps: false
	});
};
