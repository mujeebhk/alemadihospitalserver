/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TXAUDD', {
		SEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ITEMCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESCRIPTIO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SBASEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HBASEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCURNTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HCURNTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TBASEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCURNTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'TXAUDD',
		timestamps: false
	});
};
