/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APCLX', {
		CLASSID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLASSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWWITHH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WITHHOLDG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXSTAT1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTAT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTAT3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTAT4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTAT5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'APCLX',
		timestamps: false
	});
};
