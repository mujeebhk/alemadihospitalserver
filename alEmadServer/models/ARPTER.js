/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARPTER', {
		CODEPAYM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTBATCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ERRORCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MSGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRCODEPYM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRBATCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ERRITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ERRLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ERRSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARPTER',
		timestamps: false
	});
};
