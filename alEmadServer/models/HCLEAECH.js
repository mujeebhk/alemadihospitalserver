/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEAECH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCASHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVELYN1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVELYN2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL1APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL1APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL1APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL2APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL2APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL2APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALPROCESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECIPTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCSHLEAVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AIRFARE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCASHTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCASHPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLEAECH',
		timestamps: false
	});
};
