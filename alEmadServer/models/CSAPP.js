/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSAPP', {
		SELECTOR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SEQUENCE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PGMID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PGMVER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATALEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'CSAPP',
		timestamps: false
	});
};
