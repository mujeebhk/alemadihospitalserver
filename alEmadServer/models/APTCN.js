/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APTCN', {
		BATCHTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTRMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDISTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXBSE1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALL5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXREC5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXP5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APTCN',
		timestamps: false
	});
};
