/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLPAMF', {
		KEY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRBRKID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRBRKID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECTBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRGRPSCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOGRPSCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDSEG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFCTRL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DFMCSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCLEXIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRNEW1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW7: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW9: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRNEW10: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRSEGVAL10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSEGVAL10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFSEGVAL10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRORDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRORDER: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLPAMF',
		timestamps: false
	});
};
