/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APSLD', {
		IDSELECT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDVENDOR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APSLD',
		timestamps: false
	});
};
