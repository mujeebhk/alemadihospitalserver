/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPROVCH', {
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVGPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVCODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPCOMMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPCOMMPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCOMMAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCOMMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCOMMPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCOMMAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCRDCOMTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPCRDCOPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCRDCOAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCRDCOMTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCRDCOPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCRDCOAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECTALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLCASHBED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLCRDBED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCENTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TARGETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERCENTAG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MIMGRTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMSPERT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPLTPYE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPROVCH',
		timestamps: false
	});
};
