/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBDOCHIS', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		NIDCARDNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OEXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OIDCARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBDOCHIS',
		timestamps: false
	});
};
