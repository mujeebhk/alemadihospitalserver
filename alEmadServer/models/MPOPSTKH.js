/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPOPSTKH', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLSDS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLSDS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLSDS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLSDS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLSDS5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPOPSTKH',
		timestamps: false
	});
};
