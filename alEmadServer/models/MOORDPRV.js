/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOORDPRV', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERPRVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISDISCPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATDISCPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDPRVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSTPRVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSTPRVPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVIDTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MOORDPRV',
		timestamps: false
	});
};
