/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IASTAFFD', {
		CLINICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ROOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		NURSECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NURSEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELREAS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IASTAFFD',
		timestamps: false
	});
};
