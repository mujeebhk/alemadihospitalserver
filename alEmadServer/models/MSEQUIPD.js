/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSEQUIPD', {
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MACPARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MACPARNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACTOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRECISION: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSEQUIPD',
		timestamps: false
	});
};
