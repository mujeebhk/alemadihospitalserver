/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISCLMRCD', {
		UNIQUEID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIPTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ISCLMRCD',
		timestamps: false
	});
};
