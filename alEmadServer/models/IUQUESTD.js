/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUQUESTD', {
		QSTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ENTRYID: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESCRIPQUE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUQUESTD',
		timestamps: false
	});
};
