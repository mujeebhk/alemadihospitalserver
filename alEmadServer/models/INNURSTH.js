/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INNURSTH', {
		NURSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NURSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STORETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STORECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHARMLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHARMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STOREDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INNURSTH',
		timestamps: false
	});
};
