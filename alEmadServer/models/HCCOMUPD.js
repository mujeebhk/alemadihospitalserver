/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCCOMUPD', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCBYVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEWVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTMODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCCOMUPD',
		timestamps: false
	});
};
