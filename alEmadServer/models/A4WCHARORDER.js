/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('A4WCHARORDER', {
		C: {
			type: DataTypes.CHAR,
			allowNull: true
		}
	}, {
		tableName: 'A4WCHARORDER',
		timestamps: false
	});
};
