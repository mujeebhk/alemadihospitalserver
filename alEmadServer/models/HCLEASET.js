/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEASET', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALRECEIPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVSALNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEASALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCASHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCASHAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLEASET',
		timestamps: false
	});
};
