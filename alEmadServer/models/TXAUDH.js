/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TXAUDH', {
		SEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FISCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTVEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNUMBER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BUYERCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESCRIPTIO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISFOREIGN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTVENDNM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SDECIMAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SINVAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HINVAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVERABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPSEPARTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATERECOV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRECOVRAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HRECOVRAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TDECIMAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRECOVRAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'TXAUDH',
		timestamps: false
	});
};
