/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISINVIP', {
		INVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISODENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSSTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCKED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLIEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNPOSTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNPOSTADJ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTADJAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPLIEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ECLAIMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVALNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VARIFIEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WRTOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ISINVIP',
		timestamps: false
	});
};
