/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPPJH', {
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECRETNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUNCTCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PONO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SONO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCPACMO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCPACEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPPJH',
		timestamps: false
	});
};
