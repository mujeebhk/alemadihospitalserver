/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISNETWH', {
		NETWRKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETWORDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATLSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATELST2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATELST3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATELST4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATELST5: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISNETWH',
		timestamps: false
	});
};
