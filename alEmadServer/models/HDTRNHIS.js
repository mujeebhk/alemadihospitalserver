/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTRNHIS', {
		TRNHISSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNINGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNINGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNGFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNGTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMHOUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOHOUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSIGNED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSIGNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCHEDULED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHEDULEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HDTRNHIS',
		timestamps: false
	});
};
