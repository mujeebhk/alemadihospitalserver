/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPTRFD', {
		STKTRFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STKTRFLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYREQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYAUTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOMREQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYTRF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOMTRF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOMREC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYOUTSTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALOPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNIAVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTAVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LANDEDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LINETOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MREQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROPERTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPTRFD',
		timestamps: false
	});
};
