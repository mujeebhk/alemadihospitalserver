/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPDACLMVD', {
		CLAIMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMIRATEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUNTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOOTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LICENSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCLUDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCENDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEXTRESULT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NONTEXTRLT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFFRMSOUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFTOSOUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISDCOD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CYTORSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HISRSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MICRRSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADRSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSUMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARD1YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARD2YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARDSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CARDEXDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNAME1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNAME2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ODPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCOPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITID2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPPAYAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MEMBERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUSTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUEDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHFCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTATTCHYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDVLDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIMPLNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPENDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDPRVLIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REJECTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDPAYER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESUBYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLAIMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLAINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDCLMSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OREJECTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECEIVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORECVAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMPCOMPPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SECBILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECLMEXPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECLEXPPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIFFADJAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BADDEBTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WRITOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WRTOFFPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'TMPDACLMVD',
		timestamps: false
	});
};
