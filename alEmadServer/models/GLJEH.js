/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLJEH', {
		BATCHID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BTCHENTRY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELEDGER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCSYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCSPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWEDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWREVERSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		JRNLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JRNLDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		JRNLCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		JRNLQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEENTRY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRILSRCTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLDWNLK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRILAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRENTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORIGCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DETAILCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'GLJEH',
		timestamps: false
	});
};
