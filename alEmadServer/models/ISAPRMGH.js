/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISAPRMGH', {
		REQID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		READYTOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLOSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQINFO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQCREBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQCREDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQCRETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		READYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READYTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ISAPRMGH',
		timestamps: false
	});
};
