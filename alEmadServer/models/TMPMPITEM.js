/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPMPITEM', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROUPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COSTMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRICELIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHEXP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NONINVITM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESITM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UDITEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTDURG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTDURGLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NONPATIENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REORDERQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALTITM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANUFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANUITMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINFO1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINFO2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINFO3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMINFO4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FMTITMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STOCKUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYGIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LTPURCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CALDOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IVFLUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQUIDOSEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQUIDOSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIDOSEUN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LINKITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LINKDISUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXIMDDOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXDURATIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOSEVALID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LNKDOSUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DSELLUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STKUOMSUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMERGDRUG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STOCONCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROPERTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOSESUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOSECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DPURUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSESUMIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROUTECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENRICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RXXMLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTSHOWDES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WITREQADM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TSTREQUD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITRNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STRUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSULIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ONETMDSP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NONSTKMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREGSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'TMPMPITEM',
		timestamps: false
	});
};
