/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDMENITM', {
		MENUID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MENUITEMID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INGRDTQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVINGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INGMSRQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INGMSRID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INGRDTUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IDMENITM',
		timestamps: false
	});
};
