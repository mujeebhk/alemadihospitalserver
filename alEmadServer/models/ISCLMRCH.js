/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISCLMRCH', {
		UNIQUEID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECIPTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHEQUENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIPTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECIPTTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIPTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ISCLMRCH',
		timestamps: false
	});
};
