/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLJAL', {
		KEY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESEGSEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESEG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATETRANS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOCATEBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QTYFRYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYTOYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYFRPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYTOPERD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSCMD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLJAL',
		timestamps: false
	});
};
