/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSVSTTYP', {
		VSTTYCOD: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VSTTYSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VSTCLR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLCDNOTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFPROVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSLATYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGERY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERPATVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSVSTTYP',
		timestamps: false
	});
};
