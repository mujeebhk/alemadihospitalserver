/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSINVANA', {
		INVHCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANALYGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEMENABBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANASUBCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFAULTRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORMULAYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFRESULT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSINVANA',
		timestamps: false
	});
};
