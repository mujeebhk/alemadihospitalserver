/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISEMPLYD', {
		EMPLRCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EFRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ETODT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISEMPLYD',
		timestamps: false
	});
};
