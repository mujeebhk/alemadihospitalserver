/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEMPATT', {
		EMPID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ATTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ENTRYNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACESSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINUTES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPOUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPOUTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMINUTES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTINTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIFFTIME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TERMINALIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TERMINALOT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPFROMTM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPTOTM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPFROMTM2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPTOTM2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPFROMTM3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPTOTM3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRKHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRKMINUTES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCEMPATT',
		timestamps: false
	});
};
