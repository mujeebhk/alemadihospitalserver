/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCOTGRPH', {
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NORWORKED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEEKWORKED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOLWORKED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WORKRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEEKRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOLRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTWEEKS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWKHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTMONTHS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCOTGRPH',
		timestamps: false
	});
};
