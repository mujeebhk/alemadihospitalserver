/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUICDPRO', {
		MRDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDPROV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMARY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRODIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUICDPRO',
		timestamps: false
	});
};
