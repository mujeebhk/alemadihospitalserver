/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHSOHIS', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSMOKE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFCIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SINCWENS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOWQUITS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SINCWENSQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DODRUG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPEDRUG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRUGFREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOALCOHOL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALCOFREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOWQUITA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SINCWENAQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LIVWITHFAM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAYOTHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEEDAID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTSURGRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESCRIBE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCOFWORK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANYOCCUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREGNANT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREASTFEED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DURCYLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LMP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DYSMENOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLYMENO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MENORRHA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLIGOMEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMENORRH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOPMHIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOFAMHIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOSOHIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		KNOWNALGY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALCOSINCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRUGSINCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRUGQUITDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDUCATEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WILLEARNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIMLANG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNDSTLNG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRUGYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRUGSTOPYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALCOHYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALCOSTOPYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CIGYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CIGSTOPYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MENSESQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASMENDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTPAP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GAVIDA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABORTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PARA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NORMDEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABNORDEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABNORDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTCEP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SBEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHILDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRUGPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALCHPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CIGPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENOPAUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRUGFTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALCHFTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CIGRFTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOOFCHLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELIGION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOSRGHIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOPMHISBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOPMHISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOPMHISTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOFAMHISBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOFAMHISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOFAMHISTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOSOHISBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOSOHISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOSOHISTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		KNWNALGYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KNWNALGYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		KNWNALGYTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOSRGHISBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOSRGHISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOSRGHISTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GESTAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREPREGW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHGINBHE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HYPERACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EDUTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOHMMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOHMMEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOHMMEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOHMMEDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENELDCHLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEHSOHIS',
		timestamps: false
	});
};
