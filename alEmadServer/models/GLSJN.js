/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLSJN', {
		SRCEJRNL: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR01: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE01: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR02: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE02: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR03: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE03: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR04: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE04: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR05: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE05: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR06: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE06: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR07: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE07: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR08: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE08: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR09: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE09: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR11: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE11: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR12: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE12: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR13: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE13: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR14: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE14: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR15: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE15: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR16: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE16: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR17: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE17: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR18: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE18: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR19: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE19: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR20: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE20: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR21: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE21: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR22: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE22: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR23: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE23: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR24: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE24: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR25: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE25: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR26: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE26: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR27: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE27: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR28: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE28: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR29: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE29: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR30: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE30: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR31: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE31: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR32: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE32: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR33: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE33: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR34: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE34: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR35: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE35: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR36: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE36: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR37: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE37: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR38: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE38: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR39: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE39: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR40: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE40: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR41: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE41: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR42: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE42: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR43: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE43: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR44: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE44: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR45: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE45: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR46: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE46: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR47: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE47: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR48: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE48: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR49: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE49: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR50: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE50: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARETNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARETSNAM: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLSJN',
		timestamps: false
	});
};
