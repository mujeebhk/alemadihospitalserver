/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCLOCUSD', {
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCLOCUSD',
		timestamps: false
	});
};
