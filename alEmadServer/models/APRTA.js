/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APRTA', {
		TERMSCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWMULTPAYM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEVAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEDISTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISDAYSTR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYSTR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYSTR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYSTR4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYEND1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYEND2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYEND3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYEND4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISMTHADD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISMTHADD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISMTHADD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISMTHADD4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYUSE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYUSE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYUSE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISDAYUSE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODEDUETYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DUEDAYSTR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYSTR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYSTR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYSTR4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYEND1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYEND2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYEND3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYEND4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEMTHADD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEMTHADD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEMTHADD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEMTHADD4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYUSE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYUSE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYUSE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAYUSE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTENTERED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTDUETOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'APRTA',
		timestamps: false
	});
};
