/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISEXCDEP', {
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISEXCDEP',
		timestamps: false
	});
};
