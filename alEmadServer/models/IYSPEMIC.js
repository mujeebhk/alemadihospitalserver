/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYSPEMIC', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICCMT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICCMT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICCMT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICCMT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MICCMT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IYSPEMIC',
		timestamps: false
	});
};
