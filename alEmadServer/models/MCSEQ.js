/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCSEQ', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPSTKSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATREQSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATREQLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRNLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CDNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CDNLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STKTRFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STKTRFLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALRETSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALRETLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECVOUSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMHISSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDCOSLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRFINSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRFINLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCREMENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MOVSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHESEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MCSEQ',
		timestamps: false
	});
};
