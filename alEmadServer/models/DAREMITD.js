/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DAREMITD', {
		REMITSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CLAIMID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ACTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ACTIVEID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TOOTHNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDPAYER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUANTITY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLINICID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DENIALCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LISTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATSHARE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIPTGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'DAREMITD',
		timestamps: false
	});
};
