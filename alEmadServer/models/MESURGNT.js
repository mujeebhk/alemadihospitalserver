/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MESURGNT', {
		EPIUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGNOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MESURGNT',
		timestamps: false
	});
};
