/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDRECIPE', {
		RECIPEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECIPENAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBTITLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVINGS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREPARETIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COOKINGTIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READYTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOTOPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECIPPHOTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEBPAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECIPETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YIELD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WATER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALORIES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFATPCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENERGY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROTEIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALFAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUGAR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCARBOHD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FIBER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALCIUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHOSPORUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IRON: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SODIUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POTASSIUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAGNESIUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ZINC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANGANESE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELENIUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITAMINA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITAMINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		THIAMIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RIBOFLAVIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAICIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PANTOTHACD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITAMINB6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FOLATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITAMINB12: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITAMINC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SATURATFAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MONOUNFAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLYUNFAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHOLESTROL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALCOHOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CAFFEINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRAMWGHT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WGHTDESC1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRAMWGHT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WGHTDESC2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELWGHT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELMSRID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENRGYFRFAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVSIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVMSRID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUGARALCHL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANFATACD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITAMIND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALCORBOHD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALPROTEIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VITAMINK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALALCHOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALCARBS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENRGYALCHL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENRGYCARBS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENRGYPROT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALPCTALCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALPCTCARB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALPCTPROT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BIOTIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREPAREBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREPAREDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREPARETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHRISEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHRISEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHRISETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYREAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELREAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXCEPTION: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDRECIPE',
		timestamps: false
	});
};
