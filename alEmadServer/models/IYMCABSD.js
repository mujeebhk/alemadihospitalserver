/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYMCABSD', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SPECIMTYP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORGGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORGANCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ABSTPANCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ANTBIOCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTBIODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABDISKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABDISKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZONESIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESDIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTDISP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDISP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTICU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MANUALOPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IINTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IEXTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SINTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEXTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AINTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AEXTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELANTIBIO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABSTRUN1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUSPTBLITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MORPHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABSTRUN2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABSTRUN3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTFINAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTCOMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTCOMTFLG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRELRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINLRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMNDRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RULEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMDEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMDEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REASON1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REASON2: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYMCABSD',
		timestamps: false
	});
};
