/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKOPT', {
		OPTIONNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NXTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLDEFER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLCONSOL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLAPPEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TFRACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TFRNUMBER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OFXNOPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TFRDISTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TFRSRVCACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TFRPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TFRDOCLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTDOCLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEQTFR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEQENTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRECONCIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEXTRUNID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DTLSORTBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKOPT',
		timestamps: false
	});
};
