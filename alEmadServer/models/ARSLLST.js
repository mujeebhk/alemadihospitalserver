/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSLLST', {
		SEQNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		SWTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		KEYFIELD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD11: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD12: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD13: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD14: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD15: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD16: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD17: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIELD18: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARSLLST',
		timestamps: false
	});
};
