/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUFILE', {
		FILENO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTVISIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCATION1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATION2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATION3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATION4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATION5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTPROV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MERFILENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBILENO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUFILE',
		timestamps: false
	});
};
