/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEMPBNS', {
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREVBASIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BONUSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCEMPBNS',
		timestamps: false
	});
};
