/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('METESTRW', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SERVICETYP: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERENCOU: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TESTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIEWYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVEWENCOU: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIEWDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIEWTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IMAGEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMAGEENCOU: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IMAGEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IMAGETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCUSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCENCOU: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCUSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCUSTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TESTSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'METESTRW',
		timestamps: false
	});
};
