/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIBILLD', {
		BILLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILCODEGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERPRVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGSTART: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGEND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPACKAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHTDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISREGCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHTCOPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINORPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENVSTCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEVSTCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONVSTCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDCOLLVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OCOPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OCOPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATDISCPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISDISCPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCBYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCBYNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISEMERGNCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ODISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ODISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERPRVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULTIADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANNETECST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCOMPRES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOOTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICEUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMNGVN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMNTRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMNAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREFOLWUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVICEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABCOPUTIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADCOPUTIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHMCOPUTIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISCOPUTIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCOMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCNCOMPRES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCESSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCHDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SINSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SNETEXTCST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCOPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCOPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCOPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SDEDUCTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCOPAYFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		URGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MBDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERFORMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IIBILLD',
		timestamps: false
	});
};
