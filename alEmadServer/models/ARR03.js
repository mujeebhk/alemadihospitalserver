/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARR03', {
		IDR03: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJTRX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRTDEPS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYMEDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYMPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBLORDR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATRPAYMSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ATRADJSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PPDPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PPDPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPPDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UCPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UCPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTUCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DFRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWALOWADJ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTADSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RMITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STMTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PYPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PYPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPYSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RFBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RFPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RFPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ATRRFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWALOWRCED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCREATDEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCHKDUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWSHRCPND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDATEBUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTCHKBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARR03',
		timestamps: false
	});
};
