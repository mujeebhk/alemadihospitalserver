/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APRAS', {
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTAP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUSPACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PPAYACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URLZGNACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URLZLSACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RLZGNACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RLZLSACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJSTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RNDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APRAS',
		timestamps: false
	});
};
