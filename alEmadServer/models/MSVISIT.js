/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSVISIT', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFPROV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFFACL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFOTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFDETAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NATEXP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHKTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHKOUTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VSTCOPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDCTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREPAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSULTTP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISDCOD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVVSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVVSTPRO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETRF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASESELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBSCRIBBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPLYRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISEMRGNCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VSTPURPOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLINICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKPATYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREVISITYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSULTTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISREFERIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INITVISIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDCOLLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MLCYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MLCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOAPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOOFBILLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSBILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REGBILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRFIPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSFERRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHRONIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCCATDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCREGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCDPLACE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLSTANAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISINTIMATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WOUNCERDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTPROVCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTPROVDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPONSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VSTEXPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INLIMITUTL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INLIMITBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPLIMITUTL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPLIMITBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUEUENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECDEPTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKAGETYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKINYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISODTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPOINTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MLTPKGCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRONAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSULTDSC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MLCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPOFVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISFREEVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEYEARS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMONTHS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRVICD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FINICD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVDEPSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPICODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPTNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NURASMST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULTPKGTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FALLRISKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECPLANYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOSPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETWCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSVISIT',
		timestamps: false
	});
};
