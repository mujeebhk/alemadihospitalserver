/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSLOGIN', {
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASHCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASHCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSLOGIN',
		timestamps: false
	});
};
