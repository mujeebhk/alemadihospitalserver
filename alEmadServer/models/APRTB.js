/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APRTB', {
		TERMSCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PAYMNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTPAYMDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PCTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISNBRDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DUENBRDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUEDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'APRTB',
		timestamps: false
	});
};
