/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPROUTED', {
		GENRICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISDEFAULT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MPROUTED',
		timestamps: false
	});
};
