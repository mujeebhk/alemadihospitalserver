/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPRCOMD', {
		COMMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPUTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCOMMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPCOMMPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCOMMAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCASHCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCOMMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCOMMPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCOMMAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCASHCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCRDCOMTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPCRDCOPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCRDCOAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPCRDTCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCRDCOMTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCRDCOPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCRDCOAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCRDTCOMM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTLLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISALOWAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TARGETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TARGET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLTPYE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPRCOMD',
		timestamps: false
	});
};
