/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MESECDIG', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECDIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECDIGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MESECDIG',
		timestamps: false
	});
};
