/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDORDGD', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SERVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		GUESTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEALTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHOICEID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UOMID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETNOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IDORDGD',
		timestamps: false
	});
};
