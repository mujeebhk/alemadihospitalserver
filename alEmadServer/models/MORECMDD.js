/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MORECMDD', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MORECMDD',
		timestamps: false
	});
};
