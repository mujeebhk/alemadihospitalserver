/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DAREMITH', {
		REMITSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMENTREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNAPDOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIVERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECORDCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BALCASH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALRECD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPDOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'DAREMITH',
		timestamps: false
	});
};
