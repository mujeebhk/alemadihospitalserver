/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEMPATH', {
		ATTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACESSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMINUTES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WMINUTES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIFFMINS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCEMPATH',
		timestamps: false
	});
};
