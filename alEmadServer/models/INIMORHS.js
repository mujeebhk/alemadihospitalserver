/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIMORHS', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IMMTYPECOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOSEUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPLTDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPLTDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPLTDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIWDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIWDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIWDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRINGCRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANREA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INIMORHS',
		timestamps: false
	});
};
