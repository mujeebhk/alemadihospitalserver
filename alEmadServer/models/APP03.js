/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APP03', {
		RECID03: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRCTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MANLBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJTRX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMRUN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWCHKPRNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDOCORDR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPAYMBANK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGINPRD1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGINPRD2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGINPRD3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWAGECR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DTPREREG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMPREREG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTDRCTCHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMDRCTCHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTSYSCHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMSYSCHK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWSYSBTCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWFORCREG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDDFLTBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFLTRATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTPPDBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTSYSBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTNXTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTNXTADJM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPAYMBTCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PPDPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PPDPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWAGEUAPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTNXTUAPL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADNEXTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPNEXTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RMITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PYPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYNEXTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RIPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RIPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RINEXTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RCPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RCPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RCNEXTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RDPFXLEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RDNEXTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGBASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGSCHDKEY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGSCHDLNK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGLASTRUN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGADVDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXRTGRPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCHNGRMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCHKDUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWSHPYPND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDATEBUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTCHKBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APP03',
		timestamps: false
	});
};
