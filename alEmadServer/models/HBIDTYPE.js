/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBIDTYPE', {
		IDCARDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALERTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPROBATON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALERTREQ1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFDAYS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALERTREQ2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFDAYS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBIDTYPE',
		timestamps: false
	});
};
