/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARIBD', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWMANLITEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPRIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOGS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTXBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWMANLTX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXSTTS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTREV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTINV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTCOGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDJOBPROJ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWIBT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWDISCABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OCNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRTGDDTOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGAMTOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDISTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGCOGSTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGALTBTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGINVDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGINVCOGS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGINVALTB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTNETHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOGSHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOSTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPRICHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXTNHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARIBD',
		timestamps: false
	});
};
