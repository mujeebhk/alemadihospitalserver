/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHPHEXD', {
		CCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		HCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SYSTEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBSYSTEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SYSTEMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTNOTIFD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEG: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEHPHEXD',
		timestamps: false
	});
};
