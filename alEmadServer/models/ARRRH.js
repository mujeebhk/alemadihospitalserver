/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRRH', {
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DEPSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		DEPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		DATERMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPSTNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRMITTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEEXCHHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWOVRDRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEXTRETRN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELSTMTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTSTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTROUNDER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATERATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMERMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWCHKCLRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTRMITHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATECLRD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATERVRSD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRXTYPETXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLDWNLK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWNONRCVBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDINVCMTCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTXAMTCTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXBSE1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECURNRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTXCTLRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPERC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTACC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTACCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTACCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CCTRANID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARRRH',
		timestamps: false
	});
};
