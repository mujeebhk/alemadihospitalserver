/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISPATINJ', {
		INJCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPENBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPENDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FDEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FDEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ISPATINJ',
		timestamps: false
	});
};
