/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIENTHIS', {
		ENTMTCHGNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDPRICUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDPRIPLAN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDSECCUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDSECPLAN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWPRICUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWPRIPLAN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWSECCUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWSECPLAN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IIENTHIS',
		timestamps: false
	});
};
