/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIBILINC', {
		BILLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILCODEGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READYBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISDISCPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERPRVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPACKAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDUCTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRECONSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREPHRMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESERVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICEUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISEMERGNCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOOTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCOMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SINSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SNETEXTCST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCOPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCOPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SDEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCOPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SDEDUCTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCOPAYFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGTYPCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		URGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MBDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERFORMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IIBILINC',
		timestamps: false
	});
};
