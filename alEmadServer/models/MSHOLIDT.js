/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSHOLIDT', {
		HOLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEEKDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISWEEKOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NATHOLIDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSHOLIDT',
		timestamps: false
	});
};
