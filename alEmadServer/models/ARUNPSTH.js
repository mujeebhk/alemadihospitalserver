/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARUNPSTH', {
		SELSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDNATACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLYBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVCAPPLTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGAPPLYTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDRNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIPNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATETRANS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWITEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXCHRATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNAPLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNAPLAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARUNPSTH',
		timestamps: false
	});
};
