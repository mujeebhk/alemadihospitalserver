/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPREL', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		RELATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		RELATNAME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCARDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELATIDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDUPTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUEAUTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISBENFICNA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALERTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALERTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELGAIRFARE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPREL',
		timestamps: false
	});
};
