/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLORDSAM', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LABSUBSECC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SPEMENABBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTFAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOURCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDBYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHEDULEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDVALIDFD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDVALIDTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPATINST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELTEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATINST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISAUTHORIZ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TESTSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDTSTSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTENQUIRE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFLEXTEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELUIID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRITICAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCEPTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABSECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSTSITDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSUBSECD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PANEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMENDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TSTSITELOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REACANCEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDOUTTST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKLISTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISESOREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISREFERIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LBLABBREV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABNUMBER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLORDSAM',
		timestamps: false
	});
};
