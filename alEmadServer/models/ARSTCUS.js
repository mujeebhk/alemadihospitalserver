/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSTCUS', {
		STMTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWNATSTMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDNATACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTSNAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATESTART: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDPPNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEDAB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEDABRTG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEDAB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAMECUST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPSTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETERR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDAUTOCASH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBILLCYCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDSVCCHRG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDLNQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRTSTMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRTDLNQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWBALFWD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETERM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXSTTS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTCRLIMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALDUET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALDUEH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTSTT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTSTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTBEGBALFW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALFWDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALFWDH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTLASTRVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALLARV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTOPENINV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTINVPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYSTOPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVCHI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBALHI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVHIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBALHIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTIV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTPA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTDI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTWR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTRI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTDQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDINVCHI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVCHILY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTINVHIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHILT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHILT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTIVT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTCRT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTDRT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTPYT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTDIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTADT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTWRT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTRIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTINT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHIH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHIH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHILH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHILH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTIVH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTCRH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTDRH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTPYH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTDIH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTADH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTWRH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTRIH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTINH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODESLSP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCTSASPLT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRICLIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTPDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTSTMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEBSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CTACPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPARTSHIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HAMTBGNBLF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HAMTEBALFD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HAMTSTMTBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HAMTDUECUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HAMTDUEAG1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HAMTDUEAG2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HAMTDUEAG3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HAMTDUEAG4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HAMTDUEFWD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RBCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBCSTREET1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBCSTREET2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBCSTREET3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBCSTREET4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBCCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBCSTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBCPSTCDE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBCCNTYCDE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSDECIMAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURSYMBOL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARSTCUS',
		timestamps: false
	});
};
