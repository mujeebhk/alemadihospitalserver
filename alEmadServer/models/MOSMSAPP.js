/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOSMSAPP', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGYNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSYESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SMSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCMOB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATMOB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQUESTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MSGSTATUS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MESGSENTDT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MOSMSAPP',
		timestamps: false
	});
};
