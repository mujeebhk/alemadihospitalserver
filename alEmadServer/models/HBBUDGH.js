/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBBUDGH', {
		FSCALYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		BGTSETCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRTPHYMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENDPHYMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRCASTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBBUDGH',
		timestamps: false
	});
};
