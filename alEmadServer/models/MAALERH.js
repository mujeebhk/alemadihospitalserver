/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAALERH', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALRTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALRTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MESSAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHIFTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MAALERH',
		timestamps: false
	});
};
