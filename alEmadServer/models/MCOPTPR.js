/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCOPTPR', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRNPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRTNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHEPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHENEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCOPTPR',
		timestamps: false
	});
};
