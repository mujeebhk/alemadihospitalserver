/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INCONSTH', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CREATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSENTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSENTBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHERSNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILEID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INCONSTH',
		timestamps: false
	});
};
