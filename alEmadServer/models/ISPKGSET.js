/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISPKGSET', {
		PACKTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		ORDPROTYP: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		SEVERETYP: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		SPECCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CONSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLDEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ISPKGSET',
		timestamps: false
	});
};
