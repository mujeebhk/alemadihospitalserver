/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSEUR', {
		CURID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLOCKMASTE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CSEUR',
		timestamps: false
	});
};
