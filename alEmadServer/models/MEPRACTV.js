/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEPRACTV', {
		PREID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSUNTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPENSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UDIQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISINSERTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYPERDOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEPRACTV',
		timestamps: false
	});
};
