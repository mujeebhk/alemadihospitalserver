/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCOPSTKD', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVFACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FMTITMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCOPSTKD',
		timestamps: false
	});
};
