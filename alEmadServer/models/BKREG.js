/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKREG', {
		SRCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPRUNNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SORTCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PAYEEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHECK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYEENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDORNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHKDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SISSUED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTRA: {
			type: "BINARY",
			allowNull: false
		},
		LANGUAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FSCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LANGCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SDECIMALS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DDLINK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCEDOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANREVINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKREG',
		timestamps: false
	});
};
