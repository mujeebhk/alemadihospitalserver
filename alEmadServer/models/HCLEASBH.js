/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEASBH', {
		LSALSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALARYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVESAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AIRFARE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTLVPERD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAACCDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVETOPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTAIRAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCLEASBH',
		timestamps: false
	});
};
