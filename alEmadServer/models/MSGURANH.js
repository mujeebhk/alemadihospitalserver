/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSGURANH', {
		SEQUNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GURANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUARTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TTLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHONEOFF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONEEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOHOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SSN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLOYER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FEECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FMLYFEE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FMLYPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FMLYBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTIONAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTIONAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTIONAL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTIONAL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTIONAL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTIONAL6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTIONAL7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTIONAL8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRANCHID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSGURANH',
		timestamps: false
	});
};
