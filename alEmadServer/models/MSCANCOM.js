/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSCANCOM', {
		CANCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARBCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT4: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSCANCOM',
		timestamps: false
	});
};
