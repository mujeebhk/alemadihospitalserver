/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSINVHT', {
		INVHCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVLCLINC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERCCONS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFAPRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFAREAG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERPCONS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERPRMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDPRMTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLIINFREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLIININF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVIEWREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPATPRE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDTMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDTMTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDTMYMD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FILMUSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FILMCAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHORTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTOVALREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSINVHT',
		timestamps: false
	});
};
