/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSOPCONS', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSSETUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREEVSTDUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREVSTCONS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREVSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVSTDUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVSTCONS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIRSTVSTCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIRSTVSTDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESETVISIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FTVSTISNEW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREEVSTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAIDFOLCON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTFREEVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSOPCONS',
		timestamps: false
	});
};
