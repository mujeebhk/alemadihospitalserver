/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPSTR', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALARYL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALARYL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALARYL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHFTINCH: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPSTR',
		timestamps: false
	});
};
