/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DARCLMBD', {
		RCLMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTVTTYPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOOTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCLUDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TMPGRAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMPCMPPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMPPATPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TMPNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABRSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTIDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVSUBTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBSERVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENIALCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SOAPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDVLDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SYSRSLT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIARSLT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEWACTID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'DARCLMBD',
		timestamps: false
	});
};
