/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBJOBLTH', {
		OFFERSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OFFERDOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OFFERDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MIDDLENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPOINTTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		JOINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BIRTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAFFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALEARN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALDEDCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETSALARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTELMNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HBJOBLTH',
		timestamps: false
	});
};
