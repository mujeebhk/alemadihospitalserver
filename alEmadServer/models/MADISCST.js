/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MADISCST', {
		DISCSTATNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HWPATDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WHDISWIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHRELCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCHTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRSNFRFRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUPPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELATNSP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HEARNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEARDTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISONDTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEECH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPCHDTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRESSLF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHAVSLF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMBULATS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FEEDSLF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATHSLF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORALHYGN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLADDR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BOWEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIETADV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RFRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RFRADD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RFRTEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RFRLFOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREPARDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREPARDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODEDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODISOTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCLAMA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALBRET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALBRWHM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALNA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDAPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPAPWHN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCINSG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCEDG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FUNDUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WNDPRNL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRSTCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATVAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VACCINE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPBODY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MADISCST',
		timestamps: false
	});
};
