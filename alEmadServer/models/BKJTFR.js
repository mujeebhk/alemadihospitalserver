/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKJTFR', {
		PSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		KEYSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSFERNR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OGLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OSRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORATESPREA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OSAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OFAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DGLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DSRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRATESPREA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DRATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DSAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DFAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TSRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TFAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OCURNSTMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OMULTICUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCURNSTMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DMULTICUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OSRCECURND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DSRCECURND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUNCTCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'BKJTFR',
		timestamps: false
	});
};
