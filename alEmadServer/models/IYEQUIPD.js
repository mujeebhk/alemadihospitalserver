/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYEQUIPD', {
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MACORGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGISMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGISMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYEQUIPD',
		timestamps: false
	});
};
