/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SCCHSTH', {
		CSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CSTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPREFUND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPBILLREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPBILLREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPBCREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPPATREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPPATREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPREFUND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPSETREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPSETREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPPATREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPPATREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHPATREC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHPATREF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'SCCHSTH',
		timestamps: false
	});
};
