/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEAVAD', {
		LEAVEUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERIOD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PENDING: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		JOINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPRTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTLEAVELI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVAILTILDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORIZED: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESENTDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLEAVESESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FHALFMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDFRMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOHALFMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TLEAVESESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPRFRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETURNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAPPREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRLEAVSESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRLEAVSESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEEKOFFCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOLDAYSCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOLIDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISEMERLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCRUTILDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALTILDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROJAPPLD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROJBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NXTPRJAPLD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETURNSESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DUTYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUTYJOINED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLEAVAD',
		timestamps: false
	});
};
