/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEVTLHIS', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIABP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSBP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHTFT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHTIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHTKG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHTGR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEARTRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPIRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPERATOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEADCIRCUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIASTAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIASUPIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSSTAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SYSSUPIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AXILIARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TYPHANIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OXYSAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAINSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISVITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODIFIEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEVTLHIS',
		timestamps: false
	});
};
