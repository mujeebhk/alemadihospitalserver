/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEASTHMA', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASTHMA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASTMNGPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASTMNGTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFECTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXERCISE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEATHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRAVELING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMOPSY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRIGGER1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRIGGER2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRIGGER3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRIGGER4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRIGGER5: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEASTHMA',
		timestamps: false
	});
};
