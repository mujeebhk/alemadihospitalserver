/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISRATPRD', {
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLCODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISDISCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERPRVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ISRATPRD',
		timestamps: false
	});
};
