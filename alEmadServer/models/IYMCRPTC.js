/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYMCRPTC', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISGROWTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOGCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPTCMT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT11: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT12: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT13: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTCMT14: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MICRONO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMDREASON: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYMCRPTC',
		timestamps: false
	});
};
