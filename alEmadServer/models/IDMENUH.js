/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDMENUH', {
		MENUID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MENUDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUCATGRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVINGQTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CORRECTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CORRECTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CORRECTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IDMENUH',
		timestamps: false
	});
};
