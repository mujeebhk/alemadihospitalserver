/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUDOCDET', {
		DOCUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRANTPROV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IUDOCDET',
		timestamps: false
	});
};
