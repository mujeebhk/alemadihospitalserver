/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MESPIMGH', {
		SPECCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IMAGEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMGSIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MESPIMGH',
		timestamps: false
	});
};
