/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBSHIFT', {
		SHIFTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BREAKONE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREAKTWO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREAKTHREE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BKONEINTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BKONEOUTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BKTWOINTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BKTWOOUTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BKTHRINTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BKTHROUTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHFTABBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STRTTIMTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENDTIMTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHFTCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLTSHIFT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RPTTIMTWO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTTIMTWO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPTTMTHREE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OUTTMTHREE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLEXISHIFT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FLEXSFTMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBSHIFT',
		timestamps: false
	});
};
