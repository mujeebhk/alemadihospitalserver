/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLPJID', {
		POSTINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		OBATCHNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		OENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		OTRANSNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LEVCOMP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BATCHNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ENTRYNBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TRANSNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELDGR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCURNDEC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEMTCHCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONVRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCURNAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLPJID',
		timestamps: false
	});
};
