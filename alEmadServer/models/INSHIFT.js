/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INSHIFT', {
		SHIFTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHIFTTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHIFTCOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INSHIFT',
		timestamps: false
	});
};
