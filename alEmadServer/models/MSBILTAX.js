/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSBILTAX', {
		BLCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUTHORITY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PURCHTAXCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALESTAXCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PURCHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALESDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSBILTAX',
		timestamps: false
	});
};
