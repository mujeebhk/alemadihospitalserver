/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTALMAT', {
		APPSEQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRSKILDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKILLYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKILLMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDTALMAT',
		timestamps: false
	});
};
