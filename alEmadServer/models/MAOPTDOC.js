/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAOPTDOC', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREADNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREADLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREADPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREADBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREADVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCHNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCHLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCHPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCHBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCHVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRAVAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLROCCI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRHK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRDISINF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRAD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRMAINT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFTSTDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOWCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDOCCNXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALRTACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDCHARGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRACEPRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEADPRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVSEREXS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPVALTILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASHPRESTA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASHDISSTA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREDPRESTA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREDDISSTA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDRELEASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDCHRGTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDSTATIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISDISDTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSETLEREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCHSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHKOUTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRACETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINDAYCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOURLYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRVSTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRVSTWNPRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRVSTCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BIRTHSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTBABYDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMCONSENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVMEDCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISSTATNXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CSHBEDEREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRDBEDEREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOWADMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTODISSUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCAREYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARETM1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARETM2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARETM3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAKMADADM: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MAOPTDOC',
		timestamps: false
	});
};
