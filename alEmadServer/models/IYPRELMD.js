/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYPRELMD', {
		PRESEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ANTBIOCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTBIODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABDISKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABDISKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZONESIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESDIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTDISP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDISP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTICU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MANUALOPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESUSPLTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUSPTBLITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RULEID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYPRELMD',
		timestamps: false
	});
};
