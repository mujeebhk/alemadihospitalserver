/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBSMSAPP', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECVRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SMSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSSENTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAILSNTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROBENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBSMSAPP',
		timestamps: false
	});
};
