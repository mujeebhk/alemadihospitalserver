/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEIPLIST', {
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		REFERRAL: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMDTFRM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEIPLIST',
		timestamps: false
	});
};
