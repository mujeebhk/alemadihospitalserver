/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCAPPRH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPAPPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EFFECTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPRTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPRPERCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPRAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PMONGROSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PMONTHCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYEARGROSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYEARCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PMONPERKS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYEARPERK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PMONEMPCON: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PYEARECONT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NMONGROSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NMONTHCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NYEARGROSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NYEARCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NMONPERKS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NYEARPERK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NMONEMPCON: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NYEARECONT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRATUITY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARREARSCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORISBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIEWNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTPERMARK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OBJWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTRATING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FINRATING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROBYESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSERVICE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISIMPROVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNINGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FINPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OBJSAVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCESSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBJYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNINGCOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREVPROB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTENDUPTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERVICECOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROBCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCAPPRH',
		timestamps: false
	});
};
