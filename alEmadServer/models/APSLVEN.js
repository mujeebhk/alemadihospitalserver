/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APSLVEN', {
		SELSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		RECORDNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTVALUE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTVALUE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTVALUE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTVALUE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORTTYPE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTTYPE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTTYPE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORTTYPE4: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APSLVEN',
		timestamps: false
	});
};
