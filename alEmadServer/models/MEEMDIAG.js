/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEEMDIAG', {
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROBCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROBSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROBTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRBTYPSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEEMDIAG',
		timestamps: false
	});
};
