/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDASGAD', {
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHORTLISTD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBPANLCD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLDS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBPANLCD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLDS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLCD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLDS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLCD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLDS4: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDASGAD',
		timestamps: false
	});
};
