/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPITMLOC', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLOWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INUSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYGIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COSTUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LTPURCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FMTITMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOURCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPITMLOC',
		timestamps: false
	});
};
