/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DAOPBALD', {
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ACTID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTSTARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LICENSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBSERTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOOTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBSERVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBSRVALTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDPRVLIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'DAOPBALD',
		timestamps: false
	});
};
