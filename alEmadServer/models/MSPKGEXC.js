/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPKGEXC', {
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DSERVTYP: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRTTHNAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSPKGEXC',
		timestamps: false
	});
};
