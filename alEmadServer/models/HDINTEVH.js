/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDINTEVH', {
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PANELCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANELCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUNDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTSCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SECONDSCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		THIRDSCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FOURTHSCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALSCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL1CON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL2CON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL3CON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL4CON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECRUITED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECQUPDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHIEFPAN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHFPANAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHFPANNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDINTEVH',
		timestamps: false
	});
};
