/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSTNAT', {
		STMTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		IDNATACCT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NAMEACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPOST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEBSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWBALFWD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTBGNBLF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEBALFD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTSTMTBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUECUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEAG1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEAG2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEAG3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEAG4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEFWD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARSTNAT',
		timestamps: false
	});
};
