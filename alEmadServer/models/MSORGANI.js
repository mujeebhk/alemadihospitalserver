/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSORGANI', {
		ORGANICODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGANIDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OUTPTIENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INPTIENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHARMACY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSORGANI',
		timestamps: false
	});
};
