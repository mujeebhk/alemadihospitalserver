/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEPATEDU', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOBARRIER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CULPRACT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LITERACY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LACOFCONF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LANGUAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FINANPROB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMOTIONAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEVISSUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOSOFHOPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLFNVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANXFEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHYIMPAIR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENSDEF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COGNTIMPAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTVNONE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBTTRAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TECHFAM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESPVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVIEW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REASSUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESPCUL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPSUB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHRBAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHRBARCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJTEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPYSKL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETMOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TUBFEED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CROFTRTMY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRETMNTPLN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRUNACAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINMGT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANTNAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELFCARE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPLN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDREGIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SFUSEQP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CROFWND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REHABTEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHRLEARN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEARNCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDULEVEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LANGUAGCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELIGION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEPATEDU',
		timestamps: false
	});
};
