/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CYTMPLD', {
		TMPFIELD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DECIMALS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOWNULL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEXT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CYTMPLD',
		timestamps: false
	});
};
