/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPMOORDH', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPCATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPPROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPPRODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPROOMCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPROOMDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPANECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPANEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORCHTYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORCHTYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANCHTYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANCHTYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORRCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROOMTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANETOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRANDTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPATPBLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDCOLLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELREA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PKGORDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPATPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ICPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPSURGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPSURGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPANESCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPANESDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCRUBNUR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCRUBNUR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCRUBNUR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCHGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCHGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDPRVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIRTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UTILIZED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDCLASS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIMSURTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRIPRVTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANSSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANSSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANSEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANSEDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BIOPSYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALAPPRV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALUTIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGPLN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POOTCTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANSTHESTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANSTHESDRN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANSTHEOTHR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RCRMSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RCRMSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RCRMEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RCRMEDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RCRMDURN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASSTSURN1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSTSURN2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSTSURN3: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'TMPMOORDH',
		timestamps: false
	});
};
