/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLORDERD', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTFAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOURCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDBYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHEDULEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDVALIDFD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDVALIDTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPATINST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELTEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATINST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISAUTHORIZ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TESTSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDTSTSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTENQUIRE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFLEXTEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRITICAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCEPTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABSECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMENDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCOMPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDOUTTST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANTIMICRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REACANCEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPATCHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPATCHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPATCHTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKLISTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFTESTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREDISPTCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREDISPBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREDISPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREDISPTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISMICRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISESOREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPERECDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPERECTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISREFERIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRASPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRASPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFLTSTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFLTSTDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STDFEE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRNTEXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSHOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WARDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPISODENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREPAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREPATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVIEWEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIEWTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCHEDULETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLORDERD',
		timestamps: false
	});
};
