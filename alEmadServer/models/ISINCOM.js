/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISINCOM', {
		INSUCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSUSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODIFIER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FEETYCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSTYCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COTITLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONMIDLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONLAST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONPH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONPHEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLAIMID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYER1ID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYER2ID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OCNA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATLSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICELIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCFROM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLGRPREQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATELST2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPONSOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSTYP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERCAPITA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATTACHUSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGANICODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ECLAIMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LUXTAXPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAPPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEMIDMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARDNOMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPENDMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLCYNOMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLCYNOSEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECPLNABR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELIGRATES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPATID1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPATID2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSTNY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MRDPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCELFRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDLIMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILVALREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOAPYESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOGOPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISINCOM',
		timestamps: false
	});
};
