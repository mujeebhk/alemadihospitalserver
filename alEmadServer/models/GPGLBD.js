/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GPGLBD', {
		BSEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ESEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DSEQUENCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCEDEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEMTCHCD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GPGLBD',
		timestamps: false
	});
};
