/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSFSCST', {
		FSCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PGMID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS7: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS9: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS10: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS11: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS12: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS13: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'CSFSCST',
		timestamps: false
	});
};
