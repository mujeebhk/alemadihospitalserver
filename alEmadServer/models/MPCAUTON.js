/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPCAUTON', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAUTION1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CAUTION2: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPCAUTON',
		timestamps: false
	});
};
