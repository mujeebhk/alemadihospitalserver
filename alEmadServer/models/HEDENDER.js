/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HEDENDER', {
		DESEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERRORDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODULE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HEDENDER',
		timestamps: false
	});
};
