/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAADMAPR', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMAPRV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMDPUSR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMDPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMDPTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLAPRV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLDPUSR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLDPTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLNSTRDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MAADMAPR',
		timestamps: false
	});
};
