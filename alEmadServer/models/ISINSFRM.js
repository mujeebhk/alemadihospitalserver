/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISINSFRM', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPANAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCOMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PULSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEMPARAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DURILLNES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISEMEGENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHFCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAINSYMTOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SIGNISIGN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHCOND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FDIACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FDIADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SDIACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SDIADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TDIACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TDIADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FODIACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FODIADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISCHRONIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONGENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKREL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VACCIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHECKUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYCHIAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFERTIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREGNAM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPORTREL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLEANING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORTHDONT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CMFYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MANAGECOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESTLNTHST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPDTADM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLETBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPRNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPRVALID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSOFFIR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQCREBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQCREDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQCRETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		READYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		READYTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENTTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VARIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VARIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VARIFYTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLOSEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ISINSFRM',
		timestamps: false
	});
};
