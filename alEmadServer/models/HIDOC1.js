/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HIDOC1', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HISPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HISLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HISNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HIDOC1',
		timestamps: false
	});
};
