/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARNAT', {
		IDNATACCT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTMTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEDAB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DABRTG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEDAB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAMEACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPOST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDAUTOCASH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBILLCYCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDSVCCHRG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDLNQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRTSTMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPRTDLNQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWBALFWD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTCRLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALDUTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALDUHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTSTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLSTSTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLSTSTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBALFWD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBLFWDTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBLFWDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATERVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLSTRVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTOPENINV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTINVPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDAYSPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVCHI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBALHI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVHIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBALHIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTPA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTDI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTWR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTRI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTINT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTDL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDINVCHIGH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVCHILY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTINVHITC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHITC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHLIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHILT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDRTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWROFTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRIFTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINTTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHIHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHIHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHILH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHILH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDRHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWROFHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRIFHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINTTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEBSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELASTRF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTRFT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTRFH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWCHKLIMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCHKOVER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARNAT',
		timestamps: false
	});
};
