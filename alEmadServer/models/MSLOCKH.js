/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSLOCKH', {
		LOCKDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCKEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCKTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISOPLOCKED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISIPLOCKED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPHLOCKED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSLOCKH',
		timestamps: false
	});
};
