/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCDPNAFH', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TRNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARRLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMDESTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TODESTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AIRLINES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTAIRAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCDPNAFH',
		timestamps: false
	});
};
