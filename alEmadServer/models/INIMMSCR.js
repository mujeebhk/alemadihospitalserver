/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIMMSCR', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUECAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPLTDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPLTDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPLTDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIWDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIWDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVIWDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRINGCRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANDCMETS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INIMMSCR',
		timestamps: false
	});
};
