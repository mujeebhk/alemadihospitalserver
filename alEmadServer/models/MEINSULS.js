/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEINSULS', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLDSUGRFRM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLDSUGRTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOWDOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODDOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HIGHDOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOTTOPHY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEINSULS',
		timestamps: false
	});
};
