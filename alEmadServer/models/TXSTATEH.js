/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TXSTATEH', {
		GROUPID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		VERSION: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HASHMETRIC: {
			type: "BINARY",
			allowNull: false
		},
		CREATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CALCMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTTXAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'TXSTATEH',
		timestamps: false
	});
};
