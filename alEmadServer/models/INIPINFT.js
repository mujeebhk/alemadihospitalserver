/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIPINFT', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INIPINFT',
		timestamps: false
	});
};
