/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCATTMAP', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCFIELD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATAFIELD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCRYPTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCSTRVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCRYFACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCATTMAP',
		timestamps: false
	});
};
