/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPARAML', {
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LISTDATA: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFRANGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCOMM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFTSTREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPARAML',
		timestamps: false
	});
};
