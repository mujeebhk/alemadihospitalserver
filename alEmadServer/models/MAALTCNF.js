/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAALTCNF', {
		ALRTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HUSKPING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHARMACY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEITARY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHYSTHPY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRNTOFFI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSRANCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MAALTCNF',
		timestamps: false
	});
};
