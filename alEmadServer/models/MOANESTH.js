/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOANESTH', {
		INTOPSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ANACODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSITION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINPREP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINPOTHR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MOANESTH',
		timestamps: false
	});
};
