/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSCRH', {
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TABLEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEMATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATESRCE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CSCRH',
		timestamps: false
	});
};
