/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSMCVSDT', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MCTSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MCTSTGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UPLOAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TESTNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TSTSTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESULTSTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSMCVSDT',
		timestamps: false
	});
};
