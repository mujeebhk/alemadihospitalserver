/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSWARDD', {
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREADMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FMBEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATOCCP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPREADM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDREMAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDOCCP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INPRVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHKINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHKINTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHKINBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSWARDD',
		timestamps: false
	});
};
