/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTRNING', {
		TRNINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNINGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNINGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CERTFYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DURDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNDUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FHIREDAYS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DUESPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREQTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQDWMY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOLSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHRZEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHRZEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOURPRDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRADEAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDTRNING',
		timestamps: false
	});
};
