/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APVGR', {
		GROUPID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESCRIPTN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTMNTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTSETID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRTSEPCHKS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTSETID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TERMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DUPLINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DUPLAMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DUPLDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXRPTSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBJWTHHSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLASSID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWDISTBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APVGR',
		timestamps: false
	});
};
