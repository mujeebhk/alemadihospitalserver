/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPTPLD', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDSERELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDCOPYREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ODPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DENTALELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTCOPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATERNELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATCOPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MTPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTICLELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTCOPYREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKINCRELIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNCOPYREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKPHCOPPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCNSCOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNCNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCNSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCOPLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDSERPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDLIMITYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPDCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENTALPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENTLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENTLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DNTCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATERNPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATRLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATRLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MATCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTICLPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTCLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTICLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPTCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINPREA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINLIMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKNCONSLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNPHRMLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKNSERVLIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPIRYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSPTPLD',
		timestamps: false
	});
};
