/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLABRX', {
		ACCTBRKID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKID1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKID10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKSTRT10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKVSEQ10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKLEN10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABRKTITLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABRKTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAHUSAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASTUSAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADHUSAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AITUSAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASDUSAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEEFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABLKDELM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABLKDELM10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLABRX',
		timestamps: false
	});
};
