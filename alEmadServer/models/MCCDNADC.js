/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCCDNADC', {
		CDNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CDNCLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTEXP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPROMETH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTRET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCCDNADC',
		timestamps: false
	});
};
