/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSLOCKC', {
		LOCKDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCKEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCKTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISOPLOCKED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISIPLOCKED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPHLOCKED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESMODIFY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSLOCKC',
		timestamps: false
	});
};
