/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSVC', {
		CODESVCCHR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTMINCHG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTESVCCHRG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARSVC',
		timestamps: false
	});
};
