/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPKGHT', {
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LISTPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOSPFTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDSERVICE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELESERV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKPRDY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKPRDM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKPRDD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANESTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHYTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTYPECD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDCLSCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OCCUHLTHYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGOTHPAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PCKPRCDET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DYNPKGTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPKGHT',
		timestamps: false
	});
};
