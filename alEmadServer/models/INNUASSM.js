/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INNUASSM', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTERPRET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODEACCESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECEVDFRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REASONFVIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LANGSPOK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCOMPNYBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IFFEMALE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HISTORYFRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSMTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINASSM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEIGHTGAIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FUNCTIONAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABILITYCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYCHOLOG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSYCHODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINSEVERE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINONSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAINONSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINONST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINQUALTY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINCONTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINMODFAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINSYMSGN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINQLTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINSYMTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SKINASMTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINSCALE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNOTIFY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLERGY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCOMBYCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NUTDIETCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LANGSPKCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOTHR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HISOTHR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINONSTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHRMINTRVN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINSECTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONCIOUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INNUASSM',
		timestamps: false
	});
};
