/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPLYE', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MIDDLENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PSTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		JOINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYTHRU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCOUNTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCOUNTOD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPOINTTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROBUPTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTENDUPTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETIREDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTRENWDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPONSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESUMEPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SIGNPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALEARN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALDEDCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PANNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PFNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETSALARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETYEARSAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEARGROSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTEMPRCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSTAYQUAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENIORCITI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREVBASIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPORTSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BALCOMPOFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLSEG1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEG2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEG3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEG4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEG5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEG6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEG7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEG8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEG9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOCATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMBANACC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMBANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMBANNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPENTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPENDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YEARPASS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RELIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELIGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MARRYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BIRTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		JOINYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PFAPPLICAB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTWORKDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTRSNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERKS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRFEMPRENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RENTAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPHYSHAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESIGNSESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCREDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROMDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEPEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MEDCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDCATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIRTHPLACE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOTHERTONG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLOODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMERCNTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYETYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVAL9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NATIONCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACUMULATED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTEXPYRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTEXPMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCAIRFARE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUSSALPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSALPROCDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESIGNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GROSSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIMIDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPORTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISFAMLYELG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTRACTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IBANNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WPSNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AIRFAREELG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OBJWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OBJECTYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTOBJMARK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBTOBJMARK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSALPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTICEPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTLEADATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AIRFAREAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEASALDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRATAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROSGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPLATECOM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAACCDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALEAVESESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTAIRALOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELGEDUALLW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AFIRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMIDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALASTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISPJCEMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DPJCEARCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRTSRTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXMECHDATA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWIPEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTREQUIRED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMRCNTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISDAILYOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RSJOINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISFAMEMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERMMOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRSTMOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCONTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPWKWRDYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WKOFFSEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPLYE',
		timestamps: false
	});
};
