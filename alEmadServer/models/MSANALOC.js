/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSANALOC', {
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOWREC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOWPRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFRECLOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFPROLOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSANALOC',
		timestamps: false
	});
};
