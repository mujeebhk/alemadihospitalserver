/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAPRSERV', {
		IPPRENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLNCINFO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVINFO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MAPRSERV',
		timestamps: false
	});
};
