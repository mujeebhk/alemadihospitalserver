/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCITMROL', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROLFOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MINQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REORDLEVEL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SAFEQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MCITMROL',
		timestamps: false
	});
};
