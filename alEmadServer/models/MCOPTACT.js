/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCOPTACT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRFNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRRLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRRPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRRNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PSRLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSRPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PSRNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DSPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DSPPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DSPNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCOPTACT',
		timestamps: false
	});
};
