/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHPTEDU', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TOPCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TOPICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPCATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNDRSTND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOOLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GIVENPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GIVENDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEHPTEDU',
		timestamps: false
	});
};
