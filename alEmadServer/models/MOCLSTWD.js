/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOCLSTWD', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FASTDHRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LASTVIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FASTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSTVDWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANASRWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDLBLWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTIDBWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRSHRWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MKRMWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NLRMWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		JWLRTPSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JWLTPWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSRMWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNTRWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TETHWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURSTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLDGLWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WRCOTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRMEDWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHSWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATEDWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRGKWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLGLWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCORWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATLBWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IVLINWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ARTLNWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNVNWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRNTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		URNCWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NASTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BIOCWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEMWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ECGWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		XRWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRMWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLAVWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WHLBUNTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RDCLUNTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRFPLSUNTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLATUNTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHUNTS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHUNTS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREPARDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREPARDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WDHAIRYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDSKINYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHAIRYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTSKINYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WDHAIRRMK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTSKINRMK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENCNWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURCNWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANECNWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CBCWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PTTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELECTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLOODWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HBSWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCVWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIVWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLANDWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CATHEWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEENAWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROSTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACEMWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMBSTWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASPIRWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREGNWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREFDWDCHK: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MOCLSTWD',
		timestamps: false
	});
};
