/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPROLH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRPODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOPODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRCONDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOCONDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SSLFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MINFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROLFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEADTIME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MPROLH',
		timestamps: false
	});
};
