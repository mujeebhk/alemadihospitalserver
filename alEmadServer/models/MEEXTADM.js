/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEEXTADM', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOSPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMFROMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMTODT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEEXTADM',
		timestamps: false
	});
};
