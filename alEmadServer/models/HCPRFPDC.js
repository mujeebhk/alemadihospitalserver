/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPRFPDC', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		PRFPRMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPFEDBAK1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPFEDBAK2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPFEDBAK3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPFEDBAK4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPFEDBAK5: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCPRFPDC',
		timestamps: false
	});
};
