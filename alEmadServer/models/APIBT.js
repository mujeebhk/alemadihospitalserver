/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APIBT', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTLINE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRTINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APIBT',
		timestamps: false
	});
};
