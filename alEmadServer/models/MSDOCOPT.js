/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSDOCOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNUMNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPNUMBERL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPPREFIXD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPBODY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNUMNEXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GUANUMNEXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GUANUMBERL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GUAPREFIXD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUABODY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUAVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFLTLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYANDSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALREQBIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALREQPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RBILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOADRBCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOADCONB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCLOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOMODREG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOMODCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOGREQD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASHCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXLUXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXLUXAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABDEPTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISCENTBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		USERLOGREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NATIONAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUSECREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHOTOPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROOFPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACLIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHGAFTBIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHGAFTORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHGAFTPOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIJRIDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAMECOMMN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURFRECON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURCONOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VSTLOGREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCENTSTUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCENTOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCENTBFR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPENVSTSC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMEDITYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTPROVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIAGNOYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPPKGCOMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERTAXAUTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERTAXCLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFCHKT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUEMNPOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHMFACLIC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SMSSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOADDEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATPRFYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATINSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODVALTM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TDSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TDSPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TDSACC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSDOCOPT',
		timestamps: false
	});
};
