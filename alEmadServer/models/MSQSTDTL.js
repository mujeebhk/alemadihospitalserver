/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSQSTDTL', {
		QUESTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSQSTDTL',
		timestamps: false
	});
};
