/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCDOC', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANAPPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANAPPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANAPPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALARYLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALARYPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALARYBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVANCELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVANCEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVANCEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENDSERLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENDSERPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENDSERBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANRECLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANRECPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANRECBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVENCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVENCPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVENCBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRATUITLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRATUITPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRATUITBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETTLEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSALPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSALPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEAPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVEAPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEAPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDREVLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDREVPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDREVBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVIEWPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVIEWBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPDETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPDETPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPDETBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPCLMLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPCLMPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPCLMBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPAPRLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPAPRPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPAPRBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINSLOPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FINSLOPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINSLOPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCDOC',
		timestamps: false
	});
};
