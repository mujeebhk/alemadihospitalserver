/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCTMPCMD', {
		TEMPID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EARDEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARDEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCTMPCMD',
		timestamps: false
	});
};
