/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCIMOVH', {
		MOVSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPENQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPENBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRFQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRFCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MCIMOVH',
		timestamps: false
	});
};
