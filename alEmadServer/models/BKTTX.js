/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKTTX', {
		TYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUTHORITY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCLUDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKTTX',
		timestamps: false
	});
};
