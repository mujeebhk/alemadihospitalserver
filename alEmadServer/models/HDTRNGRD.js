/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTRNGRD', {
		TRNINGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		RATING: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATINGDESC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESCRPTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOSCORE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOSCORE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOSCORE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOSCORE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOSCORE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HDTRNGRD',
		timestamps: false
	});
};
