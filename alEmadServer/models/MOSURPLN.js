/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOSURPLN', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGCODE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGDESC1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FASTINGFRM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DVTPROPHY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANTIBIOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLOODTRANS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IVFLUIDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMITTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDICDISC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPLNTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MOSURPLN',
		timestamps: false
	});
};
