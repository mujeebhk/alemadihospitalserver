/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHISTDT', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOCHISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FAMHISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALGYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURGDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPEPISDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEHISTDT',
		timestamps: false
	});
};
