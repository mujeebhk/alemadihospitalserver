/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRFB', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BTCHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BTCHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BTCHTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BTCHSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTRYCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTENTRY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NBRERRORS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATECREATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTCHKPRNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARRFB',
		timestamps: false
	});
};
