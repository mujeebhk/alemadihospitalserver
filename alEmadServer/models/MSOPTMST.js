/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSOPTMST', {
		CODELIST: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT1ACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPT1NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT1TBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT2ACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPT2NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT2TBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT3ACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPT3NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT3TBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT4ACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPT4NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT4TBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT5ACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPT5NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT5TBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT6ACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPT6NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPT6TBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTDATACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTDATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPTAMTACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPTAMTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSOPTMST',
		timestamps: false
	});
};
