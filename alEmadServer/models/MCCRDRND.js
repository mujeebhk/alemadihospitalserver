/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCCRDRND', {
		CDNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CDNLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAUPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALPROAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VENITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTEXPEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTNONSTK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVERYN1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERYN5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECOVAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NRECOVAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LANDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXCOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRPEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RPEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETRPLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETRPPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETRPEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPAYACHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCCRDRND',
		timestamps: false
	});
};
