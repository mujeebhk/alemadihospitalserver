/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSDISCAT', {
		DISCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSLTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVENTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NURSEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISCLDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANESTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSDISCAT',
		timestamps: false
	});
};
