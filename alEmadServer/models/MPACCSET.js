/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPACCSET', {
		ACCTSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COSTMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTINVCTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTPAYCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTADJW: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTNSTK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTTRNSCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTIPSAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTIPSRT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTOPCOGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTCSTVAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTCONSUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTORDCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTOPSAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTOPSRT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTOPCDSL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTOPCDST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTIPCDSL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTIPCDST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTIPCOGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTOPCHDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTOPCDDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTIPCHDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTIPCDDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTPHADJW: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSHECLR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTCOGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTDISPOS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTLOAN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPACCSET',
		timestamps: false
	});
};
