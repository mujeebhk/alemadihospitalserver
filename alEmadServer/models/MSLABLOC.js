/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSLABLOC', {
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSAMCOLL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSAMRECI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSAMPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABSUBSECC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTEXT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISMICROGY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSLABLOC',
		timestamps: false
	});
};
