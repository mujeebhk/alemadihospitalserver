/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSVSTLIM', {
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		SERGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UTILIZEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONSCOPLMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONSCOPUTL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSVSTLIM',
		timestamps: false
	});
};
