/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSTAHT', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRASCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ZONECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRIOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNKNOWN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATIENTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRIAGECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ARVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARVTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRGTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODARV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFCENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICEINVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLICECUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POLICEREFR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OFFICERNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPANNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTPLAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRTEDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERFTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERUSERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSFERDOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSFERDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRNSFERSUB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERREGIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODARVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOBYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMITEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTIFIED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNAWARE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		KINUNKN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALERT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESTOVOI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESTOPAIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNCONCIOUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NORMAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTURBED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SLURRED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REGULAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IRREGULAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUXILLARY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECTAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TREATROOM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBSERROOM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WALKINCLI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECIALCLI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WAITINGAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREGNENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTERPRET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTAPPLIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTUBATED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CSPINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANNULATON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ONOXYGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PULSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		O2SAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PASTHIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HISTPREILL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ONEXAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRMEDI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRSOFINFO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLERGIES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDIGIVEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KINNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KINPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTETDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LMP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLORCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACUITY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACUITYDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINSCALE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAINSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PPAINSCALE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PPAINSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEARTRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRYING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOVEMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCITEMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VERBALEVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADCIRCUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FONTTANELS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ESCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PPAIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ONORMAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDNURSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BPSYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BPDIA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEMPPRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AXILLARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TYMPANIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORALLY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTAL2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FOREHEAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEIGHT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BMIFACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVRESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPINION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INJRYNATUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICINTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DYINGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLERGY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEENTRET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEENTRTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MBDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MBDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUEUENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'DSTAHT',
		timestamps: false
	});
};
