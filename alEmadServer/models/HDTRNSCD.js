/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTRNSCD', {
		SCHCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TRNINGCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TRNGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNINGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEEKDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRBRKHR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRBRKHR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRBRKHR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HDTRNSCD',
		timestamps: false
	});
};
