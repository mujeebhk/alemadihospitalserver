/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBRVPVH', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ATTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASHIERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYDETAL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYDETAL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRENCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTUTILIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTRNTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJUSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMSCREEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DETFAMTSUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETRF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMOPDOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMBILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISADVORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILADJDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHIFTCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFUNDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STMTGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STMTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGPOSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMADVNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREADMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQTRNCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MBRVPVH',
		timestamps: false
	});
};
