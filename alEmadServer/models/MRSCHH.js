/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRSCHH', {
		SCHEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMBRKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBRKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMHOLIDY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOHOLIDY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUNDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MONDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TUESDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEDNESDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		THURSDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRIDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SATURDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MRSCHH',
		timestamps: false
	});
};
