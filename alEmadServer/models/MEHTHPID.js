/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHTHPID', {
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		HPIELEM: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		HPICATCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HPI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HPICATDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAINTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENCOUTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCATDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMPRESSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMPRDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIFCOMCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHFCOMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MEHTHPID',
		timestamps: false
	});
};
