/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MANOTFCN', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ALRTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACKNWLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCHRGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HUSKPING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHARMACY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEITARY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHYSTHPY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRNTOFFI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSRANCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILACKNW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HUSACKNW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHAACKNW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEIACKNW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHYACKNW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRNACKNW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSACKNW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALERTDES: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MANOTFCN',
		timestamps: false
	});
};
