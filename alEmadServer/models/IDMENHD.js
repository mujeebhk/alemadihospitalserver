/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDMENHD', {
		MNUSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DAYNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MEALID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CHOICEID: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		WEEKNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFAULTCHO: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IDMENHD',
		timestamps: false
	});
};
