/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDTMPLH', {
		POSTMPLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPTEXT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPTEXT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPTEXT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDTMPLH',
		timestamps: false
	});
};
