/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEPRESCD', {
		PREID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAVORID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSUNTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPENSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISUNIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NSTARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTRXDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOREFILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRTDEA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHRONIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PURQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NDCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFIDEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPICODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQDPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHRIZ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQFCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UINPOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALTITMCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTITMNM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTSTREN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTDOSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTFREQU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHRONICID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEMORN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSENOON: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSEEVEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSENGT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSEOTHR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IGNREASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSRECPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULTIPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHEDISCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQMULFIX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREONCONLY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCHEDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREQEVERY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQMORN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQNOON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQEVEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQNIGHT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQMIDNI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQEARMOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREQDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FIXFREQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSAUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		READYBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDDEPTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UDIQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UDIDISPQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPERDOSE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STOCKUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UDILASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAPERDOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATLSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCONTNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCONTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCONTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCONTUSR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSEQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRNMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DENIALCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMADDAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VOLUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOSPERUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVERALRYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITRNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OWNMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLUCOSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TITNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DROPFCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLOWRT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STRUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVERYHRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEFOREMEAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRNSIGNS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSDOSAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVHIGDOS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVHIGDOS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DECLOWLVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLUCLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTPRESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOURVOLM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCDFULLY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATEINS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VERIFYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VERIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VERIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VERIFYTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSUETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MEPRESCD',
		timestamps: false
	});
};
