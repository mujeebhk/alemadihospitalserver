/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APRSTRT', {
		KEY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATA1: {
			type: "BINARY",
			allowNull: false
		}
	}, {
		tableName: 'APRSTRT',
		timestamps: false
	});
};
