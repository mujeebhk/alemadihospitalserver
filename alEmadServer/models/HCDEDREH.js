/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCDEDREH', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EFFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL1YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL1BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LAVEL1DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL2YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL2BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LAVEL2DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCESSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCDEDREH',
		timestamps: false
	});
};
