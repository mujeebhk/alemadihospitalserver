/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISRATLSD', {
		RATLSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BILCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOSPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREAUTHRED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISPACKAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COSTWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ISRATLSD',
		timestamps: false
	});
};
