/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPMTIMD', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EARNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYEXPAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WIPCOGSAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCPMTIMD',
		timestamps: false
	});
};
