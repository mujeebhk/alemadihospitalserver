/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APVCM', {
		VENDORID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DATEENTR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEEXPR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEFLUP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTCMNT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTCMNT10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVCOM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'APVCM',
		timestamps: false
	});
};
