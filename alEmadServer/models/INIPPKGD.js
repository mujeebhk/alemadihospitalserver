/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIPPKGD', {
		PKGUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELIUNITS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVAUNITS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALUNITS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVLCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PACKPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOSPFTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGCANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOCATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGUTIL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INIPPKGD',
		timestamps: false
	});
};
