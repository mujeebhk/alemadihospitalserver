/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPOBJ', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBJECTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBJECTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTSCORE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBJECTYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QTRLYRVW: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPOBJ',
		timestamps: false
	});
};
