/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CYTRNSD', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TMPFIELD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TMPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXT10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CYTRNSD',
		timestamps: false
	});
};
