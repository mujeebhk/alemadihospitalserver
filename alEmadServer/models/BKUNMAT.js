/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKUNMAT', {
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		UNIQUE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NUMBER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STMTCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTINSTMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYEEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYEENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTNOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTRYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OFXTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECONCILED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKUNMAT',
		timestamps: false
	});
};
