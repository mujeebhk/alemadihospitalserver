/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPITMPRI', {
		CURRCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PRILCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DECIMAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PURCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARKUP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALESPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PURUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALESUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FMTITMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPMARKUP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPSALPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WHSALPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSCOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRNMARKUP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPITMPRI',
		timestamps: false
	});
};
