/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARITT', {
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CODETAX: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARITT',
		timestamps: false
	});
};
