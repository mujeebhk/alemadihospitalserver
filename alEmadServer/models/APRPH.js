/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APRPH', {
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDRECURR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTMTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEEFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPIRETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEEXPIRE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YTDCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YTDAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMITTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TERMSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDDISTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTDISTSET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWCALCTAX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISTNET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODE1099: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMT1099: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCHEDKEY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDLINK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWTAXBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXBSECTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTTAXINCL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXCL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATENEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTGEN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPENCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPENAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTIDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTCNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTCNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTPOSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APRPH',
		timestamps: false
	});
};
