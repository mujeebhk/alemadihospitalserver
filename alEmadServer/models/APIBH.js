/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APIBH', {
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMITTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTTRX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDTRX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVCSTTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDRNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PONBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRTINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVCAPPLTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEASOF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWMANRTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCHRATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORIGRATEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TERMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTERMOVRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCAVL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWTAXBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCALCTX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMT1099: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISTSET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTALLOCTX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPAYMSCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTOTDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTGROSDST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDPPD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPSTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRECTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODEPAYPPD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODEVNDGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TERMSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDISTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ID1099CLAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTGROSTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTEXPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTAXTOBE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXOUTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTREC1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTREC2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTREC3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTREC4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTREC5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTEXP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTEXP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTEXP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTEXP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTEXP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILLAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRILLDWNLK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTRECDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXPDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ERRBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ERRENTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTPPD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDSTDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEPRCS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSBWTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSBNTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSCBASE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRTGINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAPPLYTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGTERMS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWRTGDDTOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGAMTOV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRTGRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXBSECTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORIGCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DETAILCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCEAPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APVERSION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXVERSION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXRTGRPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURNRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTXCTLRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPERC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRATERC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSE5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTGROSHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMT1099HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPPDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTVEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'APIBH',
		timestamps: false
	});
};
