/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLGSSEG', {
		USER: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWCANSEE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEGID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEGINID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENDID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLGSSEG',
		timestamps: false
	});
};
