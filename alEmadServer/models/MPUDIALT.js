/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPUDIALT', {
		UDISEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTITMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALTITMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPUDIALT',
		timestamps: false
	});
};
