/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPITMBCD', {
		DUMMY: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROUPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRICELIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLSALPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PLUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BCSALPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STOCKUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACSETDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLSALPRIIP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BCSALPRIIP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPITMBCD',
		timestamps: false
	});
};
