/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKDISTD', {
		DSETCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'BKDISTD',
		timestamps: false
	});
};
