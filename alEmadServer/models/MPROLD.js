/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPROLD', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MINQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SSL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEADDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPROLD',
		timestamps: false
	});
};
