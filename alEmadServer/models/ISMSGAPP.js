/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISMSGAPP', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPONSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MSGCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ISMSGAPP',
		timestamps: false
	});
};
