/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSSCHUSR', {
		SCHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		SHIFTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		COUNTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		USER: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHFRTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHFTOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSSCHUSR',
		timestamps: false
	});
};
