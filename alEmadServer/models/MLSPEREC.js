/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLSPEREC', {
		TSTSITELOC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLENUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEMENTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECMNABBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECSITE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECESITEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECESITEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECESITETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLSPEREC',
		timestamps: false
	});
};
