/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPRFPMH', {
		PRFPRMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFGRPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMTITLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPEMPGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCPRFPMH',
		timestamps: false
	});
};
