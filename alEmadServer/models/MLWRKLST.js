/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLWRKLST', {
		WORKLSTNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKLSTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKLSTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDOUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRASPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRASPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WRKFROMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WRKTODT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKLSTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLWRKLST',
		timestamps: false
	});
};
