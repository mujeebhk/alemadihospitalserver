/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCATTNDH', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESENTDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABSENTDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTHOLIDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWORKDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWORKHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OTHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTOTHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WORKHOLIDY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPOFFS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPOFFHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALGRACE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALLEAVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAIDHOLDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNPAIDHOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCLEAVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLPROCVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISCONVLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONVELEAVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISERLYLEAV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EARLYLEAVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPROCDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WKOFFOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLOSEBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PENDUNPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCATTNDH',
		timestamps: false
	});
};
