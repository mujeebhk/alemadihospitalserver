/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOSURREQ', {
		SURADVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQFRMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQTOTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEINSR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MOSURREQ',
		timestamps: false
	});
};
