/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DAVALH', {
		VCLMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCMPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLCGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLCGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEMBERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACLICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLAIMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLAIMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPLRCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLRDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CMPPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BROWSETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECTALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'DAVALH',
		timestamps: false
	});
};
