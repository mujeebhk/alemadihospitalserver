/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLSAMPLD', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TSTSITELOC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SPECMNABBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SPEMENTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABBRDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISAUTHRIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TESTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMSUFFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMREQVOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECINSREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECINSPAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLLECTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COLLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COLLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REJECTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REJECTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REJECTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECEIVEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQVOLUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FASTINGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FASTINGTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTSHNAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAMPLENUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTRECEIPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TESTSHNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REJREASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMCOLLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMCOLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMRECLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMRECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TSTSITEDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAMSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SITEMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SITECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SITEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BARCODTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPENUMGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COLLECTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REJECTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQUIPMENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ONHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCOMPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEMENINS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDOUTTST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMUI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKLISTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STORTRACOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STORTRADES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFTESTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREDISPTCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREDISPBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREDISPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREDISPTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TESTUSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANONHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISESOREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATEXIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURRSTATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRSTATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRSTATTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISSIGNIFIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MARKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRASPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRASPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFSAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LBLABBREV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRINTYES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLSAMPLD',
		timestamps: false
	});
};
