/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLRESHIS', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESULT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESCONFBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESCONFDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESCONFTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLRESHIS',
		timestamps: false
	});
};
