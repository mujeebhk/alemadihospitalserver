/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDMNUSCH', {
		SCHECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MENUTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MENUCATGRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MENUTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DUMMYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IDMNUSCH',
		timestamps: false
	});
};
