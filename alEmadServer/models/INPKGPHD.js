/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INPKGPHD', {
		PKGUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERPRVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INSURED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETURNQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRPPRICE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDPRVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALEPRIUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGORDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PKGLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INPKGPHD',
		timestamps: false
	});
};
