/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDINTEMD', {
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RECREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EMPID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		EVALPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVALPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EVLPICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL1SCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL2SCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL3SCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL4SCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PARAMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFIRMED1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMED2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFIRMED3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFIRMED4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBPANLCD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLCD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLCD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBPANLCD4: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDINTEMD',
		timestamps: false
	});
};
