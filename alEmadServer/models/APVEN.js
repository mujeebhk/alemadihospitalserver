/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APVEN', {
		VENDORID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHORTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATESTART: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDPPNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSTRE4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESTTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODEPSTL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANKID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRTSEPCHKS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTSETID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TERMSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DUPINVCCD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DUPAMTCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DUPDATECD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXRPTSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBJTOWTHH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXIDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXNOTE2SW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLASID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTCRLIMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALDUET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALDUEH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPPDINVT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPPDINVH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DTLASTRVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALLARV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTOPENINV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPPDINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTINVPAID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYSTOPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVCHI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBALHI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVHIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBALHIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTIV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTPA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTDI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTADJ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDINVCHI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVCHILY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTINVHIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWTHTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHILT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHILT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWTHLYTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTIVT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTCRT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTDRT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTPYT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTDIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTADT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHIH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHIH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWTHHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHILH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBALHILH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWTHLYHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTIVH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTCRH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTDRH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTPYH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTDIH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTLASTADH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDTAXREGI5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWDISTBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECHECK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVGDAYSPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVGPAYMENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVPDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVPDTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTNBRCHKS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWTXINC1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXINC5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAIL1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEBSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTACFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGTERMS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEXTCUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEGALNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHK1099AMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APVEN',
		timestamps: false
	});
};
