/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCSALDED', {
		SALUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DEDUCTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDUCTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDORID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCSALDED',
		timestamps: false
	});
};
