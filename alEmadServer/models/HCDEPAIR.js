/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCDEPAIR', {
		LEAVEUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RELATNAME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELGAIRFARE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCDEPAIR',
		timestamps: false
	});
};
