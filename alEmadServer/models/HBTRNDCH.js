/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBTRNDCH', {
		DOCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIONTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIONNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PURGEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUPERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUPERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELDOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBTRNDCH',
		timestamps: false
	});
};
