/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDAPPEXD', {
		APPSEQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSITION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPANY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPERIENCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESPONSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPMNTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDAPPEXD',
		timestamps: false
	});
};
