/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DATMPCRD', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'DATMPCRD',
		timestamps: false
	});
};
