/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSFREVST', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DEPTDOCCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FREEUPTO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCEDURE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLSEFREVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSFREVST',
		timestamps: false
	});
};
