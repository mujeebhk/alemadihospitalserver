/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUPRESCH', {
		PREID: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EPICODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEDITYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRINTDEA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISALLERGY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISREACTMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMINTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIAGNOS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGNOS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGNOS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGNOS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFADIG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPPOS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPPRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IUPRESCH',
		timestamps: false
	});
};
