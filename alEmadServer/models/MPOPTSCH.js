/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPOPTSCH', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRETLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRETPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRETNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECVLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECVPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECVNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYVLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYVPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYVNEXTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCRDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCRDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCRDNXT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPOPTSCH',
		timestamps: false
	});
};
