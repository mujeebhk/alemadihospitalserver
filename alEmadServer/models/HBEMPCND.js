/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPCND', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CONTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNSTARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVBKNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSALPRNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLDSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVEDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEAVESAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPCND',
		timestamps: false
	});
};
