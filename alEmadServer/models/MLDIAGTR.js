/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLDIAGTR', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTEREDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TESTSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG3: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLDIAGTR',
		timestamps: false
	});
};
