/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DARPTATT', {
		CLAIMSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTRESULT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NONTEXTRLT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CYTORSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HISRSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MICRRSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADRSLTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSUMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARD1YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARD2YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'DARPTATT',
		timestamps: false
	});
};
