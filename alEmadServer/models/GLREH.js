/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLREH', {
		RECID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATESTART: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWEXPIRE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEEXPIRY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATERUN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEMAINT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCELEDGER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWREVERSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		JRNLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWEXRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHEDKEY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDLINK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ZEROSRCCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'GLREH',
		timestamps: false
	});
};
