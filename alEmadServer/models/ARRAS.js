/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRAS', {
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTVSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEAINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARIDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDSUSP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASHLIAB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTDISC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTWROF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNRLGAIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNRLLOSS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RLZDGAIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RLZDLOSS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTADJ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RTGACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RNDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARRAS',
		timestamps: false
	});
};
