/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APDSD', {
		DISTSET: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGLACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWDISCABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTPCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'APDSD',
		timestamps: false
	});
};
