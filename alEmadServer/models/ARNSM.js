/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARNSM', {
		IDNATLACCT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTYR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTPERD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTADJ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTWROF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTINTT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRIF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTINVCPD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDYTOPY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDRHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPYMHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSCHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWRFHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINTHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRIFHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVPD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDRTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPYMTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSCTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWRFTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINTTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRIFTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVPTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRFHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRFTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARNSM',
		timestamps: false
	});
};
