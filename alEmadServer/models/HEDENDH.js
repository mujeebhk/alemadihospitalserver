/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HEDENDH', {
		DESEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODULENO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODULE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATETO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMDOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TODOCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NORECORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOERROR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FUNCTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HEDENDH',
		timestamps: false
	});
};
