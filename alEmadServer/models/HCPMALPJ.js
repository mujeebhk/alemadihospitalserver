/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPMALPJ', {
		ALLOCSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTENDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STDUNTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STDEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COSTVARNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYEXPACNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABBURACNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTOMER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIMECARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJSTYLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCPMALPJ',
		timestamps: false
	});
};
