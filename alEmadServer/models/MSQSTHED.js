/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSQSTHED', {
		QUESTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUESTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUESTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MANDATORY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPDEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDICALQUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSQSTHED',
		timestamps: false
	});
};
