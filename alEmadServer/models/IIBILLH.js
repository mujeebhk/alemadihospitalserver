/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIBILLH', {
		BILLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELREM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECEIPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULTIPKG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINORPROC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPATPBLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDCOLLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATDISCPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CSPROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNDOBILYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARRYFWDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CFRECPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUARANTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDAUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULTIADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPONSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJUSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITDEPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECDEPTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPISDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTRECAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCOUNTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCOUNTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MVPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMBILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OBILLAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OACCTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATEPISODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NSHIFTCODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANNETECST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCOMPRES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPOSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		XMLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHOWBILLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOAPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMCPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORGCRDAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAPPL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SECPLANYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCOMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCNCOMPRES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SINVGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SINVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SINVNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SXMLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPORTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SECBILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLAIMVALID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCLMVALID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAPPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAPPROVDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGTYPCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IIBILLH',
		timestamps: false
	});
};
