/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHPTTSD', {
		CARETSKCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CLCCONDCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLCCONDDES: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEHPTTSD',
		timestamps: false
	});
};
