/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDPOSDFH', {
		POSDEFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSREQCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CTCNEGO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECURITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEWPOSNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VACANTPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPFROM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEFROM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGETO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASSIGNBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSIGNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESPTEXT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPTEXT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPTEXT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPLACEPOS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPREQNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STEPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HDPOSDFH',
		timestamps: false
	});
};
