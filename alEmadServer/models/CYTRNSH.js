/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CYTRNSH', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CYTONO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRELIMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRELIMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRELIMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALIDATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMDREASON: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CYTRNSH',
		timestamps: false
	});
};
