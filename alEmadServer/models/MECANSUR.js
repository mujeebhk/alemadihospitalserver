/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MECANSUR', {
		ADVICEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQFRMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQFRMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQTOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BOOKEDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BOKFRMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BOKTOTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROOM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BOOKINGSTA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MECANSUR',
		timestamps: false
	});
};
