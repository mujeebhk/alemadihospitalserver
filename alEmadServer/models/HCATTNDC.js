/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCATTNDC', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESENTDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ABSENTDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTHOLIDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWORKDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTWORKHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTOTHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTOTHOURS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WORKHOLIDY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPOFFS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPOFFHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALGRACE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALLEAVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAIDHOLDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNPAIDHOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPENBALA: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTSTHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCATTNDC',
		timestamps: false
	});
};
