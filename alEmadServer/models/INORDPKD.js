/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INORDPKD', {
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREAPPROV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INORDPKD',
		timestamps: false
	});
};
