/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRTSTSCR', {
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		TESTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FROMDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TODAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRAGETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAGETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MRTSTSCR',
		timestamps: false
	});
};
