/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MBBILLB', {
		BATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INPRVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INPRVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETTLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SETTLETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETTLEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSSTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATDEDUCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALANCEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRANID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMINDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMINTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCHDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCHTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FAMILYBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATIENTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRENCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGNOS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGNOS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGNOS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGNOS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTLSTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INCREMENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRINTBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHTDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATIENTPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELRMKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISMAPCOMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETTLEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUBSCRIBBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPLYRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECALC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDCOLLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORBILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORADMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDISCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDCLSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATDISCPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFPACKCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BEDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADJUSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PTCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGPATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGCMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPOSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVALNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOAPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPLDISBAS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPLDISPAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPLDISPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPLDISAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLAIMVALID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SELECTOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVIDCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MBBILLB',
		timestamps: false
	});
};
