/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCMLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCMPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCMBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCMNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCUNINO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESCPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESCPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESCPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESCPNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESCSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLRPFC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRPROICD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRFINICD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRICP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLRAUTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRCSSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REQPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQNEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTOYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WLFLISSUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QSTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FEDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRNTONISUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IUOPT',
		timestamps: false
	});
};
