/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLRVNET', {
		ACCTID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		FSCSYR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETOBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV11: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV12: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETEQV13: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'GLRVNET',
		timestamps: false
	});
};
