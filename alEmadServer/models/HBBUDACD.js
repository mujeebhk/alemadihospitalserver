/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBBUDACD', {
		FSCALYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		BGTSETCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ELMNTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRDAMT01: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT02: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT03: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT04: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT05: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT07: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT06: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT08: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT09: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT11: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRDAMT12: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUATOT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUATOT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUATOT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUATOT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBBUDACD',
		timestamps: false
	});
};
