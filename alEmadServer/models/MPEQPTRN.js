/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPEQPTRN', {
		TRNNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EQPTRNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODOFPAY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESPMSG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ECRRECPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARDHOLDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CARDEXPIRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MSGNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEMNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRDATETIME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSSRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TERMINALID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRDAPPLBL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRDAPPID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VERIFYRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRMINLSTAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRYPTOGRAM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRDBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MID: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MPEQPTRN',
		timestamps: false
	});
};
