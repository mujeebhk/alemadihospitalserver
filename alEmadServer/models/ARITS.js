/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARITS', {
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDUOM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTYR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTPERD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTSALE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SALEDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTGRO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVGDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYSOLD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARITS',
		timestamps: false
	});
};
