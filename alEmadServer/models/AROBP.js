/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('AROBP', {
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTPAYMNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPSTNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEEXCHHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWOVRDRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDMEMOXREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWINVCDEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATELSTSTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDPREPAID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATERMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STMTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PYMCUID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'AROBP',
		timestamps: false
	});
};
