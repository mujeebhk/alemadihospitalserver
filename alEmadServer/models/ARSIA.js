/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARSIA', {
		IDSTDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBILL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTMTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELSTPRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEEFF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEEXPR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTOTCHRG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWLASTDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDCUSTSHPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTSHIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTINST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDORDENBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUSTPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDJOBNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRTINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETERM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTLSTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODESLSP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCTSASPLT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWTXBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWMANLTX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXSTTS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTXBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNOTTXBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTOTL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVTOTL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPAYMTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMSCD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YTDCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHEDKEY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHEDLINK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPIRETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHPVIACODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHPVIADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTINVCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATENEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPENCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPENAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTDATEINV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTIDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LSTCNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTCNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTPOSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARSIA',
		timestamps: false
	});
};
