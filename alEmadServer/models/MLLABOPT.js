/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLLABOPT', {
		MLLABOPT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTLAPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTIVPFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAMNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTSAPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTISAFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WORKSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WORKNOLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKNOPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKNOVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKNOBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDOUTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENDOUTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SENDOUTPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDOUTVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDOUTBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKLSTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKLSTPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKLSTVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WORKLSTBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESOORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ESOORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ESOORDPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESOORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ESOORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPTCHLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPTCHPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPTCHVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPTCHBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABREFPHY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABREFPHYD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KITSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		KITLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		KITPRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KITVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KITBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFRANGEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMSTAMPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADVANCEREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANASTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SNDOUTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLLABOPT',
		timestamps: false
	});
};
