/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TXRATE', {
		AUTHORITY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		BUYERCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMRATE10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTMAINT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'TXRATE',
		timestamps: false
	});
};
