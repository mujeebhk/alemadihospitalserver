/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEARUL', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CALTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCLEARUL',
		timestamps: false
	});
};
