/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDORDPAT', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INST10: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDORDPAT',
		timestamps: false
	});
};
