/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPATOVD', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROTOID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPBALANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSPATOVD',
		timestamps: false
	});
};
