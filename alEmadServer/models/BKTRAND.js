/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKTRAND', {
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		LINE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCEAPP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDREMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEREMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BTCHNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYORID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYORNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDORNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCEAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATESPREAD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DDLINK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECSTATCHG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECCLEARED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECONCILED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECPENDING: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHKFORM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OFXTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCPERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SRCEDOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANREVINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVINVC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LSTRECSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'BKTRAND',
		timestamps: false
	});
};
