/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APOBLJ', {
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGLACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTINVCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUEHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUETC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWDISCABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGOAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGOAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGDISTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBSERT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMTRT5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLRTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLRTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPRTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPRTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTLASTSEQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APOBLJ',
		timestamps: false
	});
};
