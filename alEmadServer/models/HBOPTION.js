/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBOPTION', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPCONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CMPFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPCODEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODEVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PFCODELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PFCODEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PFCODEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PFCODEVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPPANNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISPPFNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OVRCSTCNTR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGID1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGID2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGID3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGID4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGID5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGID6: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGID7: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGID8: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLSEGID9: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLSEGNO9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFPAYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFRECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BPLACSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOGOPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DBTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PASSWD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATABASE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TNSSERVICE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TABLESPACE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SMTPSERVER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SMTPSPORT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USESSL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTREMDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECONREMDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OBJWEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMAILID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAILPWD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WPSPAYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFRPTMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RPTMANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHFRSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BDGTPHYSYR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STRTPHYMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENDPHYMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BDGTAMNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRANCHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VERSNTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GROUPREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCEXPEART: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCEXPSART: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HRMGREMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISDCNOMANU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOADCOMTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LANGUAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PJCINTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIMCRDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHFROSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCREQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCODELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCODEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCODEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISCODEVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRVCODELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRVCODEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRVCODEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRVCODEVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPRNUMBER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOBOFERLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		JOBOFERPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOBOFERBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOBOFERVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOBSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HBOPTION',
		timestamps: false
	});
};
