/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBPRFPMD', {
		PRFPRMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ENTRYID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PASSSCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HBPRFPMD',
		timestamps: false
	});
};
