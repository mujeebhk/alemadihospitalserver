/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAPREADM', {
		IPPRENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPPRECOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADFROMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADTODT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIEFCOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMITED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATEID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFPROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFOTHER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYOFSTAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLOCKBED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREDITTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSPOLCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOAPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLGRPDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBSCRIBBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPLYRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROXAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPROVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MAPREADM',
		timestamps: false
	});
};
