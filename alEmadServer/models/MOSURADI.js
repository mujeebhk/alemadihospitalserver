/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOSURADI', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURNAME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURNAMEOTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANESCODE2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCHRGWASH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATRECOTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATENTOTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANESTCLRTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SKININCTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATLEVOTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTRFWDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENERLCONS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANESTHCONS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROPHYANTI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKINTSTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ANTIBIOTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MOSURADI',
		timestamps: false
	});
};
