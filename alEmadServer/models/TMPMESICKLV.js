/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TMPMESICKLV', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EHRSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NODAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTES: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'TMPMESICKLV',
		timestamps: false
	});
};
