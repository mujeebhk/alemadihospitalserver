/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARRBC', {
		IDCYCL: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTVSW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTMNTN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTSTMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTINTT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTSTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYSCYCL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STREET1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STREET2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STREET3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STREET4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARRBC',
		timestamps: false
	});
};
