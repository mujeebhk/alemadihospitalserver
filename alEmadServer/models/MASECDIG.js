/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MASECDIG', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SECDIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECDIGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRIMARYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MASECDIG',
		timestamps: false
	});
};
