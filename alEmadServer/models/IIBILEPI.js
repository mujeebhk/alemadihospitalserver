/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIBILEPI', {
		EPISDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTCOMPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTCOPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARMTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IIBILEPI',
		timestamps: false
	});
};
