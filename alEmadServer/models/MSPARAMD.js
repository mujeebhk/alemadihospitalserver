/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPARAMD', {
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		PARAMNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YERORMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRILOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRIHIGH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTOVALID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOVALLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTOVALHIG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCOMM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFCRILOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFCRIHIGH: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPARAMD',
		timestamps: false
	});
};
