/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARR01', {
		IDR01: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWMULTCURN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRCURCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWEDITIMPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWFRCLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTARCVDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWEDITCUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXCUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECLDRCU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEPERDCU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWACCUITEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWEDITITEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXITM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECLDRIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEPERDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWACCUSLSP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWEDITSLSP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXSAP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECLDRSA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEPERDSA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ATRRVALSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWKEEPDTLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWACCUCUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWEDITSUB: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'ARR01',
		timestamps: false
	});
};
