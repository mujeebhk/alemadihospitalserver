/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEAVRT', {
		APPLICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLICDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORISE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORISE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORISE4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORISE5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVEUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETURNAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLEAVRT',
		timestamps: false
	});
};
