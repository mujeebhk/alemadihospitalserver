/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAPREAUT', {
		IPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGICALYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRGFACTOR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MAPREAUT',
		timestamps: false
	});
};
