/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPEAR', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EARNCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EARNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLOCATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTPERCENT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BYCOMPANY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALCMETHOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REVTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUTAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTOPJC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRATEPHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DHOURPDY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BANKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISAPLEASAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPEAR',
		timestamps: false
	});
};
