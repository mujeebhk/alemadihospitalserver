/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIPORH', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVNPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARMAPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVNCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARMACTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PKGCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRANDPTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRANDCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABORDSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADORDSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHDLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RSCHDLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDCOLLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARMALOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHARMADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVENLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVENDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NURSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NURSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMORDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGERY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGCANCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELREA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COPAYMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEINDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISREFERIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSFERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSFERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRAOPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADVERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISAMTCOLL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIZED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGPATPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDPRVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIRTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UTILIZED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADMTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BEDCLSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLICYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMSCREEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADDTEXIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISDTEXIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGPOSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHREQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INREQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERRET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABDTEXIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EPISODENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAPPRV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALUTIL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISVERBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISWARDSTK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INIPORH',
		timestamps: false
	});
};
