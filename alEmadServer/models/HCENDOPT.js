/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCENDOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT7: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT8: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPONENT9: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FSERVRANG1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FSERVRANG2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FSERVRANG3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FSERVRANG4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FSERVRANG5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBDAY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBDAY2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBDAY3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBDAY4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBDAY5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTSERVRAN1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTSERVRAN2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTSERVRAN3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTSERVRAN4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTSERVRAN5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBDY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBDY2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBDY3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBDY4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBDY5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDPAYRULE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENDPAYDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRATUITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YRSROUND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRATVALYRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKYRSDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLSERVRAN1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLSERVRAN2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLSERVRAN3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLSERVRAN4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLSERVRAN5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBDY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBDY2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBDY3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBDY4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBDY5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYPREVSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FTLSRVRAH1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTLSRVRAH2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTLSRVRAH3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTLSRVRAH4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FTLSRVRAH5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBDY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBDY2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBDY3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBDY4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBDY5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDCOMP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDCOMP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDCOMP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDCOMP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDCOMP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEDCOMP6: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CALCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALFRMYR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMYR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMYR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMYR4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMYR5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMDAY1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMDAY2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMDAY3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMDAY4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CALFRMDAY5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELIGIBPER1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBPER2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBPER3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBPER4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGIBPER5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBPR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBPR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBPR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBPR4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TELIGIBPR5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBPR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBPR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBPR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBPR4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LELIGIBPR5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBPR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBPR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBPR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBPR4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TLELIGBPR5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REGELIGREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRMELIGREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REGMINYRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRMMINYRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINSLABREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINSLABYRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCENDOPT',
		timestamps: false
	});
};
