/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INSTDORH', {
		SORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SORDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCHLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FREQUENCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SORDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SORDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXFUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INSTDORH',
		timestamps: false
	});
};
