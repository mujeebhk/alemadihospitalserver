/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLKITHD', {
		KITSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		KITCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		KITDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUANTITY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RUNTESTNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLKITHD',
		timestamps: false
	});
};
