/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARCSM', {
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTYR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTPERD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTCR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTADJ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTWROF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTINTT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRIF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTINVCPD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTDTOPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDRHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWROFHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINTTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRIFHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVPDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCRTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDRTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPAYMTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTWROFTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINTTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRIFTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINVPDTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTBARVALT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AVGDAYSPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRFHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRFTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARCSM',
		timestamps: false
	});
};
