/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLRVL', {
		CURNCYCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DFLTRVLCD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FORCEREVAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SGMTSLCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SGMTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMPERIOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPERIOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FSCSYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JRNLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONVRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTGAIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTLOSS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GVALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LVALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLRVL',
		timestamps: false
	});
};
