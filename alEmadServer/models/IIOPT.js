/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IIOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILREQPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOADRBCODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOMODREG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOMODCON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RBILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RBILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOADCONB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILNOTNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILNOTLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILNOTPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILNOTBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILNOTVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECVONEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECVOLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECVOPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECVOBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECVOVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYVONEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYVOLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYVOPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYVOBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYVOVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLADJNXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLADJLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLADJPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLADJBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLADJVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUOTENEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QUOTELEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QUOTEPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUOTEBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUOTEVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPPATDEP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCLRAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHMORDCLR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRDNOREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCANNXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCANLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCANPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCANBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCANVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUTMAXCAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DFTPAYREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DFTPAYCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DFTPAYDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPPATRECAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPSHTDISAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPPKGDISAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCRDNXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCRDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCRDPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCRDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCRDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPCRDSTCAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPDSCWRTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UTILIZEADV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CARYFWDADV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULTVSTOPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULTVSTADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MULTVSTCLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLOCKMVPPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPONSNXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPONSLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPONSPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPONSBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPONSVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERCAPNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERCAPLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERCAPPFX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERCAPBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERCAPVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRINTBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRINTREC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASHCRDSEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISGVNEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXECPSRVLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LDRGREFVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISBYUSRMN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMDMANDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHGWRTOFAC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUPRESSOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDONCTLAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOAPAPPMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPRICEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALWCONSEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRTOPORDAM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LDNONCNORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LDORDBAS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTLMENTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALWPENORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAUTHMND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSETCHGYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSENCUYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKSERVBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IIOPT',
		timestamps: false
	});
};
