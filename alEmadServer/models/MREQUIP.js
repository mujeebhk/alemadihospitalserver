/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MREQUIP', {
		EQUIPECODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EQUIPEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEXTMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPIRYDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MREQUIP',
		timestamps: false
	});
};
