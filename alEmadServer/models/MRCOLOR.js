/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRCOLOR', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREESLOT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESERVSLOT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENTTEST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKOUT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAINTEANCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OUTORDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MRCOLOR',
		timestamps: false
	});
};
