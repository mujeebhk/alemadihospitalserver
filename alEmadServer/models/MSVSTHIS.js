/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSVSTHIS', {
		PATNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISBILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISORDERD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISPHRORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRVVSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVVSTPRO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REGCHARGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUBSCRIBBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPLYRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYRNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOAPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREVSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REVSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FREEVSTAVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECPLANYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSVSTHIS',
		timestamps: false
	});
};
