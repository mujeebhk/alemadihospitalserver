/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSSPECAL', {
		SPECCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEMPERATOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PULSE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESPRATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEIGHT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEIGHT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADCIRCUM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECIALTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DHAMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMAPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WORKLIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ARDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSSPECAL',
		timestamps: false
	});
};
