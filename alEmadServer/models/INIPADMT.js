/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIPADMT', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADMODETX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFOBFRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFOBFRMX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSPFRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSPFRMX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCOMPBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LANGSPOK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LANGSPKX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHLGRAD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAVTYHOB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REASNADM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATHRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BATHCLT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NURSCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEALTIM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VSTHRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TVCTRL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NONSMOK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATEQUIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TELPHN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FALPREC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PARNTCR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FALRISK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HANDWSH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WRNGGWN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRESTFD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEOFMOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCKLVAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCKLNOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHBKPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHBKPAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHBKOTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATHBKTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HEPB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BCG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMOTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMOTHTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALEQUIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		KEPTWPAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SNTHOME: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SNTHOMWT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GVNGUST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CRIB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SLEPALN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SLEPPARN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SLEEPTIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HRSOFSLP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOPRBLM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHNGBHVR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HYPACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WITHDRWN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPRESSN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHLANXI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NERVSNS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANXIETY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNCOPERTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANGRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGITATED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMBATIV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEGLECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHRTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LIVINGWT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELATNSHP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOOFPPL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FATHJOB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSURAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FEEDNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOILTN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRESSN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GROOMN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WALKIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANFR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOBLTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RCCHMOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RCCHDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SLFCRNP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MUSCNOPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MUSCDEFR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MUSCCONT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MUSCAMPU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MUSCBEDR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MUSCPAIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MUSCNTPH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEQPNON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEQWALK: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEQPWLC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEQPTRF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEQPBTD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEQPRST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEQPOTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USEQPOTX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLODGRP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HDBLDTRSN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSFREC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VDRL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HIV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HBSAG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTDON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		URSUGR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URPROTN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URACTON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URNOTDON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HVS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OTHERS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OROPHRNG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TACTILE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OXYGN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APGRSCOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APGRSCR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APGRSCR5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APGRSCR10: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENTLN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTCARDIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRUGS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURFACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLOOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERIPHLN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NALOXN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTUBATD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ETTNTT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRKNG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALLARD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOMEDHS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SNTPHARM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOVERPHY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SNTHOM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPINFDIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHICKPX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHICKPXDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TUBERCL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TUBERCDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERTUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERTUSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MENINGS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MENINGDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INFLUNZ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFLUNDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VACCOMP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHESTCRFE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLOODGRP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOTHOSP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRAVIDA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABORTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STILBIRTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOTHAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREGANCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPEDEV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LSCSS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LMP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EDD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIABETES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LIQUOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UTIAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHOAMTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHOILLNE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIV1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HBSAG1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VDRL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HVSARS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TREATMET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TORCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MOHABRH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CTG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PETHFINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPISPI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADCRFE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEIGHT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LENGT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCKLNA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLDTRSTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNRECTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADLTFEEDG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADLTHYGNE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADLTTOLET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADLTAMBUL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FEDPEDTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOIPEDTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRSPEDTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRMPEDTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WAKPEDTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRNPEDTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBPEDTXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NEODOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEOTIMEOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PETHFINEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPISPIYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TREATHIS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PSTSURHIS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MUSCOTHR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MUSCOTHRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TURNINBED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRESGROOM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMUNCTON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFCNTDEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANTIRESTC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BABDELOTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		KONAKIONY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFCTXTP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFCTXTA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECUSHIST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BBYBRNOTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPINFADT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INFCNTADT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANTBIORES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NEEDSPRV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTDEPNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INIPADMT',
		timestamps: false
	});
};
