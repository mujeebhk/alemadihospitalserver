/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARGRO', {
		IDGRP: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWACTV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDAUTOCASH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDBILLCYCL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDSVCCHG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDLNQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWBALFWD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETERM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWCROVRD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CDCRLMCUR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMCRLMCUR1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CDCRLMCUR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMCRLMCUR2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CDCRLMCUR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMCRLMCUR3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CDCRLMCUR4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMCRLMCUR4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CDCRLMCUR5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMCRLMCUR5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXSTTS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXSTTS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODESLSP1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODESLSP5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PCTSASPLT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PCTSASPLT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRTSTMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCHKLIMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCHKOVER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERDAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OVERAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVERAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVERAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVERAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OVERAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARGRO',
		timestamps: false
	});
};
