/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRRADOPT', {
		MRRADOPT: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTRAPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTIVPFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAMNEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TEXTSAPF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTISAFLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCHSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADPREFIX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADBODY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SMSSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MRRADOPT',
		timestamps: false
	});
};
