/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOPOSTOP', {
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSTO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NILBYHRS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IVFLUIDS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLOODTRANS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OBSERVTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FLUIDOPMON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBILITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FURTMANAG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DTDISCHRGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATFAMINST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPAPPOINT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLNDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPLNTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MOPOSTOP',
		timestamps: false
	});
};
