/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBSHFRST', {
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ATTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTABBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMTM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTM1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMTM2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTM2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMTM3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTM3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACTLHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHFRSSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROSGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISWEEKOFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMDATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMDATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMDATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOCATNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWAPPED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBSHFRST',
		timestamps: false
	});
};
