/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUNCURR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MULTICURR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMALLLOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITMNEG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NONINVITM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCWISEDOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAPVENDOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCNAMES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GITLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEPER1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEPER2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEPER3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEFITMST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFPAYMODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTPHACL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DNAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITMBARSEP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLWCENTRY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MRAUTHLVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POAUTHLVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRAUTHLVL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRAUTHREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PKSPRICE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEXPNODAYS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCACCESS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DMREXPNODY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVNOMANDT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DITMBTCHRQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPMARGIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DPURUOM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHIPPRICE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKUPAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSETACQAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODETYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTVRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROLOUTPO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROLOUTPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROLOUTMR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRTCASETYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDLCURREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MCOPT',
		timestamps: false
	});
};
