/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPSTOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SALESPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCNTPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DISCACCTCD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPDISCACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHRMIPACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHRMOPACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUSTINVPST: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPSTOPT',
		timestamps: false
	});
};
