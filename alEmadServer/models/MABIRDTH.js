/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MABIRDTH', {
		BIRTHSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIRTHORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BABYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEIGHTGMS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISTOWN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INFADDRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDUFATHER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EDUMOTHER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEMOTMR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BABYDELIV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREGDUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DBFRDELV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAFTDELV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DSMOKE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DTOBACO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DARECANUT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DALCOHOL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TWINSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FTHROCPTN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MTHROCPTN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOWNVLAGE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIAGNOSIS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MABIRDTH',
		timestamps: false
	});
};
