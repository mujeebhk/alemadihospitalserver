/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CSFSC', {
		FSCYEAR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERIODS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QTR4PERD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BGNDATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE11: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE12: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BGNDATE13: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE6: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE7: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE8: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE9: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE10: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE11: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE12: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE13: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUSADJ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUSCLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'CSFSC',
		timestamps: false
	});
};
