/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYEQUPAD', {
		EQUCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MACANTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTBIOCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTBIODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYEQUPAD',
		timestamps: false
	});
};
