/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOINTOPH', {
		INTOPSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTRAOPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTRAOPDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SURDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TIMEIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TIMEOUT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSNOTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELECTROSUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ELECTOTHER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESELECTRO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESELOTHER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELECTROCUT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELECTROCOU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKINBEFORE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKINAFTER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URINCATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URINOUTPUT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		URINCOLOR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IRRIGNS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IRRIGLYCIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IRROTHERS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IRRTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WASHNS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WASHGLYCIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WASHOTHERS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WASHTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BLOODLOSS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRAINTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHESTTUBE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TUBESIZ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRAINOTHER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRAINLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DRESSINGBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MOINTOPH',
		timestamps: false
	});
};
