/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLORDERH', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPORDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASETRNYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WARDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDESTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHEDULEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDVALIDFD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDVALIDTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHECKINYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHECKINPRV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKINDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHECKINTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDCANCEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNCLREMARK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATPREPYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDISTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDISTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OLDESTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEINDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CRITICAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCEPTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INITIALORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATFASTING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUSIND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDBYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMENDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COLLECTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESCONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESCONFBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESCONFDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESCONFTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMMENDNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESVALBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESVALDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESVALTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESVERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESVERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESVERTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDSAMSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPELBLGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISESOREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISREFERIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISREPPRINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTPRTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTPRTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTPRTTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRDPRTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MRDPRTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MRDPRTTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBEPRINT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISSHOW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREPAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREPATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTVALIDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LASTVALITI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLORDERH',
		timestamps: false
	});
};
