/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRLONGH', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		RADCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		REPORTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MRLONGH',
		timestamps: false
	});
};
