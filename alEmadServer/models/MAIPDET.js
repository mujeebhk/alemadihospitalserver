/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MAIPDET', {
		IPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MLC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MLCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MLCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MLCREMARK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHIEFCOMP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELREA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCREGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCDPLACE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WOUNCERDAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLSTANAME: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MAIPDET',
		timestamps: false
	});
};
