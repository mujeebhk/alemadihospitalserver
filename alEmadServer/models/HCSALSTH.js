/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCSALSTH', {
		SALUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		JOINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DESIGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MODEPAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EARNBASIC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESENTDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETSALARY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EARNAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDTAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BNKSTATGEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCSALSTH',
		timestamps: false
	});
};
