/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ISNETVPA', {
		NETWRKCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		BILCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILCODDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ISNETVPA',
		timestamps: false
	});
};
