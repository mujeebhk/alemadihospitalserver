/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TXSTATED', {
		GROUPID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		VERSION: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		TXAUTHNUM: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDITLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXABLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURTAX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURAUTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MINTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXBASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCLUDABLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXRTGCTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECOVERABL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATERECOV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTRECOV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPSEPARTE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCTEXP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LIABILITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLASS01: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS02: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS03: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS04: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS05: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS06: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS07: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS08: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS09: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLASS10: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS01: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS02: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS03: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS04: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS05: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS06: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS07: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS08: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS09: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DCLASS10: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HCLS01DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS02DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS03DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS04DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS05DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS06DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS07DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS08DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS09DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HCLS10DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS01DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS02DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS03DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS04DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS05DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS06DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS07DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS08DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS09DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DCLS10DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXRATE0101: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0102: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0103: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0104: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0105: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0106: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0107: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0108: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0109: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0110: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0201: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0202: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0203: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0204: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0205: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0206: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0207: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0208: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0209: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0210: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0301: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0302: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0303: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0304: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0305: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0306: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0307: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0308: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0309: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0310: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0401: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0402: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0403: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0404: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0405: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0406: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0407: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0408: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0409: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0410: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0501: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0502: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0503: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0504: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0505: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0506: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0507: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0508: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0509: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0510: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0601: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0602: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0603: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0604: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0605: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0606: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0607: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0608: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0609: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0610: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0701: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0702: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0703: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0704: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0705: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0706: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0707: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0708: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0709: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0710: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0801: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0802: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0803: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0804: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0805: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0806: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0807: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0808: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0809: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0810: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0901: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0902: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0903: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0904: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0905: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0906: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0907: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0908: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0909: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE0910: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1001: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1002: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1003: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1004: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1005: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1006: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1007: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1008: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1009: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRATE1010: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'TXSTATED',
		timestamps: false
	});
};
