/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSINVLAB', {
		INVHCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEFLOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSINVLAB',
		timestamps: false
	});
};
