/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSTADT', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CONCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCORE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'DSTADT',
		timestamps: false
	});
};
