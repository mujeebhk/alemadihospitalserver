/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APP01', {
		IDP01: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWMULTCURN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWEDITIMPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWFRCLST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTARCVDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWEDITVNDR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXVND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODECLDRVN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODEPERDVN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWBNKVNDR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CMNTDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CNTRVALSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NAMECTAC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTPHON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWKEEPDTLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWACCUVNDR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWCALCTAX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWEDITSUB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXBSECTL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXCTLRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTXDTLCLS: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APP01',
		timestamps: false
	});
};
