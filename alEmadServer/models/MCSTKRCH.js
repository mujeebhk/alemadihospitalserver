/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCSTKRCH', {
		RECSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STKRCNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMACSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOACSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOGROUP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORISED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCPACPOST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MCSTKRCH',
		timestamps: false
	});
};
