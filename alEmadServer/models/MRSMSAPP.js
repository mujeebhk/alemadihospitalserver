/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MRSMSAPP', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SMSYESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SMSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATMOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVMOB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRITRESON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REQUESTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MSGSTATUS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MESGSENTDT: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MRSMSAPP',
		timestamps: false
	});
};
