/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPTHIST', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DELIVERY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GESTTION: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APGARS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APGARS1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		HEADCIRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEGHT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEGUNITI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEGHT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WEGUNIT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRESTFED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FORMULA: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REACIMM1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REACMED1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHROMED1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHROILL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEVPROB1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOSP1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGERY1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJURY1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCHOOL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPORT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPORT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPORT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REACIMM2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REACMED2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHROMED2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHROILL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEVPROB2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOSP2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGERY2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJURY2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDICT2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALCOHOL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMISSUE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEXACTV2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRGUSE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIVITY2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NUTRITN2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOBACO2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHILD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIVITY3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NUTRITN3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOBACO3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ALLERGY3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHROILL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHROMED3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOSP3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SURGERY3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INJURY3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDICT3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALCOHOL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMISSUE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRGUSE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FAMILY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REACIMM3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REACMED3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEVPROB3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEXACTV3: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSPTHIST',
		timestamps: false
	});
};
