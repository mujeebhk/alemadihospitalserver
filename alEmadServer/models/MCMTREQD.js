/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCMTREQD', {
		MATREQSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MATREQLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYREQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYAUTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETEPO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QTYRECEIVE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYOUTSTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POCREATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POCREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYINPO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYOUTPO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PURENQIR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ITEMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MCMTREQD',
		timestamps: false
	});
};
