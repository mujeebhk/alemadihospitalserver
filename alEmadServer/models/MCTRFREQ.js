/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MCTRFREQ', {
		STKTRFSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYREQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRFTODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYOUTSTD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYTRF: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REQLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MATREQNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UOMREQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MCTRFREQ',
		timestamps: false
	});
};
