/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TXCLASS', {
		AUTHORITY: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CLASSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		CLASSAXIS: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		CLASS: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXEMPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'TXCLASS',
		timestamps: false
	});
};
