/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('METOPICD', {
		TEMPLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		TOPCATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		TOPICCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOPCATDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'METOPICD',
		timestamps: false
	});
};
