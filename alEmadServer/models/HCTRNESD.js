/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCTRNESD', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CALYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CALMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DIFFVAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HCTRNESD',
		timestamps: false
	});
};
