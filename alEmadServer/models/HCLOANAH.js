/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLOANAH', {
		LOANUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLLNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLLNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURRMONDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANCLOSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANSKIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOANPAYMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELREAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL2YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL2DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL2BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL2TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL3YN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL3DATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL3BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL3TIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STARTMONTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STARTYEAR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALLOAN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPORTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYLOANSAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACCENTRYYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLOANAH',
		timestamps: false
	});
};
