/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBEMPDSP', {
		EMPNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISPTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEVERITY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'HBEMPDSP',
		timestamps: false
	});
};
