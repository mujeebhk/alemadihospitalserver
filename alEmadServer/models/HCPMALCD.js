/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCPMALCD', {
		ALLOCSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTALDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALHOURS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALCTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPJCHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTNPJCHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALOCHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPJCCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTNPJCCST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTSTDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTSTPJCST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTSTNPJCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTENDCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STDEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPOSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCPMALCD',
		timestamps: false
	});
};
