/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEHINSCL', {
		BLDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRMBLDSUG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOBLDSUG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWDOSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDIUMDOSE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HIGHDOSE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEHINSCL',
		timestamps: false
	});
};
