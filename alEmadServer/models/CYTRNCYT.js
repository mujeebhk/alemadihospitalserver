/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CYTRNCYT', {
		SAMPLENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CYTONUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NUMOFSPEC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECADEQCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMDEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMDEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REASON1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REASON2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTERRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPTYPECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROSSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'CYTRNCYT',
		timestamps: false
	});
};
