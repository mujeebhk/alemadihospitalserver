/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DABILCMT', {
		BILLNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SYMPTOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SYMPTOM1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENTS2: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'DABILCMT',
		timestamps: false
	});
};
