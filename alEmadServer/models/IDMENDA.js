/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDMENDA', {
		MNUSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		DAYNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MEALID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CHOICEID: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		WEEKNO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVING: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOMID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IDMENDA',
		timestamps: false
	});
};
