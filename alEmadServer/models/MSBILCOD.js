/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSBILCOD', {
		BLCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLCODGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEDDEPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLCODTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STDFEE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FINANCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINANCEABR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMUNECOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DAYCARYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REGBILYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INCLUDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPPATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPDIFFRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPBEDREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPSURREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPPATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMGPATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERVICEMED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLCASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTREQYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVIDERTP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SERGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERCODDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCODEREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CASHDSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASHDSDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREINCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREINDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREDSCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREDSDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLIPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLIPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASHDSICOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASHDSIDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREINICOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREINIDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREDSICOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREDSIDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBDEPTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBDEPTDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCDEDIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DSERVTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISDISCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECDEPT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MOHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISFREEVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILDESCCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTTOACCT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTTYPNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FREFOLWUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LONICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOOTHYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALWZPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OBSERVYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERTAXREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TIMEBASEYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PERUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EQPIDMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHOWLABRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ICDVALDREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CPTDRGMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		QOCSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSBILCOD',
		timestamps: false
	});
};
