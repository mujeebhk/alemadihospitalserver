/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CYOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'CYOPT',
		timestamps: false
	});
};
