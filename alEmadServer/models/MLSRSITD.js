/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLSRSITD', {
		TSTSITELOC: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SPECMNABBR: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SPEMENTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TESTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECSITE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REJSITE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABSECCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABSECDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMPLENUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAMSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURRSTATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRSTATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURRSTATTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REJREASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REJECTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REJECTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REJECTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SENDOUTTST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECESITEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECESITEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECESITETI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MLSRSITD',
		timestamps: false
	});
};
