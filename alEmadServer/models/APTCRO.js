/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APTCRO', {
		BTCHTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTENTR: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		OPTFIELD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VALUE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LENGTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DECIMALS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALLOWNULL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VALIDATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APTCRO',
		timestamps: false
	});
};
