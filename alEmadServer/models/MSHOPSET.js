/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSHOPSET', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASUALTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CASUALTDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMERGNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMERGPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSHOPSET',
		timestamps: false
	});
};
