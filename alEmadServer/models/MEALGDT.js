/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEALGDT', {
		ALRCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALLERGEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIONAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALERGNCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEALGDT',
		timestamps: false
	});
};
