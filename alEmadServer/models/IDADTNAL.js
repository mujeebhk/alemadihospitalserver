/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IDADTNAL', {
		ADTNLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEALTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MEALDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOMID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WARDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BEDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MENUTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDERDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCESSBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROCESSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCESSTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IDADTNAL',
		timestamps: false
	});
};
