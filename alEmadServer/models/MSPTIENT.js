/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPTIENT', {
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RACE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AGEYYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHONEOFF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONEEXT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOHOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPE1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DECEASED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DECDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SSN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPREMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILREMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHOTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUARTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HEADCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REGFLAG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PASSPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISAEXP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISATYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BABYBORN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHILDOF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BIRTHTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONRELN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTSTATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTCNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UHID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FRTVSTCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDPMINO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NETADVANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MULPACKADV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMPLOYER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENCONSREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BLODGRP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATCAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATOTHER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATIDTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATIDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATIDOTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOBYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GUESTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTMGVST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENCONSENT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TALUK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTRICT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WALKINTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WALKIN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFERADDR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATFTPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFBYCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MARKERCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISREFERIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ISREGEXP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REGEXPIRY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ISCHRONIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILADD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTRYNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RACEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GUARADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMCONDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELADESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MARKERDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRYDES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFBYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATFTPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELEGION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FATHERNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPCREDTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IPCREDTBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATOCCUPTN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCATDET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITYNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISMERGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MERGEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MERGEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MERGETIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRIMARYPAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EMRELATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISGNEE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISGDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISGNECONT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISGNENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HIJRIDATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AFIRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMIDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ALASTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERMITNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECPATIDTP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SECPATIDOT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECPATIDNO: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPTIENT',
		timestamps: false
	});
};
