/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOINTRAH', {
		INTRASEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESSDEV: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESDEVOTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSINTBLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		POSTBLOTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANESTHETIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIATHERAPY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIATHESITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DIATHETYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOURNIQUET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOURNSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOURNFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOURNTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECIMENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECIMEN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MARKERSUT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKERTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MARKTYPOTH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ATTAUTOTSP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPESITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHLOSPIRIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AQUAIODIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ALCOIODIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWABCONT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INSTRCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FINALCOUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRAINSIYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DRAINSITU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATHINSYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CATHINSITU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PACKSITUYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PACKSITU: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLINSTR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MOINTRAH',
		timestamps: false
	});
};
