/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APINTCK', {
		RECID: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHKORPHAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIXORPHAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKSETUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIXSETUP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIXBATCH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHKVENDOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIXVENDOC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRVENDOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOVENDOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHKJOURNAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIXJOURNAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'APINTCK',
		timestamps: false
	});
};
