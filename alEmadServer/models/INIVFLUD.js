/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIVFLUD', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ADMINDT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		EMARLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROUTE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STRENGTH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OVERHRS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEPERHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DROPFCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FLOWRT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IVNURCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IVNURNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOSPERUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STOPALL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VOLOVHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VOLCLN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IVCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEFTOVRVOL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SUSPEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INIVFLUD',
		timestamps: false
	});
};
