/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APPJAID', {
		TYPEBTCH: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQENCE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		GENTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GCNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GCNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GCNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGLACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISTNET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINCLTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTOTTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTOBE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWTAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXREC5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXEXP5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'APPJAID',
		timestamps: false
	});
};
