/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IASTLOG', {
		TRANSNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PENDINGBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PENDINGDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PENDINGTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESCEDULBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESCEDULDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESCEDULTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHECKINBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKINDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHECKINTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHECKOUTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHECKOUTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CHECKOUTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOSHOWBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NOSHOWDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NOSHOWTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SNDTOPRVBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SNDTOPRVDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SNDTOPRVTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUTBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CUTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COPYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPYTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PASTEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PASTEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PASTETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WLBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		WLTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'IASTLOG',
		timestamps: false
	});
};
