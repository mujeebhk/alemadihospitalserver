/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSCLINIC', {
		CLNAME: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDR4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		USEBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTPHO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOFFICAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEBSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOGOPATH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLLOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLADD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLSTAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLCNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLPH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXRATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATELAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRODSNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTKEY: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSCLINIC',
		timestamps: false
	});
};
