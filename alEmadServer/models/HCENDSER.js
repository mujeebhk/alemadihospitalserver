/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCENDSER', {
		DOCNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TERMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEJOIN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESIGDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALEARN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YRSSER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPTWO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FORFEIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINALSET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVGRATAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ELIGDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRTSRTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCENDSER',
		timestamps: false
	});
};
