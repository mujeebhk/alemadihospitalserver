/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INORDERH', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORDEREDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCATIONID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISBILLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHARMATOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PHARMALOC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCHDLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCESSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDFROM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALIDTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPECTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXPECTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFIRMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONFIRMTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANREQBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANREQDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANREQTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOUSITEMAN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRIORITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VISITDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRITICAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXCEPTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEINDAYS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADPRIOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADSCHDLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADEXPCTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADEXPCTTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABDTEXIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADDTEXIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MISDTEXIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INITORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RESCHEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESCHEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESCHEDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMENDED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENTEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ENTEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTEREDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VERIFIEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VERIFIEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VERIFIEDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALWINFRM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALWINTO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COLLECTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANREASON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DECONFIRM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMENDEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMENDEDNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABVERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LABVERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABVERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LABVERTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADVERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RADVERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RADVERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RADVERTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		USERDEPT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTOCANDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOCANREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOCANYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOCANDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTOCANTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AKNOWCRI: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AKNOWCRIYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AKNOWCRIDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AKNOWCRITI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECIALCOL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISESOTERIC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUPLCLIINF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISREFERIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FACCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LABREFNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISANTIMICR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOBEVIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VIEWED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VIEWEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VIEWEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STATUSIND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VIEWEDTI: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXFUTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SERTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSFERBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSFERDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSFERRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRAIPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVILBILL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMMAND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORIZED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVDISCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COPAYTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDCOLLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GENBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPEBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONBILLCNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEDUCTCONS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPAYMODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUMSTAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UTILLIM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SPECDEPTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EPISDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BIRTHDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITTY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFPHYDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PREADMORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMSCREEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERRET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PHREQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INREQQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISVERBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMBILAM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRESCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'INORDERH',
		timestamps: false
	});
};
