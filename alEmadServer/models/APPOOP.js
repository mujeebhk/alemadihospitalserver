/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('APPOOP', {
		PAYMTYPE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTRMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTKEY: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMSCHD: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SHOWTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDERBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDVEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VENDPO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDRNBR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNET: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PAYMAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPLY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDTRXTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADJAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STDOCSTR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STDOCDTE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STDOCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OBSDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCPLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STRTVEND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORIGAPLY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PNDPAYTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PNDDSCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PNDADJTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTPNDBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTORIGDOC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWJOB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAPPLYTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWHOLD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TEXTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TEXTREF: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'APPOOP',
		timestamps: false
	});
};
