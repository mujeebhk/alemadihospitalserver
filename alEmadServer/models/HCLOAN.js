/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLOAN', {
		LOANCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MAXLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMMENTS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINAC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLACCTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPORTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRDLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOANLIMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HCLOAN',
		timestamps: false
	});
};
