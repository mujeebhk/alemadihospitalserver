/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARPJD', {
		TYPEBTCH: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQENCE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDCUST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDINVC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTPAYM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDTRANS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDISC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IDBANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRMIT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTLINE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FISCYR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FISCPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SRCETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLREF: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDRATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEEXCHHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TYPEDETL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOSTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOSTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXTNDHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXTNDTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAXHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAXTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLBATCH: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CNTADJNBR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTADJTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSCHCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDSCTCUR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWRTGRATE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RTGAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VALUES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LONGSERIAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PAYMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PAYMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDOC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARPJD',
		timestamps: false
	});
};
