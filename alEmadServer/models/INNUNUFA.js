/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INNUNUFA', {
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ASSMTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RISKLEVEL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIABETES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREGNANCY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RENDISHEPA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MALNUTRI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		UNDERWEIGH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HIVAIDS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HTN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COPD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CAD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GIDISORDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EATDISORD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FOODALERGY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHEWPROBLM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHRONCONST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOWALBUMIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VOMITING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIARRHEA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATTOTSCOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETCONSUL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BREASTFED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BRSTFEDFR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRSTFEDDR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BRSTFEDNA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SOLIDS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BOTLFRML: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BOTLTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BOTLAMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BOTLFREQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BOTLNA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TUBFEED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FORMTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TUBFDAMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TUBFDFRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TUBNA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ASISTFED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APETITEPD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORALFED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NGTOGT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NPO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPFORMREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TPN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VLBW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SGA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LGA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUSPCTD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIETSPECL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BMI: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APETITADLT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NUTRSUPP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FEEDDIFF: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OTHERCOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENAPRN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APETSPECL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NUSPSPECL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FEEDSPECL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNOTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTPECL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNOTPD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTXTPD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCNOTNE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCTXTNE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEICH1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WEICH3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PEDGRWCHRT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BIRTHWELST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRWCHRTPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DIFEATDRIN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SPECDIET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTRADIARA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DOCFOODALR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FLTOTRHVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ANEMIA: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INAPDIAGE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SUSMETBO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RICKETS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SEZDISOR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NUTRISNY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RISKPED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RISKNEB: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RISKADLT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'INNUNUFA',
		timestamps: false
	});
};
