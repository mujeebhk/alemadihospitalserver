/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOSCHDD', {
		SCHEDCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ORCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOWORKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DURATION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMBREAKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOBREAKHR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ROOMTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MOSCHDD',
		timestamps: false
	});
};
