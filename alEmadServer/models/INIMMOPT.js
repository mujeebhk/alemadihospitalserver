/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INIMMOPT', {
		DUMMY: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMUNLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMUNPRX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMUNBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMUNVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMUNSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATSCHLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATSCHPRX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATSCHBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATSCHVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATSCHSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		IMMORDLEN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IMMORDPRX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMORDBDY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMORDVAL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IMMORDSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INIMMOPT',
		timestamps: false
	});
};
