/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('INMISRES', {
		ORDUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ORDLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MISDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ORDEREDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CANCELDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCELTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RESSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PRILIMDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRILIMTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRILIMBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FINALDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FINALTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FINALBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDENDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADDENDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VERIYESNO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		VERIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VERIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VERIFYTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STARTTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PROCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOOFUNIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PERUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'INMISRES',
		timestamps: false
	});
};
