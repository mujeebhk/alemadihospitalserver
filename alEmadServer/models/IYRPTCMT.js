/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYRPTCMT', {
		COMENTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMENTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LONG1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LONG4: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IYRPTCMT',
		timestamps: false
	});
};
