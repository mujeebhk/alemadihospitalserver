/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('DSIMGLNG', {
		FALRIANO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		IMAGEID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SEGNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGMENT1: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT2: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT3: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT4: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT5: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT6: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT7: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT8: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT9: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT10: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT11: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT12: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT13: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT14: {
			type: "BINARY",
			allowNull: false
		},
		SEGMENT15: {
			type: "BINARY",
			allowNull: false
		}
	}, {
		tableName: 'DSIMGLNG',
		timestamps: false
	});
};
