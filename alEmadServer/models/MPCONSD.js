/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPCONSD', {
		CONSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONLSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ITEMCAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITEMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BARCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPIRYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CONQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDQTY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		UOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLDUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OLDAVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXAVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTCON: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MANITEMNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRNLSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ACCTSET: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ITMHISSEQ: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHEXP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MPCONSD',
		timestamps: false
	});
};
