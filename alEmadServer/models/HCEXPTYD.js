/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCEXPTYD', {
		EXPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GRADECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LIMTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LIMTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCEXPTYD',
		timestamps: false
	});
};
