/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARJTR', {
		TYPEBTCH: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTITEM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		CNTSEQENCE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TRANSTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONTRACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROJECT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CATEGORY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOURCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSNBR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COSTCLASS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IDDIST: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDGLACCT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDITEM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITMEAS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QTYINVC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOSTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTCOSTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CODETAX1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODETAX5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMTTAX1HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5HC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX1TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5TC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTHC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGAMTTC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RTGDATEDUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ARJTR',
		timestamps: false
	});
};
