/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLBCTL', {
		BATCHID: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACTIVESW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BTCHDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCELEDGR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATECREAT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEEDIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BATCHTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BATCHSTAT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTNGSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEBITTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENTRYCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEXTENTRY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ERRORCNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORIGSTATUS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWICT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWRVRECOG: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'GLBCTL',
		timestamps: false
	});
};
