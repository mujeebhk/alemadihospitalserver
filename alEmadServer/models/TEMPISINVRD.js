/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TEMPISINVRD', {
		INVRSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PLANDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PENDAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPLYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		STATEMNTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STMTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTOSTMT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CNTLINE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PREAPPAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREBALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVLINENO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALDEBIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALCRDIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CURBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGINBAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREADJAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ECLAIMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EANDMYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EANDMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'TEMPISINVRD',
		timestamps: false
	});
};
