/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MLSAMTRK', {
		SAMPLENUM: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ACTIONDT: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		ACTIONTI: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOURCE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ACTIONTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		USERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MLSAMTRK',
		timestamps: false
	});
};
