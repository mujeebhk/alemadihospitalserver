/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IUQUESTH', {
		QSTSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QSTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GROUPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QSTCAT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEINACTV: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATELASTMN: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QSTPATTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IUQUESTH',
		timestamps: false
	});
};
