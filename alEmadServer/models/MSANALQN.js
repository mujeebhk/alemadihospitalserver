/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSANALQN', {
		ANALYTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ANALYTNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEX: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YERORMON: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXVALUE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRILOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CRIHIGH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTOVALID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTOVALLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTOVALHIG: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTORETEST: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTORTLOW: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTORTHIGH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMMINOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOMAXOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMCRIOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOCRIOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAUTOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOAUTOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMRETOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TORETOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FROMAGEDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAGEDAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RANGEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPECLCOND: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMAGEUNT: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'MSANALQN',
		timestamps: false
	});
};
