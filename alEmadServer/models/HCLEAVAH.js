/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCLEAVAH', {
		LEAVEUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPLICDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVEDTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		JOINDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TOTALAPR: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAPPL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORISE1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORISE2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORISE3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RELIGCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUTHORISED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEAVESAPPL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FROMAPPLDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOAPPLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPSUBMIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMPTYPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYETYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CALREMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL2APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL2APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL2APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL3APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL3APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL3APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPORTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPID: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ASSETAUTHO: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AUTHORISE4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL4APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL4APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL4APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORISE5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LEVEL5APBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL5APDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL5APTM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CLEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLEAVEDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OLEAVECODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RPLEMPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETURNAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETURNAPBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETURNAPDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BRANCHCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		YRSSER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LEVEL1CMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL2CMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL3CMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL4CMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEVEL5CMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISAIRTCKET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ARRIVEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPARTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FRMDESTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TODESTION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AIRTKTISUD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AIRTKTREEM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		OPENINGBAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LOCATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AIRLNSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AIRFAREAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCLRREQ: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTCLRDONE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLRDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXTCLRCMT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ELDOCNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LEAVENCASH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETRNAUTH1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETRNAUTH2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETRNAUTH3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETRNAUTH4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETRNAUTH5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RETAUTH1BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETAUTH2BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETAUTH3BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETAUTH4BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETAUTH5BY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETAUTH1DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETAUTH2DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETAUTH3DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETAUTH4DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETAUTH5DT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCLEAVAH',
		timestamps: false
	});
};
