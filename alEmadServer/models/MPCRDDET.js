/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPCRDDET', {
		CREDSEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		BILLNUM: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INCLUDE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREDITAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPLYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BALANCE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREBALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPCRDDET',
		timestamps: false
	});
};
