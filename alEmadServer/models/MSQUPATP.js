/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSQUPATP', {
		PTTPCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		CONCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSTYP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUETYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ABBRVTN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSQUPATP',
		timestamps: false
	});
};
