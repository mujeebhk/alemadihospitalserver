/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MOCHRGH', {
		CHRHUNIQ: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CHETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETUPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SERVGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		UNITDUR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MINUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GRACEPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MINCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MAXUNIT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MAXCHARGE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADCPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RRBILCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RRBILDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MOCHRGH',
		timestamps: false
	});
};
