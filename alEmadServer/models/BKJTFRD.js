/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('BKJTFRD', {
		PSTSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		KEYSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		LINE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BANK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLACCOUNT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCECURN: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SRCEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FAMOUNT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GLACCHG: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLACCOUNTD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SWTAXBL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXAMTCALC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CODETAXGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXVCLSS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXVCLSS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXVCLSS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXVCLSS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXVCLSS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXICLSS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXICLSS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXICLSS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXICLSS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXICLSS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXINCL1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXINCL2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXINCL3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXINCL4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXINCL5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BASETAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAXRC1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAXRC2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAXRC3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAXRC4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		BASETAXRC5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXCALCBASE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMTTAX1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAX5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTXBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNOTTXBL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTTAXTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDOCTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTINCLUDE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXCLUDE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTNETDIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTGRODIST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXPENS1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXPENS2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXPENS3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXPENS4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTEXPENS5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRECVRB1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRECVRB2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRECVRB3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRECVRB4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTRECVRB5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTAMTALOC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXAMT5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALOCRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPNSRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECVBRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXTOTRC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXVERSION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TXCALCRMET: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXPNSACNT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPNSACNT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPNSACNT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPNSACNT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXPNSACNT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECBLACNT1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECBLACNT2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECBLACNT3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECBLACNT4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RECBLACNT5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CODECURNRC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXAU1DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXAU2DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXAU3DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXAU4DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TXAU5DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VCLS1DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VCLS2DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VCLS3DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VCLS4DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VCLS5DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICLS1DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICLS2DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICLS3DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICLS4DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ICLS5DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FUNGRODIS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXEXP1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXEXP2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXEXP3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXEXP4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXEXP5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXRCB1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXRCB2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXRCB3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXRCB4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXRCB5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATETYPERC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATERC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPRC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CURNRCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATERCDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNNETTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNAMTALOC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTOTTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTALLOC1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTALLOC2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTALLOC3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTALLOC4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AMTALLOC5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPNS1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPNS2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPNS3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPNS4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXEXPNS5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECVB1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECVB2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECVB3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECVB4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXRECVB5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLOC1RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLOC2RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLOC3RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLOC4RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TXALLOC5RC: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXALOC1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXALOC2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXALOC3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXALOC4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNTXALOC5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'BKJTFRD',
		timestamps: false
	});
};
