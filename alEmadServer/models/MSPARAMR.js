/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSPARAMR', {
		PARAMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LISTDATA: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		INVCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MSPARAMR',
		timestamps: false
	});
};
