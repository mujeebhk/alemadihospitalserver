/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPPOSH', {
		POSSEQ: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		IPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IDNUMBER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RECEIVEAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		YEAR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PERIOD: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVIDER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REFERENCE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CURRENCY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SUBTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DETDISCSUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTOTAL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEDUCTAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYDISC: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATDEDUCT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPREST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LINES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RECEIPTNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INSCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POLICYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRESNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPLETED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMMULPRES: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXGRPDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXINCLU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXAUTH1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXAUTH5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLASS1: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS2: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS3: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS4: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLASS5: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		TAXCLSDES1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TAXCLSDES5: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TPATRATE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATRATE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TPATAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPBASE5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT1: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT2: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT3: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT4: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TCMPAMT5: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTPATBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTCMPBH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYABLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COMPPAYABL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPATPAY: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCPER: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SHTNETAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ORGPATPBLE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TAXNETTOT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NETTAX: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		FUNCTION: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		RNDOFFAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATPAYARND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TOTALAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		COPAYLIMIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SELECT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		GLBATCHNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GLENTRYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ARBATCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ARENTRYNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DAYENDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTBTCHNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EPISDNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PREAUTH: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COUNTERID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SHIFTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INVGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INVNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMPOSNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADJUSTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FRMPOSDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POLGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREDITAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPOSAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OACCTBTCH: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATEPISODE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RETEXTCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETDISCAMT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETNETECST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETPATRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RETCOMPRES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		INVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRESTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EXTPROV: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		XMLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		EMIRATEID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ORGPOSTYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BILLCUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATETYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RATEDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		EXCHRATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		RATEOPER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		HOMECUR: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SOAPNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		QUEUENO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMPLYRCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPROVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPROVTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SECPLANYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCUSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLGRPCOD: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPLANCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPOLCYNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SCOMPRESP: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SCNCOMPRES: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SINVGENYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SINVDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SINVNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SXMLYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SETTLED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CLAIMVALID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SCLMVALID: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SAPPROVLNO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SAPPROVDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPPOSH',
		timestamps: false
	});
};
