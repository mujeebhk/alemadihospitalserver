/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('GLASV', {
		IDSEG: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SEGVAL: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEGVALDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCTRETERN: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'GLASV',
		timestamps: false
	});
};
