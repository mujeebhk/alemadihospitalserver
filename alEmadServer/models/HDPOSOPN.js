/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HDPOSOPN', {
		DOCNO: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		SEQNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		OPENDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DEPTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DESIGNDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSBUDNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		NEWVACNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		REPVACNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		OCCPPOSNUM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		POSDEFCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		POSDEFDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SKILLDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ISCOMPLETE: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HDPOSOPN',
		timestamps: false
	});
};
