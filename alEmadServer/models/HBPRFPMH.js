/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HBPRFPMH', {
		PRFPRMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFGRPCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFGRPNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMTITLE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PARAMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PRFTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPEMPGRP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TOTMARK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PRVMARK: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUTHORIZE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMMEND: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AMMENDBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AMMENDDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		GRPTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CHECKYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'HBPRFPMH',
		timestamps: false
	});
};
