/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MSFACLTY', {
		FACTYCOD: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FACTYSER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LOCLINK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INACTIVE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ADD1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADD4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CITY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		STATE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHONEEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOCELL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PHOHOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		WEBSITE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ACCLINK: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONTACT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONDESG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONHOME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONPHO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONEXT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONMOB: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONFAX: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONEMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATLAST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MSFACLTY',
		timestamps: false
	});
};
