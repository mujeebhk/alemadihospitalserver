/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('HCERRH', {
		YEAR: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		MONTH: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		TYPE: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOCDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'HCERRH',
		timestamps: false
	});
};
