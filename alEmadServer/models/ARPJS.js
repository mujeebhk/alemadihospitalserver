/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ARPJS', {
		TYPEBTCH: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		POSTSEQNCE: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DATEPOSTED: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DATEBUS: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWPRINTED: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		SWPOSTGL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DATEPOSTGL: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		SWGLCONSL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PGMVER: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'ARPJS',
		timestamps: false
	});
};
