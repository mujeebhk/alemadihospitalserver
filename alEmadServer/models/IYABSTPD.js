/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IYABSTPD', {
		ABSTCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		ANTBIOCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ANTBIODESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABDISKCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ABDISKDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RESDIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTTO: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		INTDISP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENOPER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENFROM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SENDISP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTIP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTICU: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REPORTOP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ZONESIZE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DISCONTENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		REPORTING: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MANUALOPR: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		INTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EXTCOMMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IINTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		IEXTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SINTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SEXTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AINTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AEXTCOMENT: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		RELANTIBIO: {
			type: DataTypes.INTEGER,
			allowNull: false
		}
	}, {
		tableName: 'IYABSTPD',
		timestamps: false
	});
};
