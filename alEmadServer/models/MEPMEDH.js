/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MEPMEDH', {
		IPCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PATNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FROMDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		TODATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CASETYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ENCOUUNIQ: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'MEPMEDH',
		timestamps: false
	});
};
