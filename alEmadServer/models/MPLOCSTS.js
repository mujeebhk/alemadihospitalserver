/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('MPLOCSTS', {
		ITEMCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		LOCCODE: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AVGCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYHAND: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		LTPURCOST: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYPO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		QTYGIT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STOCKUOM: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'MPLOCSTS',
		timestamps: false
	});
};
