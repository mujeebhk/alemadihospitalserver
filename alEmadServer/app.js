var express = require('express'),
errorHandler = require('express-error-handler'),
http = require('http'),
server;
var path = require('path');
var favicon = require('serve-favicon');
var winston = require('winston');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Sequelize = require('sequelize');
var index = require('./routes/index');
var users = require('./routes/users');
var proxy = require('express-http-proxy');
var middleware = require('http-proxy-middleware');
var compression = require('compression');
var cors = require('cors');
var app = express();

// Configure the logging to a file.
var logger = winston.createLogger({
  transports: [
    new(winston.transports.File)({
        name: 'info-file',
        filename: 'alemadi-info.log',
        level: 'info'
    }),
    new(winston.transports.File)({
        name: 'debug-file',
        filename: 'alemadi-debug.log',
        level: 'debug'
    }),
    new(winston.transports.File)({
        name: 'error-file',
        filename: 'alemadi-error.log',
        level: 'error'
    })
]
});

// To stop logs in any of the logging files(Remove comment line)
logger.remove('info-file');
logger.remove('error-file');
logger.remove('debug-file');

//MsSql Config
var sequelize = new Sequelize('test', 'sa', 'dost1234', {
  host: 'localhost',
  dialect: 'mssql',
  port:64568,

  pool: {
      max: 32,
      min: 0,
      idle: 10000
  },
  additional: {
      timestamps: false
          // ..
  }

});

sequelize
  .authenticate()
  .then(function(err) {
      console.log('Connection has been established successfully.');
  })
  .catch(function(err) {
      console.log('Unable to connect to the database:', err);
  });

// view engine setup
app.disable('etag');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(compression());
app.use(cors());
app.use(bodyParser.json({
  limit: "50mb"
}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/alemadiServer', proxy('localhost:8080', { timeout: 60000 }));

//Sample Apis for test
app.use('/testApi', index);
app.use('/testApi', users);


// auto generated models
const models = require('sequelize-auto-import')(sequelize, './sequelize/DbModels', {
  schemas: ['test']
});
// export all the models for your convenience
module.exports.dbModel = models;


//AlEmadi Rest Apis Path Config
var logs = require('./common/loggers')(logger);
var patientRestProvider = require('./restApis/PatientServiceProvider')(app,models,sequelize);
var doctorsRestProvider = require('./restApis/DoctorsServiceProvider')(app,models,sequelize);
var pharmacyRestProvider = require('./restApis/PharmacyServiceProvider')(app,models,sequelize);
var demoApis = require('./restApis/DemoApis')(app,models,sequelize);

app.use('/mobile/patient', patientRestProvider);
app.use('/mobile/doctor', doctorsRestProvider);
app.use('/mobile/pharmacy', pharmacyRestProvider);
app.use('/demoApi', demoApis);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function exitHandler(options, err) {
  if (options.cleanup) console.log('clean');
  if (err) console.log(err.stack);
  if (options.exit) process.exit();
}


//do something when app is closing
process.on('exit', exitHandler.bind(null, { cleanup: true }));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, { exit: true }));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, { exit: false }));

module.exports = app;
