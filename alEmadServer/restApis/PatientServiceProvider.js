module.exports = function(app, dbModels, sequelize) {

    var express = require('express');
    var router = express.Router();
    var async = require("async");
    const logs = require('../common/loggers');
    const RequestHandler = require('./RequestHandler');
    var smsUtil= require('../utils/SmsUtil');
    var otpUtils= require('../utils/OTPUtil');
    var emailUtil= require('../utils/EmailUtil');
    const PDFDocument = require('pdfkit');
    const fs = require('fs');
    var pdf = require('html-pdf');

    var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

     //Patients Registartion
     router.post('/register', function(req, res) {
        sendUserRegistrationForm(req, res, function(response) {
            return res.json(response);
        });
    });

    async function sendUserRegistrationForm(req, res, callback) {

        logs.createLogsByLogLevel(4 ,"sendUserRegistrationForm() called...");
        
        //Birthdate 
        var bdate = new Date(req.body.BDATE);
        var day = bdate.getDate();
        var month = parseInt(bdate.getMonth());
        var year = bdate.getFullYear();
        req.body.BDATE= day + ' ' + mlist[month] + ' ' + year;

        //Gender
        if(req.body.SEX == 1){
            req.body.SEX= 'Male';
        }else{
            req.body.SEX= 'Female';
        }

        //Attachment
        var mailAttachment= [];
        if(req.body.USERIMAGE != null && req.body.USERIMAGE != ""){
            var extension=req.body.USERIMAGE.substring("data:image/".length, req.body.USERIMAGE.indexOf(";base64"));
            var userImage= {
                filename: 'PatientPhoto.'+extension,
                path: req.body.USERIMAGE
            };
            mailAttachment.push(userImage);
        }

        if(req.body.INSURANCEIMAGE != null && req.body.INSURANCEIMAGE != ""){
            var imageExtension=req.body.INSURANCEIMAGE.substring("data:image/".length, req.body.INSURANCEIMAGE.indexOf(";base64"));
            var insuranceImage= {
                filename: 'PatientInsurance.'+imageExtension,
                path: req.body.INSURANCEIMAGE
            };
            mailAttachment.push(insuranceImage);
        }

        //Sending Mail to Al Emadi
        var subject ='Registration Form Submission';
        emailUtil.userRegistarationTemplate(req, function (emailMessage) {

            logs.createLogsByLogLevel(4 ,"Email sending process started");
            emailUtil.sendMail(emailMessage, subject, mailAttachment, function (emailRes) {
                logs.createLogsByLogLevel(4 ,"Is Mail Sent "+ emailRes);
            });
        });
        callback({});
    }
    
    // Patients Login with Otp
    router.post('/sendOTP', function(req, res) {

        logs.createLogsByLogLevel(4 ,'Calling sendOTPForPatientsLogin() for buisness logic inside PatientServiceProvider.js.');
        sendOTPForPatientsLogin(req, res, function(dbReturnValue) {
            if (res.statusCode == 500) {

                res.status(500);
                return res.json(dbReturnValue);

            } else if (res.statusCode == 404) {
                res.status(404);
                return res.json(dbReturnValue);
            }

            res.status(200);
            res.json(dbReturnValue);


        });

    });

async function sendOTPForPatientsLogin(req, res, callback) {
        logs.createLogsByLogLevel(4 ,"sendOTPForPatientsLogin() caleed..The Request is=" + JSON.stringify(req.body));

        // Selecting patients data.
        dbModels.MSPTIENT.findAll({
            where: {
                PATIDNUM: req.body.QatarId
            }
        }).then(async function(patientFromDb) {

            if (patientFromDb.length != 0) {
                var patcode= patientFromDb[0].dataValues.PATCODE.toString().trim();

                var generatedOtp = await otpUtils.generateOtp(patcode);
                var phoneNumberOfPatient = patientFromDb[0].dataValues.PHOCELL;
                var otpMessage= smsUtil.smsTemplates.otpMessageTemplate;
                otpMessage= otpMessage.replace("ADDOTPHERE", generatedOtp);

                smsUtil.send(phoneNumberOfPatient, otpMessage, false,  function(response){

                    if(response){
                        logs.createLogsByLogLevel(4 ,"OTP sent successfully to "+phoneNumberOfPatient);

                        var gender = 'Male';
                        if (patientFromDb[0].dataValues.SEX != 1) {
                            gender = 'Female';
                        }
                        var lastVisitedDate = patientFromDb[0].dataValues.DATLAST;
                        lastVisitedDate = lastVisitedDate.toString().substring(6, 8) + '/' + lastVisitedDate.toString().substring(4, 6) + '/' + lastVisitedDate.toString().substring(0, 4);
                        var responseObj = {
                            "PATIDNUM": patientFromDb[0].dataValues.PATIDNUM,
                            "PATCODE": patientFromDb[0].dataValues.PATCODE.toString().trim(),
                            "PHOCELL": patientFromDb[0].dataValues.PHOCELL.toString().trim(),
                            "DISNAME": patientFromDb[0].dataValues.DISNAME.toString().trim(),
                            "POLICYNO": patientFromDb[0].dataValues.POLICYNO,
                            "LAST VISITED": lastVisitedDate,
                            "GENDER": gender

                        };

                        return callback(responseObj);
                    }else{
                        logs.createLogsByLogLevel(0 ,'Error Occured while sending Otp for patients login');
                        res.status(500);
                        return callback({error: 'Sms Otp sms failed'});
                    }
                });
            } else {

                res.status(404);
                logs.createLogsByLogLevel(0 ,'The alEmadi patient with this patId do not exist');
                return callback(patientFromDb);

            }

        }).catch(function(err) {

            logs.createLogsByLogLevel(0 ,' Fetching patients details failed in sendOTPForPatientsLogin() method inside PatientServiceProvider.js..Please try again.Exception message:' + err);
            res.status(500);
            return callback(err);

        });
    }


    //APi To verify OTP
    router.post('/verifyOTP', function(req, res) {
        verifyOTPForPatientsLogin(req, res, function(otp) {
            return res.json(otp);
        });
    });

    function verifyOTPForPatientsLogin(req, res, callback) {

        otpUtils.verifyOTP(req.body.PATCODE, req.body.OTP, function(isOTPValid){

            if(isOTPValid){
                return callback(true);
            }else{
                res.status(401);
                return callback(false);
            }
        });
    }

    // Patients Lab Reports
    router.get('/labReport/patcode/:patcode', function(req, res) {

        logs.createLogsByLogLevel(4 ,'Calling getPatientsLabReport() for buisness logic inside PatientServiceProvider.js.');
        getPatientsLabReport(req, res, function(dbReturnValue) {
            res.json(dbReturnValue);
        });

    });

     // Download Patients Lab Reports as PDF
     router.get('/download/labReport/patcode/:patcode', function(req, res) {

        logs.createLogsByLogLevel(4 ,'Calling getPatientsLabReport() for buisness logic inside PatientServiceProvider.js.');
        getPatientsLabReport(req, res, function(dbReturnValue) {
            if(res.statusCode != 500 && dbReturnValue.reports.length != 0){

                generatePdfReport(dbReturnValue.reports, res);
            }else{
                return res.json({
                    Error: 'Error occured while fetching patient lab report'
                });
            }
        });
    });

    function getPatientsLabReport(req, res, callback) {

        var patcode = req.params.patcode;
        logs.createLogsByLogLevel(4 ,'Patcode =' + patcode);

        dbModels.MLORDANA.findAll({
            where: {
                PATCODE: patcode 
            },
                attributes: ['DOCNUM','INVHDESC','PARAMDESC','RESULT','NUMRESULT','ALLOWLOW','ALLOWHIGH','REPUNIT','DOCDATE'],
        }).then(function(patientsLabReport) {
            var docDates = [];
            var noOfDocNums = [];
            var noOfInvhDesc = [];
            var finalList = [];

            //Creating the unique DOCNUM and  PARAMDESC list
            for (var i = 0; i < patientsLabReport.length; i++) {

                if (noOfDocNums.indexOf(patientsLabReport[i].DOCNUM) <= -1) {
                    noOfDocNums.push(patientsLabReport[i].DOCNUM);
                    var docDateFormated = patientsLabReport[i].DOCDATE;
                    docDateFormated = docDateFormated.toString().substring(6, 8) + '/' + docDateFormated.toString().substring(4, 6) + '/' + docDateFormated.toString().substring(0, 4);
                    docDates.push(docDateFormated);
                }
                if (noOfInvhDesc.indexOf(patientsLabReport[i].INVHDESC) <= -1) {
                    noOfInvhDesc.push(patientsLabReport[i].INVHDESC);
                }
            }

            for (var i = 0; i < noOfDocNums.length; i++) {
                var patientLabReportByINVHDesc = [];

                for (var j = 0; j < noOfInvhDesc.length; j++) {
                    var tempListForDocNumSort = [];

                    //Iterating the PatientsLab report list and craeting list by DocNum
                    patientsLabReport.filter(createLabReportByDOCNUM);

                    function createLabReportByDOCNUM(patientListObject) {
                        if ((patientListObject.DOCNUM == noOfDocNums[i]) && (patientListObject.INVHDESC == noOfInvhDesc[j])) {
                            tempListForDocNumSort.push({
                                "name": patientListObject.PARAMDESC.toString().trim(),
                                "result": patientListObject.RESULT.toString().trim(),
                                "max": patientListObject.ALLOWHIGH,
                                "min": patientListObject.ALLOWLOW,
                                "unit": patientListObject.REPUNIT.toString().trim()
                            });
                        }
                    }
                    if (tempListForDocNumSort.length != 0) {
                        patientLabReportByINVHDesc.push({
                            "name": noOfInvhDesc[j].toString().trim(),
                            "subtest": tempListForDocNumSort
                        });
                    }
                }
                finalList.push({
                    "name": docDates[i],
                    "test": patientLabReportByINVHDesc
                });
            }

            callback({ "reports": finalList });
        }).catch(function(err) {

            logs.createLogsByLogLevel(0 ,' Fetching patients lab reports failed in getPatientsLabReport() method inside PatientServiceProvider.js..Please try again.Exception message:' + err);
            res.status(500);
            callback(err);

        });

    }

    function generatePdfReport(patientLabReport, res) {

        var html = fs.readFileSync('./utils/temp.html', 'utf8');

        //TODO
        var sampleLabReportData= [];
        var sampleObj={name: 'Haemoglobin', result: '12.0', unit: 'gm/dl', range: '12.0 - 15.0'};
        sampleLabReportData.push(sampleObj);
        sampleObj={name: 'Packed Cell, Volume', result: '12.0', unit: 'gm/dl', range: '12.0 - 15.0'};
        sampleLabReportData.push(sampleObj);
        sampleObj={name: 'Total Leucocyte Count TLCa', result: '12.0', unit: 'gm/dl', range: '12.0 - 15.0'};
        sampleLabReportData.push(sampleObj);

        createPdfTable(sampleLabReportData, function(labResult){
            
            html= html.toString().replace("replaceLabTableHere", labResult);
            pdf.create(html).toStream(function (err, stream) {
                if (err) return console.log(err);
    
                res.setHeader('Content-Disposition', 'attachment; filename=LabReports.pdf');
                res.setHeader('Content-Type', 'application/pdf');
                stream.pipe(res);
            });
        });
    }

    function createPdfTable(labReport, callback) {

        //TODO
        var tableString= '';
        for(var i=0; i<labReport.length; i++){
            tableString +='<tr>';
            tableString +='<td>'+ labReport[i].name +'</td>';
            tableString +='<td>'+ labReport[i].result +'</td>';
            tableString +='<td>'+ labReport[i].unit +'</td>';
            tableString +='<td>'+ labReport[i].range +'</td>';
            tableString +='</tr>';
        }
        callback(tableString);
    }

    // Patients Appointment
    router.get('/getAppointmentsList/patcode/:patcode', function (req, res) {

        logs.createLogsByLogLevel(4 ,'Calling getPatientAppointments() for buisness logic inside PatientServiceProvider.js.');
        getPatientAppointments(req, res, function (dbReturnValue) {

            res.status(res.statusCode);
            res.json(dbReturnValue);
        });

    });

    function getPatientAppointments(req, res, callback) {

        logs.createLogsByLogLevel(4 ,' getPatientAppointments() Called');

        // Selecting doctors booked slots.
        dbModels.IANEWAPP.findAll({
            where: {
                CLIENTCODE: req.params.patcode
            },
            order: [
                ['TRANSNO', 'DESC']
            ], limit: 10
        }).then(function (patientLat10AppointmentList) {

            for(var i=0; i<patientLat10AppointmentList.length;i++){
                patientLat10AppointmentList[i].dataValues.APPDATE= getStandardDateFormat(patientLat10AppointmentList[i].dataValues.APPDATE);
                patientLat10AppointmentList[i].dataValues.APPTIME= getStandardTimeFormat(patientLat10AppointmentList[i].dataValues.APPTIME);
            }

            callback(patientLat10AppointmentList);
        }).catch(function (err) {

            logs.createLogsByLogLevel(0 ,'Error occured while  fetching patients appointment List' + JSON.stringify(err));
            res.status(500);
            callback(err);

        });
    }


    // Patients Appointment
    router.post('/appointment', function (req, res) {

        logs.createLogsByLogLevel(4 ,'Calling createPatientAppointment() for buisness logic inside PatientServiceProvider.js.');
        createPatientAppointment(req, res, function (dbReturnValue) {

            res.status(res.statusCode);
            res.json(dbReturnValue);
        });

    });


    var tempTransNo= 0;
    function createPatientAppointment(req, res, callback) {

        logs.createLogsByLogLevel(4 ,' createPatientAppointment() Called');
        var createAppointment = [
            function (cb) {

                dbModels.MSPTIENT.findAll({
                    where: {
                        PATCODE: req.body.patcode
                    }
                }).then(function (patientDetails) {

                    var date = new Date();
                    var formatedDate = formatDate(date);
                    var formatedTime = formatTime(date);

                    if (patientDetails[0].dataValues.GENDER == 1) {
                        req.body.TITLE = 0
                    } else {
                        req.body.TITLE = 4
                    }
                    req.body.FIRSTNAME = patientDetails[0].dataValues.FNAME;
                    req.body.SECONDNAME = '';
                    req.body.MIDDLENAME = patientDetails[0].dataValues.MNAME;
                    req.body.LASTNAME = patientDetails[0].dataValues.LNAME;
                    req.body.GENDER = patientDetails[0].dataValues.SEX;
                    req.body.ADDRESS1 = patientDetails[0].dataValues.ADD1;
                    req.body.ADDRESS2 = patientDetails[0].dataValues.ADD2;
                    req.body.ADDRESS3 = patientDetails[0].dataValues.ADD3;
                    req.body.ADDRESS4 = patientDetails[0].dataValues.ADD4;
                    req.body.CLIENTCODE = patientDetails[0].dataValues.PATCODE;
                    req.body.CLIENTDESC = patientDetails[0].dataValues.FNAME + ' ' + patientDetails[0].dataValues.LNAME;
                    req.body.DISTRICT = 0;
                    req.body.COUNTRY = patientDetails[0].dataValues.COUNTRY;
                    req.body.ZIP = patientDetails[0].dataValues.ZIP;
                    req.body.NATION = patientDetails[0].dataValues.NATION;
                    req.body.COUNTRY = patientDetails[0].dataValues.COUNTRY;
                    req.body.HOMEPHONE = patientDetails[0].dataValues.PHOHOME;
                    req.body.MOBILE = patientDetails[0].dataValues.PHOCELL;
                    req.body.DOB = patientDetails[0].dataValues.BDATE;
                    req.body.EMAIL = patientDetails[0].dataValues.EMAIL;
                    req.body.MARITAL = patientDetails[0].dataValues.MARITAL;
                    req.body.AGEYY = patientDetails[0].dataValues.AGEYYY;
                    req.body.AGEMM = patientDetails[0].dataValues.AGEMM;
                    req.body.AGEDAY = patientDetails[0].dataValues.AGEDAY;
                    req.body.VISITTYPE = 1;
                    req.body.CONSTYPE = '';
                    req.body.CONSDESC = '';
                    req.body.ROW = 1;
                    req.body.COL = 1;
                    req.body.MERGEROW = 1;
                    req.body.CANCELRES = 1;
                    req.body.REMARKS = '';
                    req.body.APPSTATUS = 1;
                    req.body.CONSULTTYP = 0;
                    req.body.MEETING = '';
                    req.body.TRANSFER = 0;
                    req.body.CITY = 0;
                    req.body.NOSHOWRES = '';
                    req.body.ROOMCODE = '001';
                    req.body.ROOMDESC = 'DEFAULT CONSULTATION ROOM';
                    req.body.VISITID = '';
                    req.body.CREATEBY = 'USER';
                    req.body.CREATEDT = formatedDate;
                    req.body.MODIFYBY = 'USER';
                    req.body.MODIFYDT = formatedDate;
                    req.body.STANDBY = 0;
                    req.body.WAPPSTATUS = 0;
                    req.body.DEPTCODE = 1;
                    req.body.BILLEDYN = 0;
                    req.body.STANDBY = 0;
                    req.body.WAPPSTATUS = 0;
                    req.body.PATIENTINS = '';
                    req.body.NURSEINS = '';
                    req.body.SPEREQUIRE = '';
                    req.body.FILRECE = 0;
                    req.body.BLOCKSLOTS = 0;
                    req.body.PROVSTATUS = 0;
                    req.body.FILEISSUE = 0;
                    req.body.APPSNO = 0;
                    req.body.CREATETM = formatedTime;
                    req.body.PATIDNUM = patientDetails[0].dataValues.PATIDNUM;

                    cb();
                }).catch(function (err) {

                    logs.createLogsByLogLevel(0 ,' Fetching patients record failed in createPatientAppointment() method inside PatientServiceProvider.js..Please try again.Exception message:' + JSON.stringify(err));
                    res.status(500);
                    return callback(err);

                });

            },
            function (cb) {

                var date = new Date(req.body.appointmentDate);
                var formatedDate = formatDate(date);
                var formatedTime = formatTime(date);
                dbModels.IACLSCDD.findAll({
                    where: {
                        CLINICCODE: req.body.CLINICCODE, APPDATE: {
                            $and: {
                                $eq: formatedDate
                            }
                        }
                    }
                }).then(function (doctorsSchedule) {

                    req.body.AUDTDATE = doctorsSchedule[0].dataValues.AUDTDATE;
                    req.body.AUDTTIME = doctorsSchedule[0].dataValues.AUDTTIME;
                    req.body.AUDTUSER = doctorsSchedule[0].dataValues.AUDTUSER;
                    req.body.AUDTORG = doctorsSchedule[0].dataValues.AUDTORG;
                    req.body.CLINICCODE = doctorsSchedule[0].dataValues.CLINICCODE;
                    req.body.CLINICDESC = doctorsSchedule[0].dataValues.CLINICDESC;
                    req.body.PROVCODE = doctorsSchedule[0].dataValues.CLINICCODE;
                    req.body.PROVDESC = doctorsSchedule[0].dataValues.CLINICDESC;
                    req.body.APPDATE = formatedDate;
                    req.body.APPTIME = formatedTime;
                    req.body.ENDDATE = formatedDate;
                    req.body.ENDTIME = formatedTime;
                    req.body.DEFSLOT = doctorsSchedule[0].dataValues.DURATION;
                    cb();

                }).catch(function (err) {

                    logs.createLogsByLogLevel(0 ,' Fetching doctors schedule record failed in createPatientAppointment() method inside PatientServiceProvider.js..Please try again.Exception message:' + JSON.stringify(err));
                    res.status(500);
                    return callback(err);

                });
            }, function (cb) {
                dbModels.IANEWAPP.findAll({
                    attributes: ['TRANSNO'],
                    order: [
                        ['TRANSNO', 'DESC']
                    ], limit: 1
                }).then(function (dBValue) {
                    var nextVal= parseInt(dBValue[0].dataValues.TRANSNO)+1;
                    logs.createLogsByLogLevel(4 ,'IANEWAPP NextVal = ' + nextVal);
                    req.body.TRANSNO= nextVal;
                    tempTransNo= nextVal; 
                    cb();
                }).catch(function (err) {
                    logs.createLogsByLogLevel(0 ,'IANEWAPP Fetch Failed:' + JSON.stringify(err));
                    req.body.TRANSNO= tempTransNo;
                    tempTransNo= tempTransNo+1;
                    cb();
                });
            }, 
            function (cb) {

                dbModels.IANEWAPP.create(req.body).then(function (createAdminReq) {

                    createAdminReq.save();
                    logs.createLogsByLogLevel(4 ,'Success');
                    callback(createAdminReq);

                }).catch(function (err) {

                    logs.createLogsByLogLevel(0 ,'Create Failed:' + JSON.stringify(err));
                    callback(err);
                })

            }
        ];


        async.series(createAppointment, (err, results) => {
            if (err) {
                logs.createLogsByLogLevel(4 ,'Error occureed at async.series' + err);
                return next(err);
            }
        });


    }


    // Update Patients Appointment
    router.post('/updateAppointmentStatus', function (req, res) {

        logs.createLogsByLogLevel(4 ,'Calling updateAppointmentStatus() for buisness logic inside PatientServiceProvider.js.');
        updateAppointmentStatus(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });

    });

    function updateAppointmentStatus(req, res, callback) {
        logs.createLogsByLogLevel(4 ,' updateAppointmentStatus() Called');

        if (req.body.APPSTATUS != 2 && req.body.APPSTATUS != 3) {
            res.status(400);
            req.body.ERROR_MESSAGE = 'INVALID APP STATUS';
            return callback(req.body);
        } else {

             // Updating Appointment Status
             dbModels.IANEWAPP.update(req.body, {
                where: {
                    TRANSNO: req.body.TRANSNO
                }
            }).then(function (update) {
                logs.createLogsByLogLevel(4 ,'Appointment Status updated successfully');
                callback(req.body);
            }).catch(function (err) {
                logs.createLogsByLogLevel(0 ,'Appointment Status update Failed:' + JSON.stringify(err));
                res.status(500);
                callback(err);
            });
        }

    }

    function mergeObj(obj1,obj2){
        var obj3 = {};
        for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
        for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
        return obj3;
      }

      function getStandardDateFormat(date) {
          var year= date.toString().substring(0,4);
          var month= date.toString().substring(4,6);
          var day= date.toString().substring(6,8);
          return day+'/'+month+'/'+year; 
      }

      function getStandardTimeFormat(time) {

          var hh, mm;
          if (time.toString().length == 8) {
              hh = time.toString().substring(0, 2);
              mm = time.toString().substring(2, 4);
          } else if (time.toString().length == 7) {
              hh = time.toString().substring(0, 1);
              mm = time.toString().substring(1, 3);
          } else {
              hh = time.toString().substring(0, 1);
              mm = time.toString().substring(1, 2);
          }

          var date = new Date();
          date.setHours(hh);
          date.setMinutes(mm);

          var hours = date.getHours();
          var minutes = date.getMinutes();
          var ampm = hours >= 12 ? 'pm' : 'am';
          hours = hours % 12;
          hours = hours ? hours : 12; // the hour '0' should be '12'
          minutes = minutes < 10 ? '0' + minutes : minutes;

          if(hours.toString().length == 1){
            hours= '0'+hours;
          }
        
          if(minutes.toString().length == 1){
            minutes= '0'+minutes;
          }
          var strTime = hours + ':' + minutes + ' ' + ampm;
          return strTime;
      }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('');
    }

    function formatTime(date) {
        var d = new Date(date),
        hour = d.getHours().toString(),
        mm = d.getMinutes().toString(),
            sec = '0000';

            if(mm.length == 1){
                mm= '0'+mm;
            }

        return hour+mm+sec;
    }


    return router;
}