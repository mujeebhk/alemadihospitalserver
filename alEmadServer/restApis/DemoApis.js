module.exports = function (app, dbModels, sequelize) {

    var express = require('express');
    var router = express.Router();
    const fs = require('fs');

    router.post('/user/registration', function (req, res) {

        registerUser(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    async function registerUser(req, res, callback) {

        try {
            var tempData = await readFile();
            var tempJson = JSON.parse(tempData);
    
            tempJson.userData.push(req.body);
            var updatedUserJson= JSON.stringify(tempJson);
            fs.writeFile("tempDb.json", updatedUserJson, function(err, result) {
                if(err) console.log('error', err);
                console.log('User saved successfully');
            });

            callback(tempJson.userData);
          }
          catch(err) {
            console.log(err.message);
          }
       
    }


    async function readFile() {
        var tempData = fs.readFileSync('tempDb.json', 'utf8');
        return tempData;
    }

    return router;
};