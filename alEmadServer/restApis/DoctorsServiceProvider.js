module.exports = function (app, dbModels, sequelize) {

    var express = require('express');
    var router = express.Router();
    var async = require("async");
    var applicationConstants = require('../common/ApplicationConstants');
    const logs = require('../common/loggers');


    // find My Doctor(Patients Appointment Doctors History)
    router.get('/findMyDoctor/patcode/:patcode', function (req, res) {

        logs.createLogsByLogLevel(4 ,'Calling paientsAppointmentDoctorsList() for buisness logic inside DoctorsServiceProvider.js.');
        paientsAppointmentDoctorsList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    function paientsAppointmentDoctorsList(req, res, callback) {

        logs.createLogsByLogLevel(4 ,' paientsAppointmentDoctorsList() Called');

        // Selecting patients appointment history
        dbModels.IANEWAPP.findAll({
            where: {
                CLIENTCODE: req.params.patcode
            }
        }).then(function (patientsAppointmentList) {

            var emptyArray = [];
            if (patientsAppointmentList.length != 0) {
                var doctorsVisited = [];
                for (var i = 0; i < patientsAppointmentList.length; i++) {
                    if (!doctorsVisited.includes(patientsAppointmentList[i].dataValues.CLINICCODE)) {
                        doctorsVisited.push(patientsAppointmentList[i].dataValues.CLINICCODE);
                    }
                }

                if (doctorsVisited.length != 0) {
                    // Selecting doctors list.
                    dbModels.IACLINIC.findAll({
                        where: {
                            CLINICCODE: doctorsVisited
                        }
                    }).then(function (doctorsListFromDb) {

                        logs.createLogsByLogLevel(4 ,'Patient visited Doctors List fecthed successfully');
                        callback(doctorsListFromDb);
                    }).catch(function (err) {

                        logs.createLogsByLogLevel(0 ,' Fetching doctors failed in paientsAppointmentDoctorsList() method inside DoctorsServiceProvider.js..Please try again.Exception message:' + err);
                        res.status(500);
                        callback(err);

                    });
                } else {
                    logs.createLogsByLogLevel(4 ,'Patient has no appointment history');
                    callback(emptyArray);
                }
            } else {
                logs.createLogsByLogLevel(4 ,'Invalid Patcode');
                res.status(400);
                callback(emptyArray);
            }
        }).catch(function (err) {

            logs.createLogsByLogLevel(0 ,'Error occured while  fetching patients appointment List' + JSON.stringify(err));
            res.status(500);
            callback(err);
        });
    }

    // Get Doctors List by departments
    router.get('/getDoctorsListByDepartment', function (req, res) {

        logs.createLogsByLogLevel(4 ,'Calling getDoctorsListByDepartment() for buisness logic inside DoctorsServiceProvider.js.');
        getDoctorsListByDepartment(req, res, function (dbReturnValue) {
            if (res.statusCode == 500) {

                res.status(500);
                return res.json(dbReturnValue);

            }

            res.status(200);
            res.json(dbReturnValue);


        });

    });


    function getDoctorsListByDepartment(req, res, callback) {
        logs.createLogsByLogLevel(4 ,"getDoctorsListByDepartment() caleed.");

        // Selecting doctors list.
        dbModels.IACLINIC.findAll().then(function (doctorsListFromDb) {
            var uniqueDepartmentList = [];
            var finalList = [];


            //Creating the unique Deptartment list
            for (var i = 0; i < doctorsListFromDb.length; i++) {

                if (uniqueDepartmentList.indexOf(doctorsListFromDb[i].DEPTDESC) <= -1) {
                    uniqueDepartmentList.push(doctorsListFromDb[i].DEPTDESC);
                }
            }

            //Creating doctors list by dept.
            for (var i = 0; i < uniqueDepartmentList.length; i++) {
                var doctorsList = [];

                doctorsListFromDb.filter(createDoctorsListByDept);
                function createDoctorsListByDept(doctorsObject) {

                    if (uniqueDepartmentList[i] == doctorsObject.DEPTDESC) {
                        if ((uniqueDepartmentList[i].toString().toUpperCase().trim().indexOf(doctorsObject.CPRONAME.toString().toUpperCase().trim()) <= -1)) {
                            if (parseInt(doctorsObject.DEPTCODE) < 500) {
                                doctorsList.push({ "name": doctorsObject.CPRONAME.toString().trim(), "clinicCode": doctorsObject.CLINICCODE });
                            }
                        }
                    }
                }
                if (doctorsList.length != 0) {
                    finalList.push({ "name": uniqueDepartmentList[i].toString().trim(), "doctors": doctorsList });
                }
            }

            callback({ "departments": finalList });

        }).catch(function (err) {

            logs.createLogsByLogLevel(0 ,' Fetching patients details failed in getDoctorsListByDepartment() method inside DoctorsServiceProvider.js..Please try again.Exception message:' + err);
            res.status(500);
            callback(err);

        });
    };


    // Get Schedule and available appointment time of a doctor
    router.get('/getDoctorsScheduleAndAppointments/clinicCode/:clinicCode/date/:date', function (req, res) {

        logs.createLogsByLogLevel(4 ,'Calling getDoctorsScheduleAndAppointments() for buisness logic inside DoctorsServiceProvider.js.');
        getDoctorsScheduleAndAppointments(req, res, function (dbReturnValue) {
            if (res.statusCode == 500) {

                res.status(500);
                return res.json(dbReturnValue);

            } else if (res.statusCode == 404) {

                res.status(404);
                return res.json(dbReturnValue);

            }

            res.status(200);
            res.json(dbReturnValue);


        });

    });

    function getDoctorsScheduleAndAppointments(req, res, callback) {
        logs.createLogsByLogLevel(4 ,"getDoctorsScheduleAndAppointments() called.");

        var date = new Date();
        var formatedDate = formatDate(req.params.date);
        var doctorsScheduleList = [];

        async.doWhilst(function (cb) {

            // Selecting doctors schedule list.
            dbModels.IACLSCDD.findAll({
                where: {
                    CLINICCODE: req.params.clinicCode, APPDATE: {
                        $and: {
                            $eq: formatedDate
                        }
                    }
                }
            }).then(function (doctorsScheduleListFromDb) {

                if (doctorsScheduleListFromDb.length != 0) {
                    doctorsScheduleList = doctorsScheduleListFromDb;
                    cb();
                } else {
                    res.status(404);
                    return callback({ "ScheduleStatus": 'Schedule not available for the given date' })
                }



            }).catch(function (err) {

                logs.createLogsByLogLevel(0 ,' Fetching patients details failed in getDoctorsScheduleAndAppointments() method inside DoctorsServiceProvider.js..Please try again.Exception message:' + err);
                res.status(500);
                callback(err);

            });
        }, function () {

            // Selecting doctors booked slots.
            dbModels.IANEWAPP.findAll({
                where: {
                    CLINICCODE: req.params.clinicCode, APPDATE: {
                        $and: {
                            $eq: formatedDate
                        }
                    }
                }
            }).then(function (doctorsBookedSlotsFromDb) {

                var finalSlotList = [];
                if (doctorsBookedSlotsFromDb.length != 0) {

                    var doctorsFromWorkHour = formatTimeShow(doctorsScheduleList[0].dataValues.FROMWORKHR).toString().substring(0, 2);
                    var doctorsToWorkHour = formatTimeShow(doctorsScheduleList[0].dataValues.TOWORKHR).toString().substring(0, 2);

                    var slotsList = createTimeIntervalArray(0, doctorsScheduleList[0].dataValues.DURATION, doctorsFromWorkHour, doctorsToWorkHour);
                    var slotIndexToRemove = [];
                    var bookedSlotsIndex = [];
                    var temp = 0;

                    for (var i = 0; i < doctorsBookedSlotsFromDb.length; i++) {
                        var formatedBookedTime = formatTimeShow(doctorsBookedSlotsFromDb[i].dataValues.APPTIME);
                        logs.createLogsByLogLevel(4 ,'Booked Time with 12 hours format' + doctorsToWorkHour);

                        for (var j = 0; j < slotsList.length; j++) {

                            var aa1 = formatedBookedTime.split(":");
                            var aa2 = slotsList[j].split(":");

                            var d1 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa1[0], 10), parseInt(aa1[1], 10), parseInt(aa1[2], 10));
                            var d2 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa2[0], 10), parseInt(aa2[1], 10), parseInt(aa2[2], 10));
                            var dd1 = d1.valueOf();
                            var dd2 = d2.valueOf();

                            if ((d1.getHours() == d2.getHours()) && (dd1 < dd2)) {

                                logs.createLogsByLogLevel(4 ,'Slots Removed' + slotsList.indexOf(slotsList[j]));
                                logs.createLogsByLogLevel(4 ,'Booked and SlotListObject = ' + formatedBookedTime.toString() + ' ' + slotsList[j].toString());

                                if (bookedSlotsIndex.indexOf(j - 1) <= -1) {
                                    bookedSlotsIndex.push(j - 1);
                                }
                                var bookedSlotMinute = d1.getMinutes() + doctorsBookedSlotsFromDb[i].dataValues.DEFSLOT;
                                var slotMinute = d2.getMinutes();
                                if ((bookedSlotMinute > slotMinute) && (bookedSlotsIndex.indexOf(j) <= -1)) {
                                    bookedSlotsIndex.push(j);
                                }
                                break;

                            } else if ((dd1 == dd2) && (bookedSlotsIndex.indexOf(j) <= -1)) {
                                bookedSlotsIndex.push(j);
                                break;
                            }
                        }


                    }//End of outer for loop

                    for (var k = 0; k < slotsList.length; k++) {
                        if ((bookedSlotsIndex.indexOf(k) <= -1)) {
                            finalSlotList.push({ "slotTime": slotsList[k], "availableStatus": true , "CLINICCODE":  doctorsScheduleList[0].dataValues.CLINICCODE});
                        } else {
                            finalSlotList.push({ "slotTime": slotsList[k], "availableStatus": false , "CLINICCODE":  doctorsScheduleList[0].dataValues.CLINICCODE});
                        }
                    }

                    return callback(finalSlotList);

                }else{
                    logs.createLogsByLogLevel(4 ,'There are no booked appointments');
                    var doctorsFromWorkHour1 = formatTimeShow(doctorsScheduleList[0].dataValues.FROMWORKHR).toString().substring(0, 2);
                    var doctorsToWorkHour1 = formatTimeShow(doctorsScheduleList[0].dataValues.TOWORKHR).toString().substring(0, 2);
                    var slotsList1 = createTimeIntervalArray(0, doctorsScheduleList[0].dataValues.DURATION, doctorsFromWorkHour1, doctorsToWorkHour1);                    
                    
                    for (var l = 0; l < slotsList1.length; l++) {
                       
                            finalSlotList.push({ "slotTime": slotsList1[l], "availableStatus": true, "CLINICCODE":  doctorsScheduleList[0].dataValues.CLINICCODE});
                    }

                    return callback(finalSlotList);
                }

            }).catch(function (err) {

                logs.createLogsByLogLevel(0 ,' Fetching patients details failed in getDoctorsScheduleAndAppointments() method inside DoctorsServiceProvider.js..Please try again.Exception message:' + err);
                res.status(500);
                callback(err);

            });

        });
    };

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('');
    }

    function formatTimeShow(h) {

        var minutes = 0;
        if (h.toString().length == 8) {
            minutes = h.toString().substring(2, 4);
            h = h.toString().substring(0, 2);
        } else {
            minutes = h.toString().substring(1, 3);
            h = h.toString().substring(0, 1);
        }
        logs.createLogsByLogLevel(4 ,'Minutes Generated' + minutes);

        if (h.toString().length == 1) {
            h = '0' + h;
        }
        if (minutes.toString().length == 1) {
            minutes = '0' + minutes;
        }

        return h + ":" + minutes + ":00";
    }

    function createTimeIntervalArray(startTime, intervalInMinutes, startHour, endHour) {
        var x = intervalInMinutes; //minutes interval
        var times = []; // time array
        var tt = startTime; // start time
        var ap = ['am', 'pm']; // AM-PM

        //loop to increment the time and push results in array
        for (var i = 0; tt < 24 * 60; i++) {
            var hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
            var mm = (tt % 60); // getting minutes of the hour in 0-55 format

            if (hh.toString().length == 1) {
                hh = '0' + hh;
            }
            if (mm.toString().length == 1) {
                mm = '0' + mm;
            }
            if ((parseInt(hh) <= endHour) && (parseInt(hh) >= startHour)) {
                times.push(hh + ':' + mm + ":00"); // pushing data in array in [00:00 - 12:00 AM/PM format]
            }
            tt = tt + x;
        }

        logs.createLogsByLogLevel(4 ,'Generated Time Slots Array' + times);
        return times;
    }


    return router;
}