module.exports = function(app, dbModels, sequelize) {

    var express = require('express');
    var router = express.Router();
    var async = require("async");
    var applicationConstants = require('../common/ApplicationConstants');
    const logs = require('../common/loggers');


    // Get Pharmacy List of AlEmadi
    router.get('/getPharmacyList', function(req, res) {

        logs.createLogsByLogLevel(4 ,'Calling getPharmacyList() for buisness logic inside PharmacyServiceProvider.js.');
        getPharmacyList(req, res, function(dbReturnValue) {
            if (res.statusCode == 500) {

                res.status(500);
                return res.json(dbReturnValue);

            } else if (res.statusCode == 404) {

                res.status(404);
                return res.json(dbReturnValue);

            }

            res.status(200);
            res.json(dbReturnValue);


        });

    });

    function getPharmacyList(req, res, callback) {
        logs.createLogsByLogLevel(4 ,"getPharmacyList() caleed");

        // Selecting pharmacy data.
        dbModels.MPITEM.findAll({
                attributes: [['QTYHAND', 'quantity'], ['ITEMDESC', 'name']],
        }).then(function(pharmacyFromDb) {
            if (pharmacyFromDb.length != 0) {


                logs.createLogsByLogLevel(4 ,'Pharmacy List fetched successfully');
                return callback(pharmacyFromDb);
            } else {
                logs.createLogsByLogLevel(4 ,'Pharmacy List is empty');
                res.status(404);
                return callback({ "ListNotFound": 'Pharmacy list is empty' })
            }
        }).catch(function(err) {

            logs.createLogsByLogLevel(0 ,' Fetching patients details failed in getPharmacyList() method inside PharmacyProvider.js..Please try again.Exception message:' + err);
            res.status(500);
            callback(err);

        });
    };


    return router;
}