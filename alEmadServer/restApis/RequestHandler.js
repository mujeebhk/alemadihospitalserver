var moment = require('moment');


async function createPatientRequest(requestData) {

    //Date Operations
    var dbFormatBirthDate = await formatDate(requestData.BDATE);

    //Getting Age from BDate
    var birthDate = moment(requestData.BDATE);
    var today = moment();
    var ageObj = moment.duration(today.diff(birthDate));

    requestData.PATCODE = 1000;
    requestData.AUDTDATE = 20190909;
    requestData.AUDTTIME = 123456;
    requestData.AUDTUSER = "E13343";
    requestData.AUDTORG = "AEDAT";
    requestData.DISNAME = requestData.FNAME + " " + requestData.LNAME;
    requestData.ADD1 = "";
    requestData.ADD2 = "";
    requestData.ADD3 = "";
    requestData.ADD4 = "";
    requestData.CITY = "";
    requestData.STATE = "";
    requestData.COUNTRY = 91;
    requestData.ZIP = "";
    requestData.NATION = "";
    requestData.RACE = "";
    requestData.BDATE = dbFormatBirthDate;
    requestData.AGEYYY = ageObj._data.years;
    requestData.AGEMM = ageObj._data.months;
    requestData.AGEDAY = ageObj._data.days;
    requestData.MARITAL = 1;
    requestData.PHONEOFF = "";
    requestData.PHONEEXT = 1;
    requestData.FAX = "";
    requestData.PHOCELL = "";
    requestData.PHOHOME = "";
    requestData.EMAIL = "";
    requestData.PATTYPE1 = "";
    requestData.DECEASED = 1;
    requestData.DECDATE = 1;
    requestData.SSN = "";
    requestData.APPREMIN = 1;
    requestData.BILREMIN = 1;
    requestData.PHOTO = "";
    requestData.GUARCODE = "";
    requestData.GUARTYPE = 1;
    requestData.HEADTYPE = 1;
    requestData.HEADCODE = "";
    requestData.REGFLAG = 1;
    requestData.INACTIVE = 1;
    requestData.PATBAL = 1;
    requestData.VISA = 1;
    requestData.PASSPNO = "";
    requestData.VISAEXP = 1;
    requestData.VISATYP = "";
    requestData.DATLAST = 1;
    requestData.BABYBORN = 1;
    requestData.CHILDOF = "";
    requestData.LOCATCOD = "";
    requestData.BIRTHTM = 1;
    requestData.CONRELN = "";
    requestData.CONTPHONE = "";
    requestData.CONTCELL = "";
    requestData.CONTZIP = "";
    requestData.CONTCITY = "";
    requestData.CONTSTATE = "";
    requestData.CONTCNTRY = "";
    requestData.UHID = "";
    requestData.FRTVSTCOD = "";
    requestData.OLDPMINO = "";
    requestData.NETADVANCE = 1;
    requestData.BILLED = 1;
    requestData.MULPACKADV = 1;
    requestData.EMPLOYER = "";
    requestData.IDNO = "";
    requestData.GENCONSREQ = 1;
    requestData.CONSUSER = "";
    requestData.BLODGRP = 1;
    requestData.PATCAT = 1;
    requestData.PATOTHER = "";
    requestData.PATIDTYPE = 1;
    requestData.PATIDOTH = "";
    requestData.TITLE = 1;
    requestData.DOBYN = 1;
    requestData.GUESTTYPE = 1;
    requestData.FIRSTMGVST = 1;
    requestData.GENCONSENT = 1;
    requestData.TALUK = "";
    requestData.DISTRICT = "";
    requestData.WALKINTYP = 1;
    requestData.WALKIN = "";
    requestData.REFERNAME = "";
    requestData.REFERADDR = "";
    requestData.PATFTPCODE = "";
    requestData.REGDATE = 1;
    requestData.REFTYPE = 1;
    requestData.REFBYCODE = "";
    requestData.MARKERCODE = "";
    requestData.ISREFERIN = 1;
    requestData.ISREGEXP = 1;
    requestData.REGEXPIRY = 1;
    requestData.ISCHRONIC = 1;
    requestData.CREATBY = "";
    requestData.CREATDT = 1;
    requestData.MODBY = "";
    requestData.MODDT = 1;
    requestData.BILADD1 = "";
    requestData.BILADD2 = "";
    requestData.BILADD3 = "";
    requestData.BILADD4 = "";
    requestData.CNTRYNAME = "";
    requestData.NATNAME = "";
    requestData.IDDESC = "";
    requestData.RACEDESC = "";
    requestData.PATTYPDESC = "";
    requestData.GUARADESC = "";
    requestData.VISADESC = "";
    requestData.EMCONDESC = "";
    requestData.RELADESC = "";
    requestData.MARKERDESC = "";
    requestData.COUNTRYDES = "";
    requestData.REFBYDESC = "";
    requestData.PATFTPDESC = "";
    requestData.RELEGION = 1;
    requestData.FATHERNAME = "";
    requestData.OPCREDTBAL = 1;
    requestData.IPCREDTBAL = 1;
    requestData.PATOCCUPTN = "";
    requestData.PATCATDET = "";
    requestData.CITYNAME = "";
    requestData.DISTNAME = "";
    requestData.STATENAME = "";
    requestData.ISMERGE = 1;
    requestData.MERGEBY = "";
    requestData.MERGEDATE = 1;
    requestData.MERGETIME = 1;
    requestData.PRIMARYPAT = "";
    requestData.CREATTM = 1;
    requestData.EMRELATION = "";
    requestData.DISGNEE = "";
    requestData.DISGDESC = "";
    requestData.DISGNECONT = "";
    requestData.DISGNENAME = "";
    requestData.HIJRIDATE = "";
    requestData.AFIRSTNAME = "";
    requestData.AMIDNAME = "";
    requestData.ALASTNAME = "";
    requestData.PERMITNO = "";
    requestData.SECPATIDTP = 1;
    requestData.SECPATIDOT = "";
    requestData.SECPATIDNO = "";

    return requestData;
}


async function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('');
}


module.exports.createPatientRequest= createPatientRequest;