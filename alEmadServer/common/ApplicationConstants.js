module.exports = {

    //Session Details
    AVOID_AUTHENTICATED_SESSION_CHECK: false,
    secretKeyNameForSession: 'AlEmadi@!3Hosp30', //secret key Name for User token
    SessionTokenName: "alemadi_access_token", //token Name used in cookies
    userId: "userId",
    tokenBlackListed: "blackListed",
    sessionExpireTime: 60 * 60 * 24 * 30, //sessionExpireTime set to one month
    enableSecureToken: false, //change to true while deploying to production


    //Utils Control
    DISABLE_SMS: true,
    DISABLE_EMAIL: true,
    DISABLE_NOTIFICATION: true,

    //Otp Details
    OTPApiPath: "http://86.62.218.11/bms/soap/Messenger.asmx/SendSmsXml?xml=<?xml version='1.0' encoding='UTF-8'?>  <request>  <bulk_msg>  <customer_id>2658</customer_id>  <user_id>Alemadi</user_id>  <password>Alemadi@123</password>  <validity_period status='y'>  <day>1</day>  <hours>1</hours>  <mins>10</mins>  </validity_period>  <delivery_report>0</delivery_report>  <message>  <title>AlEmadiHosp</title>  <lang_id>8</lang_id>  <body><![CDATA[ SMSMESSAGEBODY ]]></body>  <values>  <msg_id>1</msg_id>  <mobile_no> REPLACEMOBILENUMBERHERE</mobile_no>  </values>  </message>  </bulk_msg>  </request>",
    ClearOTp: 15856254,
    Base32OTPSecretCode: 'IFAUCQKCIJBEEDost1234!===',

}