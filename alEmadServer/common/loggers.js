module.exports = function (logger) {

    const levels = { 
        error: 0, 
        warn: 1, 
        info: 2, 
        verbose: 3, 
        debug: 4, 
        silly: 5 
    };

    function createLogs(logs) {
        console.log(logs);
    }

    function createLogsByLogLevel(logLevel, logs) {

        console.log(logs);
        if (logLevel == levels.debug) {
            logger.debug(logs);
        } else if (logLevel == levels.info) {
            logger.info(logs);
        } else {
            logger.error(logs);
        }
    }

    module.exports.createLogs = createLogs;
    module.exports.createLogsByLogLevel = createLogsByLogLevel;
}