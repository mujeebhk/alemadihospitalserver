var ApplicationConstants = require('../common/ApplicationConstants');
var rp = require('request-promise');
const logs = require('../common/loggers');
var urlencode = require('urlencode');

var smsTemplates= {
    otpMessageTemplate:  'Your AlEmadi Login OTP is ADDOTPHERE'
};

function send(phoneNumber, message, isEncode, callback) {

    var sendSmsApiPath = ApplicationConstants.OTPApiPath;
    if(isEncode){
        message=  urlencode(message);
    }
    sendSmsApiPath = sendSmsApiPath.replace("SMSMESSAGEBODY", message);
    sendSmsApiPath = sendSmsApiPath.replace("REPLACEMOBILENUMBERHERE", phoneNumber);

    //Send OTP Api Config
    var options = {
        method: 'GET',
        uri: sendSmsApiPath,
        headers: {
            'Content-Type': 'application/xml'
        },
    };

    if (!ApplicationConstants.DISABLE_SMS) {
        rp(options)
            .then(function (parsedBody) {
                // SEND SMS  succeeded...
                logs.createLogsByLogLevel(4, "SMS sent successfully to" + phoneNumber);
                return callback(true);
            })
            .catch(function (err) {
                logs.createLogsByLogLevel(0, 'Error Occured while sending SMS' + err);
                return callback(false);
            });
    } else {
        console.log('Sending SMS Disabled');
        return callback(false);
    }
}

module.exports.send= send;
module.exports.smsTemplates= smsTemplates;