var ApplicationConstants = require('../common/ApplicationConstants');

const API_ID = "5bf8fd93-6921-4b2b-a907-6c50890c0eb8";
var headers = {
  "Content-Type": "application/json; charset=utf-8",
  "Authorization": "Basic NGE5OWY0ZWYtNTRmOC00ZGViLWI2OTUtNDI4OWZiMDJlYTRm"
};

function sendNotificationByDeviceId(playerId, message) {

  var data = {
    app_id: API_ID,
    contents: {
      "en": message
    },
    include_player_ids: [playerId]
  };

  var options = {
    host: "onesignal.com",
    port: 443,
    path: "/api/v1/notifications",
    method: "POST",
    headers: headers
  };

  if (!ApplicationConstants.DISABLE_NOTIFICATION) {
    var https = require('https');
    var req = https.request(options, function (res) {
      res.on('data', function (data) {
        console.log("Response:");
        console.log(JSON.parse(data));
      });
    });

    req.on('error', function (e) {
      console.log("ERROR:");
      console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
  } else {
    console.log('Sending Noty Disabled');
  }
}


function sendNotificationToAllDevice(notyMessage) {

  var data = {
    app_id: API_ID,
    contents: {
      "en": notyMessage
    },
    included_segments: ["All"]
  };

  var options = {
    host: "onesignal.com",
    port: 443,
    path: "/api/v1/notifications",
    method: "POST",
    headers: headers
  };

  if (!ApplicationConstants.DISABLE_NOTIFICATION) {
    var https = require('https');
    var req = https.request(options, function (res) {
      res.on('data', function (data) {
        console.log("Response:");
        console.log(JSON.parse(data));
      });
    });

    req.on('error', function (e) {
      console.log("ERROR:");
      console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
  } else {
    console.log('Sending Noty Disabled');
  }
}

function sendNotificationByTagId(deviceTagId, notyMessage) {

  var data = {
    app_id: "5bf8fd93-6921-4b2b-a907-6c50890c0eb8",
    contents: {
      "en": notyMessage
    },
    filters: [{
      "field": "tag",
      "key": "PATCODE",
      "relation": "=",
      "value": deviceTagId
    }]
  };

  var headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Authorization": "Basic NGE5OWY0ZWYtNTRmOC00ZGViLWI2OTUtNDI4OWZiMDJlYTRm"
  };

  var options = {
    host: "onesignal.com",
    port: 443,
    path: "/api/v1/notifications",
    method: "POST",
    headers: headers
  };

  if (!ApplicationConstants.DISABLE_NOTIFICATION) {
    var https = require('https');
    var req = https.request(options, function (res) {
      res.on('data', function (data) {
        console.log("Response:");
        console.log(JSON.parse(data));
      });
    });

    req.on('error', function (e) {
      console.log("ERROR:");
      console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
  } else {
    console.log('Sending Noty Disabled');
  }
}

//Testing 
//sendNotificationByDeviceId("52b06c1f-f32d-427b-b16d-88184527c1ee", "Message By Id");
//sendNotificationToAllDevice("Message From Server for all device");
//sendNotificationByTagId("1000","Message From Server for Tagged device");

module.exports.sendNotificationByDeviceId = sendNotificationByDeviceId;
module.exports.sendNotificationToAllDevice = sendNotificationToAllDevice;
module.exports.sendNotificationByTagId = sendNotificationByTagId;