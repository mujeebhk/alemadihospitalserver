const fs = require('fs');
const PDFDocument = require('./PdfTableKit');
const doc = new PDFDocument();


function createTable(doc, tableInfo) {

    doc.table(tableInfo, {
        prepareHeader: () => doc.font('Helvetica-Bold'),
        prepareRow: (row, i) => doc.font('Helvetica').fontSize(12)
    });
    //doc.rect(doc.x, 15, 470, doc.y).stroke();
}

function testTableCreate() {

    doc.pipe(fs.createWriteStream('example.pdf'));

    // Header
    doc.fontSize(20);
    doc.image('../MediaFiles/logo.png', {
        fit: [50, 50],
        align: 'left',
        valign: 'left',
        continued: true
    }).font('Times-Roman').text(`AL EMADI HOSPITAL LABRATORY`, {
        width: 500,
        align: 'center'
    });
    doc.rect(50, 60, 500, 70).stroke();

    doc.fontSize(12);
    doc.moveDown();
    doc.moveDown();
    doc.font('Helvetica').text(``, {
        width: 100,
        align: 'justify'
    });

    doc.moveDown();
    var name='Abdullah Basha'; //TODO
    doc.font('Helvetica').text(`Patient Name: `+name, {
        width: 500,
        align: 'left',
        continued: true
    });

    doc.font('Helvetica').text(`Date: 27 Sep 2019`, {
        width: 500,
        align: 'right'
    });

    doc.moveDown();
    doc.font('Helvetica').text(`Age:                25 Years`, {
        width: 500,
        align: 'left'
    });
    

    doc.moveDown();
    doc.font('Helvetica').text(`Gender:           Male`, {
        width: 500,
        align: 'left'
    });

    doc.moveDown(); // This is similar to <br>
    doc.moveDown();
    doc.moveDown();


    // Table
    const table1 = {
        headers: ['Parameter Name', 'Result', 'Unit', 'Reference Range'],
        rows: [
            ['Haemogram:', '','',''],
            ['Haemoglobin', '12.0', 'gm/dl', '12.0 - 15.0'],
            ['Packed Cell, Volume', '34.7', '%', '36.0 - 46.0'],
            ['Total Leucocyte Count TLC', '6.4', '10~9/L', '4.0 - 10.0']
        ]
    };

    createTable(doc, table1);

    doc.text('Page 1', 20, doc.page.height - 50, {
        lineBreak: false
      });
    doc.end();
}