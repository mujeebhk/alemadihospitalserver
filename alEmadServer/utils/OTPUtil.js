const otplib = require('otplib');
var applicationConstants = require('../common/ApplicationConstants');
var logs = require('../common/loggers');
var TOTP = require('onceler').TOTP;
var Store = require("jfs");
var db = new Store("data", {
    type: 'single'
});

var otpCache = {};


async function generateOtp(patcode) {
    const secret = otplib.authenticator.generateSecret();
    var token = otplib.authenticator.generate(secret);

    var otpExpiryTime = new Date();
    otpExpiryTime.setMinutes(otpExpiryTime.getMinutes() + 5);
    var otpDetails = {
        otp: token,
        patcode: patcode,
        secret: secret,
        expiryTime: otpExpiryTime.getTime()
    };
    otpCache[patcode] = JSON.stringify(otpDetails);
    db.save(patcode, token, function(err) {
        // now the data is stored in the file data/anId.json 
    });
    return token;
}

function verifyOTP(patcode, OTP, callback) {
    db.get(patcode, function(err, storedOtp) {

        if (OTP === storedOtp) {

            logs.createLogsByLogLevel(4 ,'OTP verified successfully');
            db.save(patcode, applicationConstants.ClearOTp, function(err) {
                // now the data is stored in the file data/anId.json 
            });
            return callback(true);
        } else {
            logs.createLogsByLogLevel(0 ,'Invalid OTP..Please try again');
            return callback(false);
        }


    });
}

// Time Based OTP
async function generateTOTP() {

    // create a TOTP object for a given base-32 encoded secret.
    var totp = new TOTP(Application.Base32OTPSecretCode, null, 2);
    return totp.now();
}
module.exports.generateOtp = generateOtp;
module.exports.verifyOTP = verifyOTP;
module.exports.generateTOTP = generateTOTP;