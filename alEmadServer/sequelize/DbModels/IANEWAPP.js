/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IANEWAPP', {
		TRANSNO: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			primaryKey: true
		},
		AUDTDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		AUDTUSER: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AUDTORG: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLINICDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		PROVDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLIENTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CLIENTDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TITLE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FIRSTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SECONDNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MIDDLENAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		LASTNAME: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		GENDER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ADDRESS1: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS2: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS3: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ADDRESS4: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DISTRICT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ZIP: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		COUNTRY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NATION: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		HOMEPHONE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MOBILE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		EMAIL: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		DOB: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		VISITTYPE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSTYPE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CONSDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		AGEYY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEMM: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		AGEDAY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		APPTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDDATE: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		ENDTIME: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		DEFSLOT: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		ROW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		COL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MERGEROW: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CANCELRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MARITAL: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		REMARKS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		APPSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CONSULTTYP: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		MEETING: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		TRANSFER: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		CITY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		NOSHOWRES: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ROOMDESC: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		VISITID: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		CREATEDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		MODIFYBY: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		MODIFYDT: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		STANDBY: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		WAPPSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		DEPTCODE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		BILLEDYN: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PATIENTINS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		NURSEINS: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		SPEREQUIRE: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		FILRECE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		BLOCKSLOTS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		PROVSTATUS: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FILEISSUE: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		APPSNO: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		CREATETM: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		PATIDNUM: {
			type: DataTypes.CHAR,
			allowNull: false
		}
	}, {
		tableName: 'IANEWAPP',
		timestamps: false
	});
};
