var SequelizeAuto = require('sequelize-auto')
var auto = new SequelizeAuto('test', 'sa', 'dost1234', {

    host: '192.168.1.5',
    dialect: 'mssql',
    directory: 'DbModels', // prevents the program from writing to disk
    port: '64568',
    additional: {
        timestamps: false
        //...
    }
});
 
//Run this file to auto generate Db Models
//Run this using "node .\DbModelsGenerator.js"
auto.run(function (err) {
  if (err) throw err;
 
  console.log(auto.tables); // table list
  console.log(auto.foreignKeys); // foreign key list
});