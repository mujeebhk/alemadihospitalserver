var jwt = require('jsonwebtoken');
var Cookies = require("cookies");
var sessionstore = require('sessionstore');
var ApplicationConstants = require('../common/ApplicationConstants');
var logs = require('../common/loggers');
var express = require('express');

function sessionFilter(app) {

	// Exempt session fillters
    var sessionExemptedPages = ['/user/login'];
	sessionExemptedPages.push('/user/create');
	sessionExemptedPages.push('/user/verifyOtp');

	// route middleware to verify a token
	app.use('/',
		function (req, res, next) {
			logs.createLogs('sessionFilter() Called');

			if (ApplicationConstants.AVOID_AUTHENTICATED_SESSION_CHECK) {
				logs.createLogs('Session check is disabled');
				next();
			} else {
				logs.createLogs('Session check is enabled');

				if ((sessionExemptedPages.indexOf(req.url) <= -1)) {

                    var token = req.cookies.alemadi_access_token;
                    
                    var isSessionStoreToekValid= false;
                    if(sessionstore.token != undefined && sessionstore.token != null && (token == sessionstore.token))
                        isSessionStoreToekValid= true;
					// decode token
					if (token && sessionstore.isAdminAuthenticated && isSessionStoreToekValid) {

						// verifies secret and checks expiry of tokens
						jwt.verify(
							token,
							ApplicationConstants.secretKeyNameForSession,
							function (err, decoded) {
								if (err) {

									logs.createLogs('Invalid or Expired Token.'+ err.message);
									res.status(401);
									return res.json({
										success: false,
										message: 'Invalid or Expired token.'
									});
								} else {
									logs.createLogs('User authenticated successfully');
									req.decoded = decoded;
									next();
								}
							});

					} else {

						// if there is no token return an error
						logs.createLogs('UnAuthorized Access');
						return res
							.status(401)
							.send({
								success: false,
								message: 'Unauthorised User. Access denied.'
							});

					}
				} else {

					logs.createLogs('Exempted url aceesed successfully' + req.url);
					next();
				}
			}
		});

	// After the successfull authentication, the user is allowed to use below path.
	app.use('/', express.Router());

};

module.exports.sessionFilter = sessionFilter;