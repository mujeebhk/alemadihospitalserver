const logs = require('../common/loggers');
var moment = require('moment');

function getAge(bdate) {
    var years = moment().diff(bdate, 'years');
    return years;
}

function getStandardDateFormat(date, format) {
    var year = date.toString().substring(0, 4);
    var month = date.toString().substring(4, 6);
    var day = date.toString().substring(6, 8);

    var dateFormatted = day + '/' + month + '/' + year;
    if (format && format == 'YYYY-MM-DD')
        dateFormatted = year + '-' + month + '-' + day;

    return dateFormatted;
}

function getStandardTimeFormat(time) {

    var hh, mm;
    if (time.toString().length == 8) {
        hh = time.toString().substring(0, 2);
        mm = time.toString().substring(2, 4);
    } else if (time.toString().length == 7) {
        hh = time.toString().substring(0, 1);
        mm = time.toString().substring(1, 3);
    } else {
        hh = time.toString().substring(0, 1);
        mm = time.toString().substring(1, 2);
    }

    var date = new Date();
    date.setHours(hh);
    date.setMinutes(mm);

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;

    if (hours.toString().length == 1) {
        hours = '0' + hours;
    }

    if (minutes.toString().length == 1) {
        minutes = '0' + minutes;
    }
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function getCustomDateFormat(date, format) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    if (format && format == 'DD/MM/YYYY') {
        var dateFormatted = day + '/' + month + '/' + year;
        return dateFormatted;
    }
    return [year, month, day].join('');
}

function getCustomTimeFormat(date) {
    var d = new Date(date),
        hour = d.getHours().toString(),
        mm = d.getMinutes().toString(),
        sec = '0000';

    if (mm.length == 1) {
        mm = '0' + mm;
    }

    return hour + mm + sec;
}

function formatTimeShow(h) {

    var minutes = 0;
    if (h.toString().length == 8) {
        minutes = h.toString().substring(2, 4);
        h = h.toString().substring(0, 2);
    } else {
        minutes = h.toString().substring(1, 3);
        h = h.toString().substring(0, 1);
    }
    logs.createLogs(4, 'Minutes Generated' + minutes);

    if (h.toString().length == 1) {
        h = '0' + h;
    }
    if (minutes.toString().length == 1) {
        minutes = '0' + minutes;
    }

    return h + ":" + minutes + ":00";
}


function createTimeIntervalArray(startTime, intervalInMinutes, startHour, endHour, startMin, endMin) {
    var x = intervalInMinutes; //minutes interval
    var times = []; // time array
    var tt = startTime; // start time
    var ap = ['am', 'pm']; // AM-PM

    //loop to increment the time and push results in array
    for (var i = 0; tt < 24 * 60; i++) {
        var hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
        var mm = (tt % 60); // getting minutes of the hour in 0-55 format

        if (hh.toString().length == 1) {
            hh = '0' + hh;
        }
        if (mm.toString().length == 1) {
            mm = '0' + mm;
        }

        if ((parseInt(hh) <= endHour) && (parseInt(hh) >= startHour)) {
            var isValidTime = true;
            if (parseInt(startHour) == parseInt(hh) && parseInt(startMin) > parseInt(mm)) isValidTime = false;
            if (parseInt(endHour) == parseInt(hh) && parseInt(endMin) < parseInt(mm)) isValidTime = false;

            if (isValidTime)
                times.push(hh + ':' + mm + ":00"); // pushing data in array in [00:00 - 12:00 AM/PM format]
        }
        tt = tt + x;
    }

    //logs.createLogs('Generated Time Slots Array' + times);
    return times;
}


function autoDatesGeneration(days) {

    var dates = [];
    for (var i = 0; i < days; i++) {
        var now = new Date();
        now = now.setDate(now.getDate() + i);
        dates.push(getCustomDateFormat(now));
    }
    return dates;
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return parseInt([year, month, day].join(''));
}

function formatTime(date) {
    var d = new Date(date),
        hour = d.getHours().toString(),
        mm = d.getMinutes().toString(),
        sec = '0000';

    if (mm.length == 1) {
        mm = '0' + mm;
    }

    return hour + mm + sec;
}

function compareDBDate(dbDate, dbTime, duration) {

    //DB date formatting
    var year = dbDate.toString().substring(0, 4);
    var month = dbDate.toString().substring(4, 6);
    var day = dbDate.toString().substring(6, 8);
    var dateFormatted = year + '-' + month + '-' + day;

    //DB time formatting
    var hh, mm;
    if (dbTime.toString().length == 8) {
        hh = dbTime.toString().substring(0, 2);
        mm = dbTime.toString().substring(2, 4);
    } else if (dbTime.toString().length == 7) {
        hh = dbTime.toString().substring(0, 1);
        mm = dbTime.toString().substring(1, 3);
    } else {
        hh = dbTime.toString().substring(0, 1);
        mm = dbTime.toString().substring(1, 2);
    }

    var now = new Date();
    var dbDateObj = new Date(dateFormatted);
    dbDateObj.setHours(hh);
    dbDateObj.setMinutes(mm);
    dbDateObj.setSeconds(0);

    if (now.getTime() < dbDateObj.getTime()) return 0;
    else if (now.getFullYear() == dbDateObj.getFullYear() && now.getMonth() == dbDateObj.getMonth() && now.getDate() == dbDateObj.getDate()) {
        var appMin = dbDateObj.getMinutes() + duration;
        if (dbDateObj.getHours() == now.getHours() && now.getMinutes() < appMin) return 1;
    }

    return 2;
}

module.exports.getAge = getAge;
module.exports.formatDate = formatDate;
module.exports.formatTime = formatTime;
module.exports.autoDatesGeneration = autoDatesGeneration;
module.exports.getStandardDateFormat = getStandardDateFormat;
module.exports.getStandardTimeFormat = getStandardTimeFormat;
module.exports.getCustomDateFormat = getCustomDateFormat;
module.exports.getCustomTimeFormat = getCustomTimeFormat;
module.exports.formatTimeShow = formatTimeShow;
module.exports.compareDBDate = compareDBDate;
module.exports.createTimeIntervalArray = createTimeIntervalArray;