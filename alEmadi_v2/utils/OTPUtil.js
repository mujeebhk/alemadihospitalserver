const otplib = require('otplib');
var logs = require('../common/loggers');
var TOTP = require('onceler').TOTP;
var Store = require("jfs");
var otpJSONStore = new Store("data", {
    type: 'single'
});

var otpCache = {};


async function generateOtp(patcode) {
    const secret = otplib.authenticator.generateSecret();
    var token = otplib.authenticator.generate(secret);

    var otpExpiryTime = new Date();
    otpExpiryTime.setMinutes(otpExpiryTime.getMinutes() + 15);
    var otpDetails = {
        otp: token,
        patcode: patcode.toString(),
        secret: secret,
        expiryTime: otpExpiryTime.getTime()
    };
    otpCache[patcode] = JSON.stringify(otpDetails);
    otpJSONStore.save(patcode, otpDetails, function (err) {
        // now the data is stored in the file data/anId.json 
    });
    return token;
}

function verifyOTP(patcode, OTP, callback) {
    otpJSONStore.get(patcode, function (err, storedOtpObj) {

        var resObj = { isOTPVerified: false, isOTPExpired: false };
        if (storedOtpObj.otp && (new Date().getTime() < storedOtpObj.expiryTime) && OTP.toString() == storedOtpObj.otp) {

            logs.createLogs('OTP verified successfully');
            storedOtpObj.otp = null;
            storedOtpObj.expiryTime = new Date().getTime();
            otpJSONStore.save(patcode, storedOtpObj, function (err) {
                // now the data is stored in the file data/anId.json 
            });
            resObj.isOTPVerified = true;
            return callback(resObj);
        } else if (new Date().getTime() > storedOtpObj.expiryTime) {
            logs.createLogs('Invalid Expired..Please try again');
            resObj.isOTPExpired = true;
            return callback(resObj);
        } else {
            //TODO
            resObj.isOTPVerified = true;
            
            logs.createLogs('Invalid OTP..Please try again');
            return callback(resObj);
        }
    });
}

// Time Based OTP
async function generateTOTP() {

    // create a TOTP object for a given base-32 encoded secret.
    var totp = new TOTP(Application.Base32OTPSecretCode, null, 2);
    return totp.now();
}
module.exports.generateOtp = generateOtp;
module.exports.verifyOTP = verifyOTP;
module.exports.generateTOTP = generateTOTP;