const bwipjs = require('bwip-js');


function createBarCode(text, callback) {
    bwipjs.toBuffer({
        bcid: 'code128',       // Barcode type
        text: text,    // Text to encode
        scale: 3,               // 3x scaling factor
        height: 10,              // Bar height, in millimeters
        includetext: true,            // Show human-readable text
        textxalign: 'center',        // Always good to set this
    }, function (err, png) {
        if (err) {
            return callback(false);
        } else {
           return callback(png);
        }
    });
}

module.exports.createBarCode = createBarCode;