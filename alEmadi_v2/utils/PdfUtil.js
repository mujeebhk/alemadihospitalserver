var pdf = require('html-pdf');
var html_to_pdf = require('html-pdf-node');

function create(html, callback) {

    pdf.create(html.content).toBuffer(function (err, buffer) {
        if (err) {
            console.log('Error while creating pdf Error:' + err);
            return callback(false);
        }
        return callback(buffer);
    });
}

function create_v2(html, callback) {
    var options = { margin: {top: 40, bottom: 50} };

    html_to_pdf.generatePdf(html, options).then(pdfBuffer => {
        return callback(pdfBuffer);
    });
}

module.exports.create = create;
module.exports.create_v2 = create_v2;