var ApplicationConstants = require('../common/ApplicationConstants');
var rp = require('request-promise');
const logs = require('../common/loggers');
var urlencode = require('urlencode');

var smsTemplates= {
    otpMessageTemplate:  'Your AlEmadi Login OTP is ADDOTPHERE'
};

function send(phoneNumber, message, isEncode, callback) {

    var sendSmsApiPath = ApplicationConstants.smsApi;
    if(isEncode){
        message=  urlencode(message);
    }
    sendSmsApiPath = sendSmsApiPath.replace("SMSTEMPLATE", message);
    sendSmsApiPath = sendSmsApiPath.replace("REPLACEMOBILENUMBERHERE", "974"+phoneNumber);

    //Send OTP Api Config
    var options = {
        method: 'GET',
        uri: sendSmsApiPath,
        headers: {
            'Content-Type': 'application/xml'
        },
    };

    if (!ApplicationConstants.DISABLE_SMS) {
        rp(options)
            .then(function (parsedBody) {
                // SEND SMS  succeeded...
                logs.createLogs("SMS sent successfully to" + phoneNumber);
                return callback(true);
            })
            .catch(function (err) {
                logs.createLogs('Error Occured while sending SMS' + err, 'error');
                return callback(false);
            });
    } else {
        console.log('Sending SMS Disabled', 'info');
        return callback(true);
    }
}

module.exports.send= send;
module.exports.smsTemplates= smsTemplates;