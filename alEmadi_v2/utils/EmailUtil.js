const nodemailer = require('nodemailer');
const ApplicationConstants= require('../common/ApplicationConstants');
const escape = require("escape-html");


function sendMail(messageObj, subject, attachments, callback) {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASS
        }
    });

    var mailOptions = {
        from: process.env.EMAIL_FROM,
        to: process.env.EMAIL_TO,
        subject: subject,
        text: messageObj.bodyText,
        html: messageObj.bodyHtml
    };

    if(attachments != null && attachments.length != 0){
        mailOptions.attachments=  attachments;
    }

    if (!ApplicationConstants.DISABLE_EMAIL) {
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log('Error occured while sending email. Error= ' + error);
                return callback(false);
            } else {
                console.log('Email sent: ' + mailOptions.to + '. The service provider response: ' + info.response);
                return callback(true);
            }
        });
    } else {
        console.log('Sending Email Disabled');
        return callback(false);
    }
}


function userRegistarationTemplate(req, callback) {

    const q = req.body;
    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    let bodyHtml = "";
    let bodyText = "";

    bodyHtml += "<div style='font-size:14px; color:#333; background:#ddd; font-family:sans-serif;padding:2em;'>"
    bodyHtml += "<table cellpadding='0' cellspacing='0' border='0' style='width:90%; margin:0 auto;background:#fff;border:1px solid #ccc'>";
    bodyHtml += "<tr>";
    bodyHtml += "<td colspan='2' style='font-size:12px;padding:8px;color:#999; border-bottom:1px solid #ccc;background:#f4f4f4'>";
    bodyHtml += "New registration form submission from <strong>" + q.FNAME + "</strong>";
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "QID";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.QID);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "First Name";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.FNAME);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "Last Name";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.LNAME);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "Mobile No.";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.MOBILE);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "Date of Birth";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.BDATE);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "Age";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.AGE);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "Gender";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.SEX);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "Insurance Policy No.";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.POLICYNO);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "<tr>";
    bodyHtml += "<td colspan='2' style='text-align:right;font-size:24px; font-weight:bold;padding:18px;color:#aaa; border-bottom:1px solid #ccc;background:#f4f4f4'>";
    bodyHtml += "AL EMADI HOSPITAL";
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

    bodyHtml += "</table></div>";


    bodyText = 'User Registration Form';

    var response = {
        bodyHtml: bodyHtml,
        bodyText: bodyText
    }
    return callback(response);
}

module.exports.sendMail = sendMail;
module.exports.userRegistarationTemplate = userRegistarationTemplate;