module.exports = function (dbModels, sequelizeObj) {

    var fs = require('fs');
    var express = require('express');
    var router = express.Router();
    var async = require("async");
    const { Op } = require("sequelize");
    const errooMsg = require('./ErrorMessage');
    const logs = require('../common/loggers');
    var smsUtil = require('../utils/SmsUtil');
    var otpUtils = require('../utils/OTPUtil');
    var dateUtil = require('../utils/DateUtil');
    var pdfUtil = require('../utils/PdfUtil');
    var emailUtil = require('../utils/EmailUtil');
    var chartsUtil = require('../utils/ChartsUtil');
    var Doctor = require('./Doctor');
    const jwt = require('jsonwebtoken');
    const ApplicationConstants = require('../common/ApplicationConstants');


    //Custom Sql
    const insuranceSql = 'SELECT TOP 10 A.BILLCODE,A.BILLDESC,B.SERVTYP,A.AUTHSTATUS, ORDERDATE FROM ISAPRMGT A INNER JOIN MSBILCOD B ON A.BILLCODE=B.BLCOD INNER JOIN MSVISIT C ON A.VISITID=C.VISITID INNER JOIN MSPTIENT D ON C.PATCODE =D.PATCODE WHERE A.PATCODE=REPLACEPATCODE AND B.SERVTYP in (5,7,11) AND B.INACTIVE=0 AND A.CASETYPE=1 ORDER BY ORDERDATE DESC';
    const prescriptionSql = 'SELECT TOP 10 A.PRECODE,B.ITEMNAME,B.FREQDESC,B.DOSEQTY,B.ROUTEDESC, A.PROVCODE as CLINICCODE, A.PROVNAME as CLINICDESC, CREATEDT FROM MEPRESCH A INNER JOIN MEPRESCD B ON A.PREID=B.PREID WHERE A.CANCELLED=0 AND  A.PATID=REPLACEPATCODE  order by CREATEDT desc';
    const presPdfSql = 'SELECT A.PATID, A.VISITID, A.PRECODE,B.ITEMNAME,B.FREQDESC,B.DOSEQTY,B.ROUTEDESC, B.DURATION, A.PROVCODE as CLINICCODE, A.PROVNAME as CLINICDESC, P.BDATE, P.SEX, P.DISNAME, CREATEDT FROM MEPRESCH A INNER JOIN MEPRESCD B ON A.PREID=B.PREID INNER JOIN MSPTIENT P ON A.PATID=P.PATCODE WHERE A.CANCELLED=0 AND  A.PATID=REPLACEPATCODE  AND CREATEDT=REPLACEDATE';
    const labPdfSql = 'SELECT  MAX(A.ORDBYDESC) as ORDBYDESC, MAX(A.LABSECDESC) as LABSECDESC, MAX(C.PATCODE) as PATCODE, MAX(A.PATNAME) as PATNAME, MAX(A.DOCDATE) as DOCDATE, MAX(A.DOCNUM) as DOCNUM , MAX(A.TESTNAME) as TESTNAME, MAX(B.PATIDNUM) as PATIDNUM, cast(MAX(B.SECPATIDNO) as varchar(20)) as SECPATIDNO, MAX(C.COLLDATE) as COLLDATE, MAX(C.COLLTIME) as COLLTIME, MAX(B.BDATE) as BDATE, MAX(B.SEX) as SEX, MAX(B.PHOCELL) as PHOCELL, MAX(B.NATNAME) as NATNAME, cast(MAX(C.RESULT) as varchar(16)) as RESULT FROM MLORDERD A  INNER JOIN MSPTIENT B ON CONVERT(varchar(20), A .PATCODE)=B.PATCODE  INNER JOIN MLORDANA C ON A.DOCNUM=C.DOCNUM WHERE  A.CANCELTEST=0 AND A.TESTSTATUS=15 AND  A.PATCODE=REPLACEPATCODE AND C.DOCDATE=REPLACEDATE  group by C.DOCNUM';
    const diognosisSql = 'SELECT TOP 10 ICDCODE,ICDDESC,VISTDATE FROM MSVISIT A INNER JOIN MEFINLPD B ON A.EPISDCOD=B.EHRSEQNO WHERE REPWHERE ORDER BY A.VISTDATE DESC';
    const orderSql = 'SELECT TOP 10 B.VISITID,C.BILLCODE,C.BILLDESC,C.SERVTYPE, VISTDATE FROM MSVISIT A  INNER JOIN IIBILLH B  ON A.VISITID=B.VISITID INNER JOIN IIBILLD C ON B.BILLSEQNO=C.BILLSEQNO   WHERE  B.CANCELYN=0 AND C.CANCELYN=0 AND SERVTYPE<>3 AND REPWHERE ORDER BY A.VISTDATE DESC';

    //Months names
    var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    //Patients Registartion
    router.post('/register', function (req, res) {
        sendUserRegistrationForm(req, res, function (response) {
            return res.json(response);
        });
    });

    async function sendUserRegistrationForm(req, res, callback) {

        logs.createLogs("sendUserRegistrationForm() called...");

        //Birthdate 
        var bdate = new Date(req.body.BDATE);
        var day = bdate.getDate();
        var month = parseInt(bdate.getMonth());
        var year = bdate.getFullYear();
        req.body.BDATE = day + ' ' + mlist[month] + ' ' + year;
        req.body.AGE = dateUtil.getAge(bdate.getTime());

        //Gender
        if (req.body.SEX == 1) {
            req.body.SEX = 'Male';
        } else {
            req.body.SEX = 'Female';
        }

        //Attachment
        var mailAttachment = [];
        if (req.body.USERIMAGE != null && req.body.USERIMAGE != "") {
            var extension = req.body.USERIMAGE.substring("data:image/".length, req.body.USERIMAGE.indexOf(";base64"));
            var userImage = {
                filename: 'PatientPhoto.' + extension,
                path: req.body.USERIMAGE
            };
            mailAttachment.push(userImage);
        }

        if (req.body.INSURANCEDOC) {
            var insureDocExt = req.body.INSURANCEDOC.substring("data:image/".length, req.body.INSURANCEDOC.indexOf(";base64"));
            var insuranceImage = {
                filename: 'INSURANCE.' + insureDocExt,
                path: req.body.INSURANCEDOC
            };
            mailAttachment.push(insuranceImage);
        }

        if (req.body.QIDDOCF) {
            var qidfextension = req.body.QIDDOCF.substring("data:image/".length, req.body.QIDDOCF.indexOf(";base64"));
            var qidimgf = {
                filename: 'QIDDOC_FRONT.' + qidfextension,
                path: req.body.QIDDOCF
            };
            mailAttachment.push(qidimgf);
        }

        if (req.body.QIDDOCB) {
            var qidbextension = req.body.QIDDOCB.substring("data:image/".length, req.body.QIDDOCB.indexOf(";base64"));
            var qidimgb = {
                filename: 'QIDDOC_BACK.' + qidbextension,
                path: req.body.QIDDOCB
            };
            mailAttachment.push(qidimgb);
        }

        //Sending Mail to Al Emadi
        var subject = 'Registration Form Submission';
        emailUtil.userRegistarationTemplate(req, function (emailMessage) {

            logs.createLogs("Email sending process started");
            emailUtil.sendMail(emailMessage, subject, mailAttachment, function (emailRes) {
                logs.createLogs("Is Mail Sent " + emailRes);
            });
        });
        callback({});
    }

    // Patients Login with Otp
    router.post('/otp/send', function (req, res) {

        logs.createLogs('Send otp api called');
        sendOTP(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });
    });

    async function sendOTP(req, res, callback) {
        logs.createLogs("sendOTP() called.");

        // DB process
        dbModels.MSPTIENT.findAll({
            where: {
                PATIDNUM: req.body.qatarId
            }
        }).then(async function (patientFromDb) {

            if (patientFromDb.length != 0) {
                var patcode = patientFromDb[0].dataValues.PATCODE.toString().trim();

                //Generating OTP
                var generatedOtp = await otpUtils.generateOtp(patcode);
                var phoneNumberOfPatient = patientFromDb[0].dataValues.PHOCELL;
                var otpMessage = smsUtil.smsTemplates.otpMessageTemplate;
                otpMessage = otpMessage.replace("ADDOTPHERE", generatedOtp);

                //Sending SMS 
                smsUtil.send(phoneNumberOfPatient, otpMessage, false, function (response) {

                    if (response) {
                        logs.createLogs("OTP sent successfully to " + phoneNumberOfPatient);

                        var gender = 'Male';
                        if (patientFromDb[0].dataValues.SEX != 1) {
                            gender = 'Female';
                        }
                        var lastVisitedDate = patientFromDb[0].dataValues.DATLAST;
                        lastVisitedDate = lastVisitedDate.toString().substring(6, 8) + '/' + lastVisitedDate.toString().substring(4, 6) + '/' + lastVisitedDate.toString().substring(0, 4);
                        var responseObj = {
                            "PATIDNUM": patientFromDb[0].dataValues.PATIDNUM,
                            "PATCODE": patientFromDb[0].dataValues.PATCODE.toString().trim(),
                            "PHOCELL": patientFromDb[0].dataValues.PHOCELL.toString().trim(),
                            "DISNAME": patientFromDb[0].dataValues.DISNAME.toString().trim(),
                            "POLICYNO": patientFromDb[0].dataValues.POLICYNO,
                            "LAST VISITED": lastVisitedDate,
                            "GENDER": gender

                        };

                        return callback(responseObj);
                    } else {
                        logs.createLogs('Error Occured while sending Otp for patients login', 'error');
                        res.status(500);
                        return callback({ error: 'SMS Failed' });
                    }
                });
            } else {
                res.status(404);
                logs.createLogs('The qatarId do not exist');
                return callback(patientFromDb);
            }

        }).catch(function (err) {

            logs.createLogs(' Fetching patients failed.Exception message:' + err);
            res.status(500);
            return callback(err);
        });
    }


    //verify OTP
    router.post('/otp/verify', function (req, res) {
        verifyOTP(req, res, function (otp) {
            return res.json(otp);
        });
    });

     function verifyOTP(req, res, callback) {

        otpUtils.verifyOTP(req.body.PATCODE, req.body.OTP, function (otpRes) {

            if (otpRes.isOTPVerified) {
                logs.createLogs('OTP verified successfully');
                return callback({ isOTPVerifed: true, token: generateAccessToken({ PATCODE: req.body.PATCODE }) });
            } else {
                logs.createLogs('Invalid OTP');
                res.status(401);
                return callback(false);
            }
        });
    }

    function generateAccessToken(patcode) {
        return jwt.sign(patcode, process.env.TOKEN_SECRET, { expiresIn: ApplicationConstants.sessionExpireTime });
    }

    // Patients Lab Reports
    router.get('/reports/lab/patcode/:patcode', function (req, res) {

        logs.createLogs('Lab reports api called');
        labReport(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });
    });

    function labReport(req, res, callback) {
        logs.createLogs('labReport() called');


        dbModels.MLORDANA.findAll({
            where: {
                PATCODE: req.params.patcode
            },
            attributes: ['DOCNUM', 'INVHDESC', 'PARAMDESC', 'RESULT', 'NUMRESULT', 'ALLOWLOW', 'ALLOWHIGH', 'REPUNIT', 'DOCDATE']
        }).then(function (patientsLabReport) {
            var docDates = [];
            var noOfDocNums = [];
            var noOfInvhDesc = [];
            var finalList = [];

            //Creating the unique DOCNUM and  PARAMDESC list
            for (var i = 0; i < patientsLabReport.length; i++) {

                if (noOfDocNums.indexOf(patientsLabReport[i].DOCNUM) <= -1) {
                    noOfDocNums.push(patientsLabReport[i].DOCNUM);
                    var docDateFormated = patientsLabReport[i].DOCDATE;
                    docDateFormated = docDateFormated.toString().substring(0, 4) + '-' + docDateFormated.toString().substring(4, 6) + '-' + docDateFormated.toString().substring(6, 8);
                    docDates.push(docDateFormated);
                }
                if (noOfInvhDesc.indexOf(patientsLabReport[i].INVHDESC) <= -1) {
                    noOfInvhDesc.push(patientsLabReport[i].INVHDESC);
                }
            }

            for (var z = 0; z < noOfDocNums.length; z++) {
                var patientLabReportByINVHDesc = [];

                for (var j = 0; j < noOfInvhDesc.length; j++) {
                    var tempListForDocNumSort = [];

                    //Iterating the PatientsLab report list and craeting list by DocNum
                    patientsLabReport.filter(createLabReportByDOCNUM);

                    function createLabReportByDOCNUM(patientListObject) {
                        if (patientListObject.REPUNIT && patientListObject.RESULT && patientListObject.PARAMDESC && (patientListObject.DOCNUM.toString().trim() == noOfDocNums[z].toString().trim()) && (patientListObject.INVHDESC.toString().trim() == noOfInvhDesc[j].toString().trim())) {
                            tempListForDocNumSort.push({
                                "name": patientListObject.PARAMDESC.toString().trim(),
                                "result": patientListObject.RESULT.toString().trim(),
                                "max": patientListObject.ALLOWHIGH,
                                "min": patientListObject.ALLOWLOW,
                                "unit": patientListObject.REPUNIT.toString().trim()
                            });
                        }
                    }
                    if (tempListForDocNumSort.length != 0) {
                        patientLabReportByINVHDesc.push({
                            "name": noOfInvhDesc[j].toString().trim(),
                            "subtest": tempListForDocNumSort
                        });
                    }
                }
                
                var InvhDesc= 'NULL';
                if(noOfInvhDesc[z]) InvhDesc= noOfInvhDesc[z].trim();
                finalList.push({
                    "date": docDates[z],
                    "testName": InvhDesc,
                    "test": patientLabReportByINVHDesc
                });
            }

            callback({ "reports": finalList });
        }).catch(function (err) {

            logs.createLogs('Lab reports fetch failed' + err);
            res.status(500);
            callback(err);

        });
    }

    // Patients QID verify
    router.get('/verify/qid/:qid', function (req, res) {

        logs.createLogs('QID verify called');
        verifyQID(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });

    });


    function verifyQID(req, res, callback) {
        logs.createLogs('verifyQID() Called');

        var resObj = { isQIDExist: false };
        dbModels.MSPTIENT.findAll({
            where: {
                PATIDNUM: req.params.qid
            }
        }).then(function (patientList) {
            if (patientList.length != 0) {
                resObj.PATCODE = patientList[0].dataValues.PATCODE;
                if (patientList[0].dataValues.MOBILE && patientList[0].dataValues.MOBILE != '')
                    resObj.MOBILE = patientList[0].dataValues.MOBILE;
                else
                    resObj.MOBILE = patientList[0].dataValues.PHOCELL;
                resObj.QID = patientList[0].dataValues.PATIDNUM;
                resObj.FNAME = patientList[0].dataValues.FNAME;
                resObj.LNAME = patientList[0].dataValues.LNAME;

                resObj.isQIDExist = true;
            }
            res.json(resObj);
        }).catch(function (err) {

            logs.createLogs('fetching patient failed ' + JSON.stringify(err), 'error');
            res.status(500);
            callback(err);
        });
    }

    // Patients Appointment by id
    router.get('/appointment/id/:id', function (req, res) {

        logs.createLogs('Appointment by id called');
        req.body.isReqByID = true;
        getPatientAppointments(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });

    });

    // Patients Appointment
    router.get('/appointment/patcode/:patcode', function (req, res) {

        logs.createLogs('Appointment list called');
        getPatientAppointments(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });

    });

    function getPatientAppointments(req, res, callback) {
        logs.createLogs('getPatientAppointments() Called');

        var whereCond = {
            CLIENTCODE: req.params.patcode
        };

        if (req.body.isReqByID) {
            whereCond.TRANSNO = req.params.id;
            delete whereCond.CLIENTCODE;
        }

        var formatedDate = dateUtil.formatDate(new Date().getTime());
        if (req.query.completed || req.query.completed == 'true') {
            whereCond.APPDATE = {
                [Op.lte]: formatedDate
            };
            whereCond.APPSTATUS = 1;
        }

        // Db process
        dbModels.IANEWAPP.findAll({
            where: whereCond,
            order: [
                ['APPDATE', 'DESC']
            ], limit: 10
        }).then(function (appointmentList) {

            var deptcodelist = [];
            for (var j = 0; j < appointmentList.length; j++) {
                if (!deptcodelist.includes(appointmentList[j].DEPTCODE.trim()))
                    deptcodelist.push(appointmentList[j].DEPTCODE.trim());
            }

            dbModels.IACLINIC.findAll({
                attributes: ['DEPTCODE', 'DEPTDESC'],
                where: {
                    DEPTCODE: deptcodelist
                }
            }).then(function (deptNamesList) {
                var deptNameObj = {};
                for (var k = 0; k < deptNamesList.length; k++) {
                    deptNameObj[deptNamesList[k].dataValues.DEPTCODE] = deptNamesList[k].dataValues.DEPTDESC;
                }

                var resObj = [];
                for (var i = 0; i < appointmentList.length; i++) {

                    var status = dateUtil.compareDBDate(appointmentList[i].dataValues.APPDATE, appointmentList[i].dataValues.APPTIME, appointmentList[i].dataValues.DEFSLOT);
                    if ((req.query.completed || req.query.completed == 'true') && status != 2) continue;

                    var locationObj = Doctor.all_centers[1];
                    if (appointmentList[i].dataValues.CLINICCODE && appointmentList[i].dataValues.CLINICCODE.toString().includes("N")) locationObj = Doctor.all_centers[0];

                    var iconName= appointmentList[i].dataValues.CLINICDESC.replace(/ /g, "").toUpperCase()+".jpg";
                    var appObj = {
                        TRANSNO: appointmentList[i].dataValues.TRANSNO,
                        CLIENTCODE: appointmentList[i].dataValues.CLIENTCODE,
                        CLIENTDESC: appointmentList[i].dataValues.CLIENTDESC,
                        CLINICCODE: appointmentList[i].dataValues.CLINICCODE,
                        CLINICDESC: appointmentList[i].dataValues.CLINICDESC,
                        DEPTCODE: appointmentList[i].dataValues.DEPTCODE,
                        DEPTDESC: deptNameObj[appointmentList[i].dataValues.DEPTCODE.trim()],
                        APPDATE: dateUtil.getStandardDateFormat(appointmentList[i].dataValues.APPDATE),
                        APPTIME: dateUtil.getStandardTimeFormat(appointmentList[i].dataValues.APPTIME),
                        APPSTATUS: appointmentList[i].dataValues.APPSTATUS,
                        ICONURL: '/images/doctor/icon/'+iconName,
                        STATUS: status,
                        LOCATION: locationObj,
                        CONSULTTYPE: 2
                    };

                    if (appObj.APPDATE && appObj.APPTIME) {
                        var now = new Date();
                        var splittedDate = appObj.APPDATE.split("/");
                        now = new Date(splittedDate[2] + '-' + splittedDate[1] + '-' + splittedDate[0]);
                        var splittedTime = appObj.APPTIME.split(' ')[0].split(":");
                        now.setHours(splittedTime[0]);
                        now.setMinutes(splittedTime[1]);

                        appObj.DATESTR= parseInt(appointmentList[i].dataValues.APPDATE.toString()+appointmentList[i].dataValues.APPTIME.toString());
                        appObj.APPDATESTR = now.toString();
                    }

                    resObj.push(appObj);
                }
                resObj.sort(compare);
                callback(resObj);
            }).catch(function (err) {
                logs.createLogs('fetching deptnames failed ' + JSON.stringify(err), 'error');
                res.status(500);
                callback(err);
            });
        }).catch(function (err) {

            logs.createLogs('fetching patients appointment List failed ' + JSON.stringify(err), 'error');
            res.status(500);
            callback(err);
        });
    }


    // Patients Appointment
    router.post('/appointment/book', function (req, res) {

        logs.createLogs('Patient create appointment api called');
        bookAppointment(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });

    });


    var tempTransNo = 0;
    function bookAppointment(req, res, callback) {
        logs.createLogs('bookAppointment() Called');

        //resObj
        var resObj = {
            appointment_id: null,
            return_message: "Failed",
            error: {
                message: null,
                code: null
            }
        };

        //Invalid request check

        if (!req.body.FNAME && !req.body.MOBILE && !req.body.PATCODE) {
            res.status(400);
            logs.createLogs(errooMsg.INVALID_REQUEST);
            resObj.return_message = errooMsg.INVALID_REQUEST;
            resObj.error.message = errooMsg.INVALID_REQUEST;
            resObj.error.code = 10009;
            return res.json(resObj);
        }

        var date = new Date(req.body.DATE);
        if (date.getTime() < new Date().getTime()) {
            res.status(400);
            logs.createLogs(errooMsg.INVALID_APP_DATE);
            resObj.return_message = errooMsg.INVALID_APP_DATE;
            resObj.error.message = errooMsg.INVALID_APP_DATE;
            resObj.error.code = 10001;
            return res.json(resObj);
        }

        var now = new Date();
        var formatedDate = dateUtil.formatDate(date);
        var formatedTime = dateUtil.formatTime(date);
        var nowFormatedDate = dateUtil.formatDate(now);
        var nowFormatedTime = dateUtil.formatTime(now);
        var createAppointment = [
            function (cb) {

                // Check for timeslot availability
                dbModels.IANEWAPP.findAll({
                    where: {
                        CLINICCODE: req.body.CLINICCODE,
                        APPDATE: {
                            [Op.eq]: parseFloat(formatedDate)
                        },
                        APPTIME: {
                            [Op.eq]: parseFloat(formatedTime)
                        },
                        APPSTATUS: 1
                    }
                }).then(function (dBValue) {
                    if (dBValue.length == 0) {
                        logs.createLogs('Timeslot availble to book');
                        cb();
                    } else {
                        res.status(400);
                        logs.createLogs(errooMsg.ALREADY_BOOKED);
                        resObj.return_message = errooMsg.ALREADY_BOOKED;
                        resObj.error.message = errooMsg.ALREADY_BOOKED;
                        resObj.error.code = 10002;
                        return res.json(resObj);
                    }
                }).catch(function (err) {
                    logs.createLogs('IANEWAPP Fetch Failed:' + JSON.stringify(err));
                    res.status(500);
                    resObj.return_message = errooMsg.APPOINTMENT_CREATE_FAILED;
                    resObj.error.message = errooMsg.APPOINTMENT_CREATE_FAILED;
                    resObj.error.code = 10003;
                    return callback(resObj);
                });
            }, function (cb) {

                if (req.body.PATCODE) {
                    dbModels.MSPTIENT.findAll({
                        where: {
                            PATCODE: req.body.PATCODE
                        }
                    }).then(function (patientDetails) {

                        var date = new Date();
                        var formatedDate = dateUtil.formatDate(date);
                        var formatedTime = dateUtil.formatTime(date);

                        if (patientDetails[0].dataValues.GENDER == 1) {
                            req.body.TITLE = 0;
                        } else {
                            req.body.TITLE = 4;
                        }

                        if (patientDetails[0].dataValues.PATCODE)
                            req.body.CLIENTCODE = patientDetails[0].dataValues.PATCODE;
                        else req.body.CLIENTCODE = patientDetails[0].dataValues.PATIDNUM;

                        req.body.FIRSTNAME = patientDetails[0].dataValues.FNAME;
                        req.body.SECONDNAME = '';
                        req.body.MIDDLENAME = '';
                        req.body.LASTNAME = patientDetails[0].dataValues.LNAME;
                        req.body.GENDER = patientDetails[0].dataValues.SEX;
                        req.body.ADDRESS1 = '';
                        req.body.ADDRESS2 = '';
                        req.body.ADDRESS3 = '';
                        req.body.ADDRESS4 = '';
                        req.body.PATIDNUM = patientDetails[0].dataValues.PATIDNUM;
                        req.body.CLIENTDESC = patientDetails[0].dataValues.FNAME + ' ' + patientDetails[0].dataValues.LNAME;
                        req.body.DISTRICT = 0;
                        req.body.COUNTRY = '';
                        req.body.ZIP = '';
                        req.body.NATION = patientDetails[0].dataValues.NATION;
                        req.body.COUNTRY = patientDetails[0].dataValues.COUNTRY;
                        req.body.HOMEPHONE = '';
                        req.body.MOBILE = patientDetails[0].dataValues.PHOCELL;
                        req.body.DOB = '0';
                        req.body.EMAIL = patientDetails[0].dataValues.EMAIL;
                        req.body.MARITAL = patientDetails[0].dataValues.MARITAL;
                        req.body.AGEYY = 0;
                        req.body.AGEMM = 0;
                        req.body.AGEDAY = 0;
                        req.body.VISITTYPE = 5;
                        req.body.CONSTYPE = '';
                        req.body.CONSDESC = '';
                        req.body.COL = 1;
                        req.body.MERGEROW = 1;
                        req.body.CANCELRES = '';
                        req.body.REMARKS = 'BOOKED FROM MOBILE APP';
                        req.body.APPSTATUS = 1;
                        req.body.CONSULTTYP = 0;
                        req.body.MEETING = '';
                        req.body.TRANSFER = 0;
                        req.body.CITY = 0;
                        req.body.NOSHOWRES = '';
                        req.body.ROOMCODE = '001';
                        req.body.ROOMDESC = 'DEFAULT CONSULTATION ROOM';
                        req.body.VISITID = '';
                        req.body.CREATEBY = 'USER';
                        req.body.CREATEDT = nowFormatedDate;
                        req.body.MODIFYBY = '';
                        req.body.MODIFYDT = 0;
                        req.body.STANDBY = 0;
                        req.body.WAPPSTATUS = 0;
                        req.body.BILLEDYN = 0;
                        req.body.STANDBY = 0;
                        req.body.WAPPSTATUS = 0;
                        req.body.PATIENTINS = '';
                        req.body.NURSEINS = '';
                        req.body.SPEREQUIRE = '';
                        req.body.FILRECE = 0;
                        req.body.BLOCKSLOTS = 0;
                        req.body.PROVSTATUS = 0;
                        req.body.FILEISSUE = 0;
                        req.body.APPSNO = 0;
                        req.body.CREATETM = nowFormatedTime;

                        cb();
                    }).catch(function (err) {
                        logs.createLogs('Fetching patients record failed. Exception message:' + JSON.stringify(err), 'error');
                        res.status(500);
                        resObj.return_message = errooMsg.APPOINTMENT_CREATE_FAILED;
                        resObj.error.message = errooMsg.APPOINTMENT_CREATE_FAILED;
                        resObj.error.code = 10004;
                        return callback(resObj);
                    });
                } else {
                    req.body.FIRSTNAME = req.body.FNAME;
                    req.body.LASTNAME = req.body.LNAME;

                    req.body.TITLE = 0;
                    req.body.CLIENTCODE = '';
                    req.body.SECONDNAME = '';
                    req.body.MIDDLENAME = '';
                    req.body.GENDER = 3;
                    req.body.ADDRESS1 = '';
                    req.body.ADDRESS2 = '';
                    req.body.ADDRESS3 = '';
                    req.body.ADDRESS4 = '';
                    req.body.PATIDNUM = '';
                    req.body.CLIENTDESC = req.body.FNAME + ' ' + req.body.LNAME;
                    req.body.DISTRICT = 0;
                    req.body.COUNTRY = '';
                    req.body.ZIP = '';
                    req.body.NATION = '';
                    req.body.COUNTRY = '';
                    req.body.HOMEPHONE = '';
                    req.body.MOBILE = req.body.MOBILE;
                    req.body.DOB = '0';
                    req.body.EMAIL = '';
                    req.body.MARITAL = '';
                    req.body.AGEYY = 0;
                    req.body.AGEMM = 0;
                    req.body.AGEDAY = 0;
                    req.body.VISITTYPE = 5;
                    req.body.CONSTYPE = '';
                    req.body.CONSDESC = '';
                    req.body.COL = 1;
                    req.body.MERGEROW = 1;
                    req.body.CANCELRES = '';
                    req.body.REMARKS = 'BOOKED FROM MOBILE APP';
                    req.body.APPSTATUS = 1;
                    req.body.CONSULTTYP = 0;
                    req.body.MEETING = '';
                    req.body.TRANSFER = 0;
                    req.body.CITY = 0;
                    req.body.NOSHOWRES = '';
                    req.body.ROOMCODE = '001';
                    req.body.ROOMDESC = 'DEFAULT CONSULTATION ROOM';
                    req.body.VISITID = '';
                    req.body.CREATEBY = 'USER';
                    req.body.CREATEDT = nowFormatedDate;
                    req.body.MODIFYBY = '';
                    req.body.MODIFYDT = 0;
                    req.body.STANDBY = 0;
                    req.body.WAPPSTATUS = 0;
                    req.body.BILLEDYN = 0;
                    req.body.STANDBY = 0;
                    req.body.WAPPSTATUS = 0;
                    req.body.PATIENTINS = '';
                    req.body.NURSEINS = '';
                    req.body.SPEREQUIRE = '';
                    req.body.FILRECE = 0;
                    req.body.BLOCKSLOTS = 0;
                    req.body.PROVSTATUS = 0;
                    req.body.FILEISSUE = 0;
                    req.body.APPSNO = 0;
                    req.body.CREATETM = nowFormatedTime;
                    cb();
                }

            }, function (cb) {

                dbModels.IACLSCDD.findAll({
                    where: {
                        CLINICCODE: req.body.CLINICCODE, APPDATE: {
                            [Op.eq]: parseFloat(formatedDate)
                        }
                    }
                }).then(function (doctorsSchedule) {

                    if (doctorsSchedule.length != 0) {

                        req.body.ROW = 6;
                        var slotsListObj = {};
                        req.body.get_first_available = false;
                        Doctor.getSlotsList(slotsListObj, doctorsSchedule);
                        var stdDate = dateUtil.getStandardDateFormat(formatedDate);
                        for (var z = 0; z < slotsListObj[stdDate].length; z++) {
                            var slotTime = parseInt(slotsListObj[stdDate][z].slotTime.replace(/:/g, "") + "0000").toString();
                            if (slotTime == formatedTime) {
                                req.body.ROW = z + 5;
                                break;
                            }
                        }

                        req.body.AUDTDATE = nowFormatedDate;
                        req.body.AUDTTIME = nowFormatedTime;
                        req.body.AUDTUSER = doctorsSchedule[0].dataValues.AUDTUSER;
                        req.body.AUDTORG = doctorsSchedule[0].dataValues.AUDTORG;
                        req.body.CLINICCODE = doctorsSchedule[0].dataValues.CLINICCODE;
                        req.body.CLINICDESC = doctorsSchedule[0].dataValues.CLINICDESC;
                        req.body.PROVCODE = doctorsSchedule[0].dataValues.CLINICCODE;
                        req.body.PROVDESC = doctorsSchedule[0].dataValues.CLINICDESC;
                        req.body.DEFSLOT = doctorsSchedule[0].dataValues.DURATION;
                        req.body.DEPTCODE = doctorsSchedule[0].dataValues.DEPTCODE;
                        req.body.APPDATE = formatedDate;
                        req.body.APPTIME = formatedTime;
                        req.body.ENDDATE = formatedDate;
                        req.body.ENDTIME = formatedTime;
                        cb();
                    } else {
                        res.status(400);
                        logs.createLogs(errooMsg.INVALID_APP_DATE);
                        resObj.return_message = errooMsg.INVALID_APP_DATE;
                        resObj.error.message = errooMsg.INVALID_APP_DATE;
                        resObj.error.code = 10005;
                        return res.json(resObj);
                    }

                }).catch(function (err) {

                    logs.createLogs(' Fetching doctors schedule record failed. Exception message:' + JSON.stringify(err), 'error');
                    res.status(500);
                    resObj.return_message = errooMsg.APPOINTMENT_CREATE_FAILED;
                    resObj.error.message = errooMsg.APPOINTMENT_CREATE_FAILED;
                    resObj.error.code = 10006;
                    return callback(resObj);
                });
            }, function (cb) {
                dbModels.IANEWAPP.findAll({
                    attributes: ['TRANSNO'],
                    order: [
                        ['TRANSNO', 'DESC']
                    ], limit: 1
                }).then(function (dBValue) {
                    var nextVal = parseInt(dBValue[0].dataValues.TRANSNO) + 1;
                    logs.createLogs(4, 'IANEWAPP NextVal = ' + nextVal);
                    req.body.TRANSNO = nextVal;
                    tempTransNo = nextVal;
                    cb();
                }).catch(function (err) {
                    logs.createLogs('IANEWAPP Fetch Failed:' + JSON.stringify(err));
                    req.body.TRANSNO = tempTransNo;
                    tempTransNo = tempTransNo + 1;
                    cb();
                });
            }, function (cb) {

                dbModels.IANEWAPP.create(req.body).then(function (createAppointmentReq) {

                    createAppointmentReq.save();
                    logs.createLogs('Appointment Create Success');

                    req.body.SOURCE = 1;
                    req.body.STATUS = 0;
                    req.body.APPNUMBER = createAppointmentReq.dataValues.TRANSNO.toString();
                    req.body.EPSODECODE = 0;
                    req.body.PATCODE = req.body.CLIENTCODE;
                    req.body.PATNAME = req.body.CLIENTDESC;
                    req.body.CHECKINYN = 0;
                    req.body.CKINPROCOD = '';
                    req.body.CKINPRODES = '';
                    req.body.QUEUENO = 0;
                    req.body.CUSTTYPE = 0;
                    req.body.POLGRPCODE = '';
                    req.body.POLGRPDESC = '';
                    req.body.NURSEDONE = 0;
                    req.body.INITVISIT = 1;
                    req.body.AGEYEARS = 0;
                    req.body.CKINTIME = 0;

                    dbModels.MEPATAPP.create(req.body).then(function (appbackUp) {
                        appbackUp.save();
                    });

                    resObj.appointment_id = createAppointmentReq.dataValues.TRANSNO.toString();
                    resObj.return_message = "Success";
                    callback(resObj);


                }).catch(function (err) {
                    logs.createLogs('Appointment Create Failed:' + JSON.stringify(err), 'error');
                    res.status = 500;
                    resObj.return_message = errooMsg.APPOINTMENT_CREATE_FAILED;
                    resObj.error.message = errooMsg.APPOINTMENT_CREATE_FAILED;
                    resObj.error.code = 10007;
                    return callback(resObj);
                });
            }
        ];

        async.series(createAppointment, (err, results) => {
            if (err) {
                logs.createLogs('Error occureed at async.series' + err, 'error');
                res.status = 500;
                resObj.return_message = errooMsg.APPOINTMENT_CREATE_FAILED;
                resObj.error.message = errooMsg.APPOINTMENT_CREATE_FAILED;
                resObj.error.code = 10008;
                return callback(resObj);
            }
        });
    }


    // Update Patients Appointment
    router.post('/appointment/update', function (req, res) {

        logs.createLogs('Calling updateAppointment api called');
        updateAppointment(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });

    });

    function updateAppointment(req, res, callback) {
        logs.createLogs(' updateAppointment() Called');

        //resObj
        var resObj = {
            return_message: "Failed",
            error: {
                message: null,
                code: null
            }
        };
        var formatedDate, formatedTime = null;
        if (req.body.DATE) {
            var date = new Date(req.body.DATE);
            formatedDate = dateUtil.formatDate(date);
            formatedTime = dateUtil.formatTime(date);
        }
        var updateAppointment = [
            function (cb) {

                if (req.body.DATE) {
                    dbModels.IANEWAPP.findAll({
                        where: {
                            TRANSNO: req.body.APPOINTMENTID
                        }
                    }).then(function (dBValue) {
                        if (dBValue.length == 0) {
                            res.status(400);
                            logs.createLogs(errooMsg.INVALID_REQUEST);
                            resObj.error.message = errooMsg.INVALID_REQUEST;
                            resObj.error.code = 20003;
                            return res.json(resObj);
                        } else {
                            req.body.CLINICCODE = dBValue[0].dataValues.CLINICCODE;
                            cb();
                        }
                    }).catch(function (err) {
                        logs.createLogs('IANEWAPP Fetch Failed:' + JSON.stringify(err));
                        res.status(500);
                        resObj.return_message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                        resObj.error.message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                        resObj.error.code = 20004;
                        return callback(resObj);
                    });
                } else {
                    cb();
                }
            },
            function (cb) {

                if (req.body.DATE) {
                    // Check for timeslot availability
                    dbModels.IANEWAPP.findAll({
                        where: {
                            CLINICCODE: req.body.CLINICCODE,
                            APPDATE: {
                                [Op.eq]: parseFloat(formatedDate)
                            },
                            APPTIME: {
                                [Op.eq]: parseFloat(formatedTime)
                            },
                            APPSTATUS: 1
                        }
                    }).then(function (dBValue) {
                        if (dBValue.length == 0) {
                            logs.createLogs('Timeslot availble to book');
                            cb();
                        } else {
                            res.status(400);
                            logs.createLogs(errooMsg.ALREADY_BOOKED);
                            resObj.return_message = errooMsg.ALREADY_BOOKED;
                            resObj.error.message = errooMsg.ALREADY_BOOKED;
                            resObj.error.code = 20005;
                            return res.json(resObj);
                        }
                    }).catch(function (err) {
                        logs.createLogs('IANEWAPP Fetch Failed:' + JSON.stringify(err));
                        res.status(500);
                        resObj.return_message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                        resObj.error.message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                        resObj.error.code = 20006;
                        return callback(resObj);
                    });
                } else {
                    cb();
                }
            }, function (cb) {

                if (req.body.DATE) {
                    dbModels.IACLSCDD.findAll({
                        where: {
                            CLINICCODE: req.body.CLINICCODE, APPDATE: {
                                [Op.eq]: parseFloat(formatedDate)
                            }
                        }
                    }).then(function (doctorsSchedule) {

                        if (doctorsSchedule.length != 0) {

                            req.body.ROW = 6;
                            var slotsListObj = {};
                            req.body.get_first_available = false;
                            Doctor.getSlotsList(slotsListObj, doctorsSchedule);
                            var stdDate = dateUtil.getStandardDateFormat(formatedDate);
                            for (var z = 0; z < slotsListObj[stdDate].length; z++) {
                                var slotTime = parseInt(slotsListObj[stdDate][z].slotTime.replace(/:/g, "") + "0000").toString();
                                if (slotTime == formatedTime) {
                                    req.body.ROW = z + 5;
                                    break;
                                }
                            }
                            req.body.APPDATE = formatedDate;
                            req.body.APPTIME = formatedTime;
                            req.body.ENDDATE = formatedDate;
                            req.body.ENDTIME = formatedTime;
                            cb();
                        } else {
                            res.status(400);
                            logs.createLogs(errooMsg.INVALID_APP_DATE);
                            resObj.return_message = errooMsg.INVALID_APP_DATE;
                            resObj.error.message = errooMsg.INVALID_APP_DATE;
                            resObj.error.code = 20007;
                            return res.json(resObj);
                        }

                    }).catch(function (err) {

                        logs.createLogs(' Fetching doctors schedule record failed. Exception message:' + JSON.stringify(err), 'error');
                        res.status(500);
                        resObj.return_message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                        resObj.error.message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                        resObj.error.code = 20008;
                        return callback(resObj);
                    });
                } else {
                    cb();
                }
            }, function (cb) {
                // Updating Appointment Status
                dbModels.IANEWAPP.update(req.body, {
                    where: {
                        TRANSNO: req.body.APPOINTMENTID
                    }
                }).then(function (update) {
                    logs.createLogs('Appointment Status updated successfully');
                    resObj.return_message = "Success";
                    callback(resObj);

                    if (req.body.APPSTATUS == 2) {
                        // Deleting Appoinment in MEPATAPP
                        dbModels.MEPATAPP.destroy({
                            where: {
                                APPNUMBER: req.body.APPOINTMENTID
                            }
                        }).then(function (deletedReport) { });
                    }
                }).catch(function (err) {
                    logs.createLogs('Appointment Status update Failed:' + JSON.stringify(err));
                    resObj.error.message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                    resObj.error.code = 20009;
                    res.status(500);
                    callback(resObj);
                });
            }
        ];


        if (req.body.APPSTATUS && (!req.body.APPOINTMENTID || req.body.APPSTATUS != 2)) {
            res.status(400);
            resObj.error.message = errooMsg.INVALID_REQUEST;
            resObj.error.code = 20001;
            return callback(resObj);
        } else if (!req.body.APPSTATUS && (!req.body.DATE || !req.body.APPOINTMENTID)) {
            res.status(400);
            resObj.error.message = errooMsg.INVALID_REQUEST;
            resObj.error.code = 20010;
            return callback(resObj);
        } else {

            async.series(updateAppointment, (err, results) => {
                if (err) {
                    logs.createLogs('Error occureed at async.series' + err, 'error');
                    res.status = 500;
                    resObj.return_message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                    resObj.error.message = errooMsg.APPOINTMENT_UPDATE_FAILED;
                    resObj.error.code = 20011;
                    return callback(resObj);
                }
            });
        }
    }

    // find My Doctor(Patients Appointment Doctors History)
    router.get('/findMyDoctor/patcode/:patcode', function (req, res) {

        logs.createLogs('findMyDoctor api called.');
        appointmentDoctorsList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    function appointmentDoctorsList(req, res, callback) {

        logs.createLogs('appointmentDoctorsList() Called');

        // Selecting patients appointment history
        dbModels.IANEWAPP.findAll({
            where: {
                CLIENTCODE: req.params.patcode
            }
        }).then(function (patientsAppointmentList) {

            if (patientsAppointmentList.length != 0) {

                var doctorsVisited = [];
                for (var i = 0; i < patientsAppointmentList.length; i++) {
                    if (!doctorsVisited.includes(patientsAppointmentList[i].dataValues.CLINICCODE)) {
                        doctorsVisited.push(patientsAppointmentList[i].dataValues.CLINICCODE);
                    }
                }

                // Selecting doctors list.
                if (doctorsVisited.length != 0) {
                    dbModels.IACLINIC.findAll({
                        where: {
                            CLINICCODE: doctorsVisited
                        }
                    }).then(function (doctorsListFromDb) {

                        logs.createLogs('Patient visited Doctors List fecthed successfully');
                        callback(doctorsListFromDb);
                    }).catch(function (err) {

                        logs.createLogs('Fetching doctors failed' + err, 'error');
                        res.status(500);
                        callback(err);

                    });
                } else {
                    logs.createLogs('Patient has no appointment history');
                    callback([]);
                }
            } else {
                logs.createLogs('Invalid Patcode');
                res.status(400);
                callback([]);
            }
        }).catch(function (err) {

            logs.createLogs('Error occured while  fetching patients appointment List' + JSON.stringify(err), 'error');
            res.status(500);
            callback(err);
        });
    }


    // Patient insurance list
    router.get('/insurance/patcode/:patcode', function (req, res) {

        logs.createLogs('insurance api called.');
        getInsuranceList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    var authstatus = { 1: 'In Progress', 2: 'Approved', 3: 'Rejected' };
    function getInsuranceList(req, res, callback) {
        logs.createLogs('getInsuranceList method called');

        var resObj = {
            'Lab': null,
            'Surgery': null,
            'MRI': null
        };

        var sqlQuery = insuranceSql;
        sqlQuery = sqlQuery.replace('REPLACEPATCODE', "'"+req.params.patcode+"'");
        sequelizeObj.query(sqlQuery, { type: sequelizeObj.QueryTypes.SELECT })
            .then(function (dBValue) {

                var labList = [];
                var surgeryList = [];
                var mriList = [];

                for (var i = 0; i < dBValue.length; i++) {
                    var insuranceObj = {
                        'ID': dBValue[i].BILLCODE,
                        'DESC': dBValue[i].BILLDESC,
                        'STATUS': authstatus[dBValue[i].AUTHSTATUS],
                        'DATE': dateUtil.getStandardDateFormat(dBValue[i].ORDERDATE, 'YYYY-MM-DD')
                    };

                    if (dBValue[i].SERVTYP == 5) labList.push(insuranceObj);
                    else if (dBValue[i].SERVTYP == 7) surgeryList.push(insuranceObj);
                    else mriList.push(insuranceObj);
                }
                resObj.Lab = labList;
                resObj.Surgery = surgeryList;
                resObj.MRI = mriList;
                callback(resObj);
            }).catch(function (err) {
                logs.createLogs('insurance Sql Fetch Failed:' + JSON.stringify(err));
                res.status(500);
                callback(err);
            });
    }

    // Patient prescription list
    router.get('/prescription/patcode/:patcode', function (req, res) {

        logs.createLogs('prescription api called.');
        getPrescriptionList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    function getPrescriptionList(req, res, callback) {
        logs.createLogs('getPrescriptionList method called');

        var sqlQuery = prescriptionSql;
        sqlQuery = sqlQuery.replace('REPLACEPATCODE', req.params.patcode);
        sequelizeObj.query(sqlQuery, { type: sequelizeObj.QueryTypes.SELECT })
            .then(function (dBValue) {
                for (var i = 0; i < dBValue.length; i++) {
                    dBValue[i].CREATEDT = dateUtil.getStandardDateFormat(dBValue[i].CREATEDT, 'YYYY-MM-DD');
                }
                callback(dBValue);
            }).catch(function (err) {
                logs.createLogs('prescription Sql Fetch Failed:' + JSON.stringify(err));
                res.status(500);
                callback(err);
            });
    }

    // Patient diognosis by appid
    router.get('/diagnosis/appid/:appid', function (req, res) {

        logs.createLogs('diognosis api by appid called.');
        req.body.isByPatcode = false;
        getDiognosisList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    // Patient diognosis list
    router.get('/diagnosis/patcode/:patcode', function (req, res) {

        logs.createLogs('diognosis api called.');
        req.body.isByPatcode = true;
        getDiognosisList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    function getDiognosisList(req, res, callback) {
        logs.createLogs('getDiognosisList method called');

        var sqlQuery = diognosisSql;
        if (req.body.isByPatcode)
            sqlQuery = sqlQuery.replace('REPWHERE', 'A.PATCODE=' + req.params.patcode);
        else
            sqlQuery = sqlQuery.replace('REPWHERE', 'A.APPTNO=' + req.params.appid);

        sequelizeObj.query(sqlQuery, { type: sequelizeObj.QueryTypes.SELECT })
            .then(function (dBValue) {
                for (var i = 0; i < dBValue.length; i++) {
                    dBValue[i].VISTDATE = dateUtil.getStandardDateFormat(dBValue[i].VISTDATE, 'YYYY-MM-DD');
                }
                callback(dBValue);
            }).catch(function (err) {
                logs.createLogs('diognosis Sql Fetch Failed:' + JSON.stringify(err));
                res.status(500);
                callback(err);
            });
    }

    // Patient orders by appid
    router.get('/orders/appid/:appid', function (req, res) {

        logs.createLogs('orders api by appid called.');
        req.body.isByPatcode = false;
        getOrdersList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    // Patient orders list
    router.get('/orders/patcode/:patcode', function (req, res) {

        logs.createLogs('orders api called.');
        req.body.isByPatcode = true;
        getOrdersList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    function getOrdersList(req, res, callback) {
        logs.createLogs('getOrdersList method called');

        var sqlQuery = orderSql;
        if (req.body.isByPatcode)
            sqlQuery = sqlQuery.replace('REPWHERE', 'A.PATCODE=' + req.params.patcode);
        else
            sqlQuery = sqlQuery.replace('REPWHERE', 'A.APPTNO=' + req.params.appid);

        sequelizeObj.query(sqlQuery, { type: sequelizeObj.QueryTypes.SELECT })
            .then(function (dBValue) {
                for (var i = 0; i < dBValue.length; i++) {
                    dBValue[i].VISTDATE = dateUtil.getStandardDateFormat(dBValue[i].VISTDATE, 'YYYY-MM-DD');
                }
                callback(dBValue);
            }).catch(function (err) {
                logs.createLogs('orders Sql Fetch Failed:' + JSON.stringify(err));
                res.status(500);
                callback(err);
            });
    }

    // Test PDF
    router.get('/pdf/test', function (req, res) {
        logs.createLogs('prescription pdf api called');

        var presTemplate = fs.readFileSync('./templates/prescription.html', 'utf8');
        var file = { content: presTemplate };
        pdfUtil.create_v2(file, function (pdfBuffer) {
            if (pdfBuffer) {
                res.writeHead(200, {
                    'Content-Type': 'application/pdf',
                    'Content-Disposition': 'filename=prescription\"' + req.host + '.pdf'
                });
                res.end(pdfBuffer, 'binary');
            } else {
                sendDummyPdf(res, 'prescription');
            }
        });
    });

    // Patient prescription pdf
    router.get('/prescription/pdf', function (req, res) {
        logs.createLogs('prescription pdf api called');

        var resObj = {
            error: {
                message: null,
                code: null
            }
        };

        var sqlQuery = presPdfSql;
        sqlQuery = sqlQuery.replace('REPLACEPATCODE', req.query.patcode);
        sqlQuery = sqlQuery.replace('REPLACEDATE', req.query.date.replace(/-/g, ""));
        sequelizeObj.query(sqlQuery, { type: sequelizeObj.QueryTypes.SELECT })
            .then(function (dBValue) {

                if (dBValue.length != 0) {
                    var presTemplate = fs.readFileSync('./templates/prescription.html', 'utf8');

                    //Calculating Age
                    var age = 0;
                    if (dBValue[0].BDATE && dBValue[0].BDATE != '') {
                        var bdate = dateUtil.getStandardDateFormat(dBValue[0].BDATE, 'YYYY-MM-DD');
                        var dateObj = new Date(bdate);
                        age = dateUtil.getAge(dateObj.getTime());
                    }

                    //Gender
                    var gender = 'Male';
                    if (dBValue[0].SEX && dBValue[0].SEX == 2) gender = 'Female';

                    //Replacing placeholders
                    var presDateSplitted = req.query.date.split("-");
                    presTemplate = presTemplate.replace('REPUHID', dBValue[0].PATID);
                    presTemplate = presTemplate.replace('PRESDATE', presDateSplitted[2] + " " + mlist[presDateSplitted[1] - 1] + " " + presDateSplitted[0]);
                    presTemplate = presTemplate.replace('PATNAME', dBValue[0].DISNAME);
                    presTemplate = presTemplate.replace('PATAGE', age + ' Yrs');
                    presTemplate = presTemplate.replace('PRESNO', dBValue[0].PRECODE);
                    presTemplate = presTemplate.replace('PATGEN', gender);
                    presTemplate = presTemplate.replace('VISITID', dBValue[0].VISITID);
                    presTemplate = presTemplate.replace('DOCTORNAME', dBValue[0].CLINICDESC);

                    //Replaceing Prescription 
                    var presHtml = '';
                    for (var i = 0; i < dBValue.length; i++) {
                        presHtml = presHtml + "<tr> <td>" + i + 1 + "</td> <td>" + dBValue[i].ITEMNAME + "</td> <td>" + dBValue[i].DOSEQTY + "</td> <td>" + dBValue[i].FREQDESC + "</td> <td>" + dBValue[i].DURATION + " Days</td> </tr>";
                    }
                    presTemplate = presTemplate.replace('REPLACEPRES', presHtml);

                    var file = { content: presTemplate };
                    pdfUtil.create_v2(file, function (pdfBuffer) {
                        if (pdfBuffer) {
                            res.writeHead(200, {
                                'Content-Type': 'application/pdf',
                                'Content-Disposition': 'filename=prescription\"' + req.query.date + '.pdf'
                            });
                            res.end(pdfBuffer, 'binary');
                        } else {
                            sendDummyPdf(res, 'prescription');
                        }
                    });

                } else {
                    sendDummyPdf(res, 'prescription');
                }
            }).catch(function (err) {
                logs.createLogs('prescription Sql Fetch Failed:' + JSON.stringify(err));
                sendDummyPdf(res, 'prescription');
            });
    });


    // Patient lab-reports pdf
    router.get('/report/lab/pdf', function (req, res) {
        logs.createLogs('lab reports pdf api called');

        var resObj = {
            error: {
                message: null,
                code: null
            }
        };

        var sqlQuery = labPdfSql;
        sqlQuery = sqlQuery.replace('REPLACEPATCODE', req.query.patcode);
        sqlQuery = sqlQuery.replace('REPLACEDATE', req.query.date.replace(/-/g, ""));
        sequelizeObj.query(sqlQuery, { type: sequelizeObj.QueryTypes.SELECT })
            .then(function (dBValue) {

                if (dBValue.length != 0) {

                    var mainTestByDocnum = {};
                    var docNumList = [];
                    for (var w = 0; w < dBValue.length; w++) {
                        if (!docNumList.includes(dBValue[w].DOCNUM.trim()))
                            docNumList.push(dBValue[w].DOCNUM.trim());

                        mainTestByDocnum[dBValue[w].DOCNUM.trim()] = docNumList[w];
                    }
                    dbModels.MLORDANA.findAll({
                        where: {
                            DOCNUM: docNumList
                        },
                        attributes: ['DOCNUM', 'INVHDESC', 'PARAMDESC', 'RESULT', 'NUMRESULT', 'ALLOWLOW', 'ALLOWHIGH', 'REPUNIT', 'DOCDATE']
                    }).then(function (labWithSubTest) {
                        var docDates = [];
                        var noOfDocNums = [];
                        var noOfInvhDesc = [];
                        var finalList = [];

                        //Creating the unique DOCNUM and  PARAMDESC list
                        for (var m = 0; m < labWithSubTest.length; m++) {

                            if (noOfDocNums.indexOf(labWithSubTest[m].DOCNUM) <= -1) {
                                noOfDocNums.push(labWithSubTest[m].DOCNUM);
                                var docDateFormated = labWithSubTest[m].DOCDATE;
                                docDateFormated = docDateFormated.toString().substring(0, 4) + '-' + docDateFormated.toString().substring(4, 6) + '-' + docDateFormated.toString().substring(6, 8);
                                docDates.push(docDateFormated);
                            }
                            if (noOfInvhDesc.indexOf(labWithSubTest[m].INVHDESC) <= -1) {
                                noOfInvhDesc.push(labWithSubTest[m].INVHDESC);
                            }
                        }

                        for (var z = 0; z < noOfDocNums.length; z++) {
                            var patientLabReportByINVHDesc = [];

                            for (var j = 0; j < noOfInvhDesc.length; j++) {
                                var tempListForDocNumSort = [];

                                //Iterating the PatientsLab report list and craeting list by DocNum
                                labWithSubTest.filter(createLabReportByDOCNUM);

                                function createLabReportByDOCNUM(patientListObject) {
                                    if (patientListObject.REPUNIT && patientListObject.RESULT && patientListObject.PARAMDESC && (patientListObject.DOCNUM.toString().trim() == noOfDocNums[z].toString().trim()) && (patientListObject.INVHDESC.toString().trim() == noOfInvhDesc[j].toString().trim())) {
                                        tempListForDocNumSort.push({
                                            "name": patientListObject.PARAMDESC.toString().trim(),
                                            "result": patientListObject.RESULT.toString().trim(),
                                            "max": patientListObject.ALLOWHIGH,
                                            "min": patientListObject.ALLOWLOW,
                                            "unit": patientListObject.REPUNIT.toString().trim()
                                        });
                                    }
                                }
                                if (tempListForDocNumSort.length != 0) {
                                    patientLabReportByINVHDesc.push({
                                        "name": noOfInvhDesc[j].toString().trim(),
                                        "subtest": tempListForDocNumSort
                                    });
                                }
                            }
                            finalList.push({
                                "docnum": noOfDocNums[z].trim(),
                                "test": patientLabReportByINVHDesc
                            });

                        }

                        //Calculating Age
                        var age = 0;
                        if (dBValue[0].BDATE && dBValue[0].BDATE != '') {
                            var bdate = dateUtil.getStandardDateFormat(dBValue[0].BDATE, 'YYYY-MM-DD');
                            var dateObj = new Date(bdate);
                            age = dateUtil.getAge(dateObj.getTime());
                        }

                        //Gender
                        var gender = 'Male';
                        if (dBValue[0].SEX && dBValue[0].SEX == 2) gender = 'Female';

                        //Replacing placeholders
                        var labDateSplitted = req.query.date.split("-");
                        var pdfHeader= "<br> <img class='logo' src='http://localhost:3001/images/logo.png' align='left' alt='Logo' width='400' height='90' /> <br>";
                        var pdfFooter= "<br><br><br><br> <img class='certificate' src='http://localhost:3001/images/certificate.png' alt='certificate' width='140' height='140' /> <br>    <p class='address'>Al Emadi Hospital - Al Hilal West, Doha, Qatar PO.Box : 50000 <br> Phone: +974 4477 6444</p> <p class='address'>Email: info@alemadihospital.com.qa</p>";

                        //Replaceing Lab report
                        var labTemplate =null;
                        for (var i = 0; i < dBValue.length; i++) {

                            var labHTML = '';
                            if(labTemplate){ 
                                var templateClone= fs.readFileSync('./templates/labreport.html', 'utf8'); 
                                labTemplate = labTemplate+ templateClone;
                                labTemplate = labTemplate.replace("PDFHEADER", "");
                                if(i==dBValue.length-1)
                                labTemplate = labTemplate.replace("PDFFOOTER", pdfFooter);
                                else
                                labTemplate = labTemplate.replace("PDFFOOTER", "");
                            }
                            else{ 
                                labTemplate = fs.readFileSync('./templates/labreport.html', 'utf8');
                                labTemplate = labTemplate.replace("PDFHEADER", pdfHeader);
                                labTemplate = labTemplate.replace("PDFFOOTER", "");
                            }

                            labTemplate = labTemplate.replace("PATID", dBValue[0].PATCODE);
                            labTemplate = labTemplate.replace("PATNAME", dBValue[0].PATNAME);
                            labTemplate = labTemplate.replace("REQON", dBValue[0].COLLDATE.toString().substring(6, 8) + " " + mlist[parseInt(dBValue[0].COLLDATE.toString().substring(4, 6)) - 1] + " " + dBValue[0].COLLDATE.toString().substring(0, 4));
                            labTemplate = labTemplate.replace("REQTIME", labDateSplitted[2] + " " + mlist[labDateSplitted[1] - 1] + " " + labDateSplitted[0]);
                            labTemplate = labTemplate.replace("REPON", labDateSplitted[2] + " " + mlist[labDateSplitted[1] - 1] + " " + labDateSplitted[0]);
                            labTemplate = labTemplate.replace("PATQID", dBValue[0].PATIDNUM);
                            labTemplate = labTemplate.replace("PATNATION", dBValue[0].NATNAME);
                            labTemplate = labTemplate.replace("PATMOBILE", dBValue[0].PHOCELL);
                            labTemplate = labTemplate.replace("PATAGE", age);
                            labTemplate = labTemplate.replace("PATGENDER", gender);
                            labTemplate = labTemplate.replace("REFDOC", dBValue[0].ORDBYDESC);
                            labTemplate = labTemplate.replace("LABID", dBValue[i].DOCNUM);

                                if (finalList[i].docnum == dBValue[i].DOCNUM.trim()) {
                                    for (var y = 0; y < finalList[i].test.length; y++) {
                                        labHTML = labHTML + "<tr> <td> <b><u>" +  finalList[i].test[y].name  + "</u></b> </td> <td></td> <td> </td> <td> </td> <td> </td> </tr> "
                                        for (var n = 0; n < finalList[i].test[y].subtest.length; n++) {
                                            labHTML = labHTML + "<tr> <td>" + finalList[i].test[y].subtest[n].name + "</td> <td> " + finalList[i].test[y].subtest[n].result + "</td> <td>" + finalList[i].test[y].subtest[n].unit + " </td> <td> </td> <td>" + finalList[i].test[y].subtest[n].min + "-" + finalList[i].test[y].subtest[n].max + " </td> </tr>";
                                        }
                                    }
                                }

                                labTemplate = labTemplate.replace('REPLACELABREP', labHTML);
                        }

                        var file = { content: labTemplate };
                        pdfUtil.create_v2(file, function (pdfBuffer) {
                            if (pdfBuffer) {
                                res.writeHead(200, {
                                    'Content-Type': 'application/pdf',
                                    'Content-Disposition': 'filename=labreport\"' + req.query.date + '.pdf'
                                });
                                res.end(pdfBuffer, 'binary');
                            } else {
                                sendDummyPdf(res, 'lab');
                            }
                        });
                    });
                } else {
                    sendDummyPdf(res, 'lab');
                }
            }).catch(function (err) {
                logs.createLogs('prescription Sql Fetch Failed:' + JSON.stringify(err));
                sendDummyPdf(res, 'lab');
            });
    });

    // Patient QID bar code image
    router.get('/charts/barcode/qid/:qid', function (req, res) {
        logs.createLogs('QID barcode api called');

        chartsUtil.createBarCode(req.params.qid, function (barcode) {
            if (barcode != false) {
                res.writeHead(200, {
                    'Content-Type': 'image/png',
                    'Content-Disposition': 'filename=qid.png'
                });
                res.end(barcode, 'binary');
            } else {
                var resObj = { error: {} };
                resObj.error.message = errooMsg.INTERNAL_ERROR;
                resObj.error.code = 99021;
                res.status(500);
                res.json(resObj);
            }
        });
    });

    function sendDummyPdf(res, type) {

        var filename = 'lab_null.pdf';
        if (type == 'prescription') filename = 'prescription_null.pdf';

        var pdf = fs.readFileSync('./public/pdf/' + filename);
        res.writeHead(200, {
            'Content-Type': 'application/pdf',
            'Content-Disposition': 'filename=dummy.pdf'
        });
        res.end(pdf, 'binary');
    }

    //Logout
    router.post('/session/logout', function (req, res) {
        res.json({
            session_status: 'expired'
        });
    });

    //Test
    router.get('/test', function (req, res) {
        res.json({ isSessionValid: true });
    });

    function compare( a, b ) {
        if ( a.DATESTR > b.DATESTR ){
          return -1;
        }
        if ( a.DATESTR < b.DATESTR ){
          return 1;
        }
        return 0;
    }

    return router;
};