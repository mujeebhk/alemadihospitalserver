module.exports = function (dbModels, sequelize) {

    var express = require('express');
    var router = express.Router();
    const errooMsg = require('./ErrorMessage');
    const logs = require('../common/loggers');

    //Search pharmacy
    router.get('/search', function (req, res) {
        search(req, res, function (response) {
            return res.json(response);
        });
    });

    function search(req, res, callback) {
        logs.createLogs("search() called");

        if(req.query.itemcode==''){
            req.query.itemcode= 'TRAMADOL (TRAMAL) 100 MG/2ML AMPULE';
        }else req.query.itemcode= req.query.itemcode.toString().toUpperCase().trim();

        var loccode = ['PLOC001', 'PLOC165', 'PLOC096'];
        var locObj = [{ "LOCDESC": "PHARMACY MAIN STORE", "LOCCODE": "PLOC001", "INSTOCK": false }, { "LOCDESC": "NORTH GATE PHARMACY", "LOCCODE": "PLOC165", "INSTOCK": false }, { "LOCDESC": "OPD PHARMACY", "LOCCODE": "PLOC096", "INSTOCK": false }];
        var customSql = "SELECT B.ITEMCODE,A.ITEMDESC,B.QTYHAND,C.LOCDESC,C.LOCCODE FROM MPITEM A INNER JOIN MPITMLOC B ON A.ITEMCODE=B.ITEMCODE INNER JOIN MPLOC C ON B.LOCCODE=C.LOCCODE WHERE A.ITEMDESC like '%" + req.query.itemcode + "%' AND (B.LOCCODE='PLOC001' OR B.LOCCODE='PLOC096' OR B.LOCCODE='PLOC165' )";
        sequelize.query(customSql, { type: sequelize.QueryTypes.SELECT })
            .then(function (dBValue) {

                var locObjCopy = Object.assign([], locObj);
                for (var z = 0; z < locObjCopy.length; z++) {
                    for (var i = 0; i < dBValue.length; i++) {
                        if (locObjCopy[z].LOCCODE == dBValue[i].LOCCODE.trim()) {
                            if (dBValue[i].QTYHAND > 0) locObjCopy[z].INSTOCK = true;
                            for (var key in dBValue[i]) {
                                locObjCopy[z][key] = dBValue[i][key];
                                if(key != 'QTYHAND') locObjCopy[z][key] =locObjCopy[z][key].trim();
                            }
                        }
                    }
                }
                callback(locObjCopy);
            }).catch(function (err) {
                logs.createLogs('Searching Pharmacy by custom query list failed. Error:' + err);
                res.status(500);
                callback(err);
            });
    }

    //Pharmacy items list
    router.get('/stock/list', function (req, res) {
        getPharmacyItemsList(req, res, function (response) {
            return res.json(response);
        });
    });


    function getPharmacyItemsList(req, res, callback) {
        logs.createLogs("getPharmacyItemsList() called");

        // Selecting pharmacy data.
        dbModels.MPITEM.findAll({
            attributes: [['QTYHAND', 'quantity'], ['ITEMDESC', 'name']]
        }).then(function (pharmacyFromDb) {
            logs.createLogs('Pharmacy List fetched successfully');
            return callback(pharmacyFromDb);
        }).catch(function (err) {
            logs.createLogs(' Fetching Pharmacy list failed. Error:' + err);
            res.status(500);
            callback(err);
        });
    }

    return router;
}