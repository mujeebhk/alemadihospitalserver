module.exports = function (dbModels, sequelizeObj) {

    var express = require('express');
    var router = express.Router();
    const logs = require('../common/loggers');


    //Custom SQL
    const insuranceNameListSql = 'SELECT INSUCODE, NAME FROM ISINCOM WHERE INSUCODE IS NOT NULL';

    // Insurance Names List
    router.get('/names/list', function (req, res) {

        logs.createLogs('insurance names list api called.');
        getInsuranceNamesList(req, res, function (dbReturnValue) {
            return res.json(dbReturnValue);
        });
    });

    function getInsuranceNamesList(req, res, callback) {
        logs.createLogs('getInsuranceNamesList method called');

        sequelizeObj.query(insuranceNameListSql, { type: sequelizeObj.QueryTypes.SELECT })
            .then(function (dBValue) {
                callback(dBValue);
            }).catch(function (err) {
                logs.createLogs('insurance names list Sql Fetch Failed:' + JSON.stringify(err));
                res.status(500);
                callback(err);
            });
    }

    return router;
};