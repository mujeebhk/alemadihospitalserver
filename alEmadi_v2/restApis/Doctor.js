const e = require('express');

module.exports = function (dbModels, sequelize) {

    var express = require('express');
    var router = express.Router();
    var async = require("async");
    const logs = require('../common/loggers');
    var dateUtil = require('../utils/DateUtil');
    const { Op } = require("sequelize");
    const errooMsg = require('./ErrorMessage');

    //CneterList Json
    const all_centers = [
        {
            id: "1",
            name: "North Center"
        },
        {
            id: "2",
            name: "Hilal Center"
        }
    ];

    // Get Doctors by clinicCode
    router.get('/clinicCode/:clinicCode', function (req, res) {

        logs.createLogs('Doctors by clinicCode api called');
        getDoctor(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });
    });

    function getDoctor(req, res, callback) {
        logs.createLogs("getDoctor() called.");

        var resObj = {
            CLINICCODE: null,
            CLINICDESC: null,
            DEPTCODE: null,
            DEPTDESC: null,
            error: {
                message: null,
                code: null
            }
        };

        // Selecting doctors list.
        dbModels.IACLINIC.findAll({
            where: {
                CLINICCODE: req.params.clinicCode
            }
        }).then(function (dbData) {
            if (dbData.length != 0) {
                var locationObj = all_centers[1];
                if (dbData[0].dataValues.CLINICCODE && dbData[0].dataValues.CLINICCODE.toString().includes("N")) locationObj = all_centers[0];
                resObj.CLINICCODE = req.params.clinicCode;
                resObj.CLINICDESC = dbData[0].dataValues.CLINICDESC.trim();
                resObj.DEPTCODE = dbData[0].dataValues.DEPTCODE;
                resObj.DEPTDESC = dbData[0].dataValues.DEPTDESC.trim();
                resObj.location = locationObj;
            } else {
                resObj.error.message = errooMsg.INVALID_ID;
                resObj.error.code = '21001';
            }
            callback(resObj);
        }).catch(function (err) {
            logs.createLogs('Fetching doctor failed' + err, 'error');
            res.status(500);
            resObj.error.message = errooMsg.INTERNAL_ERROR;
            resObj.error.code = '21002';
            callback(resObj);
        });
    }

    // Get Doctors List by departments
    router.get('/list', function (req, res) {

        logs.createLogs('Doctors list api called');
        getDoctorsListByDepartment(req, res, function (dbReturnValue) {
            res.json(dbReturnValue);
        });
    });


    function getDoctorsListByDepartment(req, res, callback) {
        logs.createLogs("getDoctorsListByDepartment() called.");

        // Selecting doctors list.
        dbModels.IACLINIC.findAll().then(function (doctorsListFromDb) {
            var uniqueDepartmentList = [];
            var finalList = [];


            //Creating the unique Deptartment list
            for (var i = 0; i < doctorsListFromDb.length; i++) {

                if (uniqueDepartmentList.indexOf(doctorsListFromDb[i].DEPTDESC) <= -1) {
                    uniqueDepartmentList.push(doctorsListFromDb[i].DEPTDESC);
                }
            }

            //Creating doctors list by dept.
            for (var y = 0; y < uniqueDepartmentList.length; y++) {
                var doctorsList = [];

                doctorsListFromDb.filter(createDoctorsListByDept);
                function createDoctorsListByDept(doctorsObject) {

                    if (uniqueDepartmentList[y] == doctorsObject.DEPTDESC) {
                        if ((uniqueDepartmentList[y].toString().toUpperCase().trim().indexOf(doctorsObject.CPRONAME.toString().toUpperCase().trim()) <= -1)) {
                            if (parseInt(doctorsObject.DEPTCODE) < 500) {
                                var locationObj = all_centers[1];
                                if (doctorsObject.CLINICCODE && doctorsObject.CLINICCODE.toString().includes("N")) locationObj = all_centers[0];
                                var iconName= doctorsObject.CLINICCODE+".jpg";
                                if(doctorsObject.CLINICCODE)
                                iconName=doctorsObject.CLINICCODE.toString().replace(/ /g, "").replace(/[^0-9]/g,'')+".jpg";
                                doctorsList.push({ "name": doctorsObject.CPRONAME.toString().trim(), "clinicCode": doctorsObject.CLINICCODE, "location": locationObj, iconURL: '/images/doctor/icon/'+iconName });
                            }
                        }
                    }
                }
                if (doctorsList.length != 0) {
                    finalList.push({ "name": uniqueDepartmentList[y].toString().trim(), "doctors": doctorsList });
                }
            }

            //ResObj
            callback({ "departments": finalList });
        }).catch(function (err) {
            logs.createLogs('Fetching doctor failed' + err, 'error');
            res.status(500);
            callback(err);
        });
    }

    // Get Schedule and available appointment time of a doctor
    router.get('/timeslots', function (req, res) {

        logs.createLogs('calling doctors getDoctorsScheduleAndAppointments()');
        getDoctorsScheduleAndAppointments(req, res, function (dbReturnValue) {

            if (req.query.date) {
                res.json(dbReturnValue);
            } else {
                var resObj = {
                    one_week: {},
                    curr_month: {},
                    next_month: {}
                };

                var now = new Date();
                var todayDate = now.getDate() + '/' + parseInt(now.getMonth() + 1) + '/' + now.getFullYear();
                var weekDateObj = new Date();
                weekDateObj.setDate(now.getDate() + 7);
                var weekLastDate = weekDateObj.getDate();
                var weekMonth = weekDateObj.getMonth() + 1;
                var curr_month = now.getMonth() + 1;
                var next_month = now.getMonth() + 2;

                for (var key in dbReturnValue) {
                    var keySplitted = key.split("/");

                    if (key == todayDate) {
                        for (var q = 0; q < dbReturnValue[key].length; q++) {
                            var splittedTime = dbReturnValue[key][q].slotTime.split(":");
                            if (parseInt(splittedTime[0]) <= now.getHours()) {
                                dbReturnValue[key][q].availableStatus = false;

                                if ((parseInt(splittedTime[0]) == now.getHours())) {
                                    if (parseInt(splittedTime[1]) <= now.getMinutes())
                                        dbReturnValue[key][q].availableStatus = false;
                                    else dbReturnValue[key][q].availableStatus = true;
                                }
                            }
                        }
                    }

                    if (parseInt(keySplitted[0]) <= weekLastDate) {
                        resObj.one_week[key] = (dbReturnValue[key]);
                    }
                    if (parseInt(keySplitted[1]) == curr_month) {
                        resObj.curr_month[key] = (dbReturnValue[key]);
                    }

                    if (parseInt(keySplitted[1]) == next_month) {
                        resObj.next_month[key] = (dbReturnValue[key]);
                    }
                }

                res.json(resObj);
            }

        });
    });

    function getDoctorsScheduleAndAppointments(req, res, callback) {
        logs.createLogs("getDoctorsScheduleAndAppointments() called.");

        //Where Cond
        var whereCond = { CLINICCODE: req.query.clinicCode };
        if (req.query.date) {
            var formatedDate = parseFloat(dateUtil.getCustomDateFormat(req.query.date));
            whereCond.APPDATE = parseFloat(formatedDate);
        } else {
            var now = new Date();
            var twomonths = new Date();
            twomonths.setDate(now.getDate() + 62);

            var todayDate = parseFloat(dateUtil.getCustomDateFormat(now.getTime()));
            var nextMonthDate = parseFloat(dateUtil.getCustomDateFormat(twomonths.getTime()));
            whereCond.APPDATE = {
                [Op.lte]: parseFloat(nextMonthDate),
                [Op.gte]: parseFloat(todayDate)
            };
        }

        var doctorsScheduleList = [];
        async.doWhilst(function (cb) {

            // Selecting doctors schedule list.
            dbModels.IACLSCDD.findAll({
                where: whereCond
            }).then(function (doctorsScheduleListFromDb) {

                if (doctorsScheduleListFromDb.length != 0) {
                    doctorsScheduleList = doctorsScheduleListFromDb;
                    cb();
                } else {
                    res.status(404);
                    return callback({ "ScheduleStatus": 'Schedule not available for the given date' })
                }
            }).catch(function (err) {
                logs.createLogs(' Fetching doctor schedule details failed' + err, 'error');
                res.status(500);
                callback(err);
            });
        }, function () {

            // Selecting doctors booked slots.
            var resobj = {};
            whereCond.APPSTATUS = 1;
            dbModels.IANEWAPP.findAll({
                where: whereCond
            }).then(function (doctorsBookedSlotsFromDb) {

                if (doctorsBookedSlotsFromDb.length != 0) {

                    for (var n = 0; n < doctorsScheduleList.length; n++) {
                        var finalSlotList = [];
                        var scheduleDate = dateUtil.getStandardDateFormat(doctorsScheduleList[n].dataValues.APPDATE);

                        var doctorsFromWorkHour = dateUtil.formatTimeShow(doctorsScheduleList[n].dataValues.FROMWORKHR).toString().substring(0, 2);
                        var doctorsToWorkHour = dateUtil.formatTimeShow(doctorsScheduleList[n].dataValues.TOWORKHR).toString().substring(0, 2);
                        var doctorsFromMin = dateUtil.formatTimeShow(doctorsScheduleList[n].dataValues.FROMWORKHR).toString().substring(3, 5);
                        var doctorsToMin = dateUtil.formatTimeShow(doctorsScheduleList[n].dataValues.TOWORKHR).toString().substring(3, 5);

                        var slotsList = dateUtil.createTimeIntervalArray(0, doctorsScheduleList[n].dataValues.DURATION, doctorsFromWorkHour, doctorsToWorkHour, doctorsFromMin, doctorsToMin);
                        var bookedSlotsIndex = [];

                        var bookedSlots = [];
                        for (var x = 0; x < doctorsBookedSlotsFromDb.length; x++) {
                            if (doctorsScheduleList[n].dataValues.APPDATE == doctorsBookedSlotsFromDb[x].dataValues.APPDATE) {
                                var slotObj = doctorsBookedSlotsFromDb[x].dataValues;
                                bookedSlots.push(slotObj);
                            }
                        }

                        for (var i = 0; i < bookedSlots.length; i++) {
                            var formatedBookedTime = dateUtil.formatTimeShow(bookedSlots[i].APPTIME);

                            for (var j = 0; j < slotsList.length; j++) {

                                var aa1 = formatedBookedTime.split(":");
                                var aa2 = slotsList[j].split(":");

                                var d1 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa1[0], 10), parseInt(aa1[1], 10), parseInt(aa1[2], 10));
                                var d2 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa2[0], 10), parseInt(aa2[1], 10), parseInt(aa2[2], 10));
                                var dd1 = d1.valueOf();
                                var dd2 = d2.valueOf();

                                if ((d1.getHours() == d2.getHours()) && (dd1 < dd2)) {

                                    logs.createLogs('Slots Removed' + slotsList.indexOf(slotsList[j]));
                                    logs.createLogs('Booked and SlotListObject = ' + formatedBookedTime.toString() + ' ' + slotsList[j].toString());

                                    if (bookedSlotsIndex.indexOf(j - 1) <= -1) {
                                        bookedSlotsIndex.push(j - 1);
                                    }
                                    var bookedSlotMinute = d1.getMinutes() + bookedSlots[i].DEFSLOT;
                                    var slotMinute = d2.getMinutes();
                                    if ((bookedSlotMinute > slotMinute) && (bookedSlotsIndex.indexOf(j) <= -1)) {
                                        bookedSlotsIndex.push(j);
                                    }
                                    break;

                                } else if ((dd1 == dd2) && (bookedSlotsIndex.indexOf(j) <= -1)) {
                                    bookedSlotsIndex.push(j);
                                    break;
                                }
                            }

                        }//End of outer for loop


                        // Dividing slot status
                        for (var k = 0; k < slotsList.length; k++) {
                            if ((bookedSlotsIndex.indexOf(k) <= -1)) {
                                finalSlotList.push({ "slotTime": slotsList[k], "availableStatus": true, "CLINICCODE": doctorsScheduleList[n].dataValues.CLINICCODE });
                            } else {
                                finalSlotList.push({ "slotTime": slotsList[k], "availableStatus": false, "CLINICCODE": doctorsScheduleList[n].dataValues.CLINICCODE });
                            }
                        }

                        resobj[scheduleDate] = finalSlotList;
                    }

                    //ResObj
                    updateTimeSlots(resobj);
                    return callback(resobj);
                } else {
                    logs.createLogs('There are no booked appointments');

                    getSlotsList(resobj, doctorsScheduleList);
                    updateTimeSlots(resobj);
                    return callback(resobj);
                }

            }).catch(function (err) {
                logs.createLogs(' Fetching appointment failed' + err, 'error');
                res.status(500);
                callback(err);
            });
        });
    }

    function updateTimeSlots(slotsObj) {
        var formattedTodaysDate = dateUtil.getCustomDateFormat(new Date().getTime(), 'DD/MM/YYYY');
        for (var i = 0; i < slotsObj[formattedTodaysDate].length; i++) {
            var slotHour = slotsObj[formattedTodaysDate][i].slotTime.split(":")[0];
            var slotMinute = slotsObj[formattedTodaysDate][i].slotTime.split(":")[1];
            var slotTime = new Date();
            slotTime.setHours(parseInt(slotHour) - 2);
            slotTime.setMinutes(parseInt(slotMinute));

            if (slotTime.getTime() < new Date().getTime())
                slotsObj[formattedTodaysDate][i].availableStatus = false;
        }
    }

    function getSlotsList(resobj, doctorsScheduleList) {
        for (var m = 0; m < doctorsScheduleList.length; m++) {
            var finalSlotList1 = [];
            var scheduleDate1 = dateUtil.getStandardDateFormat(doctorsScheduleList[m].dataValues.APPDATE);

            var doctorsFromWorkHour1 = dateUtil.formatTimeShow(doctorsScheduleList[m].dataValues.FROMWORKHR).toString().substring(0, 2);
            var doctorsToWorkHour1 = dateUtil.formatTimeShow(doctorsScheduleList[m].dataValues.TOWORKHR).toString().substring(0, 2);
            var scheduleFromMin = dateUtil.formatTimeShow(doctorsScheduleList[m].dataValues.FROMWORKHR).toString().substring(3, 5);
            var scheduleToMin = dateUtil.formatTimeShow(doctorsScheduleList[m].dataValues.TOWORKHR).toString().substring(3, 5);
            var slotsList1 = dateUtil.createTimeIntervalArray(0, doctorsScheduleList[m].dataValues.DURATION, doctorsFromWorkHour1, doctorsToWorkHour1, scheduleFromMin, scheduleToMin);

            for (var l = 0; l < slotsList1.length; l++) {
                finalSlotList1.push({ "slotTime": slotsList1[l], "availableStatus": true, "CLINICCODE": doctorsScheduleList[m].dataValues.CLINICCODE });
            }
            resobj[scheduleDate1] = finalSlotList1;
        }
    }

    module.exports.all_centers = all_centers;
    module.exports.getSlotsList = getSlotsList;
    return router;
};