module.exports = {
    INVALID_CREDENTIALS: 'Invalid authentication credentials',
    MISSING: 'Mandatory fields are not supplied',
    INVALID_FROM_DATE: 'from_date is greater than to_date',
    INVALID_DATERANGE: 'Invalid from_date or to_date',
    ALREADY_BOOKED: "Time slot already booked",
    INVALID_APP_DATE: "Invalid appointment_date",
    INVALID_APP_ID: 'Invalid appointment_id',
    TIMESLOT_FETCH_FAIL: 'Failed to get timeslots',
    LOGIN_FAILED: 'Failed to login user',
    PATIEN_FETCH_FAILED: 'Failed to login user',
    APPOINTMENT_CREATE_FAILED: 'Failed to create',
    APPOINTMENT_UPDATE_FAILED: 'Failed to update appointment',
    APPOINTMENT_CANCEL_FAILED: 'Failed to cancel appointment',
    INVALID_REQUEST: "Invalid Request",
    INVALID_ID: "Invalid ID",
    INTERNAL_ERROR: "Internal Server Error",
    UNAUTHORIZED: "Unauthorized Access"
};