var express = require('express'),
  errorHandler = require('express-error-handler'),
  http = require('http'),
  server;
var fs = require('fs');
var path = require('path');
var winston = require('winston');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Sequelize = require('sequelize');
var index = require('./routes/index');
var users = require('./routes/users');
var compression = require('compression');
var cors = require('cors');
const dotenv = require('dotenv');
var app = express();
var session = require('express-session');
var ApplicationConstants = require('./common/ApplicationConstants');

// Environment Config
dotenv.config();

// Configure the logging to a file.
var logger = winston.createLogger({
  transports: [
    new (winston.transports.File)({
      name: 'info-file',
      filename: 'logs/alemadi-info.log',
      level: 'info'
    }),
    new (winston.transports.File)({
      name: 'debug-file',
      filename: 'logs/alemadi-debug.log',
      level: 'debug'
    }),
    new (winston.transports.File)({
      name: 'error-file',
      filename: 'logs/alemadi-error.log',
      level: 'error'
    })
  ]
});

// To stop logs in any of the logging files(Remove comment line)
//logger.remove('info-file');
//logger.remove('error-file');
//logger.remove('debug-file');

//MsSql Config
var sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: 'mssql',
  port: process.env.DB_PORT,
  dialectOptions: {
    options: {
      requestTimeout: 30000
    }
  },
  pool: {
    max: 32,
    min: 0,
    idle: 10000
  },
  additional: {
    timestamps: false
  }
});

sequelize
  .authenticate()
  .then(function (err) {
    console.log('Connection has been established successfully.');
  })
  .catch(function (err) {
    console.log('Unable to connect to the database:', err);
  });


//cors config
const corsConfig = {
  credentials: true,
  origin: ['http://localhost:3000', 'http://localhost:8100', 'http://localhost', 'capacitor://localhost'],
};

// view engine setup
app.disable('etag');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(compression());
app.use(cors(corsConfig));
app.use(bodyParser.json({
  limit: "50mb"
}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// auto generated models
const models = require('sequelize-auto-import')(sequelize, 'models');
models.IACLINIC.removeAttribute('id');
models.IACLSCDD.removeAttribute('id');
models.IANEWAPP.removeAttribute('id');
models.MSPTIENT.removeAttribute('id');
module.exports.dbModel = models;


//Session Filters
var filter = require('./SessionFilter');
filter.sessionFilter(app);

//Sample Apis for test
app.use('/testApi', index);
app.use('/testApi', users);

//Rest Apis Path Config
var logs = require('./common/loggers')(logger);
var patient = require('./restApis/Patient')(models, sequelize);
var doctor = require('./restApis/Doctor')(models, sequelize);
var insurance = require('./restApis/Insurance')(models, sequelize);
var pharmacy = require('./restApis/Pharmacy')(models, sequelize);

app.use('/alemadi/mobile/patient', patient);
app.use('/alemadi/mobile/doctor', doctor);
app.use('/alemadi/mobile/insurance', insurance);
app.use('/alemadi/mobile/pharmacy', pharmacy);
app.use('/alemadi/session', patient);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
var defaultDocImage = fs.readFileSync('./public/images/doctor/icon/default.png');
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);

  var url = req.originalUrl.split('/');
  if (url[1] == 'images' && url[3] == 'icon') {
    console.log(req.originalUrl);
    res.writeHead(200, {
      'Content-Type': 'image/png',
      'Content-Disposition': 'filename=default.png'
    });
    res.end(defaultDocImage, 'binary');
  }else res.render('error');
});

function exitHandler(options, err) {
  if (options.cleanup) console.log('clean');
  if (err) console.log(err.stack);
  if (options.exit) process.exit();
}


//do something when app is closing
process.on('exit', exitHandler.bind(null, { cleanup: true }));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, { exit: true }));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, { exit: false }));

module.exports = app;
