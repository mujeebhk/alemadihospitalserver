var SequelizeAuto = require('sequelize-auto');

var auto = new SequelizeAuto('meddy', 'sa', 'dost1234', {
  host: 'localhost',
  dialect: 'mssql',
  port: 52258,
  directory: 'models', // prevents the program from writing to disk
  additional: {
    timestamps: false
    //...
  }
});

//Run this file to auto generate Db Models
//Run this using "node .\autoModelsGenerator.js"
auto.run(function (err) {
  if (err) throw err;

  console.log(auto.tables); // table list
  console.log(auto.foreignKeys); // foreign key list
});