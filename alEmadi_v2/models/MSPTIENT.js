const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MSPTIENT', {
    PATCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ADD1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ADD2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ADD3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ADD4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CITY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STATE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COUNTRY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ZIP: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NATION: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RACE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEYYY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEMM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEDAY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SEX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MARITAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PHONEOFF: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PHONEEXT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FAX: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PHOCELL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PHOHOME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EMAIL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATTYPE1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DECEASED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DECDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SSN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    APPREMIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILREMIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PHOTO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GUARCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GUARTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    HEADTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    HEADCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REGFLAG: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INACTIVE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATBAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VISA: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PASSPNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VISAEXP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VISATYP: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DATLAST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BABYBORN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CHILDOF: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LOCATCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BIRTHTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONRELN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONTPHONE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONTCELL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONTZIP: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONTCITY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONTSTATE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONTCNTRY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UHID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FRTVSTCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OLDPMINO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NETADVANCE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MULPACKADV: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMPLOYER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IDNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GENCONSREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONSUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BLODGRP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATCAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATOTHER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATIDTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATIDNUM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATIDOTH: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TITLE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOBYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GUESTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FIRSTMGVST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GENCONSENT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TALUK: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISTRICT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    WALKINTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WALKIN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFERNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFERADDR: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATFTPCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REGDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REFTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REFBYCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MARKERCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISREFERIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISREGEXP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REGEXPIRY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISCHRONIC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILADD1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILADD2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILADD3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILADD4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CNTRYNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NATNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IDDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RACEDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATTYPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GUARADESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VISADESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EMCONDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RELADESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MARKERDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COUNTRYDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFBYDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATFTPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RELEGION: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FATHERNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OPCREDTBAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    IPCREDTBAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATOCCUPTN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATCATDET: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CITYNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISTNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STATENAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISMERGE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MERGEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MERGEDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MERGETIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRIMARYPAT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMRELATION: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISGNEE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISGDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISGNECONT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISGNENAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HIJRIDATE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AFIRSTNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AMIDNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ALASTNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLICYNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PERMITNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SECPATIDTP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SECPATIDOT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SECPATIDNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MSPTIENT',
    schema: 'dbo',
    timestamps: false
  });
};
