var DataTypes = require("sequelize").DataTypes;
var _AUTH = require("./AUTH");
var _IACLINIC = require("./IACLINIC");
var _IACLSCDD = require("./IACLSCDD");
var _IANEWAPP = require("./IANEWAPP");
var _MEPRESCD = require("./MEPRESCD");
var _MEPRESCH = require("./MEPRESCH");
var _MLORDANA = require("./MLORDANA");
var _MLORDERD = require("./MLORDERD");
var _MLORDERH = require("./MLORDERH");
var _MPITEM = require("./MPITEM");
var _MSPTIENT = require("./MSPTIENT");
var _OLDIANEWAPP = require("./OLDIANEWAPP");

function initModels(sequelize) {
  var AUTH = _AUTH(sequelize, DataTypes);
  var IACLINIC = _IACLINIC(sequelize, DataTypes);
  var IACLSCDD = _IACLSCDD(sequelize, DataTypes);
  var IANEWAPP = _IANEWAPP(sequelize, DataTypes);
  var MEPRESCD = _MEPRESCD(sequelize, DataTypes);
  var MEPRESCH = _MEPRESCH(sequelize, DataTypes);
  var MLORDANA = _MLORDANA(sequelize, DataTypes);
  var MLORDERD = _MLORDERD(sequelize, DataTypes);
  var MLORDERH = _MLORDERH(sequelize, DataTypes);
  var MPITEM = _MPITEM(sequelize, DataTypes);
  var MSPTIENT = _MSPTIENT(sequelize, DataTypes);
  var OLDIANEWAPP = _OLDIANEWAPP(sequelize, DataTypes);


  return {
    AUTH,
    IACLINIC,
    IACLSCDD,
    IANEWAPP,
    MEPRESCD,
    MEPRESCH,
    MLORDANA,
    MLORDERD,
    MLORDERH,
    MPITEM,
    MSPTIENT,
    OLDIANEWAPP,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
