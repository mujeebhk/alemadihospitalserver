const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MEPATAPP', {
    PROVCODE: {
      type: DataTypes.CHAR(22),
      allowNull: false,
      primaryKey: true
    },
    APPDATE: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false,
      primaryKey: true
    },
    APPTIME: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false,
      primaryKey: true
    },
    SOURCE: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    AUDTDATE: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    AUDTTIME: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    AUDTUSER: {
      type: DataTypes.CHAR(8),
      allowNull: false
    },
    AUDTORG: {
      type: DataTypes.CHAR(6),
      allowNull: false
    },
    STATUS: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    APPNUMBER: {
      type: DataTypes.DECIMAL(19,0),
      allowNull: false
    },
    VISITID: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    EPSODECODE: {
      type: DataTypes.DECIMAL(19,0),
      allowNull: false
    },
    PATCODE: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    PATNAME: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    CHECKINYN: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    DEPTCODE: {
      type: DataTypes.CHAR(15),
      allowNull: false
    },
    CKINPROCOD: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    CKINPRODES: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    QUEUENO: {
      type: DataTypes.DECIMAL(19,0),
      allowNull: false
    },
    CUSTTYPE: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    POLGRPCODE: {
      type: DataTypes.CHAR(15),
      allowNull: false
    },
    POLGRPDESC: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    NURSEDONE: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    INITVISIT: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    AGEYEARS: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    CKINTIME: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'MEPATAPP',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "MEPATAPP_KEY_0",
        unique: true,
        fields: [
          { name: "PROVCODE" },
          { name: "APPDATE" },
          { name: "APPTIME" },
          { name: "SOURCE" },
        ]
      },
    ]
  });
};
