const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('IANEWAPP', {
    TRANSNO: {
      type: DataTypes.DECIMAL(19,0),
      allowNull: false,
      primaryKey: true
    },
    AUDTDATE: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    AUDTTIME: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    AUDTUSER: {
      type: DataTypes.CHAR(8),
      allowNull: false
    },
    AUDTORG: {
      type: DataTypes.CHAR(6),
      allowNull: false
    },
    CLINICCODE: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    CLINICDESC: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    PROVCODE: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    PROVDESC: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    CLIENTCODE: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    CLIENTDESC: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    TITLE: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    FIRSTNAME: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    SECONDNAME: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    MIDDLENAME: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    LASTNAME: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    GENDER: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    ADDRESS1: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    ADDRESS2: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    ADDRESS3: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    ADDRESS4: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    DISTRICT: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    ZIP: {
      type: DataTypes.CHAR(10),
      allowNull: false
    },
    COUNTRY: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    NATION: {
      type: DataTypes.CHAR(15),
      allowNull: false
    },
    HOMEPHONE: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    MOBILE: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    EMAIL: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    DOB: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    VISITTYPE: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    CONSTYPE: {
      type: DataTypes.CHAR(15),
      allowNull: false
    },
    CONSDESC: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    AGEYY: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    AGEMM: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    AGEDAY: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    APPDATE: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    APPTIME: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    ENDDATE: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    ENDTIME: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    DEFSLOT: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    ROW: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    COL: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    MERGEROW: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    CANCELRES: {
      type: DataTypes.CHAR(250),
      allowNull: false
    },
    MARITAL: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    REMARKS: {
      type: DataTypes.CHAR(250),
      allowNull: false
    },
    APPSTATUS: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    CONSULTTYP: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    MEETING: {
      type: DataTypes.CHAR(250),
      allowNull: false
    },
    TRANSFER: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    CITY: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    NOSHOWRES: {
      type: DataTypes.CHAR(250),
      allowNull: false
    },
    ROOMCODE: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    ROOMDESC: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    VISITID: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    CREATEBY: {
      type: DataTypes.CHAR(15),
      allowNull: false
    },
    CREATEDT: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    MODIFYBY: {
      type: DataTypes.CHAR(15),
      allowNull: false
    },
    MODIFYDT: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    STANDBY: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    WAPPSTATUS: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    DEPTCODE: {
      type: DataTypes.CHAR(15),
      allowNull: false
    },
    BILLEDYN: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    PATIENTINS: {
      type: DataTypes.CHAR(250),
      allowNull: false
    },
    NURSEINS: {
      type: DataTypes.CHAR(250),
      allowNull: false
    },
    SPEREQUIRE: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    FILRECE: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    BLOCKSLOTS: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    PROVSTATUS: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    FILEISSUE: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    APPSNO: {
      type: DataTypes.DECIMAL(19,0),
      allowNull: false
    },
    CREATETM: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    PATIDNUM: {
      type: DataTypes.CHAR(40),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'IANEWAPP',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IANEWAPP_KEY_0",
        unique: true,
        fields: [
          { name: "TRANSNO" },
        ]
      },
    ]
  });
};
