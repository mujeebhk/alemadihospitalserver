const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('AUTH', {
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    USERNAME: {
      type: DataTypes.STRING(300),
      allowNull: false,
      unique: "AUTHUNIQUE"
    },
    PASSWORD: {
      type: DataTypes.STRING(300),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'AUTH',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "AUTHUNIQUE",
        unique: true,
        fields: [
          { name: "USERNAME" },
        ]
      },
      {
        name: "PK__AUTH__3214EC27D501C1BD",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
  });
};
