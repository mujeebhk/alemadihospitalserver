const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('IIBILLD', {
    BILLSEQNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LINENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLLINENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SERVTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERVCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SERGRPCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILCODEGRP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONSTYPE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LOCCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BATCHNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EXPIRYDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INSURED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    UOM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UNITCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NETEXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMPRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AVGCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEPTCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FROMUI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ORDUNIQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDLINENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERPRVCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SERPRVTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDPRVCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MOD1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MOD2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MOD3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MOD4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PKGSTART: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PKGEND: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISPACKAGE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ICDCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUTHREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SHTDISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISREGCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SHTCOPAY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MINORPROC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXGRPCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXGRPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXINCLU: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXAUTH1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXAUTH2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXAUTH3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXAUTH4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXAUTH5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLASS1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLASS2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLASS3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLASS4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLASS5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLSDES1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLSDES2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLSDES3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLSDES4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLSDES5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TPATRATE1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATRATE2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATRATE3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATRATE4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATRATE5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TOTPATBH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TOTCMPBH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATTAX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATPAYABLE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMPTAX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMPPAYABL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODIFYBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODIFYDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GENVSTCNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPEVSTCNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONVSTCNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDCOLLVST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OCOPAYPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OCOPAYAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYFLAG: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATDISCPAY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISDISCPAY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GRNNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GRNLSEQNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ACCTSET: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PKGTOTAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SALEPRIUOM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SALEPRICE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MRPPRICE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PKGPRICE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PKGLOSS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PKGDISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCBYCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISCBYNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISEMERGNCY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BARCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXTOTAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NETTAXTOT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ITMHISSEQ: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STAXINCL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ODISCPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ODISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERPRVDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MULTIADV: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREAUTHREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREAUTHTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SERVNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RATLSTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPTDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CANEXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANDISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANNETECST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANPATRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCOMPRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCBY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TOOTHNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PRICEUOM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMMNGVN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMMNTRNNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMMNAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FREFOLWUP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERVICEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PEXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LABCOPUTIL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RADCOPUTIL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PHMCOPUTIL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MISCOPUTIL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCOMPRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCNCOMPRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REPORTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REPORTTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PROCESSDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCHDDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SINSURED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SUNITCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SEXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SDISCPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SDISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SNETEXTCST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCOPAYPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCOPAYAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SDEDUCTAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SDEDUCTPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCOPAYTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SDEDUCTTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCOPAYFLAG: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    URGCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MBDCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PERFORMBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    F190: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'IIBILLD',
    schema: 'dbo',
    timestamps: false
  });
};
