const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MLORDANA', {
    DOCNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TESTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LINENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PARAMCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SAMPLENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPEMENDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPEMENABBR: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPEABBRDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ANALYTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INVHDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PARAMDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RESULT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NUMRESULT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ALLOWLOW: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ALLOWHIGH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CRILOW: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CRIHIGH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VALLOW: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VALHIGH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ACTSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ANASTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ANAOLDSTAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELLED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REACANCEL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CANCELBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CANCELDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VALIDATEED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VALIDATEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VALIDATEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VALIDATETM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RUN1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RUN2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RUN3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FINALRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LABSECCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LABSECDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SAMPLECODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RLXTESTREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RLXTESTEXT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RXTESTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RXTESTDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUTOVALREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TSTSITELOC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TSTSITEDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISRLXTXT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DATER1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TIMER1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DATER2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TIMER2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DATER3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TIMER3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EQUIPMENT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EQUCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CRITICAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LABSUBSECC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LABSUBSECD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SENDOUTTST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WORKLSTNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    WORKLSTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CRITICALVL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CRITICALBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CRITICALDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CRITICALTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RETEST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPEINSREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CRICOMMENT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RESCONFIRM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CULTURE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESCONFBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RESCONFDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESCONFTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FACCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPEMENTYPE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUTORETEST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SUPPRESS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CRITICTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISPSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISAMENDED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISREFERIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SEX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEINDAYS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATHWAY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODIFYTEST: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODTESTDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INCUBATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MINALLOW: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MAXALLOW: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESRNDOFF: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESULTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MARKER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POSTTOINF: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REPUNIT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PROCESS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WORKSHTGEN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WORKLSTTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COLLDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COLLTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESENTBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RESENTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESENTTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RELEASE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NOTIFIY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NOTIFYREM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOCDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LASTRESULT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LABNUM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EQUCODE2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EQUCODE3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONVUNIT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONVFACTOR: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONVRESULT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONVRMIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONVRMAX: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MLORDANA',
    schema: 'dbo',
    timestamps: false
  });
};
