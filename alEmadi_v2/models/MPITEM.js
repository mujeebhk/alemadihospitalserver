const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MPITEM', {
    ITEMCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ITEMDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STRUCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CATECODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GROUPCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ACCTSET: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COSTMETHOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRICELIST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BARCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BATCHEXP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEFBATCHNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NONINVITM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRESITM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    UDITEM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GENCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FORM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STRENGTH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONTDURG: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CNTDURGLST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NONPATIENT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MINQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MAXQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REORDERQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ALTITM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MANUFCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MANUITMNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ITMINFO1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ITMINFO2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ITMINFO3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ITMINFO4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FMTITMNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SEGMENT1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SEGMENT2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SEGMENT3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SEGMENT4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SEGMENT5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STOCKUOM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LASTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INACTIVE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AVGCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    QTYHAND: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    QTYPO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    QTYGIT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LTPURCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODIFYBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODIFYDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INVITEM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CALDOSE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    IVFLUID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EQUIDOSEYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EQUIDOSE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EQUIDOSEUN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LINKITEM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LINKDISUNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MAXIMDDOS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MAXDURATIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSEVALID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LNKDOSUNIT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DSELLUOM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STKUOMSUM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMERGDRUG: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STOCONCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ACTIVEDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PROPERTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSESUM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSECODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DPURUOM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DOSESUMIP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ROUTECODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GENRICDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RXXMLYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DNTSHOWDES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WITREQADM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TSTREQUD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INVHCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TITRNYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STRUOM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ENDDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INSULIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ONETMDSP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NONSTKMED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREGSTAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MPITEM',
    schema: 'dbo',
    timestamps: false
  });
};
