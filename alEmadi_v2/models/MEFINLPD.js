const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MEFINLPD', {
    EHRSEQNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LINENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ICDCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ICDDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DIAGTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ENCOUUNIQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BIOHAZARD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISRISK: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMMAND: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATEDBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INACTIVE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DATEINACT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TOOTHFROM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INFTHRT: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MEFINLPD',
    schema: 'dbo',
    timestamps: false
  });
};
