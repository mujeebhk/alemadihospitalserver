const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('IIBILLH', {
    VISITID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLSEQNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FROMDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TODATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VISITID1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VISITTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PROVCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PROVDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CUSTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CUSTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CUSTNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLGRPCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POLGRPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PLANCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PLANDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLICYNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COUNTCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SHIFTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BRANCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BRANNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SUBTOTAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NETTOTAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SHTDISCPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SHTDISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SHTNETAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMPRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RECEIVEAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMMENTS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CANCELYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CANCELREM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ADVANCE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRINTED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POSTED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RECEIPTNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MULTIPKG: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MINORPROC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXGRPCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXGRPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXINCLU: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXAUTH1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXAUTH2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXAUTH3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXAUTH4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXAUTH5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLASS1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLASS2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLASS3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLASS4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLASS5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXCLSDES1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLSDES2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLSDES3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLSDES4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TAXCLSDES5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TPATRATE1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATRATE2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATRATE3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATRATE4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATRATE5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATBASE5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TPATAMT5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPBASE5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT3: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT4: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TCMPAMT5: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TOTPATBH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TOTCMPBH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATTAX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATPAYABLE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMPTAX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMPPAYABL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORGPATPBLE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAXNETAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NETTAX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODIFYBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODIFYDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDCOLLYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GENBILLCNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPEBILLCNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONBILLCNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTCONS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYLIMIT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYMODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATDISCPAY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GLBATCHNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GLENTRYNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ARBATCHNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ARENTRYNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DAYENDNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POSTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POSTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POSTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ACCTBTCHNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PKGDISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREDITAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CSPROVCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UNDOBILYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CARRYFWDYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CFRECPTNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GUARANTTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMPCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISPBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISPDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISPTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREDAUTHBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MULTIADV: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INVGENYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INVNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPONSCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ADJUSTED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VISITDEPT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPECDEPTYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EPISDNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ACTRECAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCOUNTBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISCOUNTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MVPCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FROMBILLNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OADVANCE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OBILLAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OPATRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OACCTBTCH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATEPISODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NSHIFTCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANEXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANDISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANNETECST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANPATRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCOMPRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INVDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORGPOSTYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPROVLNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    XMLYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SHOWBILLYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SOAPNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ENCOUUNIQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMCPTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ORGCRDAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLCUR: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RATETYPE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RATEDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXCHRATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RATEOPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    HOMECUR: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EMAPPL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EMPOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPROVDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPROVTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SECPLANYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCUSTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCUSTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPOLGRPCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPLANCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPOLCYNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SCOMPRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCNCOMPRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SINVGENYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SINVDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SINVNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SXMLYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REPORTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REPORTTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SECBILLNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SDISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CLAIMVALID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCLMVALID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SAPPROVLNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SAPPROVDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PKGTYPCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'IIBILLH',
    schema: 'dbo',
    timestamps: false
  });
};
