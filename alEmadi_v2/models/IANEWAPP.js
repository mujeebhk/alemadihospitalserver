/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IANEWAPP', {
		TRANSNO: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		AUDTDATE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		AUDTTIME: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		AUDTUSER: {
			type: DataTypes.STRING,
			allowNull: true
		},
		AUDTORG: {
			type: DataTypes.STRING,
			allowNull: true
		},
		CLINICCODE: {
			type: DataTypes.STRING,
			allowNull: true
		},
		CLINICDESC: {
			type: DataTypes.STRING,
			allowNull: true
		},
		PROVCODE: {
			type: DataTypes.STRING,
			allowNull: true
		},
		PROVDESC: {
			type: DataTypes.STRING,
			allowNull: true
		},
		CLIENTCODE: {
			type: DataTypes.STRING,
			allowNull: true
		},
		CLIENTDESC: {
			type: DataTypes.STRING,
			allowNull: true
		},
		TITLE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		FIRSTNAME: {
			type: DataTypes.STRING,
			allowNull: true
		},
		SECONDNAME: {
			type: DataTypes.STRING,
			allowNull: true
		},
		MIDDLENAME: {
			type: DataTypes.STRING,
			allowNull: true
		},
		LASTNAME: {
			type: DataTypes.STRING,
			allowNull: true
		},
		GENDER: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		ADDRESS1: {
			type: DataTypes.STRING,
			allowNull: true
		},
		ADDRESS2: {
			type: DataTypes.STRING,
			allowNull: true
		},
		ADDRESS3: {
			type: DataTypes.STRING,
			allowNull: true
		},
		ADDRESS4: {
			type: DataTypes.STRING,
			allowNull: true
		},
		DISTRICT: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		ZIP: {
			type: DataTypes.STRING,
			allowNull: true
		},
		COUNTRY: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		NATION: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		HOMEPHONE: {
			type: DataTypes.STRING,
			allowNull: true
		},
		MOBILE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		EMAIL: {
			type: DataTypes.STRING,
			allowNull: true
		},
		DOB: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		VISITTYPE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		CONSTYPE: {
			type: DataTypes.STRING,
			allowNull: true
		},
		CONSDESC: {
			type: DataTypes.STRING,
			allowNull: true
		},
		AGEYY: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		AGEMM: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		AGEDAY: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		APPDATE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		APPTIME: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		ENDDATE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		ENDTIME: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		DEFSLOT: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		ROW: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		COL: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		MERGEROW: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		CANCELRES: {
			type: DataTypes.STRING,
			allowNull: true
		},
		MARITAL: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		REMARKS: {
			type: DataTypes.STRING,
			allowNull: true
		},
		APPSTATUS: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		CONSULTTYP: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		MEETING: {
			type: DataTypes.STRING,
			allowNull: true
		},
		TRANSFER: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		CITY: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		NOSHOWRES: {
			type: DataTypes.STRING,
			allowNull: true
		},
		ROOMCODE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		ROOMDESC: {
			type: DataTypes.STRING,
			allowNull: true
		},
		VISITID: {
			type: DataTypes.STRING,
			allowNull: true
		},
		CREATEBY: {
			type: DataTypes.STRING,
			allowNull: true
		},
		CREATEDT: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		MODIFYBY: {
			type: DataTypes.STRING,
			allowNull: true
		},
		MODIFYDT: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		STANDBY: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		WAPPSTATUS: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		DEPTCODE: {
			type: DataTypes.STRING,
			allowNull: true
		},
		BILLEDYN: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		PATIENTINS: {
			type: DataTypes.STRING,
			allowNull: true
		},
		NURSEINS: {
			type: DataTypes.STRING,
			allowNull: true
		},
		SPEREQUIRE: {
			type: DataTypes.STRING,
			allowNull: true
		},
		FILRECE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		BLOCKSLOTS: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		PROVSTATUS: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		FILEISSUE: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		APPSNO: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		CREATETM: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		PATIDNUM: {
			type: DataTypes.STRING,
			allowNull: true
		}
	}, {
		tableName: 'IANEWAPP',
		timestamps: false
	});
};
