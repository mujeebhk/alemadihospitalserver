const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MEPRESCH', {
    PREID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PRECODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EPICODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VISITID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VSTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VSTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRETIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CASETYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PAYTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MEDITYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELLED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISALLERGY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISREACTMED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WARDNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    WARDDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BEDTYPE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BEDNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BEDDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INIICDCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INIICDDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FINICDCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FINICDDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INACTIVE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DATLAST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PROVCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PROVNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DEPTCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEPTNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMPPOS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMPPRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SELECT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODIFYBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODIFYDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ENCOUCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISDISCH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SUMPOS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CUSTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PLANCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PLANGCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INSCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLICYNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMMAND: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRNTSTAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ACKWNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IVPRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VERBORD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATSTAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NOTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INSLNPRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONTPRESS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WRDSTKISS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STOPPRESC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DIABPRESC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TOBEADMIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INSULINSCH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STKWARDNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OWNMEDCN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VERIFYYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MEPRESCH',
    schema: 'dbo',
    timestamps: false
  });
};
