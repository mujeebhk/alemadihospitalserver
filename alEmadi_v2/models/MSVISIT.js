const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MSVISIT', {
    VISITID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VISTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VISTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VISTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PROVIDER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEPTID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CLOSEYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REFTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REFPROV: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFFACL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFOTH: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFDETAL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NATEXP: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CHKTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CHKOUTYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CHKOTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VSTCOPAY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INACTIVE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INSCOMP: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INSPOLCY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLCYNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DEDCTBAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREDITTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREPAT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONSULTTP: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OTHERS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EPISDCOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRVVSTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRVVSTPRO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CASETRF: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TRFDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REQNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CASESELECT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SUBSCRIBBY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMPLYRCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISEMRGNCY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VSTPURPOSE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CLINICCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PACKPATYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FREVISITYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PACKCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONSULTTYP: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISREFERIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INITVISIT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDCOLLYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GENCOUNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPECOUNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONCOUNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MLCYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MLCCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SOAPNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLGRPCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLGRPDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLICYNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INSNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NOOFBILLS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONSBILLED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REGBILLED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TRFBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TRFTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TRFIPCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TRANSFERRE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CHRONIC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    UNITCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISCCATCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISCCATDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ACCREGCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ACCDPLACE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLSTANAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISINTIMATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WOUNCERDAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTPROVCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EXTPROVDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPONSCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VSTEXPDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPECCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INLIMITUTL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INLIMITBAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OPLIMITUTL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OPLIMITBAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    QUEUENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPECDEPTYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PACKAGETYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CHKINYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REFEMPCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EPISODTOT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPOINTYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INJCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MLTPKGCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PRONAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DEPTNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONSNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EMPLYRNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CLINICDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PACKDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFDNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPECDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONSULTDSC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MLCDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TYPOFVST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REGDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISFREEVST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEYEARS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEMONTHS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEDAYS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRVICD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FINICD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUTHDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PROVDEPSET: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EPICODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPLNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PRESCTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPTNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ENCOUUNIQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NURASMST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MULTPKGTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPLDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ELIGAMOUNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ELIGIBTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FALLRISKYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SECPLANYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCUSTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCUSTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPOLGRPCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPLANCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SPOLCYNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HOSPNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NETWCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PKGYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONSCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMMENT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LOCCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MRDICD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREAUTHREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHORIZE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHSTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHENDDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFERNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MSVISIT',
    schema: 'dbo',
    timestamps: false
  });
};
