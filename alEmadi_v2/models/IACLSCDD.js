const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('IACLSCDD', {
    SCHEDCODE: {
      type: DataTypes.CHAR(20),
      allowNull: false,
      primaryKey: true
    },
    CLINICCODE: {
      type: DataTypes.CHAR(22),
      allowNull: false,
      primaryKey: true
    },
    APPDATE: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false,
      primaryKey: true
    },
    AUDTDATE: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    AUDTTIME: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    AUDTUSER: {
      type: DataTypes.CHAR(8),
      allowNull: false
    },
    AUDTORG: {
      type: DataTypes.CHAR(6),
      allowNull: false
    },
    CLINICDESC: {
      type: DataTypes.CHAR(60),
      allowNull: false
    },
    FROMWORKHR: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    TOWORKHR: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    DURATION: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    FRMBREAKHR: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    TOBREAKHR: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    FROMBRKHR2: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    TOBREAKHR2: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    FROMBRKHR3: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    TOBREAKHR3: {
      type: DataTypes.DECIMAL(9,0),
      allowNull: false
    },
    DEPTCODE: {
      type: DataTypes.CHAR(22),
      allowNull: false
    },
    CANCELAPP: {
      type: DataTypes.SMALLINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'IACLSCDD',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IACLSCDD_KEY_0",
        unique: true,
        fields: [
          { name: "SCHEDCODE" },
          { name: "CLINICCODE" },
          { name: "APPDATE" },
        ]
      },
    ]
  });
};
