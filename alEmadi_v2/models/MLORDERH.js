const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MLORDERH', {
    DOCNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IPORDNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SOURCE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VISITCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ADMCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DOCDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOCTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDERDBY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CASETYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CASETRNYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WARDNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BEDNUM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOB: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SEX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDESTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCHEDULEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDVALIDFD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDVALIDTD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CHECKINYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CHECKINPRV: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CHECKINDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CHECKINTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDCANCEL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CANCELDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CNCLREMARK: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATPREPYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDISTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OLDISTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OLDESTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEINDAYS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INTCOMMENT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EXTCOMMENT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CRITICAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXCEPTION: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INITIALORD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATFASTING: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STATUSIND: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDBYDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONFIRMBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONFIRMDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONFIRMTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AMENDED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COLLECTTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESCONFIRM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESCONFBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RESCONFDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESCONFTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AMMENDNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESVALBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RESVALDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESVALTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESVERBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RESVERDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RESVERTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDSAMSTAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPELBLGEN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISESOREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISREFERIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VIEWED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISREPPRINT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTPRTBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EXTPRTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTPRTTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MRDPRTBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MRDPRTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MRDPRTTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TOBEPRINT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISSHOW: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMMAND: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LABNUM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREPAT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREPATDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LASTVALIDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LASTVALITI: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MLORDERH',
    schema: 'dbo',
    timestamps: false
  });
};
