const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('IACLINIC', {
    CLINICCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DEPTCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CLINICDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DEPTDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LINKPROYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPROCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPRONAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LINKLOCYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LOCCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LOCDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INACTIVE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODIFYBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODIFYDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SLOTS: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'IACLINIC',
    schema: 'dbo',
    timestamps: false
  });
};
