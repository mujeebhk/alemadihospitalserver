const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MSBILCOD', {
    BLCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BLDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BLCODGRP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MEDDEPT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERVCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SERVGRP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERVTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PACKAGE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BLCODTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STDFEE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FINANCE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FINANCEABR: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONSTYPE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IMUNECOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DLOC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODETYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SEX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OPCONS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OPYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    IPYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DAYCARYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REGBILYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INCLUDE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OPPATTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OPDIFFRATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    IPBEDREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    IPSURREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    IPPATTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EMGPATTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERVICEMED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ALLCASE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPTREQYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPTDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PROVIDERTP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DATLAST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INACTIVE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DATEINAC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREATBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREATDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MODDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERGRPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SERCODDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILGRPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LOCDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GLDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DEPTDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CONSDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PROCODEREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CASHDSCOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CASHDSDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREINCOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREINDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREDSCOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREDSDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GLIPCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GLIPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CASHDSICOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CASHDSIDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREINICOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREINIDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREDSICOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREDSIDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SUBDEPTCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SUBDEPTDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLCDEDIT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DSERVTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISDISCOUNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPECDEPT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MAXDISCPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MOHCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISFREEVST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILDESCCHG: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POSTTOACCT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ACTTYPNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ACTTYPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREFOLWUP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LONICCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TOOTHYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ALWZPRC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OBSERVYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SERTAXREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TIMEBASEYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PERUNIT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EQPIDMAND: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SHOWLABRES: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ICDVALDREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPTDRGMAN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    QOCSCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MSBILCOD',
    schema: 'dbo',
    timestamps: false
  });
};
