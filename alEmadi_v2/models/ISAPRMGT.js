const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ISAPRMGT', {
    VISITID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ORDCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LINENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UNITCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NETEXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DEDUCTTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COPAYAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INSSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FROMSCREEN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREAUTHREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EHRSEQNO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDERDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDERTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REQSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REQCREBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REQCREDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REQCRETI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    READYBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    READYDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    READYTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SENTBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SENTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SENTTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CLOSEBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CLOSEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CLOSETI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CUSTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CUSTNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLGRPCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POLGRPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PLANCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PLANDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POLICYNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUTHBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUTHDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SELECT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPROVLNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PREAUTHTYP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CASETYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOCNEEDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DOCENQNEED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOCENQCLS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOCCLSDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOCCLSBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMMENTS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SERVTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POSTOTYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDEREDBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CPTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMPRESP: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    APPRVDAMT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REFERNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'ISAPRMGT',
    schema: 'dbo',
    timestamps: false
  });
};
