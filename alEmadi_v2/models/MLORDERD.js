const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MLORDERD', {
    DOCNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LABSECCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TESTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PRIORITY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTFAV: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SOURCE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INVGCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INVGDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TESTNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LABNUM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDERDBY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDBYDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SAMSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCHEDULEDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDVALIDFD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDVALIDTD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISPATINST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELTEST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CANCELDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CANCELTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATINST: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISAUTHORIZ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TESTSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OLDTSTSTAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTENQUIRE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REFLEXTEST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CRITICAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXCEPTION: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LABSECDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AMENDED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATCOMPL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SENDOUTTST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ANTIMICRO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REACANCEL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISPATCH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISPATCHBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISPATCHDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISPATCHTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FACCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FACDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    WORKLISTNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFTESTCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISPSTATUS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREDISPTCH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREDISPBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PREDISPDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREDISPTI: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISPATCHNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ISMICRO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISESOREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SEX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AGEY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPERECDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SPERECTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISREFERIN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TRASPCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TRASPDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFLTSTCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REFLTSTDES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PATCHK: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOCDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOCTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATCODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STDFEE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRNTEXT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISSHOW: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ORDERNO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VISITCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CASETYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    WARDNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BEDNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EPISODENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CREPAT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CREPATDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMMENT1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMMENT2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMMENT3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    COMPCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REVIEWED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REVIEWEDBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REVIEWDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REVIEWTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCHEDULETM: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MLORDERD',
    schema: 'dbo',
    timestamps: false
  });
};
