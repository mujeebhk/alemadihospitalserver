const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MEPRESCD', {
    PREID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LINENO: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUDTUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUDTORG: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FAVORID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BILLCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GENCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GENENAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ITEMCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ITEMNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FORM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STRENGTH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DUOMCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DOSEUNIT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSUNTDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQUENCY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FRETIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISPENSE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISUNIT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DISDAY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ADMTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DURATION: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STARTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NENDDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NSTARTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ENDDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    LASTRXDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REFILL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NOREFILL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRTDEA: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REMARKS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PREMODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MODEDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CHRONIC: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PURQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    POSQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ICDCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NDCCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SELECT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONFIDEN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    COMPLINE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PATID: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VISITID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EPICODE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VSTDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REQDPER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    REQDDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHRIZ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUTHDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    REQFCH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    UINPOS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ALTITMCD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ALTITMNM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ALTFORM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ALTSTREN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ALTDOSE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ALTFREQU: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ROUTE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CHRONICID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LOCCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DOSEMORN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSENOON: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSEEVEN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSENGT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSEOTHR: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PROVCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IGNREASON: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DOSRECPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MULTIPER: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCHEDISCH: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FREQMULFIX: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FREONCONLY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SCHEDDATE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCHEDTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STARTTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ENDTIME: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FREQEVERY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQMORN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQNOON: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQEVEN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQNIGHT: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQMIDNI: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQEARMOR: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FREQDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FIXFREQ: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    INSURED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    UNITCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    NETEXTCOST: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PREAUTHREQ: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    AUTHORIZE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INSAUTHBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AUTHNUM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    READYBILL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BILLED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ITEMGRPCOD: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    MEDDEPTCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UDIQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    UDIDISPQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    QTYPERDOSE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STOCKUOM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UDILASTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TAPERDOSE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RATLSTTYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCONTNYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCONTDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCONTTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DISCONTUSR: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ROUTEDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DOSEQTY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CPTCODE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CPTDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STAT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRNMED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DENIALCOD: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PRIMADDAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OVERHRS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VOLUNIT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DOSPERUNT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OVERALRYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OVERUSER: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TITRNYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    OWNMED: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GLUCOSE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    TITNDESC: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DROPFCT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    FLOWRT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    STRUOM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    EVERYHRS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    BEFOREMEAL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    PRNSIGNS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    INSDOSAGE: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ADVHIGDOS1: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ADVHIGDOS2: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    DECLOWLVL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    GLUCLEVEL: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    CONTPRESS: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    HOURVOLM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    SCDFULLY: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    RATEINS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VERIFYYN: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VERIFYBY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    VERIFYDT: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    VERIFYTM: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    ISSUETYPE: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'MEPRESCD',
    schema: 'dbo',
    timestamps: false
  });
};
