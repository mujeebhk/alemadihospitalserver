module.exports = {

    //Session Details
    AVOID_AUTHENTICATED_SESSION_CHECK: true,
    sessionExpireTime: 60 * 60 * 24 * 30, //sessionExpireTime set to one month
    tokenName: 'token',

    //Utils Control
    DISABLE_SMS: true,
    DISABLE_EMAIL: true,

    //SMS/OTP Details
    smsApi: "https://messaging.ooredoo.qa/bms/soap/Messenger.asmx/SendSmsXml?xml=<?xml version='1.0' encoding='UTF-8'?> <request> <bulk_msg> <customer_id>2369</customer_id> <user_id>AlEmadiHosp</user_id> <password>G@p4rN5ut7i</password> <validity_period status='y'> <day>1</day> <hours>1</hours> <mins>10</mins> </validity_period> <delivery_report>0</delivery_report> <message> <title>AlEmadiHosp</title> <lang_id>8</lang_id> <body><![CDATA[ SMSTEMPLATE ]]></body> <values> <msg_id>1</msg_id> <mobile_no> REPLACEMOBILENUMBERHERE</mobile_no> </values> </message> </bulk_msg> </request>",
    ClearOTp: 15856254,
    Base32OTPSecretCode: 'IFAUCQKCIJBEEDost1234!==='
};