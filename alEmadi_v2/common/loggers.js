module.exports = function (logger) {

    function createLogs(logs, type) {

        if (type && type == 'error')
            logger.error(logs);
        else if (type && type == 'info')
            logger.info(logs);
        else
            logger.debug(logs);
        console.log(logs);
    }

    module.exports.createLogs = createLogs;
};