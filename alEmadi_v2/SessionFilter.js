var ApplicationConstants = require('./common/ApplicationConstants');
var logs = require('./common/loggers');
var ErrorMessage = require('./restApis/ErrorMessage');
var express = require('express');
const jwt = require('jsonwebtoken');

function sessionFilter(app) {

    // Exempt session fillters
    var sessionExemptedPages = ['/patient/register', '/insurance/names/list'];
    sessionExemptedPages.push('/patient/otp/send');
    sessionExemptedPages.push('/patient/otp/verify');
    var allowedUrl = ['/patient/verify/qid/', 'patient/charts/barcode/qid/'];

    // route middleware to verify a therapist sessionId
    app.use('/alemadi/mobile',
        function (req, res, next) {
            logs.createLogs('mobile app sessionFilter() Called');

            if (ApplicationConstants.AVOID_AUTHENTICATED_SESSION_CHECK) {
                logs.createLogs('Session check is disabled');
                next();
            } else {
                logs.createLogs('Session check is enabled');

                var isValidURL = false;
                for (var i = 0; i < allowedUrl.length; i++) {
                    if (allowedUrl.indexOf(req.url) != -1) {
                        break;
                    } else if (req.url.includes(allowedUrl[i])) {
                        isValidURL = true;
                        break;
                    }
                }

                if ((sessionExemptedPages.indexOf(req.url) <= -1) && !isValidURL) {

                    var token = null;

                    if (req.headers[ApplicationConstants.tokenName])
                        token = req.headers[ApplicationConstants.tokenName];
                    else token = req.query[ApplicationConstants.tokenName];

                    if (token == null) {
                        logs.createLogs('Invalid/null authorization token', 'error');
                        return res.status(401).json({ error: { message: ErrorMessage.UNAUTHORIZED, code: '90002' } });
                    }

                    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {

                        if (err) {
                            logs.createLogs(err);
                            logs.createLogs('UnAuthorized Access');
                            return res.status(401)
                                .send({
                                    success: false,
                                    message: 'Unauthorised Patient. Access denied.',
                                    error: {
                                        message: ErrorMessage.UNAUTHORIZED,
                                        code: '90001'
                                    }
                                });
                        }

                        logs.createLogs('Patient authenticated successfully');
                        next();
                    });

                } else {

                    logs.createLogs('Patient Exempted url aceesed successfully' + req.url);
                    next();
                }
            }
        });

    // After the successfull authentication, the user is allowed to use below path.
    app.use('/', express.Router());

}

module.exports.sessionFilter = sessionFilter;